FROM tomcat:9.0.80-jdk8

COPY ./tomcat-users.xml ./conf/tomcat-users.xml

COPY . /app

WORKDIR /app/attune

# Update stretch repositories
RUN sed -i -e 's/deb.debian.org/archive.debian.org/g' \
           -e 's|security.debian.org|archive.debian.org/|g' \
           -e '/stretch-updates/d' /etc/apt/sources.list

RUN apt-get update && apt-get install -y --allow-unauthenticated maven
RUN mvn install:install-file \
    -Dfile=./libs/neo4j-cypher-dsl-2.0.1.jar \
    -DgroupId=org.neo4j \
    -DartifactId=neo4j-cypher-dsl \
    -Dversion=2.0.1 \
    -Dpackaging=jar \
    -DgeneratePom=true
RUN mvn clean -q install
RUN cp ./target/attune.war /usr/local/tomcat/webapps/attune.war
