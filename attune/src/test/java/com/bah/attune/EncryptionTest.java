package com.bah.attune;

import com.bah.attune.util.Encryptor;
import org.junit.Ignore;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.web.WebAppConfiguration;
import org.springframework.transaction.annotation.Transactional;

import java.io.IOException;

@Ignore
@SuppressWarnings("unchecked")
@Transactional
@ContextConfiguration("/spring.xml")
@RunWith(SpringJUnit4ClassRunner.class)
@WebAppConfiguration
public class EncryptionTest
{
   private static final Logger LOGGER = LoggerFactory.getLogger(EncryptionTest.class);

   @Autowired
   Encryptor encryptor;
   
   @Test
   public void testEncrypt() throws IOException
   {
      String test = "password";
      String encryptedText;

      LOGGER.info("Encrypting: " + test);
      encryptedText = encryptor.encrypt(test);

      LOGGER.info(encryptedText);
      LOGGER.info("Done.");

      LOGGER.info("Decrypting: " + encryptedText);
      String clearText = encryptor.decrypt(encryptedText);
      LOGGER.info(clearText);
      LOGGER.info("Done.");

   }

}
