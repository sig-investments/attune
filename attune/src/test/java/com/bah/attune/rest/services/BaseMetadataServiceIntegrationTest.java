package com.bah.attune.rest.services;

import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;

import java.nio.charset.Charset;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Properties;

import org.apache.commons.codec.binary.Base64;
import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.JSONValue;
import org.json.simple.parser.JSONParser;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;

import com.bah.attune.data.AttuneException;
import com.bah.attune.rest.AbstractRestInvoker;
import com.bah.attune.rest.Neo4JUtils;

public abstract class BaseMetadataServiceIntegrationTest extends AbstractRestInvoker {
	protected Logger LOGGER = LoggerFactory.getLogger(BaseMetadataServiceIntegrationTest.class);
	protected Properties props = null;
	protected String attuneDataServiceUrlPrefix = null;
	protected String attuneMetaDataServiceUrlPrefix = null;
	protected JSONParser parser = new JSONParser();
	
	public BaseMetadataServiceIntegrationTest() {
		try{			
			props = new Properties();
			props.load(this.getClass().getClassLoader().getResourceAsStream("test.properties"));
			if(System.getProperty("attune.data.service.url.prefix") != null){	
				props.put("attune.data.service.url.prefix", System.getProperty("attune.data.service.url.prefix"));
			}
			if(System.getProperty("attune.metadata.service.url.prefix") != null){	
				props.put("attune.metadata.service.url.prefix", System.getProperty("attune.metadata.service.url.prefix"));
			}
			if(System.getProperty("attune.credentials") != null){	
				props.put("attune.credentials", System.getProperty("attune.credentials"));
			}
			attuneDataServiceUrlPrefix = props.getProperty("attune.data.service.url.prefix");
			attuneMetaDataServiceUrlPrefix = props.getProperty("attune.metadata.service.url.prefix");
		} catch (Exception e){
			e.printStackTrace();
			throw new RuntimeException(e.getMessage(), e);
		}
	}
	
	protected void checkJSON(String json) throws Exception{
		assertNotNull(json);
		System.out.println(json);
		System.out.println();
		Object jsonObj = parser.parse(json);
		assertNotNull(jsonObj);
		if(jsonObj instanceof JSONArray){
			JSONArray jsonArray = (JSONArray)jsonObj;
			assertTrue(jsonArray.size() > 0);
		} else if(jsonObj instanceof JSONObject){
			JSONObject jsonObject = (JSONObject)jsonObj;
			assertTrue(!jsonObject.isEmpty());
		}
	}
	
	protected ResponseEntity<String> getData (String url) throws Exception{
		return this.getData(url, null);
	}
	
	@SuppressWarnings("unchecked")
	protected ResponseEntity<String> getData (String url, Map<String, ?> uriVariables) throws Exception{
		try{
			if(uriVariables == null){
				uriVariables = new HashMap<String, String>();
			}
			ResponseEntity<String> responseEntity = this.callRestService(url, HttpMethod.GET, null, String.class, uriVariables);			
			return responseEntity;
		} catch(AttuneException ae) {
			throw ae;
		} catch(Exception e) {
			throw new AttuneException(e);
		}
	}
	
	@SuppressWarnings("unchecked")
	protected ResponseEntity<String> insertData(String jsonInput, String url) throws AttuneException {		
		try{
			ResponseEntity<String> responseEntity = this.callRestService(url, HttpMethod.POST, jsonInput, String.class);			
			return responseEntity;
		} catch(AttuneException ae) {
			throw ae;
		} catch(Exception e) {
			throw new AttuneException(e);
		}
	}
	
	@SuppressWarnings("unchecked")
	protected ResponseEntity<String> updateData(String jsonInput, String url) throws AttuneException{		
		try{
			ResponseEntity<String> responseEntity = this.callRestService(url, HttpMethod.PUT, jsonInput, String.class);			
			return responseEntity;
		} catch(AttuneException ae) {
			throw ae;
		} catch(Exception e) {
			throw new AttuneException(e);
		}
	}
	
	@SuppressWarnings("unchecked")
	protected ResponseEntity<String> deleteData(String url) throws AttuneException{		
		try{
			ResponseEntity<String> responseEntity = this.callRestService(url, HttpMethod.DELETE, null, String.class);			
			return responseEntity;
		} catch(AttuneException ae) {
			throw ae;
		} catch(Exception e) {
			throw new AttuneException(e);
		}
	}
	
	protected String getNodesByType(String type) throws Exception{
		try{
			String url = new StringBuilder(attuneDataServiceUrlPrefix).append("/nodes/").append(type).toString();
			ResponseEntity<String> responseEntity = this.getData(url);			
			int statusCode = responseEntity.getStatusCode().value();
			String json = (responseEntity.hasBody() ? (String)responseEntity.getBody() : new JSONObject().toJSONString());
			if(!((statusCode >= 200) && (statusCode < 300))){
				String msg = json;
				throw new AttuneException(msg.toString(), responseEntity.getStatusCode());
			}
			return json;
		} catch(AttuneException ae) {
			throw ae;
		} catch(Exception e) {
			throw new AttuneException(e);
		}
	}
	
	protected void deleteNodesAndRelationshipsByType(String type) throws Exception{
		String nodes = this.getNodesByType(type);
		JSONArray jsonArray = (JSONArray) parser.parse(nodes);
		for(int i = 0; i < jsonArray.size(); i++){				
			JSONObject jsonObject = (JSONObject) jsonArray.get(i);
			String nodeId = this.getId(jsonObject.toJSONString());
			this.deleteNodeRelationships(nodeId, "all");
			this.deleteNode(nodeId);
		}
	}
	
	protected String insertNode(String type, Map<String, Object> properties) throws Exception{
		String url = new StringBuilder(attuneDataServiceUrlPrefix).append("/node/").append(type).toString();
		String data = null;
		if(properties != null && !properties.isEmpty()){
			data = new JSONObject(properties).toJSONString();
		}
		ResponseEntity<String> responseEntity = this.insertData(data, url);			
		int statusCode = responseEntity.getStatusCode().value();
		String json = (responseEntity.hasBody() ? (String)responseEntity.getBody() : new JSONObject().toJSONString());
		if(!((statusCode >= 200) && (statusCode < 300))){
			String msg = json;
			throw new AttuneException(msg.toString(), responseEntity.getStatusCode());
		}
		return json;
	}
	
	protected void deleteNode(String id) throws Exception{
		String url = new StringBuilder(attuneDataServiceUrlPrefix).append("/node/").append(id).toString();
		ResponseEntity<String> responseEntity = this.deleteData(url);			
		int statusCode = responseEntity.getStatusCode().value();
		String json = (responseEntity.hasBody() ? (String)responseEntity.getBody() : new JSONObject().toJSONString());
		if(!((statusCode >= 200) && (statusCode < 300))){
			String msg = json;
			throw new AttuneException(msg.toString(), responseEntity.getStatusCode());
		}
	}
	
	protected void deleteNodeRelationships(String nodeId, String dir) throws Exception{
		String url = new StringBuilder(attuneDataServiceUrlPrefix).append("/node/").append(nodeId).append("/relationships/").append(dir).toString();
		ResponseEntity<String> responseEntity = this.deleteData(url);			
		int statusCode = responseEntity.getStatusCode().value();
		String json = (responseEntity.hasBody() ? (String)responseEntity.getBody() : new JSONObject().toJSONString());
		if(!((statusCode >= 200) && (statusCode < 300))){
			String msg = json;
			throw new AttuneException(msg.toString(), responseEntity.getStatusCode());
		}
	}
	
	protected String validateEntities() throws Exception{
		String url = new StringBuilder(attuneMetaDataServiceUrlPrefix).append("/validate").toString();
		ResponseEntity<String> responseEntity = this.getData(url);			
		int statusCode = responseEntity.getStatusCode().value();
		String json = (responseEntity.hasBody() ? (String)responseEntity.getBody() : new JSONObject().toJSONString());
		if(!((statusCode >= 200) && (statusCode < 300))){
			String msg = json;
			throw new AttuneException(msg.toString(), responseEntity.getStatusCode());
		}
		return json;
	}
	
	protected List<String> getEntityTypes() throws Exception{
		List<String> entityTypes = new ArrayList<String>();
		String url = new StringBuilder(attuneMetaDataServiceUrlPrefix).append("/entityTypes").toString();
		ResponseEntity<String> responseEntity = this.getData(url);			
		int statusCode = responseEntity.getStatusCode().value();
		String json = (responseEntity.hasBody() ? (String)responseEntity.getBody() : new JSONObject().toJSONString());
		if(!((statusCode >= 200) && (statusCode < 300))){
			String msg = json;
			throw new AttuneException(msg.toString(), responseEntity.getStatusCode());
		}
		if(json != null){
			Object jsonObj = parser.parse(json);
			JSONArray jsonArray = (JSONArray)jsonObj;
			for(int i = 0; i < jsonArray.size(); i++){
				String entityType = (String)jsonArray.get(i);
				entityTypes.add(entityType);
			}
		}
		return entityTypes;
	}
	
	protected String getEntities(String propName, String propValue) throws Exception{
		String url = new StringBuilder(attuneMetaDataServiceUrlPrefix).append("/entities").toString();
		if(propName != null && propValue != null){
			Map<String, String> queryParams = new HashMap<String, String>();
			queryParams.put("propName", propName);
			queryParams.put("propValue", propValue);
			url = url + this.getQueryParamString(queryParams);
		}		
		ResponseEntity<String> responseEntity = this.getData(url);			
		int statusCode = responseEntity.getStatusCode().value();
		String json = (responseEntity.hasBody() ? (String)responseEntity.getBody() : new JSONObject().toJSONString());
		if(!((statusCode >= 200) && (statusCode < 300))){
			String msg = json;
			throw new AttuneException(msg.toString(), responseEntity.getStatusCode());
		}
		return json;
	}
	
	protected String insertEntity(String entityType, Map<String, Object> properties) throws Exception{
		String url = new StringBuilder(attuneMetaDataServiceUrlPrefix).append("/entity/").append(entityType).toString();		
		ResponseEntity<String> responseEntity = this.insertData(new JSONObject(properties).toJSONString(), url);			
		int statusCode = responseEntity.getStatusCode().value();
		String json = (responseEntity.hasBody() ? (String)responseEntity.getBody() : new JSONObject().toJSONString());
		if(!((statusCode >= 200) && (statusCode < 300))){
			String msg = json;
			throw new AttuneException(msg.toString(), responseEntity.getStatusCode());
		}
		return json;
	}
	
	protected String getEntity(String entityType) throws Exception{
		String url = new StringBuilder(attuneMetaDataServiceUrlPrefix).append("/entity/").append(entityType).toString();
		ResponseEntity<String> responseEntity = this.getData(url);			
		int statusCode = responseEntity.getStatusCode().value();
		String json = (responseEntity.hasBody() ? (String)responseEntity.getBody() : new JSONObject().toJSONString());
		if(!((statusCode >= 200) && (statusCode < 300))){
			String msg = json;
			throw new AttuneException(msg.toString(), responseEntity.getStatusCode());
		}
		return json;
	}
	
	protected void deleteEntity(String entityType) throws Exception{
		String url = new StringBuilder(attuneMetaDataServiceUrlPrefix).append("/entity/").append(entityType).toString();
		ResponseEntity<String> responseEntity = this.deleteData(url);			
		int statusCode = responseEntity.getStatusCode().value();
		String json = (responseEntity.hasBody() ? (String)responseEntity.getBody() : new JSONObject().toJSONString());
		if(!((statusCode >= 200) && (statusCode < 300))){
			String msg = json;
			throw new AttuneException(msg.toString(), responseEntity.getStatusCode());
		}
	}
	
	protected void deleteEntity(Long id) throws Exception{
		String url = new StringBuilder(attuneMetaDataServiceUrlPrefix).append("/entity/").append(String.valueOf(id)).toString();
		ResponseEntity<String> responseEntity = this.deleteData(url);			
		int statusCode = responseEntity.getStatusCode().value();
		String json = (responseEntity.hasBody() ? (String)responseEntity.getBody() : new JSONObject().toJSONString());
		if(!((statusCode >= 200) && (statusCode < 300))){
			String msg = json;
			throw new AttuneException(msg.toString(), responseEntity.getStatusCode());
		}
	}
	
	protected void deleteAllEntities() throws Exception{
		String url = new StringBuilder(attuneMetaDataServiceUrlPrefix).append("/entities").toString();
		ResponseEntity<String> responseEntity = this.deleteData(url);			
		int statusCode = responseEntity.getStatusCode().value();
		String json = (responseEntity.hasBody() ? (String)responseEntity.getBody() : new JSONObject().toJSONString());
		if(!((statusCode >= 200) && (statusCode < 300))){
			String msg = json;
			throw new AttuneException(msg.toString(), responseEntity.getStatusCode());
		}
	}
	
	protected Map<String, Object> getEntityProperties(String entityType) throws Exception{
		Map<String, Object> properties = new HashMap<String, Object>();
		String url = new StringBuilder(attuneMetaDataServiceUrlPrefix).append("/entity/").append(entityType).append("/properties").toString();
		ResponseEntity<String> responseEntity = this.getData(url);			
		int statusCode = responseEntity.getStatusCode().value();
		String json = (responseEntity.hasBody() ? (String)responseEntity.getBody() : null);
		if(!((statusCode >= 200) && (statusCode < 300))){
			String msg = json;
			throw new AttuneException(msg.toString(), responseEntity.getStatusCode());
		}
		if(json != null){
			Object jsonObj = parser.parse(json);
			JSONObject jsonObject = (JSONObject)jsonObj;
			for(Object key:jsonObject.keySet()){
				String propName = (String)key;
				Object propValue = jsonObject.get(propName);
				properties.put(propName, propValue);
			}
		}
		return properties;
	}
	
	protected void updateEntityProperties(String entityType, Map<String, Object> properties) throws Exception{
		String url = new StringBuilder(attuneMetaDataServiceUrlPrefix).append("/entity/").append(entityType).append("/properties").toString();
		String data = null;
		if(properties != null && !properties.isEmpty()){
			data = new JSONObject(properties).toJSONString();
		}
		ResponseEntity<String> responseEntity = this.updateData(data, url);			
		int statusCode = responseEntity.getStatusCode().value();
		String json = (responseEntity.hasBody() ? (String)responseEntity.getBody() : new JSONObject().toJSONString());
		if(!((statusCode >= 200) && (statusCode < 300))){
			String msg = json;
			throw new AttuneException(msg.toString(), responseEntity.getStatusCode());
		}
	}
	
	protected void deleteEntityProperties(String entityType) throws Exception{
		String url = new StringBuilder(attuneMetaDataServiceUrlPrefix).append("/entity/").append(entityType).append("/properties").toString();
		ResponseEntity<String> responseEntity = this.deleteData(url);			
		int statusCode = responseEntity.getStatusCode().value();
		String json = (responseEntity.hasBody() ? (String)responseEntity.getBody() : new JSONObject().toJSONString());
		if(!((statusCode >= 200) && (statusCode < 300))){
			String msg = json;
			throw new AttuneException(msg.toString(), responseEntity.getStatusCode());
		}
	}
	
	protected Object getEntityProperty(String entityType, String propName) throws Exception{
		String url = new StringBuilder(attuneMetaDataServiceUrlPrefix).append("/entity/").append(entityType).append("/property/").append(propName).toString();
		ResponseEntity<String> responseEntity = this.getData(url);			
		int statusCode = responseEntity.getStatusCode().value();
		String json = (responseEntity.hasBody() ? (String)responseEntity.getBody() : null);
		if(!((statusCode >= 200) && (statusCode < 300))){
			String msg = json;
			throw new AttuneException(msg.toString(), responseEntity.getStatusCode());
		}
		if(json == null){
			return null;
		}
		return JSONValue.parse(json);
	}
	
	protected void updateEntityProperty(String entityType, String propName, Object propValue) throws Exception{
		String url = new StringBuilder(attuneMetaDataServiceUrlPrefix).append("/entity/").append(entityType).append("/property/").append(propName).toString();
		String data = JSONValue.toJSONString(propValue);
		ResponseEntity<String> responseEntity = this.updateData(data, url);			
		int statusCode = responseEntity.getStatusCode().value();
		String json = (responseEntity.hasBody() ? (String)responseEntity.getBody() : new JSONObject().toJSONString());
		if(!((statusCode >= 200) && (statusCode < 300))){
			String msg = json;
			throw new AttuneException(msg.toString(), responseEntity.getStatusCode());
		}
	}
	
	protected void deleteEntityProperty(String entityType, String propName) throws Exception{
		String url = new StringBuilder(attuneMetaDataServiceUrlPrefix).append("/entity/").append(entityType).append("/property/").append(propName).toString();
		ResponseEntity<String> responseEntity = this.deleteData(url);			
		int statusCode = responseEntity.getStatusCode().value();
		String json = (responseEntity.hasBody() ? (String)responseEntity.getBody() : new JSONObject().toJSONString());
		if(!((statusCode >= 200) && (statusCode < 300))){
			String msg = json;
			throw new AttuneException(msg.toString(), responseEntity.getStatusCode());
		}
	}
	
	protected String getEntityRelationship(String relationshipId) throws Exception{
		String url = new StringBuilder(attuneMetaDataServiceUrlPrefix).append("/relationship/").append(relationshipId).toString();		
		ResponseEntity<String> responseEntity = this.getData(url);			
		int statusCode = responseEntity.getStatusCode().value();
		String json = (responseEntity.hasBody() ? (String)responseEntity.getBody() : new JSONObject().toJSONString());
		if(!((statusCode >= 200) && (statusCode < 300))){
			String msg = json;
			throw new AttuneException(msg.toString(), responseEntity.getStatusCode());
		}
		return json;
	}
	
	protected String getEntityRelationships(String entityType, String relationshipDir, List<String> relTypes) throws Exception{
		String url = new StringBuilder(attuneMetaDataServiceUrlPrefix).append("/entity/").append(entityType).append("/relationships/").append(relationshipDir).toString();
		if(relTypes != null && !relTypes.isEmpty()){
			url = url + this.getQueryParamString("relType", relTypes);
		}
		ResponseEntity<String> responseEntity = this.getData(url);			
		int statusCode = responseEntity.getStatusCode().value();
		String json = (responseEntity.hasBody() ? (String)responseEntity.getBody() : new JSONObject().toJSONString());
		if(!((statusCode >= 200) && (statusCode < 300))){
			String msg = json;
			throw new AttuneException(msg.toString(), responseEntity.getStatusCode());
		}
		return json;
	}
	
	protected String insertEntityRelationship(String startEntityType, String relationshipType, String endEntityType, boolean isRequired) throws Exception{		
		Map<String, Object> properties = new HashMap<String, Object>();
		properties.put("required", isRequired);
		return this.insertEntityRelationship(startEntityType, relationshipType, endEntityType, properties);
	}
	
	protected String insertEntityRelationship(String startEntityType, String relationshipType, String endEntityType, Map<String, Object> properties) throws Exception{
		String url = new StringBuilder(attuneMetaDataServiceUrlPrefix).append("/entity/").append(startEntityType).append("/relationships/").append(relationshipType).append("/").append(endEntityType).toString();
		String data = null;
		if(properties != null && !properties.isEmpty()){
			data = new JSONObject(properties).toJSONString();
		}
		ResponseEntity<String> responseEntity = this.insertData(data, url);			
		int statusCode = responseEntity.getStatusCode().value();
		String json = (responseEntity.hasBody() ? (String)responseEntity.getBody() : new JSONObject().toJSONString());
		if(!((statusCode >= 200) && (statusCode < 300))){
			String msg = json;
			throw new AttuneException(msg.toString(), responseEntity.getStatusCode());
		}
		return json;
	}
	
	protected void deleteEntityRelationship(String id) throws Exception{
		String url = new StringBuilder(attuneMetaDataServiceUrlPrefix).append("/relationship/").append(id).toString();
		ResponseEntity<String> responseEntity = this.deleteData(url);			
		int statusCode = responseEntity.getStatusCode().value();
		String json = (responseEntity.hasBody() ? (String)responseEntity.getBody() : new JSONObject().toJSONString());
		if(!((statusCode >= 200) && (statusCode < 300))){
			String msg = json;
			throw new AttuneException(msg.toString(), responseEntity.getStatusCode());
		}
	}
	
	protected void deleteEntityRelationships(String entityType, String dir, List<String> relTypes) throws Exception{
		String url = new StringBuilder(attuneMetaDataServiceUrlPrefix).append("/entity/").append(entityType).append("/relationships/").append(dir).toString();
		if(relTypes != null && !relTypes.isEmpty()){
			url = url + this.getQueryParamString("relType", relTypes);
		}
		ResponseEntity<String> responseEntity = this.deleteData(url);			
		int statusCode = responseEntity.getStatusCode().value();
		String json = (responseEntity.hasBody() ? (String)responseEntity.getBody() : new JSONObject().toJSONString());
		if(!((statusCode >= 200) && (statusCode < 300))){
			String msg = json;
			throw new AttuneException(msg.toString(), responseEntity.getStatusCode());
		}
	}
	
	protected void deleteEntityAndRelationships(String entityType) throws Exception {
		String entity = this.getEntity(entityType);
		JSONObject jsonObject = (JSONObject) parser.parse(entity);
		if(!jsonObject.isEmpty()){
			String entityId = this.getId(jsonObject.toJSONString());
			this.deleteEntityRelationships(entityType, "all", null);
			this.deleteEntity(Long.parseLong(entityId));
		}
	}
	
	protected Object getEntityRelationshipProperty(String relationshipId, String propName) throws Exception{
		String url = new StringBuilder(attuneMetaDataServiceUrlPrefix).append("/relationship/").append(relationshipId).append("/property/").append(propName).toString();
		ResponseEntity<String> responseEntity = this.getData(url);			
		int statusCode = responseEntity.getStatusCode().value();
		String json = (responseEntity.hasBody() ? (String)responseEntity.getBody() : null);
		if(!((statusCode >= 200) && (statusCode < 300))){
			String msg = json;
			throw new AttuneException(msg.toString(), responseEntity.getStatusCode());
		}
		if(json == null){
			return null;
		}
		return JSONValue.parse(json).toString();
	}
	
	protected void deleteEntityRelationshipProperty(String relationshipId, String propName) throws Exception{
		String url = new StringBuilder(attuneMetaDataServiceUrlPrefix).append("/relationship/").append(relationshipId).append("/property/").append(propName).toString();
		ResponseEntity<String> responseEntity = this.deleteData(url);			
		int statusCode = responseEntity.getStatusCode().value();
		String json = (responseEntity.hasBody() ? (String)responseEntity.getBody() : new JSONObject().toJSONString());
		if(!((statusCode >= 200) && (statusCode < 300))){
			String msg = json;
			throw new AttuneException(msg.toString(), responseEntity.getStatusCode());
		}
	}
			
	protected Map<String, Object> getEntityRelationshipProperties(String relationshipId) throws Exception{
		Map<String, Object> properties = new HashMap<String, Object>();
		String url = new StringBuilder(attuneMetaDataServiceUrlPrefix).append("/relationship/").append(relationshipId).append("/properties").toString();
		ResponseEntity<String> responseEntity = this.getData(url);			
		int statusCode = responseEntity.getStatusCode().value();
		String json = (responseEntity.hasBody() ? (String)responseEntity.getBody() : null);
		if(!((statusCode >= 200) && (statusCode < 300))){
			String msg = json;
			throw new AttuneException(msg.toString(), responseEntity.getStatusCode());
		}
		if(json != null){
			Object jsonObj = parser.parse(json);
			JSONObject jsonObject = (JSONObject)jsonObj;
			for(Object key:jsonObject.keySet()){
				String propName = (String)key;
				Object propValue = jsonObject.get(propName);
				properties.put(propName, propValue);
			}
		}
		return properties;
	}
	
	protected void updateEntityRelationshipProperties(String relationshipId, Map<String, Object> properties) throws Exception{
		String url = new StringBuilder(attuneMetaDataServiceUrlPrefix).append("/relationship/").append(relationshipId).append("/properties").toString();
		String data = null;
		if(properties != null && !properties.isEmpty()){
			data = new JSONObject(properties).toJSONString();
		}
		ResponseEntity<String> responseEntity = this.updateData(data, url);			
		int statusCode = responseEntity.getStatusCode().value();
		String json = (responseEntity.hasBody() ? (String)responseEntity.getBody() : new JSONObject().toJSONString());
		if(!((statusCode >= 200) && (statusCode < 300))){
			String msg = json;
			throw new AttuneException(msg.toString(), responseEntity.getStatusCode());
		}
	}
	
	protected void deleteEntityRelationshipProperties(String relationshipId) throws Exception{
		String url = new StringBuilder(attuneMetaDataServiceUrlPrefix).append("/relationship/").append(relationshipId).append("/properties").toString();
		ResponseEntity<String> responseEntity = this.deleteData(url);			
		int statusCode = responseEntity.getStatusCode().value();
		String json = (responseEntity.hasBody() ? (String)responseEntity.getBody() : new JSONObject().toJSONString());
		if(!((statusCode >= 200) && (statusCode < 300))){
			String msg = json;
			throw new AttuneException(msg.toString(), responseEntity.getStatusCode());
		}
	}
	
	protected void updateEntityRelationshipProperty(String relationshipId, String propName, String propValue) throws Exception{
		String url = new StringBuilder(attuneMetaDataServiceUrlPrefix).append("/relationship/").append(relationshipId).append("/property/").append(propName).toString();
		String data = JSONValue.toJSONString(propValue);
		ResponseEntity<String> responseEntity = this.updateData(data, url);			
		int statusCode = responseEntity.getStatusCode().value();
		String json = (responseEntity.hasBody() ? (String)responseEntity.getBody() : new JSONObject().toJSONString());
		if(!((statusCode >= 200) && (statusCode < 300))){
			String msg = json;
			throw new AttuneException(msg.toString(), responseEntity.getStatusCode());
		}
	}
	
	protected String getEntityRelationshipTypes() throws Exception{
		String url = new StringBuilder(attuneMetaDataServiceUrlPrefix).append("/relationship/types").toString();
		ResponseEntity<String> responseEntity = this.getData(url);			
		int statusCode = responseEntity.getStatusCode().value();
		String json = (responseEntity.hasBody() ? (String)responseEntity.getBody() : new JSONObject().toJSONString());
		if(!((statusCode >= 200) && (statusCode < 300))){
			String msg = json;
			throw new AttuneException(msg.toString(), responseEntity.getStatusCode());
		}
		return json;
	}
	
	protected String getEntityRelationshipCount(String entityType, String dir, String relType) throws Exception{
		List<String> relTypes = null;
		if(relType != null){
			relTypes = new ArrayList<String>();
			relTypes.add(relType);
		}
		return this.getEntityRelationshipCount(entityType, dir, relTypes);
	}
	
	protected String getEntityRelationshipCount(String entityType, String dir, List<String> relTypes) throws Exception{
		String url = new StringBuilder(attuneMetaDataServiceUrlPrefix).append("/entity/").append(entityType).append("/relationshipCount/").append(dir).toString();
		if(relTypes != null && !relTypes.isEmpty()){
			url = url + this.getQueryParamString("relType", relTypes);
		}
		ResponseEntity<String> responseEntity = this.getData(url);			
		int statusCode = responseEntity.getStatusCode().value();
		String json = (responseEntity.hasBody() ? (String)responseEntity.getBody() : new JSONObject().toJSONString());
		if(!((statusCode >= 200) && (statusCode < 300))){
			String msg = json;
			throw new AttuneException(msg.toString(), responseEntity.getStatusCode());
		}
		return json;
	}
	
	protected String getId(String json) throws Exception{
		return Neo4JUtils.getId(json);
	}
	
	protected boolean doesRootExist() throws Exception{
		boolean hasRoot = false;
		String json = this.getNodesByType("Metadata");
		if(json != null && !Neo4JUtils.isEmpty(json)){
			Object jsonObj = parser.parse(json);
			JSONObject jsonObject = null;
			JSONArray jsonArray = (JSONArray)jsonObj;
			if(!jsonArray.isEmpty()){			
				for(int index = 0; index < jsonArray.size(); index++){
					jsonObject = (JSONObject) jsonArray.get(index);
					boolean isRoot = false;
					if(jsonObject.get("isRoot") != null){
						if(toString().getClass().isAssignableFrom(jsonObject.get("isRoot").getClass())){
							isRoot = Boolean.parseBoolean((String)jsonObject.get("isRoot"));
						} else {
							isRoot = (Boolean)jsonObject.get("isRoot");
						}
					}					
					if(isRoot){
						hasRoot = true;
						break;
					}
				}
			}
		}
		return hasRoot;
	}
	
	@Override
	protected HttpHeaders createHeaders(final String url, final HttpMethod httpMethod){
        return new HttpHeaders(){
            /**
			 * 
			 */
			private static final long serialVersionUID = 1L;

			{            	
            	String attuneCredentials = props.getProperty("attune.credentials");
        		byte[] encodedAuth = Base64.encodeBase64(attuneCredentials.getBytes(Charset.forName("US-ASCII")));
        		String authHeader = "Basic " + new String( encodedAuth );
        		set( "Authorization", authHeader );
                setContentType(MediaType.APPLICATION_JSON);
            }
        };
    }
}
