package com.bah.attune.rest.services;

import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;

import java.io.IOException;
import java.io.InputStream;
import java.nio.charset.Charset;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Properties;

import org.apache.commons.codec.binary.Base64;
import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.JSONValue;
import org.json.simple.parser.JSONParser;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;

import com.bah.attune.data.AttuneException;
import com.bah.attune.rest.AbstractRestInvoker;
import com.bah.attune.rest.Neo4JUtils;

public abstract class BaseDataServiceIntegrationTest extends AbstractRestInvoker {

	protected Logger LOGGER = LoggerFactory.getLogger(BaseDataServiceIntegrationTest.class);

	protected Properties props = null;
	protected String attuneDataServiceUrlPrefix = null;
	protected String attuneMetaDataServiceUrlPrefix = null;
	protected JSONParser parser = new JSONParser();

	public BaseDataServiceIntegrationTest() {
		InputStream stream = null;
		try{			
			props = new Properties();
			stream = this.getClass().getClassLoader().getResourceAsStream("test.properties");
			props.load(stream);
			if(System.getProperty("attune.data.service.url.prefix") != null){	
				props.put("attune.data.service.url.prefix", System.getProperty("attune.data.service.url.prefix"));
			}
			if(System.getProperty("attune.metadata.service.url.prefix") != null){	
				props.put("attune.metadata.service.url.prefix", System.getProperty("attune.metadata.service.url.prefix"));
			}
			if(System.getProperty("attune.credentials") != null){	
				props.put("attune.credentials", System.getProperty("attune.credentials"));
			}
			attuneDataServiceUrlPrefix = props.getProperty("attune.data.service.url.prefix");
			attuneMetaDataServiceUrlPrefix = props.getProperty("attune.metadata.service.url.prefix");
		} catch (Exception e){
			e.printStackTrace();
			throw new RuntimeException(e.getMessage(), e);
		} finally {
			if (stream != null){
				safeClose(stream);
			}
		}
	}

	public static void safeClose(InputStream fis) {
		if (fis != null) {
			try {
				fis.close();
			} catch (IOException e) {
				//log(e);
			}
		}
	}


	protected void checkJSON(String json) throws Exception{
		assertNotNull(json);
		System.out.println(json);
		System.out.println();
		Object jsonObj = parser.parse(json);
		assertNotNull(jsonObj);
		if(jsonObj instanceof JSONArray){
			JSONArray jsonArray = (JSONArray)jsonObj;
			assertTrue(jsonArray.size() > 0);
		} else if(jsonObj instanceof JSONObject){
			JSONObject jsonObject = (JSONObject)jsonObj;
			assertTrue(!jsonObject.isEmpty());
		}
	}
	
	protected ResponseEntity<String> getData (String url) throws Exception{
		return this.getData(url, null);
	}
	
	@SuppressWarnings("unchecked")
	protected ResponseEntity<String> getData (String url, Map<String, ?> uriVariables) throws Exception{
		try{
			if(uriVariables == null){
				uriVariables = new HashMap<String, String>();
			}
			ResponseEntity<String> responseEntity = this.callRestService(url, HttpMethod.GET, null, String.class, uriVariables);			
			return responseEntity;
		} catch(AttuneException ae) {
			LOGGER.error(ae.getMessage(), ae);
			throw ae;
		} catch(Exception e) {
			LOGGER.error(e.getMessage(), e);
			throw new AttuneException(e);
		}
	}
	
	@SuppressWarnings("unchecked")
	protected ResponseEntity<String> insertData(String jsonInput, String url) throws AttuneException {		
		try{
			ResponseEntity<String> responseEntity = this.callRestService(url, HttpMethod.POST, jsonInput, String.class);			
			return responseEntity;
		} catch(AttuneException ae) {
			LOGGER.error(ae.getMessage(), ae);
			throw ae;
		} catch(Exception e) {
			LOGGER.error(e.getMessage(), e);
			throw new AttuneException(e);
		}
	}
	
	@SuppressWarnings("unchecked")
	protected ResponseEntity<String> updateData(String jsonInput, String url) throws AttuneException{		
		try{
			ResponseEntity<String> responseEntity = this.callRestService(url, HttpMethod.PUT, jsonInput, String.class);			
			return responseEntity;
		} catch(AttuneException ae) {
			LOGGER.error(ae.getMessage(), ae);
			throw ae;
		} catch(Exception e) {
			LOGGER.error(e.getMessage(), e);
			throw new AttuneException(e);
		}
	}
	
	@SuppressWarnings("unchecked")
	protected ResponseEntity<String> deleteData(String url) throws AttuneException{		
		try{
			ResponseEntity<String> responseEntity = this.callRestService(url, HttpMethod.DELETE, null, String.class);			
			return responseEntity;
		} catch(AttuneException ae) {
			LOGGER.error(ae.getMessage(), ae);
			throw ae;
		} catch(Exception e) {
			LOGGER.error(e.getMessage(), e);
			throw new AttuneException(e);
		}
	}
	
	protected String getNodesByType(String type) throws Exception{
		String url = new StringBuilder(attuneDataServiceUrlPrefix).append("/nodes/").append(type).toString();
		ResponseEntity<String> responseEntity = this.getData(url);			
		int statusCode = responseEntity.getStatusCode().value();
		String json = (responseEntity.hasBody() ? (String)responseEntity.getBody() : new JSONObject().toJSONString());
		if(!((statusCode >= 200) && (statusCode < 300))){
			String msg = json;
			throw new AttuneException(msg.toString(), responseEntity.getStatusCode());
		}
		return json;
	}
	
	protected void deleteNodesAndRelationshipsByType(String type) throws Exception{
		String nodes = this.getNodesByType(type);
		JSONArray jsonArray = (JSONArray) parser.parse(nodes);
		for(int i = 0; i < jsonArray.size(); i++){				
			JSONObject jsonObject = (JSONObject) jsonArray.get(i);
			String nodeId = this.getId(jsonObject.toJSONString());
			this.deleteNodeRelationships(nodeId, "all");
			this.deleteNode(nodeId);
		}
	}
		
	protected void deleteNodesByType(String type) throws Exception{
		String url = new StringBuilder(attuneDataServiceUrlPrefix).append("/nodes/").append(type).toString();
		ResponseEntity<String> responseEntity = this.deleteData(url);			
		int statusCode = responseEntity.getStatusCode().value();
		String json = (responseEntity.hasBody() ? (String)responseEntity.getBody() : new JSONObject().toJSONString());
		if(!((statusCode >= 200) && (statusCode < 300))){
			String msg = json;
			throw new AttuneException(msg.toString(), responseEntity.getStatusCode());
		}
	}
	
	protected String getNode(String nodeId) throws Exception{
		String url = new StringBuilder(attuneDataServiceUrlPrefix).append("/node/").append(nodeId).toString();
		ResponseEntity<String> responseEntity = this.getData(url);			
		int statusCode = responseEntity.getStatusCode().value();
		String json = (responseEntity.hasBody() ? (String)responseEntity.getBody() : new JSONObject().toJSONString());
		if(!((statusCode >= 200) && (statusCode < 300))){
			String msg = json;
			throw new AttuneException(msg.toString(), responseEntity.getStatusCode());
		}
		return json;
	}
	
	protected String insertNode(String type, Map<String, Object> properties) throws Exception{
		String url = new StringBuilder(attuneDataServiceUrlPrefix).append("/node/").append(type).toString();
		String data = null;
		if(properties != null && !properties.isEmpty()){
			data = new JSONObject(properties).toJSONString();
		}
		ResponseEntity<String> responseEntity = this.insertData(data, url);			
		int statusCode = responseEntity.getStatusCode().value();
		String json = (responseEntity.hasBody() ? (String)responseEntity.getBody() : new JSONObject().toJSONString());
		if(!((statusCode >= 200) && (statusCode < 300))){
			String msg = json;
			throw new AttuneException(msg.toString(), responseEntity.getStatusCode());
		}
		return json;
	}

	protected void deleteNode(String id) throws Exception{
		String url = new StringBuilder(attuneDataServiceUrlPrefix).append("/node/").append(id).toString();
		ResponseEntity<String> responseEntity = this.deleteData(url);			
		int statusCode = responseEntity.getStatusCode().value();
		String json = (responseEntity.hasBody() ? (String)responseEntity.getBody() : new JSONObject().toJSONString());
		if(!((statusCode >= 200) && (statusCode < 300))){
			String msg = json;
			throw new AttuneException(msg.toString(), responseEntity.getStatusCode());
		}
	}
	
	protected void deleteAllNodes() throws Exception{
		String url = new StringBuilder(attuneDataServiceUrlPrefix).append("/nodes").toString();
		ResponseEntity<String> responseEntity = this.deleteData(url);			
		int statusCode = responseEntity.getStatusCode().value();
		String json = (responseEntity.hasBody() ? (String)responseEntity.getBody() : new JSONObject().toJSONString());
		if(!((statusCode >= 200) && (statusCode < 300))){
			String msg = json;
			throw new AttuneException(msg.toString(), responseEntity.getStatusCode());
		}
	}

	protected Object getNodeProperty(String nodeId, String propName) throws Exception{
		String url = new StringBuilder(attuneDataServiceUrlPrefix).append("/node/").append(nodeId).append("/property/").append(propName).toString();
		ResponseEntity<String> responseEntity = this.getData(url);			
		int statusCode = responseEntity.getStatusCode().value();
		String json = (responseEntity.hasBody() ? (String)responseEntity.getBody() : null);
		if(!((statusCode >= 200) && (statusCode < 300))){
			if (json != null){
				String msg = json;
				throw new AttuneException(msg.toString(), responseEntity.getStatusCode());
			}
		}
		Object value = null;
		if(json != null){
			value = JSONValue.parse(json);
		}
		return value;
	}
	
	protected void deleteNodeProperty(String nodeId, String propName) throws Exception{
		String url = new StringBuilder(attuneDataServiceUrlPrefix).append("/node/").append(nodeId).append("/property/").append(propName).toString();
		ResponseEntity<String> responseEntity = this.deleteData(url);			
		int statusCode = responseEntity.getStatusCode().value();
		String json = (responseEntity.hasBody() ? (String)responseEntity.getBody() : new JSONObject().toJSONString());
		if(!((statusCode >= 200) && (statusCode < 300))){
			String msg = json;
			throw new AttuneException(msg.toString(), responseEntity.getStatusCode());
		}
	}
	
	protected Map<String, Object> getNodeProperties(String nodeId) throws Exception{
		Map<String, Object> properties = new HashMap<String, Object>();
		String url = new StringBuilder(attuneDataServiceUrlPrefix).append("/node/").append(nodeId).append("/properties").toString();
		ResponseEntity<String> responseEntity = this.getData(url);			
		int statusCode = responseEntity.getStatusCode().value();
		String json = (responseEntity.hasBody() ? (String)responseEntity.getBody() : null);
		if(!((statusCode >= 200) && (statusCode < 300))){
			if (json != null){
				String msg = json;
				throw new AttuneException(msg.toString(), responseEntity.getStatusCode());
			}
		}
		if(json != null){
			Object jsonObj = parser.parse(json);
			JSONObject jsonObject = (JSONObject)jsonObj;
			for(Object key:jsonObject.keySet()){
				String propName = (String)key;
				Object propValue = jsonObject.get(propName);
				properties.put(propName, propValue);
			}
		}
		return properties;
	}	
	
	protected void updateNodeProperties(String id, Map<String, Object> properties) throws Exception{
		String url = new StringBuilder(attuneDataServiceUrlPrefix).append("/node/").append(id).append("/properties").toString();
		String data = null;
		if(properties != null && !properties.isEmpty()){
			data = new JSONObject(properties).toJSONString();
		}
		ResponseEntity<String> responseEntity = this.updateData(data, url);			
		int statusCode = responseEntity.getStatusCode().value();
		String json = (responseEntity.hasBody() ? (String)responseEntity.getBody() : new JSONObject().toJSONString());
		if(!((statusCode >= 200) && (statusCode < 300))){
			String msg = json;
			throw new AttuneException(msg.toString(), responseEntity.getStatusCode());
		}
	}
	
	protected void deleteNodeProperties(String nodeId) throws Exception{
		String url = new StringBuilder(attuneDataServiceUrlPrefix).append("/node/").append(nodeId).append("/properties").toString();
		ResponseEntity<String> responseEntity = this.deleteData(url);			
		int statusCode = responseEntity.getStatusCode().value();
		String json = (responseEntity.hasBody() ? (String)responseEntity.getBody() : new JSONObject().toJSONString());
		if(!((statusCode >= 200) && (statusCode < 300))){
			String msg = json;
			throw new AttuneException(msg.toString(), responseEntity.getStatusCode());
		}
	}

	protected void updateNodeProperty(String id, String propName, Object propValue) throws Exception{
		String url = new StringBuilder(attuneDataServiceUrlPrefix).append("/node/").append(id).append("/property/").append(propName).toString();
		String data = JSONValue.toJSONString(propValue);
		ResponseEntity<String> responseEntity = this.updateData(data, url);			
		int statusCode = responseEntity.getStatusCode().value();
		String json = (responseEntity.hasBody() ? (String)responseEntity.getBody() : new JSONObject().toJSONString());
		if(!((statusCode >= 200) && (statusCode < 300))){
			String msg = json;
			throw new AttuneException(msg.toString(), responseEntity.getStatusCode());
		}
	}

	protected String getNodeRelationship(String relationshipId) throws Exception{
		String url = new StringBuilder(attuneDataServiceUrlPrefix).append("/relationship/").append(relationshipId).toString();
		ResponseEntity<String> responseEntity = this.getData(url);			
		int statusCode = responseEntity.getStatusCode().value();
		String json = (responseEntity.hasBody() ? (String)responseEntity.getBody() : new JSONObject().toJSONString());
		if(!((statusCode >= 200) && (statusCode < 300))){
			String msg = json;
			throw new AttuneException(msg.toString(), responseEntity.getStatusCode());
		}
		return json;
	}

	protected String getNodeRelationships(String nodeId, String relationshipDir, List<String> relTypes) throws Exception{
		String url = new StringBuilder(attuneDataServiceUrlPrefix).append("/node/").append(nodeId).append("/relationships/").append(relationshipDir).toString();
		if(relTypes != null && !relTypes.isEmpty()){
			url = url + this.getQueryParamString("relType", relTypes);
		}
		ResponseEntity<String> responseEntity = this.getData(url);			
		int statusCode = responseEntity.getStatusCode().value();
		String json = (responseEntity.hasBody() ? (String)responseEntity.getBody() : new JSONObject().toJSONString());
		if(!((statusCode >= 200) && (statusCode < 300))){
			String msg = json;
			throw new AttuneException(msg.toString(), responseEntity.getStatusCode());
		}
		return json;
	}

	protected String insertNodeRelationship(String startNodeId, String relType, String endNodeId, Map<String, Object> properties) throws Exception{
		String url = new StringBuilder(attuneDataServiceUrlPrefix).append("/node/").append(startNodeId).append("/relationships/").append(relType).append("/").append(endNodeId).toString();
		String data = null;
		if(properties != null && !properties.isEmpty()){
			data = new JSONObject(properties).toJSONString();
		}
		ResponseEntity<String> responseEntity = this.insertData(data, url);			
		int statusCode = responseEntity.getStatusCode().value();
		String json = (responseEntity.hasBody() ? (String)responseEntity.getBody() : new JSONObject().toJSONString());
		if(!((statusCode >= 200) && (statusCode < 300))){
			String msg = json;
			throw new AttuneException(msg.toString(), responseEntity.getStatusCode());
		}
		return json;
	}

	protected void deleteNodeRelationship(String id) throws Exception{
		String url = new StringBuilder(attuneDataServiceUrlPrefix).append("/relationship/").append(id).toString();
		ResponseEntity<String> responseEntity = this.deleteData(url);			
		int statusCode = responseEntity.getStatusCode().value();
		String json = (responseEntity.hasBody() ? (String)responseEntity.getBody() : new JSONObject().toJSONString());
		if(!((statusCode >= 200) && (statusCode < 300))){
			String msg = json;
			throw new AttuneException(msg.toString(), responseEntity.getStatusCode());
		}
	}

	protected void deleteNodeRelationships(String nodeId, String dir) throws Exception{
		String url = new StringBuilder(attuneDataServiceUrlPrefix).append("/node/").append(nodeId).append("/relationships/").append(dir).toString();
		ResponseEntity<String> responseEntity = this.deleteData(url);			
		int statusCode = responseEntity.getStatusCode().value();
		String json = (responseEntity.hasBody() ? (String)responseEntity.getBody() : new JSONObject().toJSONString());
		if(!((statusCode >= 200) && (statusCode < 300))){
			String msg = json;
			throw new AttuneException(msg.toString(), responseEntity.getStatusCode());
		}
	}

	protected Object getNodeRelationshipProperty(String relationshipId, String propName) throws Exception{
		String url = new StringBuilder(attuneDataServiceUrlPrefix).append("/relationship/").append(relationshipId).append("/property/").append(propName).toString();
		ResponseEntity<String> responseEntity = this.getData(url);			
		int statusCode = responseEntity.getStatusCode().value();
		String json = (responseEntity.hasBody() ? (String)responseEntity.getBody() : null);
		if(!((statusCode >= 200) && (statusCode < 300))){
			if (json != null){
				String msg = json;
				throw new AttuneException(msg.toString(), responseEntity.getStatusCode());
			}
		}
		Object value = null;
		if(json != null){
			value = JSONValue.parse(json).toString();
		}
		return value;
	}

	protected void deleteNodeRelationshipProperty(String relationshipId, String propName) throws Exception{
		String url = new StringBuilder(attuneDataServiceUrlPrefix).append("/relationship/").append(relationshipId).append("/property/").append(propName).toString();
		ResponseEntity<String> responseEntity = this.deleteData(url);			
		int statusCode = responseEntity.getStatusCode().value();
		String json = (responseEntity.hasBody() ? (String)responseEntity.getBody() : new JSONObject().toJSONString());
		if(!((statusCode >= 200) && (statusCode < 300))){
			String msg = json;
			throw new AttuneException(msg.toString(), responseEntity.getStatusCode());
		}
	}

	protected Map<String, Object> getNodeRelationshipProperties(String relationshipId) throws Exception{
		Map<String, Object> properties = new HashMap<String, Object>();
		String url = new StringBuilder(attuneDataServiceUrlPrefix).append("/relationship/").append(relationshipId).append("/properties").toString();
		ResponseEntity<String> responseEntity = this.getData(url);			
		int statusCode = responseEntity.getStatusCode().value();
		String json = (responseEntity.hasBody() ? (String)responseEntity.getBody() : null);
		if(!((statusCode >= 200) && (statusCode < 300))){
			String msg = json;
			throw new AttuneException(msg.toString(), responseEntity.getStatusCode());
		}
		if(json != null){
			Object jsonObj = parser.parse(json);
			JSONObject jsonObject = (JSONObject)jsonObj;
			for(Object key:jsonObject.keySet()){
				String propName = (String)key;
				Object propValue = jsonObject.get(propName);
				properties.put(propName, propValue);
			}
		}
		return properties;
	}

	protected void updateNodeRelationshipProperties(String id, Map<String, Object> properties) throws Exception{
		String url = new StringBuilder(attuneDataServiceUrlPrefix).append("/relationship/").append(id).append("/properties").toString();
		String data = null;
		if(properties != null && !properties.isEmpty()){
			data = new JSONObject(properties).toJSONString();
		}
		ResponseEntity<String> responseEntity = this.updateData(data, url);			
		int statusCode = responseEntity.getStatusCode().value();
		String json = (responseEntity.hasBody() ? (String)responseEntity.getBody() : new JSONObject().toJSONString());
		if(!((statusCode >= 200) && (statusCode < 300))){
			String msg = json;
			throw new AttuneException(msg.toString(), responseEntity.getStatusCode());
		}
	}

	protected void deleteNodeRelationshipProperties(String relationshipId) throws Exception{
		String url = new StringBuilder(attuneDataServiceUrlPrefix).append("/relationship/").append(relationshipId).append("/properties").toString();
		ResponseEntity<String> responseEntity = this.deleteData(url);			
		int statusCode = responseEntity.getStatusCode().value();
		String json = (responseEntity.hasBody() ? (String)responseEntity.getBody() : new JSONObject().toJSONString());
		if(!((statusCode >= 200) && (statusCode < 300))){
			String msg = json;
			throw new AttuneException(msg.toString(), responseEntity.getStatusCode());
		}
	}

	protected void updateNodeRelationshipProperty(String id, String propName, String propValue) throws Exception{
		String url = new StringBuilder(attuneDataServiceUrlPrefix).append("/relationship/").append(id).append("/property/").append(propName).toString();
		String data = JSONValue.toJSONString(propValue);
		ResponseEntity<String> responseEntity = this.updateData(data, url);			
		int statusCode = responseEntity.getStatusCode().value();
		String json = (responseEntity.hasBody() ? (String)responseEntity.getBody() : new JSONObject().toJSONString());
		if(!((statusCode >= 200) && (statusCode < 300))){
			String msg = json;
			throw new AttuneException(msg.toString(), responseEntity.getStatusCode());
		}
	}

	protected String getNodeRelationshipsByType(String nodeId, String dir, String relType) throws Exception{
		String url = new StringBuilder(attuneDataServiceUrlPrefix).append("/node/").append(nodeId).append("/relationships/").append(dir).toString();
		if(relType != null){
			List<String> relTypes = new ArrayList<String>();
			relTypes.add(relType);
			url = url + this.getQueryParamString("relType", relTypes);
		}
		ResponseEntity<String> responseEntity = this.getData(url);			
		int statusCode = responseEntity.getStatusCode().value();
		String json = (responseEntity.hasBody() ? (String)responseEntity.getBody() : new JSONObject().toJSONString());
		if(!((statusCode >= 200) && (statusCode < 300))){
			String msg = json;
			throw new AttuneException(msg.toString(), responseEntity.getStatusCode());
		}
		return json;
	}

	protected void deleteNodeRelationshipsByType(String nodeId, String dir, String relType) throws Exception{
		String url = new StringBuilder(attuneDataServiceUrlPrefix).append("/node/").append(nodeId).append("/relationships/").append(dir).toString();
		if(relType != null){
			List<String> relTypes = new ArrayList<String>();
			relTypes.add(relType);
			url = url + this.getQueryParamString("relType", relTypes);
		}
		ResponseEntity<String> responseEntity = this.deleteData(url);			
		int statusCode = responseEntity.getStatusCode().value();
		String json = (responseEntity.hasBody() ? (String)responseEntity.getBody() : new JSONObject().toJSONString());
		if(!((statusCode >= 200) && (statusCode < 300))){
			String msg = json;
			throw new AttuneException(msg.toString(), responseEntity.getStatusCode());
		}
	}

	protected String getNodeRelationshipTypes() throws Exception{
		String url = new StringBuilder(attuneDataServiceUrlPrefix).append("/relationship/types").toString();
		ResponseEntity<String> responseEntity = this.getData(url);			
		int statusCode = responseEntity.getStatusCode().value();
		String json = (responseEntity.hasBody() ? (String)responseEntity.getBody() : new JSONObject().toJSONString());
		if(!((statusCode >= 200) && (statusCode < 300))){
			String msg = json;
			throw new AttuneException(msg.toString(), responseEntity.getStatusCode());
		}
		return json;
	}

	protected String getNodeRelationshipCount(String nodeId, String dir, String relType) throws Exception{
		List<String> relTypes = null;
		if(relType != null){
			relTypes = new ArrayList<String>();
			relTypes.add(relType);
		}
		return this.getNodeRelationshipCount(nodeId, dir, relTypes);
	}

	protected String getNodeRelationshipCount(String nodeId, String dir, List<String> relTypes) throws Exception{
		String url = new StringBuilder(attuneDataServiceUrlPrefix).append("/node/").append(nodeId).append("/relationshipCount/").append(dir).toString();
		if(relTypes != null && !relTypes.isEmpty()){
			url = url + this.getQueryParamString("relType", relTypes);
		}
		ResponseEntity<String> responseEntity = this.getData(url);			
		int statusCode = responseEntity.getStatusCode().value();
		String json = (responseEntity.hasBody() ? (String)responseEntity.getBody() : new JSONObject().toJSONString());
		if(!((statusCode >= 200) && (statusCode < 300))){
			String msg = json;
			throw new AttuneException(msg.toString(), responseEntity.getStatusCode());
		}
		return json;
	}

	protected String getNodes(String type, String propName, String propValue) throws Exception{
		String url = new StringBuilder(attuneDataServiceUrlPrefix).append("/nodes/").append(type).toString();		
		if(propName != null && propValue != null){
			Map<String, String> queryParams = new HashMap<String, String>();
			queryParams.put("propName", propName);
			queryParams.put("propValue", propValue);
			url = url + this.getQueryParamString(queryParams);
		}		
		ResponseEntity<String> responseEntity = this.getData(url);			
		int statusCode = responseEntity.getStatusCode().value();
		String json = (responseEntity.hasBody() ? (String)responseEntity.getBody() : new JSONObject().toJSONString());
		if(!((statusCode >= 200) && (statusCode < 300))){
			String msg = json;
			throw new AttuneException(msg.toString(), responseEntity.getStatusCode());
		}
		return json;
	}

	protected List<String> getAllNodeTypes() throws Exception{
		List<String> nodeTypes = new ArrayList<String>();
		String url = new StringBuilder(attuneDataServiceUrlPrefix).append("/nodeTypes").toString();
		ResponseEntity<String> responseEntity = this.getData(url);			
		int statusCode = responseEntity.getStatusCode().value();
		String json = (responseEntity.hasBody() ? (String)responseEntity.getBody() : null);
		if(!((statusCode >= 200) && (statusCode < 300))){
			String msg = json;
			throw new AttuneException(msg.toString(), responseEntity.getStatusCode());
		}
		if(json != null){
			Object jsonObj = parser.parse(json);
			JSONArray jsonArray = (JSONArray)jsonObj;
			for(int i = 0; i < jsonArray.size(); i++){
				String nodeType = (String)jsonArray.get(i);
				nodeTypes.add(nodeType);
			}
		}
		return nodeTypes;
	}

	protected String validateNodes() throws Exception{
		String url = new StringBuilder(attuneDataServiceUrlPrefix).append("/validate").toString();
		ResponseEntity<String> responseEntity = this.getData(url);			
		int statusCode = responseEntity.getStatusCode().value();
		String json = (responseEntity.hasBody() ? (String)responseEntity.getBody() : new JSONObject().toJSONString());
		if(!((statusCode >= 200) && (statusCode < 300))){
			String msg = json;
			throw new AttuneException(msg.toString(), responseEntity.getStatusCode());
		}
		return json;
	}

	protected String insertEntity(String entityType, Map<String, Object> properties) throws Exception{
		String url = new StringBuilder(attuneMetaDataServiceUrlPrefix).append("/entity/").append(entityType).toString();
		String data = null;
		if(properties != null && !properties.isEmpty()){
			data = new JSONObject(properties).toJSONString();
		}
		ResponseEntity<String> responseEntity = this.insertData(data, url);			
		int statusCode = responseEntity.getStatusCode().value();
		String json = (responseEntity.hasBody() ? (String)responseEntity.getBody() : new JSONObject().toJSONString());
		if(!((statusCode >= 200) && (statusCode < 300))){
			String msg = json;
			throw new AttuneException(msg.toString(), responseEntity.getStatusCode());
		}
		return json;
	}

	protected String getEntity(String entityType) throws Exception{
		String url = new StringBuilder(attuneMetaDataServiceUrlPrefix).append("/entity/").append(entityType).toString();
		ResponseEntity<String> responseEntity = this.getData(url);			
		int statusCode = responseEntity.getStatusCode().value();
		String json = (responseEntity.hasBody() ? (String)responseEntity.getBody() : new JSONObject().toJSONString());
		if(!((statusCode >= 200) && (statusCode < 300))){
			String msg = json;
			throw new AttuneException(msg.toString(), responseEntity.getStatusCode());
		}
		return json;
	}

	protected void deleteEntity(Long id) throws Exception{
		String url = new StringBuilder(attuneMetaDataServiceUrlPrefix).append("/entity/").append(String.valueOf(id)).toString();
		ResponseEntity<String> responseEntity = this.deleteData(url);			
		int statusCode = responseEntity.getStatusCode().value();
		String json = (responseEntity.hasBody() ? (String)responseEntity.getBody() : new JSONObject().toJSONString());
		if(!((statusCode >= 200) && (statusCode < 300))){
			String msg = json;
			throw new AttuneException(msg.toString(), responseEntity.getStatusCode());
		}
	}

	protected Map<String, Object> getEntityProperties(String entityType) throws Exception{
		Map<String, Object> properties = new HashMap<String, Object>();
		String url = new StringBuilder(attuneMetaDataServiceUrlPrefix).append("/entity/").append(entityType).append("/properties").toString();
		ResponseEntity<String> responseEntity = this.getData(url);			
		int statusCode = responseEntity.getStatusCode().value();
		String json = (responseEntity.hasBody() ? (String)responseEntity.getBody() : null);
		if(!((statusCode >= 200) && (statusCode < 300))){
			String msg = json;
			throw new AttuneException(msg.toString(), responseEntity.getStatusCode());
		}
		if(json != null){
			Object jsonObj = parser.parse(json);
			JSONObject jsonObject = (JSONObject)jsonObj;
			for(Object key:jsonObject.keySet()){
				String propName = (String)key;
				Object propValue = jsonObject.get(propName);
				properties.put(propName, propValue);
			}
		}
		return properties;
	}

	protected void updateEntityProperties(String entityType, Map<String, Object> properties) throws Exception{
		String url = new StringBuilder(attuneMetaDataServiceUrlPrefix).append("/entity/").append(entityType).append("/properties").toString();
		String data = null;
		if(properties != null && !properties.isEmpty()){
			data = new JSONObject(properties).toJSONString();
		}
		ResponseEntity<String> responseEntity = this.updateData(data, url);			
		int statusCode = responseEntity.getStatusCode().value();
		String json = (responseEntity.hasBody() ? (String)responseEntity.getBody() : new JSONObject().toJSONString());
		if(!((statusCode >= 200) && (statusCode < 300))){
			String msg = json;
			throw new AttuneException(msg.toString(), responseEntity.getStatusCode());
		}
	}

	protected String getEntityRelationships(String entityType, String relationshipDir, List<String> relTypes) throws Exception{
		String url = new StringBuilder(attuneMetaDataServiceUrlPrefix).append("/entity/").append(entityType).append("/relationships/").append(relationshipDir).toString();
		if(relTypes != null && !relTypes.isEmpty()){
			url = url + this.getQueryParamString("relType", relTypes);
		}
		ResponseEntity<String> responseEntity = this.getData(url);			
		int statusCode = responseEntity.getStatusCode().value();
		String json = (responseEntity.hasBody() ? (String)responseEntity.getBody() : new JSONObject().toJSONString());
		if(!((statusCode >= 200) && (statusCode < 300))){
			String msg = json;
			throw new AttuneException(msg.toString(), responseEntity.getStatusCode());
		}
		return json;
	}

	protected String insertEntityRelationship(String startEntityType, String relationshipType, String endEntityType, Map<String, Object> properties) throws Exception{
		String url = new StringBuilder(attuneMetaDataServiceUrlPrefix).append("/entity/").append(startEntityType).append("/relationships/").append(relationshipType).append("/").append(endEntityType).toString();
		String data = null;
		if(properties != null && !properties.isEmpty()){
			data = new JSONObject(properties).toJSONString();
		}
		ResponseEntity<String> responseEntity = this.insertData(data, url);			
		int statusCode = responseEntity.getStatusCode().value();
		String json = (responseEntity.hasBody() ? (String)responseEntity.getBody() : new JSONObject().toJSONString());
		if(!((statusCode >= 200) && (statusCode < 300))){
			String msg = json;
			throw new AttuneException(msg.toString(), responseEntity.getStatusCode());
		}
		return json;
	}

	protected void deleteEntityRelationship(String id) throws Exception{
		String url = new StringBuilder(attuneMetaDataServiceUrlPrefix).append("/relationship/").append(id).toString();
		ResponseEntity<String> responseEntity = this.deleteData(url);			
		int statusCode = responseEntity.getStatusCode().value();
		String json = (responseEntity.hasBody() ? (String)responseEntity.getBody() : new JSONObject().toJSONString());
		if(!((statusCode >= 200) && (statusCode < 300))){
			String msg = json;
			throw new AttuneException(msg.toString(), responseEntity.getStatusCode());
		}
	}

	protected void deleteEntityRelationships(String entityType, String dir, List<String> relTypes) throws Exception{
		String url = new StringBuilder(attuneMetaDataServiceUrlPrefix).append("/entity/").append(entityType).append("/relationships/").append(dir).toString();
		if(relTypes != null && !relTypes.isEmpty()){
			url = url + this.getQueryParamString("relType", relTypes);
		}
		ResponseEntity<String> responseEntity = this.deleteData(url);			
		int statusCode = responseEntity.getStatusCode().value();
		String json = (responseEntity.hasBody() ? (String)responseEntity.getBody() : new JSONObject().toJSONString());
		if(!((statusCode >= 200) && (statusCode < 300))){
			String msg = json;
			throw new AttuneException(msg.toString(), responseEntity.getStatusCode());
		}
	}
	
	protected void deleteEntityAndRelationships(String entityType) throws Exception {
		String entity = this.getEntity(entityType);
		JSONObject jsonObject = (JSONObject) parser.parse(entity);
		if(!jsonObject.isEmpty()){
			String entityId = this.getId(jsonObject.toJSONString());
			this.deleteEntityRelationships(entityType, "all", null);
			this.deleteEntity(Long.parseLong(entityId));
		}
	}
	
	protected String getId(String json) throws Exception{
		return Neo4JUtils.getId(json);
	}
	
	protected boolean doesRootExist() throws Exception{
		boolean hasRoot = false;
		String json = this.getNodesByType("Metadata");
		if(json != null){
			Object jsonObj = parser.parse(json);
			JSONObject jsonObject = null;
			JSONArray jsonArray = (JSONArray)jsonObj;
			if(!jsonArray.isEmpty()){			
				for(int index = 0; index < jsonArray.size(); index++){
					jsonObject = (JSONObject) jsonArray.get(index);
					boolean isRoot = false;
					if(jsonObject.get("isRoot") != null){
						if(toString().getClass().isAssignableFrom(jsonObject.get("isRoot").getClass())){
							isRoot = Boolean.parseBoolean((String)jsonObject.get("isRoot"));
						} else {
							isRoot = (Boolean)jsonObject.get("isRoot");
						}
					}					
					if(isRoot){
						hasRoot = true;
						break;
					}
				}
			}
		}
		return hasRoot;
	}

	protected String getNodePropertyValue(String json, String propName) throws Exception{
		return Neo4JUtils.getDataValue(json, propName);
	}

	@Override
	protected HttpHeaders createHeaders(final String url, final HttpMethod httpMethod){
		return new HttpHeaders(){
			/**
			 * 
			 */
			private static final long serialVersionUID = 1L;

			{            	
				String attuneCredentials = props.getProperty("attune.credentials");
				byte[] encodedAuth = Base64.encodeBase64(attuneCredentials.getBytes(Charset.forName("US-ASCII")));
				String authHeader = "Basic " + new String( encodedAuth );
				set( "Authorization", authHeader );
				setContentType(MediaType.APPLICATION_JSON);
			}
		};
	}
}
