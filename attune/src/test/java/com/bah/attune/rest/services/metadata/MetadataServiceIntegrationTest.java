package com.bah.attune.rest.services.metadata;

import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNull;
import static org.junit.Assert.assertTrue;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.JSONValue;
import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.springframework.http.HttpStatus;

import com.bah.attune.rest.Neo4JUtils;

import com.bah.attune.rest.services.BaseMetadataServiceIntegrationTest;

public class MetadataServiceIntegrationTest extends BaseMetadataServiceIntegrationTest{
	
	private static final String ENTITY_TYPE_DASHBOARD = "Dashboard";
	private static final String BAR_TYPE = "Metadata Bar Type";
	private static final String FOO_TYPE = "Metadata Foo Type";
	private static final String ENTITY_TYPE_BAM = "Metadata-Type-BAM";
	private static final String ENTITY_TYPE_BAR = "Metadata-Type-Bar";
	private static final String ENTITY_TYPE_FOO = "Metadata-Type-Foo";

	public MetadataServiceIntegrationTest() {
		super();
	}
	
	@Before
	public void setup() throws Exception{
		//Clean up any old data
		System.out.println("Cleaning up old data before test...");
		this.deleteEntityAndRelationships(ENTITY_TYPE_FOO);		
		this.deleteEntityAndRelationships(ENTITY_TYPE_BAR);		
		this.deleteEntityAndRelationships(ENTITY_TYPE_BAM);
		this.deleteEntityAndRelationships(FOO_TYPE);		
		this.deleteEntityAndRelationships(BAR_TYPE);
		
		assertTrue(Neo4JUtils.isEmpty(this.getEntity(ENTITY_TYPE_FOO)));
		assertTrue(Neo4JUtils.isEmpty(this.getEntity(ENTITY_TYPE_BAR)));
		assertTrue(Neo4JUtils.isEmpty(this.getEntity(ENTITY_TYPE_BAM)));
		assertTrue(Neo4JUtils.isEmpty(this.getEntity(FOO_TYPE)));
		assertTrue(Neo4JUtils.isEmpty(this.getEntity(BAR_TYPE)));
	}
	
	@Test
	public void testMetadataEntities() throws Exception {
		try{
			//Test inserting a new Metadata Entity
			boolean hasRoot = doesRootExist();
			Map<String, Object> fooProperties = new HashMap<String, Object>();
			fooProperties.put("Name", ENTITY_TYPE_FOO);
			fooProperties.put("isRoot", !hasRoot);
			fooProperties.put("fieldList", "Name,desc,booleanField,intField,doubleField,shortField,longField,floatField");
			fooProperties.put("requiredFields", "Name,desc");		
			String jsonResult = this.insertEntity(ENTITY_TYPE_FOO, fooProperties);
			this.checkJSON(jsonResult);
			String entityId = this.getId(jsonResult);
			
			//Create Dashboard if needed
			if(Neo4JUtils.isEmpty(this.getEntity(ENTITY_TYPE_DASHBOARD))){
				Map<String, Object> properties = new HashMap<String, Object>();
				properties.put("Name", ENTITY_TYPE_DASHBOARD);
				properties.put("isRoot", false);
				properties.put("fieldList", "Name,groupBy,chartType,alertCheck,displayList,entity,alertValue");
				properties.put("requiredFields", "Name,groupBy,chartType,displayList,entity");		
				this.insertEntity(ENTITY_TYPE_DASHBOARD, properties);				
			}
			
			//Test retrieving entity properties
			assertTrue(ENTITY_TYPE_FOO.equals(this.getEntityProperty(ENTITY_TYPE_FOO, "Name")));
			assertTrue(Boolean.valueOf(!hasRoot).equals(this.getEntityProperty(ENTITY_TYPE_FOO, "isRoot")));		
			assertNotNull(this.getEntityProperty(ENTITY_TYPE_FOO, "fieldList"));
			assertNotNull(this.getEntityProperty(ENTITY_TYPE_FOO, "requiredFields"));		
			Map<String, Object> fooProps = this.getEntityProperties(ENTITY_TYPE_FOO);
			assertTrue(fooProperties.keySet().containsAll(fooProps.keySet()));
			for(String key:fooProperties.keySet()){
				assertTrue(key+" is not equal: "+fooProperties.get(key)+":"+fooProps.get(key), JSONValue.toJSONString(fooProperties.get(key)).equals(JSONValue.toJSONString(fooProps.get(key))));
			}
			
			//Test inserting another new Metadata Entity
			Map<String, Object> barProperties = new HashMap<String, Object>();
			barProperties.put("Name", ENTITY_TYPE_BAR);
			barProperties.put("isRoot", !hasRoot);
			barProperties.put("fieldList", "Name,desc,booleanField,intField,doubleField,shortField,longField,floatField");
			barProperties.put("requiredFields", "Name,desc");
			jsonResult = this.insertEntity(ENTITY_TYPE_BAR, barProperties);
			this.checkJSON(jsonResult);
			entityId = this.getId(jsonResult);
			
			//Test retrieving entity properties
			assertTrue(ENTITY_TYPE_BAR.equals(this.getEntityProperty(ENTITY_TYPE_BAR, "Name")));
			assertTrue(Boolean.valueOf(!hasRoot).equals(this.getEntityProperty(ENTITY_TYPE_BAR, "isRoot")));		
			assertNotNull(this.getEntityProperty(ENTITY_TYPE_BAR, "fieldList"));
			assertNotNull(this.getEntityProperty(ENTITY_TYPE_BAR, "requiredFields"));		
			Map<String, Object> barProps = this.getEntityProperties(ENTITY_TYPE_BAR);
			assertTrue(barProperties.keySet().containsAll(barProps.keySet()));
			for(String key:barProperties.keySet()){
				assertTrue(key+" is not equal: "+barProperties.get(key)+":"+barProps.get(key), JSONValue.toJSONString(barProperties.get(key)).equals(JSONValue.toJSONString(barProps.get(key))));
			}
			
			//Test get all entity types
			List<String> types = this.getEntityTypes();
			assertTrue(types.contains(ENTITY_TYPE_FOO));
			assertTrue(types.contains(ENTITY_TYPE_BAR));
			
			//Test get entity by type and property
			String json = this.getEntity(ENTITY_TYPE_FOO);
			this.checkJSON(json);
			assertTrue(this.getEntities("Name", ENTITY_TYPE_FOO).contains(json));
			
			//Test update entity property
			this.updateEntityProperty(ENTITY_TYPE_FOO, "requiredFields", "Name,desc,booleanField");		
			assertTrue("Name,desc,booleanField".equals(this.getEntityProperty(ENTITY_TYPE_FOO, "requiredFields")));
			
			//Test update entity properties		
			this.updateEntityProperties(ENTITY_TYPE_FOO, fooProperties);
			assertTrue("Name,desc".equals(this.getEntityProperty(ENTITY_TYPE_FOO, "requiredFields")));		
			Map<String, Object> props = this.getEntityProperties(ENTITY_TYPE_FOO);
			assertTrue(fooProperties.keySet().containsAll(props.keySet()));
			for(String key:fooProperties.keySet()){
				assertTrue(key+" is not equal: "+fooProperties.get(key)+":"+props.get(key), JSONValue.toJSONString(fooProperties.get(key)).equals(JSONValue.toJSONString(props.get(key))));
			}
			
			//Test Deleting a property
			this.updateEntityProperty(ENTITY_TYPE_FOO, "newProperty", "New Test Property");		
			assertTrue("New Test Property".equals(this.getEntityProperty(ENTITY_TYPE_FOO, "newProperty")));
			this.deleteEntityProperty(ENTITY_TYPE_FOO, "newProperty");
			assertNull(this.getEntityProperty(ENTITY_TYPE_FOO, "newProperty"));
			
			//Test Delete all properties
			this.updateEntityProperty(ENTITY_TYPE_FOO, "newProperty", "New Test Property");
			this.deleteEntityProperties(ENTITY_TYPE_FOO);
			props = this.getEntityProperties(ENTITY_TYPE_FOO);
			assertNull(props.get("newProperty"));
			assertTrue(props.containsKey("Name"));
			assertTrue(props.containsKey("isRoot"));
			assertTrue(props.containsKey("fieldList"));
			assertTrue(props.containsKey("requiredFields"));
			
			//Test delete entity
			this.deleteEntity(ENTITY_TYPE_FOO);		
			String jsonEntities = this.getEntity(ENTITY_TYPE_FOO);
			Object jsonObj = parser.parse(jsonEntities);
			if(jsonObj instanceof JSONArray){
				JSONArray jsonArray = (JSONArray)jsonObj;
				assertTrue(jsonArray.isEmpty());
			}
			
			this.deleteEntity(ENTITY_TYPE_BAR);		
			jsonEntities = this.getEntity(ENTITY_TYPE_BAR);
			jsonObj = parser.parse(jsonEntities);
			if(jsonObj instanceof JSONArray){
				JSONArray jsonArray = (JSONArray)jsonObj;
				assertTrue(jsonArray.isEmpty());
			}
		} catch(Throwable t){
			t.printStackTrace();
			System.out.println("ERROR: "+t.getMessage());
			throw t;
		}
	}
	
	@Test
	public void testMetadataRelationships() throws Exception{
		try{
			Map<String, Object> properties = new HashMap<String, Object>();
			//Test inserting Foo Metadata Entity Type
			properties.put("Name", ENTITY_TYPE_FOO);
			if(this.doesRootExist()){
				properties.put("isRoot", false);
			} else {
				properties.put("isRoot", true);
			}
			properties.put("fieldList", "Name,desc,booleanField,intField,doubleField,shortField,longField,floatField");
			properties.put("requiredFields", "Name,desc");
			this.insertEntity(ENTITY_TYPE_FOO, properties);
			
			//Create Dashboard if needed
			if(Neo4JUtils.isEmpty(this.getEntity(ENTITY_TYPE_DASHBOARD))){
				Map<String, Object> dashboardProperties = new HashMap<String, Object>();
				dashboardProperties.put("Name", ENTITY_TYPE_DASHBOARD);
				dashboardProperties.put("isRoot", false);
				dashboardProperties.put("fieldList", "Name,groupBy,chartType,alertCheck,displayList,entity,alertValue");
				dashboardProperties.put("requiredFields", "Name,groupBy,chartType,displayList,entity");		
				this.insertEntity(ENTITY_TYPE_DASHBOARD, dashboardProperties);				
			}
			
			//Test inserting Bar Metadata Entity Type
			properties.put("Name", ENTITY_TYPE_BAR);
			properties.put("isRoot", false);
			this.insertEntity(ENTITY_TYPE_BAR, properties);
			
			//Test inserting BAM Metadata Entity Type
			properties.put("Name", ENTITY_TYPE_BAM);
			properties.put("isRoot", false);
			this.insertEntity(ENTITY_TYPE_BAM, properties);
			
			//Test Creating Metadata Entity Relationships
			Map<String, Object> relProperties = new HashMap<String, Object>();
			relProperties.put("Name", "Foo-has-Bar");		
			relProperties.put("desc", "Foo Has Bar Relationship");
			relProperties.put("booleanField", false);
			relProperties.put("intField", (int)30);
			relProperties.put("doubleField", (double)3.001);
			relProperties.put("shortField", (short)3.1);
			relProperties.put("longField", (long)30001123);
			relProperties.put("floatField", (float)3.0001123);
			relProperties.put("required", true);
			String relId1 = Neo4JUtils.getId(this.insertEntityRelationship(ENTITY_TYPE_FOO, "has", ENTITY_TYPE_BAR, relProperties));
			
			relProperties.put("Name", "Bar-likes-BAM");		
			relProperties.put("desc", "Bar Likes BAM Relationship");
			relProperties.put("required", false);
			String relId2 = Neo4JUtils.getId(this.insertEntityRelationship(ENTITY_TYPE_BAR, "likes", ENTITY_TYPE_BAM, relProperties));
			
			//Test get relationship count
			assertTrue(1 == Integer.parseInt(this.getEntityRelationshipCount(ENTITY_TYPE_FOO, "all", "has")));
			assertTrue(0 == Integer.parseInt(this.getEntityRelationshipCount(ENTITY_TYPE_FOO, "all", "likes")));
			assertTrue(1 == Integer.parseInt(this.getEntityRelationshipCount(ENTITY_TYPE_BAR, "all", "likes")));
			assertTrue(1 == Integer.parseInt(this.getEntityRelationshipCount(ENTITY_TYPE_BAR, "in", (String)null)));
			assertTrue(1 == Integer.parseInt(this.getEntityRelationshipCount(ENTITY_TYPE_BAR, "out", (String)null)));
			assertTrue(1 == Integer.parseInt(this.getEntityRelationshipCount(ENTITY_TYPE_BAM, "all", "likes")));
			assertTrue(0 == Integer.parseInt(this.getEntityRelationshipCount(ENTITY_TYPE_BAM, "in", "has")));
			assertTrue(0 == Integer.parseInt(this.getEntityRelationshipCount(ENTITY_TYPE_BAM, "out", (String)null)));
			assertTrue(1 == Integer.parseInt(this.getEntityRelationshipCount(ENTITY_TYPE_BAM, "all", (String)null)));
			
			//Test get relationships by type
			List<String> relTypes = new ArrayList<String>();
			relTypes.add("has");
			this.checkJSON(this.getEntityRelationships(ENTITY_TYPE_FOO, "all", relTypes));
			relTypes.add("likes");
			this.checkJSON(this.getEntityRelationships(ENTITY_TYPE_BAR, "all", relTypes));
			
			//Test get relationships by id
			this.checkJSON(this.getEntityRelationship(relId1));
			this.checkJSON(this.getEntityRelationship(relId2));
			
			//Test get relationships by direction and type
			relTypes = new ArrayList<String>();
			this.checkJSON(this.getEntityRelationships(ENTITY_TYPE_FOO, "all", null));
			this.checkJSON(this.getEntityRelationships(ENTITY_TYPE_BAR, "out", null));
			this.checkJSON(this.getEntityRelationships(ENTITY_TYPE_BAM, "in", null));
			
			relTypes.add("has");
			this.checkJSON(this.getEntityRelationships(ENTITY_TYPE_FOO, "all", relTypes));
			this.checkJSON(this.getEntityRelationships(ENTITY_TYPE_BAR, "in", relTypes));
			this.checkJSON(this.getEntityRelationships(ENTITY_TYPE_FOO, "out", relTypes));
			
			relTypes.add("likes");
			this.checkJSON(this.getEntityRelationships(ENTITY_TYPE_BAR, "all", relTypes));
			this.checkJSON(this.getEntityRelationships(ENTITY_TYPE_BAM, "in", relTypes));
			this.checkJSON(this.getEntityRelationships(ENTITY_TYPE_BAR, "out", relTypes));
			
			//Test get Relationship Types
			List<String> relationshipTypes = (List<String>)Neo4JUtils.getValue(this.getEntityRelationshipTypes());
			assertTrue(relationshipTypes.contains("has"));
			assertTrue(relationshipTypes.contains("likes"));
			
			//Test Deleting a property
			Set<String> keptProperties = new HashSet<String>();
			keptProperties.add("Name");
			keptProperties.add("desc");
			keptProperties.add("booleanField");
			
			Set<String> removedProperties = new HashSet<String>();		
			removedProperties.add("intField");
			removedProperties.add("doubleField");
			removedProperties.add("shortField");
			removedProperties.add("longField");
			removedProperties.add("floatField");
			
			for(String key:removedProperties){
				this.deleteEntityRelationshipProperty(relId1, key);
			}
					
			Map<String, Object> props = this.getEntityRelationshipProperties(relId1);
			for(String key:keptProperties){
				assertTrue(props.get(key) != null);
			}
			
			//Test Delete all properties
			this.deleteEntityRelationshipProperties(relId1);
			props = this.getEntityRelationshipProperties(relId1);
			assertTrue(props.isEmpty());
			
			//Test Delete relationship
			this.deleteEntityRelationship(relId1);		
			assertTrue(Neo4JUtils.isEmpty(this.getEntityRelationships(ENTITY_TYPE_FOO, "all", null)));
			this.deleteEntityRelationships(ENTITY_TYPE_BAR, "in", relTypes);
			assertTrue(Neo4JUtils.isEmpty(this.getEntityRelationships(ENTITY_TYPE_BAR, "in", relTypes)));
			this.deleteEntityRelationships(ENTITY_TYPE_BAR, "out", null);
			assertTrue(Neo4JUtils.isEmpty(this.getEntityRelationships(ENTITY_TYPE_BAR, "out", null)));
			this.deleteEntityRelationships(ENTITY_TYPE_BAM, "in", null);
			assertTrue(Neo4JUtils.isEmpty(this.getEntityRelationships(ENTITY_TYPE_BAM, "in", null)));
		} catch(Throwable t){
			t.printStackTrace();
			System.out.println("ERROR: "+t.getMessage());
			throw t;
		}
	}
	
	@Test
	public void testMetadataValidation() throws Exception{
		try{
			//Test insert new entity with out required fields
			boolean hasRoot = doesRootExist();
			Map<String, Object> properties = new HashMap<String, Object>();
			properties.put("Name", ENTITY_TYPE_FOO);
			if(hasRoot){
				properties.put("isRoot", false);
			} else {
				properties.put("isRoot", true);
			}
			properties.put("fieldList", "Name,desc,booleanField,intField,doubleField,shortField,longField,floatField");
			properties.put("requiredFields", "Name,desc");
			
			String[] missingFields = new String[]{"requiredFields", "isRoot"};
			for(String missingField:missingFields){
				Map<String, Object> props = new HashMap<String, Object>(properties);
				props.remove(missingField);			
				try{				
					this.insertEntity(ENTITY_TYPE_FOO, props);
					Assert.fail("Exception should have been thrown here!");
				} catch(Exception e){
					StringBuilder sb = new StringBuilder("Metadata entity type ").append(ENTITY_TYPE_FOO).append(" must contain a ").append(missingField).append(" field!");
					String msg = e.getMessage();
					assertNotNull(msg);
					assertTrue(msg.contains(sb.toString()));
				}			
			}
			
			//Test updating an existing entity's properties without including the required ones
			String entityId = this.getId(this.insertEntity(ENTITY_TYPE_FOO, properties));
			try{						
				for(String missingField:missingFields){
					Map<String, Object> props = new HashMap<String, Object>();
					props.put("Name", ENTITY_TYPE_FOO);		
					props.put("isRoot", false);
					props.put("fieldList", "Name,desc,booleanField,intField,doubleField,shortField,longField,floatField");
					props.put("requiredFields", "Name,desc");
					props.remove(missingField);
					try{
						this.updateEntityProperties(ENTITY_TYPE_FOO, props);
						Assert.fail("Exception should have been thrown here!");
					} catch(Exception e){
						StringBuilder sb = new StringBuilder("Metadata entity type ").append(ENTITY_TYPE_FOO).append(" must contain a ").append(missingField).append(" field!");
						String msg = e.getMessage();
						assertNotNull(msg);
						assertTrue(msg.contains(sb.toString()));
					}
				}
			} catch(Exception e){
				throw e;
			}
			
			//Test updating a property with a null value
			try{
				this.updateEntityProperty(ENTITY_TYPE_FOO, "newField", null);
				Assert.fail("Exception should have been thrown here!");
			} catch(Exception e){
				//StringBuilder sb = new StringBuilder("Property values can not be null!");
				StringBuilder sb = new StringBuilder(HttpStatus.BAD_REQUEST.value());
				String msg = e.getMessage();
				assertNotNull(msg);
				assertTrue(msg.contains(sb.toString()));
			}
			
			//Test changing the name
			try{
				this.updateEntityProperty(ENTITY_TYPE_FOO, "Name", ENTITY_TYPE_BAR);
				Assert.fail("Exception should have been thrown here!");
			} catch(Exception e){
				StringBuilder sb = new StringBuilder("Can not change Name property for Metadata Entity type ").append(ENTITY_TYPE_FOO);
				String msg = e.getMessage();
				assertNotNull(msg);
				assertTrue(msg.contains(sb.toString()));
			}
			
			//Test deleting a required property
			try{
				this.deleteEntityProperty(ENTITY_TYPE_FOO, "requiredFields");
				Assert.fail("Exception should have been thrown here!");
			} catch(Exception e){
				StringBuilder sb = new StringBuilder("Metadata entity type ").append(ENTITY_TYPE_FOO).append(" must contain a requiredFields field!");
				String msg = e.getMessage();
				assertNotNull(msg);
				assertTrue(msg.contains(sb.toString()));
			}
					
			//Test adding a relationship that's not in the associated metadata
			try{			
				this.insertEntityRelationship(ENTITY_TYPE_FOO, "has", ENTITY_TYPE_BAR, null);		
				Assert.fail("Exception should have been thrown here!");
			} catch(Exception e){
				StringBuilder sb = new StringBuilder("Metadata Entity of type ").append(ENTITY_TYPE_BAR).append(" does not exist!");
				String msg = e.getMessage();
				assertNotNull(msg);
				assertTrue(msg.contains(sb.toString()));
			}
			
			try{
				Map<String, Object> props = new HashMap<String, Object>();
				props.put("Name", ENTITY_TYPE_BAM);		
				props.put("isRoot", false);
				props.put("fieldList", "Name,desc,booleanField,intField,doubleField,shortField,longField,floatField");
				props.put("requiredFields", "Name,desc");
				this.insertEntity(ENTITY_TYPE_BAM, props);
				Map<String, Object> relProperties = new HashMap<String, Object>();
				relProperties.put("required", false);
				this.insertEntityRelationship(ENTITY_TYPE_BAR, "likes", ENTITY_TYPE_BAM, relProperties);		
				Assert.fail("Exception should have been thrown here!");
			} catch(Exception e){
				StringBuilder sb = new StringBuilder("Metadata Entity of type ").append(ENTITY_TYPE_BAR).append(" does not exist!");
				String msg = e.getMessage();
				assertNotNull(msg);
				assertTrue(msg.contains(sb.toString()));
			}
			
			//Test Entity validation		
			properties = new HashMap<String, Object>();
			properties.put("Name", ENTITY_TYPE_FOO);
			properties.put("isRoot", !hasRoot);
			properties.put("fieldList", "Name,desc,booleanField,intField,doubleField,shortField,longField,floatField");
			properties.put("requiredFields", "Name,desc");
			
			String jsonResult = this.insertEntity(ENTITY_TYPE_FOO, properties);
			this.checkJSON(jsonResult);
			
			properties.put("Name", ENTITY_TYPE_BAR);
			jsonResult = this.insertEntity(ENTITY_TYPE_BAR, properties);
			this.checkJSON(jsonResult);
			
			properties.put("Name", ENTITY_TYPE_BAM);
			jsonResult = this.insertEntity(ENTITY_TYPE_BAM, properties);
			this.checkJSON(jsonResult);
			
			//Test Creating Metadata Entity Relationships
			Map<String, Object> relProperties = new HashMap<String, Object>();
			relProperties.put("Name", "Foo-has-Bar");		
			relProperties.put("desc", "Foo Has Bar Relationship");
			relProperties.put("required", true);
			String relId1 = Neo4JUtils.getId(this.insertEntityRelationship(ENTITY_TYPE_FOO, "has", ENTITY_TYPE_BAR, relProperties));
			
			relProperties.put("Name", "Bar-likes-BAM");		
			relProperties.put("desc", "Bar Likes BAM Relationship");
			relProperties.put("required", false);
			String relId2 = Neo4JUtils.getId(this.insertEntityRelationship(ENTITY_TYPE_BAR, "likes", ENTITY_TYPE_BAM, relProperties));
					
			String reportJson = this.validateEntities();
			JSONObject report = (JSONObject)parser.parse(reportJson);
			JSONArray errorDetails = (JSONArray)report.get("errorDetails");
			
//			assertTrue(((Long)report.get("totalErrors")).intValue() == 0);
//			assertTrue(((Long)report.get("totalInvalidNodes")).intValue() == 0);
//			assertTrue(((Long)report.get("totalInvalidRelationships")).intValue() == 0);
//			assertTrue(((Long)report.get("totalInvalidOutRelationships")).intValue() == 0);
//			assertTrue(((Long)report.get("totalInvalidInRelationships")).intValue() == 0);
//			assertTrue(errorDetails.size() == 0);
			
			assertTrue(errorDetails.size() == ((Long)report.get("totalErrors")).intValue());
		} catch(Throwable t){
			t.printStackTrace();
			System.out.println("ERROR: "+t.getMessage());
			throw t;
		}
	}
	
	@Test
	public void testURLEncodeing() throws Exception{
		try{
			Map<String, Object> properties = new HashMap<String, Object>();
			//Test inserting Metadata Entity Type with spaces and special characters
			properties.put("Name", FOO_TYPE);
			if(this.doesRootExist()){
				properties.put("isRoot", false);
			} else {
				properties.put("isRoot", true);
			}
			properties.put("fieldList", "Name,desc,booleanField,intField,doubleField,shortField,longField,floatField,my field");
			properties.put("requiredFields", "Name,desc");
			properties.put("some property", "some value");
			this.getId(this.insertEntity(FOO_TYPE, properties));
			
			//Create Dashboard if needed
			if(Neo4JUtils.isEmpty(this.getEntity(ENTITY_TYPE_DASHBOARD))){
				Map<String, Object> dashboardProperties = new HashMap<String, Object>();
				dashboardProperties.put("Name", ENTITY_TYPE_DASHBOARD);
				dashboardProperties.put("isRoot", false);
				dashboardProperties.put("fieldList", "Name,groupBy,chartType,alertCheck,displayList,entity,alertValue");
				dashboardProperties.put("requiredFields", "Name,groupBy,chartType,displayList,entity");		
				this.insertEntity(ENTITY_TYPE_DASHBOARD, dashboardProperties);				
			}
			
			//Test inserting Bar Metadata Entity Type
			properties.put("Name", BAR_TYPE);
			properties.put("isRoot", false);
			properties.put("some property [~'()!]", "some value [~'()!]");
			properties.put("The string @foo-bar", "The+string+%C3%BC%40foo-bar @foo-bar");
			properties.put("The+string+%+C3%BC%++40foo-bar @foo-bar", "The+string+%+C3%BC%++40foo-bar @foo-bar");
			properties.put("(~)!'string' @foo-bar*The+string+%+C3%BC%++40foo-bar @foo-bar", "(~)!'string' @foo-bar*The+string+%+C3%BC%++40foo-bar @foo-bar");		
			this.getId(this.insertEntity(BAR_TYPE, properties));
			
			//Test updating / retrieving properties of Metadata Entity Type with spaces and special characters
			String value = (String)this.getEntityProperty(BAR_TYPE, "some property [~'()!]");
			assertTrue("some value [~'()!]".equals(value));		
			value = (String)this.getEntityProperty(BAR_TYPE, "The string @foo-bar");
			assertTrue("The+string+%C3%BC%40foo-bar @foo-bar".equals(value));
			
			value = (String)this.getEntityProperty(BAR_TYPE, "The+string+%+C3%BC%++40foo-bar @foo-bar");
			assertTrue("The+string+%+C3%BC%++40foo-bar @foo-bar".equals(value));
			
			value = (String)this.getEntityProperty(BAR_TYPE, "(~)!'string' @foo-bar*The+string+%+C3%BC%++40foo-bar @foo-bar");
			assertTrue("(~)!'string' @foo-bar*The+string+%+C3%BC%++40foo-bar @foo-bar".equals(value));
					
			//Note that by default Tomcat doesn't allow the / or \ characters in URLs, so if you want to enable this, you have to set this in your
			//<Tomcat Home>\bin\setEnv.bat (.sh) file:
			//
			// set CATALINA_OPTS=%CATALINA_OPTS% -Dorg.apache.tomcat.util.buf.UDecoder.ALLOW_ENCODED_SLASH=true -Dorg.apache.catalina.connector.CoyoteAdapter.ALLOW_BACKSLASH=true
			//
			this.updateEntityProperty(BAR_TYPE, "!()\\The 'string' @foo-bar*The+string+%C3%BC%40foo-bar @foo-bar", "!()\\The 'string' @foo-bar*The+string+%C3%BC%40foo-bar @foo-bar");
			value = (String)this.getEntityProperty(BAR_TYPE, "!()\\The 'string' @foo-bar*The+string+%C3%BC%40foo-bar @foo-bar");
			assertTrue("!()\\The 'string' @foo-bar*The+string+%C3%BC%40foo-bar @foo-bar".equals(value));
					
			//Test Creating Metadata Entity Relationships
			Map<String, Object> relProperties = new HashMap<String, Object>();
			relProperties.put("Name", "Foo Type has Bar Type");		
			relProperties.put("desc", "Foo Has Bar Relationship");
			relProperties.put("some property", "some value");
			relProperties.put("required", true);
			String relId1 = Neo4JUtils.getId(this.insertEntityRelationship(FOO_TYPE, "has a", BAR_TYPE, relProperties));
					
			//Test get relationship count
			assertTrue(1 == Integer.parseInt(this.getEntityRelationshipCount(FOO_TYPE, "all", "has a")));
			assertTrue(1 == Integer.parseInt(this.getEntityRelationshipCount(FOO_TYPE, "out", "has a")));
			assertTrue(1 == Integer.parseInt(this.getEntityRelationshipCount(BAR_TYPE, "in", "has a")));
			
			//Test get relationships by type
			List<String> relTypes = new ArrayList<String>();
			relTypes.add("has a");
			this.checkJSON(this.getEntityRelationships(FOO_TYPE, "all", relTypes));
			this.checkJSON(this.getEntityRelationships(BAR_TYPE, "all", relTypes));
			
			//Test properties
			this.updateEntityRelationshipProperty(relId1, "()!\\The 'string' @foo-bar*The+string+%C3%BC%40foo-bar @foo-bar", "()!\\The 'string' @foo-bar*The+string+%C3%BC%40foo-bar @foo-bar");
			value = (String)this.getEntityRelationshipProperty(relId1, "()!\\The 'string' @foo-bar*The+string+%C3%BC%40foo-bar @foo-bar");
			assertTrue("()!\\The 'string' @foo-bar*The+string+%C3%BC%40foo-bar @foo-bar".equals(value));
			
			//Test get relationships by id
			this.checkJSON(this.getEntityRelationship(relId1));
			
			//Test get relationships by direction and type
			relTypes = new ArrayList<String>();
			this.checkJSON(this.getEntityRelationships(FOO_TYPE, "out", null));
			this.checkJSON(this.getEntityRelationships(BAR_TYPE, "in", null));
			
			relTypes.add("has a");
			this.checkJSON(this.getEntityRelationships(FOO_TYPE, "all", relTypes));
			this.checkJSON(this.getEntityRelationships(BAR_TYPE, "in", relTypes));
			this.checkJSON(this.getEntityRelationships(FOO_TYPE, "out", relTypes));
					
			//Test get Relationship Types
			//List<String> relationshipTypes = (List<String>)Neo4JUtils.getValue(this.getEntityRelationshipTypes());
			String relationshipTypes = this.getEntityRelationshipTypes();
			assertTrue(relationshipTypes.contains("has a"));
			
			//Test getting properties
			assertTrue("some value".equals(this.getEntityProperty(FOO_TYPE, "some property")));
			Map<String, Object> fooTypeProps = this.getEntityProperties(FOO_TYPE);
			assertTrue("some value".equals(fooTypeProps.get("some property")));
			
			//Test updating properties
			this.updateEntityProperty(FOO_TYPE, "some property", "some new value");
			assertTrue("some new value".equals(this.getEntityProperty(FOO_TYPE, "some property")));
			
			//Test deleteing properties
			this.deleteEntityProperty(FOO_TYPE, "some property");
			fooTypeProps = this.getEntityProperties(FOO_TYPE);
			assertTrue(!fooTypeProps.containsKey("some property"));
		} catch(Throwable t){
			t.printStackTrace();
			System.out.println("ERROR: "+t.getMessage());
			throw t;
		}
	}
	
	@After
	public void cleanup() throws Exception{		
		System.out.println("Verifying Entity cleanup tasks committed...");
		String result = "{\"test\": \"test\"}";
				
		this.deleteEntityAndRelationships(ENTITY_TYPE_FOO);		
		this.deleteEntityAndRelationships(ENTITY_TYPE_BAR);		
		this.deleteEntityAndRelationships(ENTITY_TYPE_BAM);
		this.deleteEntityAndRelationships(FOO_TYPE);		
		this.deleteEntityAndRelationships(BAR_TYPE);
		this.deleteEntityAndRelationships(ENTITY_TYPE_DASHBOARD);
		
		System.out.println("Verifying Entity cleanup tasks committed...");
		result = "{\"test\": \"test\"}";
		while(!Neo4JUtils.isEmpty(result)){
			result = this.getEntity(ENTITY_TYPE_DASHBOARD);
		}
		result = "{\"test\": \"test\"}";
		while(!Neo4JUtils.isEmpty(result)){
			result = this.getEntity(ENTITY_TYPE_BAM);
		}
		result = "{\"test\": \"test\"}";
		while(!Neo4JUtils.isEmpty(result)){
			result = this.getEntity(ENTITY_TYPE_BAR);
		}
		result = "{\"test\": \"test\"}";
		while(!Neo4JUtils.isEmpty(result)){
			result = this.getEntity(ENTITY_TYPE_FOO);
		}
		result = "{\"test\": \"test\"}";
		while(!Neo4JUtils.isEmpty(result)){
			result = this.getEntity(BAR_TYPE);
		}
		result = "{\"test\": \"test\"}";
		while(!Neo4JUtils.isEmpty(result)){
			result = this.getEntity(FOO_TYPE);
		}
		
		assertTrue(Neo4JUtils.isEmpty(this.getEntity(ENTITY_TYPE_FOO)));
		assertTrue(Neo4JUtils.isEmpty(this.getEntity(ENTITY_TYPE_BAR)));
		assertTrue(Neo4JUtils.isEmpty(this.getEntity(ENTITY_TYPE_BAM)));
		assertTrue(Neo4JUtils.isEmpty(this.getEntity(FOO_TYPE)));
		assertTrue(Neo4JUtils.isEmpty(this.getEntity(BAR_TYPE)));		
	}
	
}
