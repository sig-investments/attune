package com.bah.attune.rest.services.data;

import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNull;
import static org.junit.Assert.assertTrue;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.JSONValue;
import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.springframework.http.HttpStatus;

import com.bah.attune.rest.Neo4JUtils;

import com.bah.attune.rest.services.BaseDataServiceIntegrationTest;

public class DataServiceIntegrationTest extends BaseDataServiceIntegrationTest {
	
	String metadataNodeJson = null;
	
	public DataServiceIntegrationTest() {
		super();
	}
	
	@Before
	public void setup() throws Exception{		
		Map<String, Object> properties = new HashMap<String, Object>();
		//Foo Metadata Entity Type
		properties.put("Name", "Foo");		
		if(this.doesRootExist()){
			properties.put("isRoot", false);
		} else {
			properties.put("isRoot", true);
		}
		properties.put("fieldList", "Name,desc,booleanField,intField,doubleField,shortField,longField,floatField");
		properties.put("requiredFields", "Name,desc");
		try{
			metadataNodeJson = this.insertEntity("Foo", properties);
		} catch(Exception e){
			properties.put("isRoot", true);
			metadataNodeJson = this.insertEntity("Foo", properties);
		}
		
		//Bar Metadata Entity Type
		properties.put("Name", "Bar");
		properties.put("isRoot", false);
		this.insertEntity("Bar", properties);
		
		//BAM Metadata Entity Type
		properties.put("Name", "BAM");
		properties.put("isRoot", false);
		this.insertEntity("BAM", properties);
		
		//Create Metadata Entity Relationships
		Map<String, Object> relProperties = new HashMap<String, Object>();
		relProperties.put("Name", "Foo-has-Bar");		
		relProperties.put("desc", "Foo Has Bar Relationship");
		relProperties.put("booleanField", false);
		relProperties.put("intField", (int)30);
		relProperties.put("doubleField", (double)3.001);
		relProperties.put("shortField", (short)3.1);
		relProperties.put("longField", (long)30001123);
		relProperties.put("floatField", (float)3.0001123);
		relProperties.put("required", true);
		this.insertEntityRelationship("Foo", "has", "Bar", relProperties);
		this.insertEntityRelationship("Foo", "has a", "Bar", relProperties);
		
		relProperties.put("Name", "Bar-likes-BAM");		
		relProperties.put("desc", "Bar Likes BAM Relationship");
		relProperties.put("required", false);
		this.insertEntityRelationship("Bar", "likes", "BAM", relProperties);
		
		//Create Dashboard if needed
		if(Neo4JUtils.isEmpty(this.getEntity("Dashboard"))){
			Map<String, Object> dashboardProperties = new HashMap<String, Object>();
			dashboardProperties.put("Name", "Dashboard");
			dashboardProperties.put("isRoot", false);
			dashboardProperties.put("fieldList", "Name,groupBy,chartType,alertCheck,displayList,entity,alertValue");
			dashboardProperties.put("requiredFields", "Name,groupBy,chartType,displayList,entity");		
			this.insertEntity("Dashboard", dashboardProperties);				
		}
		
		if(Neo4JUtils.isEmpty(this.getNodesByType("Dashboard"))){
			Map<String, Object> dashboardProperties = new HashMap<String, Object>();
			dashboardProperties.put("Name", "entity1");
			dashboardProperties.put("groupBy", "Name");
			dashboardProperties.put("chartType", "Horizontal Bar Chart");
			dashboardProperties.put("displayList", "*");
			dashboardProperties.put("entity", "FooService");
			this.insertNode("Dashboard", dashboardProperties);				
		}
		
		//Clean up any old data
		this.deleteNodesAndRelationshipsByType("Foo");
		this.deleteNodesAndRelationshipsByType("Bar");
		this.deleteNodesAndRelationshipsByType("BAM");
	}
	
	@After
	public void cleanup() throws Exception{
		this.deleteNodesAndRelationshipsByType("Dashboard");
		this.deleteNodesAndRelationshipsByType("BAM");
		this.deleteNodesAndRelationshipsByType("Bar");		
		this.deleteNodesAndRelationshipsByType("Foo");
		
		System.out.println("Verifying Node cleanup tasks committed...");
		String result = "{\"test\": \"test\"}";
		while(!Neo4JUtils.isEmpty(result)){
			result = this.getNodesByType("Dashboard");
		}
		result = "{\"test\": \"test\"}";
		while(!Neo4JUtils.isEmpty(result)){
			result = this.getNodesByType("BAM");
		}
		result = "{\"test\": \"test\"}";
		while(!Neo4JUtils.isEmpty(result)){
			result = this.getNodesByType("Bar");
		}
		result = "{\"test\": \"test\"}";
		while(!Neo4JUtils.isEmpty(result)){
			result = this.getNodesByType("Foo");
		}
		
		this.deleteEntityAndRelationships("Dashboard");
		this.deleteEntityAndRelationships("BAM");
		this.deleteEntityAndRelationships("Bar");
		this.deleteEntityAndRelationships("Foo");		
						
		System.out.println("Verifying Entity cleanup tasks committed...");
		result = "{\"test\": \"test\"}";
		while(!Neo4JUtils.isEmpty(result)){
			result = this.getEntity("Dashboard");
		}
		result = "{\"test\": \"test\"}";
		while(!Neo4JUtils.isEmpty(result)){
			result = this.getEntity("BAM");
		}
		result = "{\"test\": \"test\"}";
		while(!Neo4JUtils.isEmpty(result)){
			result = this.getEntity("Bar");
		}
		result = "{\"test\": \"test\"}";
		while(!Neo4JUtils.isEmpty(result)){
			result = this.getEntity("Foo");
		}
		
		assertTrue(Neo4JUtils.isEmpty(this.getEntity("Foo")));
		assertTrue(Neo4JUtils.isEmpty(this.getEntity("Bar")));
		assertTrue(Neo4JUtils.isEmpty(this.getEntity("BAM")));
	}
	
	@Test
	public void testNodes() throws Exception{
		try{
			//Test inserting a new Node
			boolean hasRoot = doesRootExist();
			Map<String, Object> properties = new HashMap<String, Object>();
			properties.put("Name", "Foo Name");		
			properties.put("desc", "Some New Entity");
			properties.put("booleanField", false);
			properties.put("intField", (int)30);
			properties.put("doubleField", (double)3.001);
			properties.put("shortField", (short)3.1);
			properties.put("longField", (long)30001123);
			properties.put("floatField", (float)3.0001123);
			
			String jsonResult = this.insertNode("Foo", properties);
			this.checkJSON(jsonResult);
			String nodeId = this.getId(jsonResult);
			
			//Test retrieving node properties
			assertTrue("Foo Name".equals(this.getNodeProperty(nodeId, "Name")));
			assertTrue("Some New Entity".equals(this.getNodeProperty(nodeId, "desc")));
			assertTrue(Boolean.FALSE.equals(this.getNodeProperty(nodeId, "booleanField")));
			assertTrue(new Integer(30).equals(new Integer(((Number)this.getNodeProperty(nodeId, "intField")).intValue())));
			assertTrue(new Double(3.001).equals(new Double(((Number)this.getNodeProperty(nodeId, "doubleField")).doubleValue())));
			assertTrue(new Short((short)3.1).equals(new Short(((Number)this.getNodeProperty(nodeId, "shortField")).shortValue())));
			assertTrue(new Long(30001123).equals(new Long(((Number)this.getNodeProperty(nodeId, "longField")).longValue())));
			assertTrue(new Float(3.0001123).equals(new Float(((Number)this.getNodeProperty(nodeId, "floatField")).floatValue())));
			
			Map<String, Object> p = this.getNodeProperties(nodeId);
			assertTrue(properties.keySet().containsAll(p.keySet()));
			for(String key:properties.keySet()){
				assertTrue(key+" is not equal: "+properties.get(key)+":"+p.get(key), JSONValue.toJSONString(properties.get(key)).equals(JSONValue.toJSONString(p.get(key))));
			}
			
			//Insert second Node
			properties = new HashMap<String, Object>();
			properties.put("Name", "Foo2 Name");		
			properties.put("desc", "Some Newer Entity");
			properties.put("booleanField", false);
			properties.put("intField", (int)30);
			properties.put("doubleField", (double)3.001);
			properties.put("shortField", (short)3.1);
			properties.put("longField", (long)30001123);
			properties.put("floatField", (float)3.0001123);
			String node2Json = this.insertNode("Foo", properties);
			String node2Id = this.getId(node2Json);
			
			JSONArray jsonArray = null;
			
			//Test getting nodes by type
			String jsonNodes = this.getNodesByType("Foo");
			Object jsonObj = parser.parse(jsonNodes);
			if(jsonObj instanceof JSONArray){
				jsonArray = (JSONArray)jsonObj;
				assertTrue(jsonArray.size() == 2);
				for(int i = 0; i < jsonArray.size(); i++){				
					JSONObject jsonObject = (JSONObject) jsonArray.get(i);
					String nextNodeId = this.getId(jsonObject.toJSONString());
					//Test retrieving a single Node by Id
					String json = this.getNode(nextNodeId);
					this.checkJSON(json);
				}
			}
			
			//Test get all node types
			List<String> types = getAllNodeTypes();
			assertTrue(types.contains("Foo"));
			
			//Test get nodes by type and property
			String json = this.getNodes("Foo", "desc", "Some Newer Entity");
			this.checkJSON(json);
			assertTrue("Some Newer Entity".equals(this.getNodePropertyValue(json, "desc")));
			assertTrue(node2Id.equals(this.getId(json)));
			
			
			//Test update node property
			this.updateNodeProperty(nodeId, "desc", "New Description");
			this.updateNodeProperty(nodeId, "booleanField", Boolean.TRUE);
			this.updateNodeProperty(nodeId, "intField", new Integer(40));
			this.updateNodeProperty(nodeId, "doubleField", new Double(4.001));
			this.updateNodeProperty(nodeId, "shortField", new Short((short)4.1));
			this.updateNodeProperty(nodeId, "longField", new Long(40001123));
			this.updateNodeProperty(nodeId, "floatField", new Float(4.0001123));
			this.updateNodeProperty(nodeId, "newField", "newValue");
			assertTrue("Foo Name".equals(this.getNodeProperty(nodeId, "Name")));
			assertTrue("New Description".equals(this.getNodeProperty(nodeId, "desc")));
			assertTrue(Boolean.TRUE.equals(this.getNodeProperty(nodeId, "booleanField")));
			assertTrue(new Integer(40).equals(new Integer(((Number)this.getNodeProperty(nodeId, "intField")).intValue())));
			assertTrue(new Double(4.001).equals(new Double(((Number)this.getNodeProperty(nodeId, "doubleField")).doubleValue())));
			assertTrue(new Short((short)4.1).equals(new Short(((Number)this.getNodeProperty(nodeId, "shortField")).shortValue())));
			assertTrue(new Long(40001123).equals(new Long(((Number)this.getNodeProperty(nodeId, "longField")).longValue())));
			assertTrue(new Float(4.0001123).equals(new Float(((Number)this.getNodeProperty(nodeId, "floatField")).floatValue())));
			assertTrue("newValue".equals(this.getNodeProperty(nodeId, "newField")));
			
			//Test update node properties
			properties = new HashMap<String, Object>();
			properties.put("Name", "Foo Name");		
			properties.put("desc", "Updated Entity");
			properties.put("booleanField2", false);
			properties.put("intField2", (int)30);
			properties.put("doubleField2", (double)3.001);
			properties.put("shortField2", (short)3.1);
			properties.put("longField2", (long)30001123);
			properties.put("floatField2", (float)3.0001123);
			this.updateNodeProperties(nodeId, properties);
			
			Map<String, Object> props = this.getNodeProperties(nodeId);
			assertTrue(properties.keySet().containsAll(props.keySet()));
			for(String key:properties.keySet()){
				assertTrue(key+" is not equal: "+properties.get(key)+":"+p.get(key), JSONValue.toJSONString(properties.get(key)).equals(JSONValue.toJSONString(props.get(key))));
			}
			
			//Test Deleting a property
			Set<String> keptProperties = new HashSet<String>();
			keptProperties.add("Name");
			keptProperties.add("desc");
			keptProperties.add("booleanField2");
			
			Set<String> removedProperties = new HashSet<String>();		
			removedProperties.add("intField2");
			removedProperties.add("doubleField2");
			removedProperties.add("shortField2");
			removedProperties.add("longField2");
			removedProperties.add("floatField2");
			
			for(String key:removedProperties){
				this.deleteNodeProperty(nodeId, key);
			}
					
			props = this.getNodeProperties(nodeId);
			for(String key:keptProperties){
				assertTrue(props.get(key) != null);
			}
			
			//Test Delete all properties
			this.updateNodeProperty(nodeId, "newProperty", "New Test Property");
			this.deleteNodeProperties(nodeId);
			props = this.getNodeProperties(nodeId);
			assertNull(props.get("newProperty"));
			assertTrue(props.containsKey("Name"));
			assertTrue(props.containsKey("desc"));
			
			//Test delete Node
			jsonNodes = this.getNodesByType("Foo");
			jsonObj = parser.parse(jsonNodes);
			if(jsonObj instanceof JSONArray){
				jsonArray = (JSONArray)jsonObj;
				for(int i = 0; i < jsonArray.size(); i++){				
					JSONObject jsonObject = (JSONObject) jsonArray.get(i);
					nodeId = this.getId(jsonObject.toJSONString());
					this.deleteNode(nodeId);
				}
			}
			
			jsonNodes = this.getNodesByType("Foo");
			jsonObj = parser.parse(jsonNodes);
			if(jsonObj instanceof JSONArray){
				jsonArray = (JSONArray)jsonObj;
				assertTrue(jsonArray.isEmpty());
			}
		} catch(Throwable t){
			t.printStackTrace();
			System.out.println("ERROR: "+t.getMessage());
			throw t;
		}
	}
	
	@Test
	public void testNodeValidation() throws Exception {
		try{
			//Test insert new Nodes with out required fields
			boolean hasRoot = doesRootExist();
			Map<String, Object> properties = new HashMap<String, Object>();
			properties.put("Name", "Foo Name");		
			properties.put("desc", "Some New Entity");
			properties.put("booleanField", false);
			properties.put("intField", (int)30);
			properties.put("doubleField", (double)3.001);
			properties.put("shortField", (short)3.1);
			properties.put("longField", (long)30001123);
			properties.put("floatField", (float)3.0001123);
			if(!hasRoot){
				properties.put("isRoot", true);
			}
			
			String[] missingFields = new String[]{"Name", "desc"};
			for(String missingField:missingFields){
				Map<String, Object> props = new HashMap<String, Object>(properties);
				props.remove(missingField);			
				try{				
					this.insertNode("Foo", props);
					Assert.fail("Exception should have been thrown here!");
				} catch(Exception e){
					StringBuilder sb = new StringBuilder("Field ").append(missingField).append(" is required!");
					String msg = e.getMessage();
					assertNotNull(msg);
					assertTrue(msg.contains(sb.toString()));
				}			
			}
			
			//Test updating an existing node's properties without including the required ones
			String nodeId = this.getId(this.insertNode("Foo", properties));
			try{						
				for(String missingField:missingFields){
					Map<String, Object> props = new HashMap<String, Object>();
					props.put("Name", "Foo Name");		
					props.put("desc", "Some New Entity");
					props.put("booleanField", true);
					props.put("intField", (int)50);
					props.put("doubleField", (double)2.05);
					props.put("longField", (long)999999);
					props.put("floatField", (float)8.0762987);
					props.remove(missingField);
					try{
						this.updateNodeProperties(nodeId, props);
						Assert.fail("Exception should have been thrown here!");
					} catch(Exception e){
						StringBuilder sb = new StringBuilder("Property ").append(missingField).append(" is required!");
						String msg = e.getMessage();
						assertNotNull(msg);
						assertTrue(msg.contains(sb.toString()));
					}
				}
			} catch(Exception e){
				throw e;
			}
			
			//Test updating a property with a null value
			try{
				this.updateNodeProperty(nodeId, "booleanField", null);
				Assert.fail("Exception should have been thrown here!");
			} catch(Exception e){
				//StringBuilder sb = new StringBuilder("Property values can not be null!");
				StringBuilder sb = new StringBuilder(HttpStatus.BAD_REQUEST.value());
				String msg = e.getMessage();
				assertNotNull(msg);
				assertTrue(msg.contains(sb.toString()));
			}
			
			//Test deleting a required property
			try{
				this.deleteNodeProperty(nodeId, "Name");
				Assert.fail("Exception should have been thrown here!");
			} catch(Exception e){
				StringBuilder sb = new StringBuilder("Property Name is required!");
				String msg = e.getMessage();
				assertNotNull(msg);
				assertTrue(msg.contains(sb.toString()));
			}
					
			//Test using the data API to add metadata		
			try{
				Map<String, Object> props = new HashMap<String, Object>(properties);
				props.put("Name", "Metadata");
				this.insertNode("Metadata", props);
				Assert.fail("Exception should have been thrown here!");
			} catch(Exception e){
				StringBuilder sb = new StringBuilder("Can not modify Metadata through this API.");
				String msg = e.getMessage();
				assertNotNull(msg);
				assertTrue(msg.toString().contains(sb.toString()));
			}
			
			//Test using the data API to delete metadata		
			try{
				String metadataNodeId = Neo4JUtils.getId(metadataNodeJson);
				this.deleteNode(metadataNodeId);
				Assert.fail("Exception should have been thrown here!");
			} catch(Exception e){
				StringBuilder sb = new StringBuilder("Can not modify Metadata through this API.");
				String msg = e.getMessage();
				assertNotNull(msg);
				assertTrue(msg.toString().contains(sb.toString()));
			}
			
			//Test using the data API to update metadata properties		
			try{
				String metadataNodeId = Neo4JUtils.getId(metadataNodeJson);
				Map<String, Object> metadataProps = Neo4JUtils.getDataProperties(metadataNodeJson);
				this.updateNodeProperties(metadataNodeId, metadataProps);
				Assert.fail("Exception should have been thrown here!");
			} catch(Exception e){
				StringBuilder sb = new StringBuilder("Can not modify Metadata through this API.");
				String msg = e.getMessage();
				assertNotNull(msg);
				assertTrue(msg.toString().contains(sb.toString()));
			}
			
			//Test using the data API to delete metadata properties		
			try{
				String metadataNodeId = Neo4JUtils.getId(metadataNodeJson);
				this.deleteNodeProperties(metadataNodeId);
				Assert.fail("Exception should have been thrown here!");
			} catch(Exception e){
				StringBuilder sb = new StringBuilder("Can not modify Metadata through this API.");
				String msg = e.getMessage();
				assertNotNull(msg);
				assertTrue(msg.toString().contains(sb.toString()));
			}
			
			//Test using the data API to update metadata property		
			try{
				String metadataNodeId = Neo4JUtils.getId(metadataNodeJson);
				this.updateNodeProperty(metadataNodeId, "newProp", "newValue");
				Assert.fail("Exception should have been thrown here!");
			} catch(Exception e){
				StringBuilder sb = new StringBuilder("Can not modify Metadata through this API.");
				String msg = e.getMessage();
				assertNotNull(msg);
				assertTrue(msg.toString().contains(sb.toString()));
			}
			
			//Test using the data API to delete metadata property		
			try{
				String metadataNodeId = Neo4JUtils.getId(metadataNodeJson);
				this.deleteNodeProperty(metadataNodeId, "newProp");
				Assert.fail("Exception should have been thrown here!");
			} catch(Exception e){
				StringBuilder sb = new StringBuilder("Can not modify Metadata through this API.");
				String msg = e.getMessage();
				assertNotNull(msg);
				assertTrue(msg.toString().contains(sb.toString()));
			}
			
			//Test adding a node before adding it's associated metadata
			try{
				this.insertNode("Zing", properties);
				Assert.fail("Exception should have been thrown here!");
			} catch(Exception e){
				StringBuilder sb = new StringBuilder("Metadata for entity of type Zing does not exist!");
				String msg = e.getMessage();
				assertNotNull(msg);
				assertTrue(msg.contains(sb.toString()));
			}
			
			//Test adding a relationship that's not in the associated metadata
			String barNodeId = null;
			try{			
				Map<String, Object> barProperties = new HashMap<String, Object>();
				barProperties.put("Name", "Bar Name");		
				barProperties.put("desc", "New Bar Entity");
				barProperties.put("booleanField", false);
				barProperties.put("intField", (int)10);
				barProperties.put("doubleField", (double)20.01);
				if(!hasRoot){
					barProperties.put("isRoot", true);
				}
				barNodeId = Neo4JUtils.getId(this.insertNode("Bar", barProperties));
				
				//First delete the metadata relationship between Foo and Bar
				List<String> relTypes = new ArrayList<String>();
				relTypes.add("has");
				String relId = Neo4JUtils.getId(this.getEntityRelationships("Foo", "out", relTypes));
				this.deleteEntityRelationship(relId);
				
				String startNodeId = nodeId; //Foo
				String endNodeId = barNodeId; //BAM
				this.insertNodeRelationship(startNodeId, "has", endNodeId, null);		
				Assert.fail("Exception should have been thrown here!");
			} catch(Exception e){
				StringBuilder sb = new StringBuilder("Metadata for entity of type Foo does not define relationship of type 'has' to entity of type Bar");
				String msg = e.getMessage();
				assertNotNull(msg);
				assertTrue(msg.contains(sb.toString()));
			}
			
			try{
				Map<String, Object> bamProperties = new HashMap<String, Object>();
				bamProperties.put("Name", "BAM Name");		
				bamProperties.put("desc", "New Bar Entity");
				bamProperties.put("booleanField", false);
				bamProperties.put("intField", (int)10);
				bamProperties.put("doubleField", (double)20.01);
				if(!hasRoot){
					bamProperties.put("isRoot", true);
				}
				String bamNodeId = Neo4JUtils.getId(this.insertNode("BAM", bamProperties));
				
				//First delete the metadata relationship between Bar and BAM
				List<String> relTypes = new ArrayList<String>();
				relTypes.add("likes");
				String relId = Neo4JUtils.getId(this.getEntityRelationships("BAM", "in", relTypes));
				this.deleteEntityRelationship(relId);
				
				String startNodeId = barNodeId; //Bar
				String endNodeId = bamNodeId; //BAM
				this.insertNodeRelationship(startNodeId, "likes", endNodeId, null);		
				Assert.fail("Exception should have been thrown here!");
			} catch(Exception e){
				StringBuilder sb = new StringBuilder("Metadata for entity of type Bar does not define relationship of type 'likes' to entity of type BAM");
				String msg = e.getMessage();
				assertNotNull(msg);
				assertTrue(msg.contains(sb.toString()));
			}
			
			//Test Node validation					
			//Make sure Metadata Entity Relationships exist
			Map<String, Object> relProperties = new HashMap<String, Object>();
			relProperties.put("Name", "Foo-has-Bar");		
			relProperties.put("desc", "Foo Has Bar Relationship");
			relProperties.put("booleanField", false);
			relProperties.put("intField", (int)30);
			relProperties.put("doubleField", (double)3.001);
			relProperties.put("shortField", (short)3.1);
			relProperties.put("longField", (long)30001123);
			relProperties.put("floatField", (float)3.0001123);
			relProperties.put("required", true);
			this.insertEntityRelationship("Foo", "has", "Bar", relProperties);
			
			relProperties.put("Name", "Bar-likes-BAM");		
			relProperties.put("desc", "Bar Likes BAM Relationship");
			relProperties.put("required", false);
			this.insertEntityRelationship("Bar", "likes", "BAM", relProperties);
			
			Map<String, Object> fooProperties = new HashMap<String, Object>();
			fooProperties.put("Name", "Foo Test Name");		
			fooProperties.put("desc", "Test Foo Instance");
			if(!hasRoot){
				properties.put("isRoot", true);
			}
			//Create an instance of Foo with out required relationship to Bar, and then add new required field before we validate 
			this.insertNode("Foo", fooProperties);
			
			Map<String, Object> fooMetadataProps = this.getEntityProperties("Foo");
			fooMetadataProps.put("fieldList", "Name,desc,booleanField,intField,doubleField,shortField,longField,floatField,testField");
			fooMetadataProps.put("requiredFields", "Name,desc,testField");
			this.updateEntityProperties("Foo", fooMetadataProps);
					
			String reportJson = this.validateNodes();
			JSONObject report = (JSONObject)parser.parse(reportJson);
			
	//		File tmp = new File("C:\\eclipseWorkspaces\\attune-rest-service\\target\\validation.json");
	//		if(tmp.exists()){
	//			tmp.delete();
	//		}
	//		tmp.createNewFile();
	//		FileOutputStream fos = new FileOutputStream(tmp);
	//		fos.write(reportJson.getBytes());
	//		fos.close();
			
			assertTrue(((Long)report.get("totalErrors")).intValue() > 0);
			assertTrue(((Long)report.get("totalInvalidNodes")).intValue() > 0);
			assertTrue(((Long)report.get("totalInvalidRelationships")).intValue() > 0);
			assertTrue(((Long)report.get("totalInvalidOutRelationships")).intValue() > 0);
			assertTrue(((Long)report.get("totalInvalidInRelationships")).intValue() > 0);
			
			JSONArray errorDetails = (JSONArray)report.get("errorDetails");
			assertTrue(errorDetails.size() > 0);
			assertTrue(errorDetails.toJSONString().contains("Field testField is required!"));
		} catch(Throwable t){
			t.printStackTrace();
			System.out.println("ERROR: "+t.getMessage());
			throw t;
		}
	}
	
	@SuppressWarnings("unchecked")
	@Test
	public void testRelationships() throws Exception {
		try{
			//Insert new test Nodes
			boolean hasRoot = doesRootExist();
			
			Map<String, Object> properties = new HashMap<String, Object>();
			//Foo Metadata Entity Type
			properties.put("Name", "Foo");
			properties.put("isRoot", true);
			if(hasRoot){
				properties.put("isRoot", false);
			} else {
				properties.put("isRoot", true);
			}
			properties.put("fieldList", "Name,desc,booleanField,intField,doubleField,shortField,longField,floatField");
			properties.put("requiredFields", "Name,desc");
			this.insertEntity("Foo", properties);
			
			assertTrue(!Neo4JUtils.isEmpty(this.getEntity("Foo")));
			
			Map<String, Object> node1Properties = new HashMap<String, Object>();
			node1Properties.put("Name", "Foo Name");		
			node1Properties.put("desc", "Some New Entity");
			node1Properties.put("booleanField", false);
			node1Properties.put("intField", (int)30);
			node1Properties.put("doubleField", (double)3.001);
			node1Properties.put("shortField", (short)3.1);
			node1Properties.put("longField", (long)30001123);
			node1Properties.put("floatField", (float)3.0001123);
			if(!hasRoot){
				node1Properties.put("isRoot", true);
			}
			String node1JsonResult = this.insertNode("Foo", node1Properties);
			this.checkJSON(node1JsonResult);
			String node1Id = this.getId(node1JsonResult);
			
			Map<String, Object> node2Properties = new HashMap<String, Object>();
			node2Properties.put("Name", "Bar Name");		
			node2Properties.put("desc", "Some New Entity");
			node2Properties.put("booleanField", false);
			node2Properties.put("intField", (int)30);
			node2Properties.put("doubleField", (double)3.001);
			node2Properties.put("shortField", (short)3.1);
			node2Properties.put("longField", (long)30001123);
			node2Properties.put("floatField", (float)3.0001123);
			String node2JsonResult = this.insertNode("Bar", node2Properties);
			this.checkJSON(node2JsonResult);
			String node2Id = this.getId(node2JsonResult);
			
			Map<String, Object> node3Properties = new HashMap<String, Object>();
			node3Properties.put("Name", "BAM Name");		
			node3Properties.put("desc", "Some New Entity");
			node3Properties.put("booleanField", false);
			node3Properties.put("intField", (int)30);
			node3Properties.put("doubleField", (double)3.001);
			node3Properties.put("shortField", (short)3.1);
			node3Properties.put("longField", (long)30001123);
			node3Properties.put("floatField", (float)3.0001123);
			String node3JsonResult = this.insertNode("BAM", node3Properties);
			this.checkJSON(node3JsonResult);
			String node3Id = this.getId(node3JsonResult);
			
			Map<String, Object> node4Properties = new HashMap<String, Object>();
			node4Properties.put("Name", "Another Bar Name");		
			node4Properties.put("desc", "Another Bar Entity");
			String node4JsonResult = this.insertNode("Bar", node4Properties);
			this.checkJSON(node4JsonResult);
			String node4Id = this.getId(node4JsonResult);
			
			//Create relationships
			Map<String, Object> relProperties = new HashMap<String, Object>();
			relProperties.put("Name", "Rel Name");		
			relProperties.put("desc", "New Relationship");
			relProperties.put("booleanField", false);
			relProperties.put("intField", (int)30);
			relProperties.put("doubleField", (double)3.001);
			relProperties.put("shortField", (short)3.1);
			relProperties.put("longField", (long)30001123);
			relProperties.put("floatField", (float)3.0001123);
			
			String rel1to2Json = this.insertNodeRelationship(node1Id, "has", node2Id, relProperties);
			this.checkJSON(rel1to2Json);
			String rel1to2Id = this.getId(rel1to2Json);
			
			String rel2to3Json = this.insertNodeRelationship(node2Id, "likes", node3Id, null);
			this.checkJSON(rel2to3Json);
			String rel2to3Id = this.getId(rel2to3Json);
			
			String rel1to4Json = this.insertNodeRelationship(node1Id, "has", node4Id, null);
			this.checkJSON(rel1to4Json);
			String rel1to4Id = this.getId(rel1to4Json);
			
			//Test get relationship count
			assertTrue(2 == Integer.parseInt(this.getNodeRelationshipCount(node1Id, "all", "has")));
			assertTrue(0 == Integer.parseInt(this.getNodeRelationshipCount(node1Id, "all", "likes")));
			assertTrue(1 == Integer.parseInt(this.getNodeRelationshipCount(node2Id, "all", "likes")));
			assertTrue(1 == Integer.parseInt(this.getNodeRelationshipCount(node2Id, "in", (String)null)));
			assertTrue(1 == Integer.parseInt(this.getNodeRelationshipCount(node2Id, "out", (String)null)));		
			assertTrue(0 == Integer.parseInt(this.getNodeRelationshipCount(node3Id, "all", "has")));
			assertTrue(1 == Integer.parseInt(this.getNodeRelationshipCount(node3Id, "all", (String)null)));
			assertTrue(1 == Integer.parseInt(this.getNodeRelationshipCount(node3Id, "all", "likes")));
			assertTrue(0 == Integer.parseInt(this.getNodeRelationshipCount(node4Id, "all", "likes")));
			assertTrue(1 == Integer.parseInt(this.getNodeRelationshipCount(node4Id, "in", "has")));
			assertTrue(1 == Integer.parseInt(this.getNodeRelationshipCount(node4Id, "all", (String)null)));
			
			//Test get relationships by type
			this.checkJSON(this.getNodeRelationshipsByType(node1Id, "all", "has"));
			this.checkJSON(this.getNodeRelationshipsByType(node2Id, "all", "likes"));
			
			//Test get relationships by id
			this.checkJSON(this.getNodeRelationship(rel1to2Id));
			this.checkJSON(this.getNodeRelationship(rel2to3Id));
			this.checkJSON(this.getNodeRelationship(rel1to4Id));
			
			//Test get relationships by direction and type
			List<String> relTypes = new ArrayList<String>();
			this.checkJSON(this.getNodeRelationships(node1Id, "all", null));
			this.checkJSON(this.getNodeRelationships(node2Id, "in", null));
			this.checkJSON(this.getNodeRelationships(node1Id, "out", null));
			
			relTypes.add("has");
			this.checkJSON(this.getNodeRelationships(node1Id, "all", relTypes));
			this.checkJSON(this.getNodeRelationships(node4Id, "in", relTypes));
			this.checkJSON(this.getNodeRelationships(node1Id, "out", relTypes));
			
			relTypes.add("likes");
			this.checkJSON(this.getNodeRelationships(node2Id, "all", relTypes));
			this.checkJSON(this.getNodeRelationships(node3Id, "in", relTypes));
			this.checkJSON(this.getNodeRelationships(node2Id, "out", relTypes));
			
			//Test get Relationship Types
			List<String> relationshipTypes = (List<String>)Neo4JUtils.getValue(this.getNodeRelationshipTypes());
			assertTrue(relationshipTypes.contains("has"));
			assertTrue(relationshipTypes.contains("likes"));
			
			//Test Deleting a property
			Set<String> keptProperties = new HashSet<String>();
			keptProperties.add("Name");
			keptProperties.add("desc");
			keptProperties.add("booleanField");
			
			Set<String> removedProperties = new HashSet<String>();		
			removedProperties.add("intField");
			removedProperties.add("doubleField");
			removedProperties.add("shortField");
			removedProperties.add("longField");
			removedProperties.add("floatField");
			
			for(String key:removedProperties){
				this.deleteNodeRelationshipProperty(rel1to2Id, key);
			}
					
			Map<String, Object> props = this.getNodeRelationshipProperties(rel1to2Id);
			for(String key:keptProperties){
				assertTrue(props.get(key) != null);
			}
			
			//Test Delete all properties
			this.deleteNodeRelationshipProperties(rel1to2Id);
			props = this.getNodeRelationshipProperties(rel1to2Id);
			assertTrue(props.isEmpty());
			
			//Test Delete relationship
			this.deleteNodeRelationship(rel1to2Id);
			this.deleteNodeRelationship(rel2to3Id);
			assertTrue(Neo4JUtils.isEmpty(this.getNodeRelationships(node2Id, "all", null)));
			this.deleteNodeRelationshipsByType(node1Id, "all", "has");
			assertTrue(Neo4JUtils.isEmpty(this.getNodeRelationshipsByType(node1Id, "all", "has")));
			assertTrue(Neo4JUtils.isEmpty(this.getNodeRelationships(node1Id, "all", null)));
			assertTrue(Neo4JUtils.isEmpty(this.getNodeRelationships(node2Id, "all", null)));
			assertTrue(Neo4JUtils.isEmpty(this.getNodeRelationships(node3Id, "all", null)));
			assertTrue(Neo4JUtils.isEmpty(this.getNodeRelationships(node4Id, "all", null)));
		} catch(Throwable t){
			t.printStackTrace();
			System.out.println("ERROR: "+t.getMessage());
			throw t;
		}
	}
	
	@Test
	public void testURLEncodeing() throws Exception{
		try{
			//Test inserting Node with spaces and special characters
			//Insert new test Nodes
			boolean hasRoot = doesRootExist();
			Map<String, Object> properties = new HashMap<String, Object>();
			properties.put("Name", "Foo Name");		
			properties.put("desc", "Some New Foo Entity");
			properties.put("some property [~'()!]", "some value [~'()!]");
			properties.put("The string @foo-bar", "The+string+%C3%BC%40foo-bar @foo-bar");
			properties.put("The+string+%+C3%BC%++40foo-bar @foo-bar", "The+string+%+C3%BC%++40foo-bar @foo-bar");
			properties.put("(~)!'string' @foo-bar*The+string+%+C3%BC%++40foo-bar @foo-bar", "(~)!'string' @foo-bar*The+string+%+C3%BC%++40foo-bar @foo-bar");
			if(!hasRoot){
				properties.put("isRoot", true);
			}
			String node1JsonResult = this.insertNode("Foo", properties);
			this.checkJSON(node1JsonResult);
			String node1Id = this.getId(node1JsonResult);
					
			properties.put("Name", "Bar Name");		
			properties.put("desc", "Some New Bar Entity");
			String node2JsonResult = this.insertNode("Bar", properties);
			this.checkJSON(node2JsonResult);
			String node2Id = this.getId(node2JsonResult);
					
			//Test updating / retrieving properties of Metadata Entity Type with spaces and special characters
			String value = (String)this.getNodeProperty(node1Id, "some property [~'()!]");
			assertTrue("some value [~'()!]".equals(value));		
			value = (String)this.getNodeProperty(node2Id, "The string @foo-bar");
			assertTrue("The+string+%C3%BC%40foo-bar @foo-bar".equals(value));
			
			value = (String)this.getNodeProperty(node2Id, "The+string+%+C3%BC%++40foo-bar @foo-bar");
			assertTrue("The+string+%+C3%BC%++40foo-bar @foo-bar".equals(value));
			
			value = (String)this.getNodeProperty(node2Id, "(~)!'string' @foo-bar*The+string+%+C3%BC%++40foo-bar @foo-bar");
			assertTrue("(~)!'string' @foo-bar*The+string+%+C3%BC%++40foo-bar @foo-bar".equals(value));
					
			//Note that by default Tomcat doesn't allow the / or \ characters in URLs, so if you want to enable this, you have to set this in your
			//<Tomcat Home>\bin\setEnv.bat (.sh) file:
			//
			// set CATALINA_OPTS=%CATALINA_OPTS% -Dorg.apache.tomcat.util.buf.UDecoder.ALLOW_ENCODED_SLASH=true -Dorg.apache.catalina.connector.CoyoteAdapter.ALLOW_BACKSLASH=true
			//
			this.updateNodeProperty(node2Id, "!()\\The 'string' @foo-bar*The+string+%C3%BC%40foo-bar @foo-bar", "!()\\The 'string' @foo-bar*The+string+%C3%BC%40foo-bar @foo-bar");
			value = (String)this.getNodeProperty(node2Id, "!()\\The 'string' @foo-bar*The+string+%C3%BC%40foo-bar @foo-bar");
			assertTrue("!()\\The 'string' @foo-bar*The+string+%C3%BC%40foo-bar @foo-bar".equals(value));
					
			this.updateNodeProperty(node2Id, "()!\\%&", "()!\\%&");
			value = (String)this.getNodeProperty(node2Id, "()!\\%&");
			assertTrue("()!\\%&".equals(value));
			
			//Test Creating Metadata Entity Relationships
			Map<String, Object> relProperties = new HashMap<String, Object>();
			relProperties.put("Name", "Foo Type has Bar Type");		
			relProperties.put("desc", "Foo Has Bar Relationship");
			relProperties.put("some property", "some value");
			relProperties.put("required", true);
			String relId1 = Neo4JUtils.getId(this.insertNodeRelationship(node1Id, "has a", node2Id, relProperties));
					
			//Test get relationship count
			assertTrue(1 == Integer.parseInt(this.getNodeRelationshipCount(node1Id, "all", "has a")));
			assertTrue(1 == Integer.parseInt(this.getNodeRelationshipCount(node1Id, "out", "has a")));
			assertTrue(1 == Integer.parseInt(this.getNodeRelationshipCount(node2Id, "in", "has a")));
			
			//Test get relationships by type
			List<String> relTypes = new ArrayList<String>();
			relTypes.add("has a");
			this.checkJSON(this.getNodeRelationships(node1Id, "all", relTypes));
			this.checkJSON(this.getNodeRelationships(node2Id, "all", relTypes));
			
			//Test Relationship properties
			this.updateNodeRelationshipProperty(relId1, "()!\\The 'string' @foo-bar*The+string+%C3%BC%40foo-bar @foo-bar", "()!\\The 'string' @foo-bar*The+string+%C3%BC%40foo-bar @foo-bar");
			value = (String)this.getNodeRelationshipProperty(relId1, "()!\\The 'string' @foo-bar*The+string+%C3%BC%40foo-bar @foo-bar");
			assertTrue("()!\\The 'string' @foo-bar*The+string+%C3%BC%40foo-bar @foo-bar".equals(value));
			
			//Test get relationships by id
			this.checkJSON(this.getNodeRelationship(relId1));
			
			//Test get relationships by direction and type
			relTypes = new ArrayList<String>();
			this.checkJSON(this.getNodeRelationships(node1Id, "out", null));
			this.checkJSON(this.getNodeRelationships(node2Id, "in", null));
			
			relTypes.add("has a");
			this.checkJSON(this.getNodeRelationships(node1Id, "all", relTypes));
			this.checkJSON(this.getNodeRelationships(node2Id, "in", relTypes));
			this.checkJSON(this.getNodeRelationships(node1Id, "out", relTypes));
					
			//Test get Relationship Types
			List<String> relationshipTypes = (List<String>)Neo4JUtils.getValue(this.getNodeRelationshipTypes());
			assertTrue(relationshipTypes.contains("has a"));
			
			//Test getting Relationship properties
			assertTrue("some value".equals(this.getNodeRelationshipProperty(relId1, "some property")));
			relProperties = this.getNodeRelationshipProperties(relId1);
			assertTrue("some value".equals(relProperties.get("some property")));
			assertTrue("()!\\The 'string' @foo-bar*The+string+%C3%BC%40foo-bar @foo-bar".equals(relProperties.get("()!\\The 'string' @foo-bar*The+string+%C3%BC%40foo-bar @foo-bar")));
			
			//Test updating Relationship properties
			relProperties.put("some property", "some other value");
			relProperties.put("()!\\%&", "()!\\%&");
			this.updateNodeRelationshipProperties(relId1, relProperties);
			assertTrue("some other value".equals(this.getNodeRelationshipProperty(relId1, "some property")));
			assertTrue("()!\\%&".equals(relProperties.get("()!\\%&")));
			
			//Test deleteing Relationship properties
			this.deleteNodeRelationshipProperty(relId1, "some property");
			this.deleteNodeRelationshipProperty(relId1, "()!\\%&");
			relProperties = this.getNodeRelationshipProperties(relId1);
			assertTrue(!relProperties.containsKey("some property"));
			assertTrue(!relProperties.containsKey("()!\\%&"));
			
			//Test deleteing Node properties
			this.deleteNodeProperty(node2Id, "some property [~'()!]");
			this.deleteNodeProperty(node2Id, "()!\\%&");
			properties = this.getNodeProperties(node2Id);
			assertTrue(!properties.containsKey("some property [~'()!]"));
			assertTrue(!properties.containsKey("()!\\%&"));
		} catch(Throwable t){
			t.printStackTrace();
			System.out.println("ERROR: "+t.getMessage());
			throw t;
		}
	}
}
