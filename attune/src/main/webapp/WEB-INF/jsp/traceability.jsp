<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
         pageEncoding="ISO-8859-1" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jstl/core_rt" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
   <meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
   <title>Traceability</title>
</head>

<style>
   #traceability-container
   {
      margin-right: 0px;
      margin-left: 0px;
      margin-top: 0px;
      margin-bottom: 20px;
      width: 100%;
   }

   #traceability
   {
      position: relative;
      width: 100%;
      height: 53em;
      margin: auto;
      padding: 20px;
      overflow: auto;
      background-color: #f1f1ef;
      border-top: 15px solid #aec9df;
      border-bottom: 15px solid #aec9df;
   }

   .node
   {
      cursor: pointer;
   }

   .node circle
   {
      fill: #fff;
      stroke: steelblue;
      stroke-width: 3px;
   }

   .node gap
   {
      fill: #fff;
      stroke: #990000;
      stroke-width: 3px;
   }

   .node overlap
   {
      fill: #fff;
      stroke: #FFA500;
      stroke-width: 3px;
   }

   .node text
   {
      font: 12px sans-serif;
   }

   .link
   {
      fill: none;
      stroke: #ccc;
      stroke-width: 2px;
   }

   #searchDiv
   {
      float: right;
      margin-right: 1% !important;
      margin-top: 1% !important;
   }

   #searchInput
   {
      height: 2.5em;
      width: 60%;
      border: 1px solid #cac9c9;
      color: #666;
      font-size: 16px;
      margin-top: 8px;
   }

   .search-box:focus
   {
      box-shadow: 0 0 15px 5px #b0e0ee;
      border: 2px solid #bebede;
   }

   .traceability-button
   {
      margin-top: 8px;
   }

   #searchButton
   {
      width: 20%;
      float: right;
      background-color: #2e6da4 !important;
      background-image: url('./icons/magnifying_glass.png');
      -webkit-background-size: 25px;
      background-size: 25px;
      background-repeat: no-repeat;
      background-position: 50% center;
   }

   #resetButton
   {
      width: 20%;
      float: right;
      background-image: url('./icons/reset-icon-white.png');
      -webkit-background-size: 25px;
      background-size: 25px;
      background-repeat: no-repeat;
      background-position: 50% center;
      background-color: #666 !important;
      border-right: 5px;
   }

   .showDownload
   {
      width: 15%;
      float: right;
      font-size: 16px;
      background-color: #f1f1ef;
      border: 1px solid #cac9c9;
      height: 3.5em;
      width: 125px;
      margin: 15px;
   }

   #download
   {
      margin-left: 0px;
   }

   #showHideButton
   {
      margin-right: 15px;
      margin-left: 5px;
      padding: 5px;
      width: 140px;
   }

   #downloadPng
   {
      width: 20%;
      float: right;
      font-size: 20px;
   }

   .tooltip
   {
      width: 40em;
      border: 2px solid #ccc;
   }

   .tooltip #heading
   {
      background-color: #4b85b1;
      padding: 5px;
      height: 3em;
      border-bottom: 1px solid #bbb;
      justify-content: center;
      font-size: medium;
      color: whitesmoke;
      display: flex;
      align-items: center;
   }

   .tooltip #description
   {
      background-color: #f8f8f7;
      padding: 10px;
   }
</style>

<script>
	var isOriginal = false;
	var searchedNodes = [];

	$(document).ready()
	{
		//used to truncate display string
		String.prototype.trunc = function (n)
		{
			return this.substr(0, n - 1) + (this.length > n ? '... ' : '');
		};

		//store mouse position for tooltip
		var currentMousePos = {
			x: -1,
			y: -1
		};
		$(document).mousemove(function (event)
		{
			currentMousePos.x = event.pageX;
			currentMousePos.y = event.pageY;
		});

		//scroll to tree root
		function fixScroll(x, y)
		{
			$('#traceability').animate({
				scrollTop: x - 300
			}, 700);
			$('#traceability').animate({
				scrollLeft: y
			}, 700);
			//$.smoothScroll({ speed: 700 }, offset);
			return false;
		}

		var treeData = [];
		var coreData = [];
		var nodes = [];

		$
			.getJSON(
				"getLinkNodeData.exec",
				{
					id: '1',
					label: null,
					name: null,
					levels: 10
				},
				function (node)
				{
					treeData.push(node);
					coreData.push(node);

					// ************** Generate the tree diagram	 *****************
					var margin = {
						top: 20,
						right: 120,
						bottom: 20,
						left: 200
					}, width = document.body.scrollWidth * 2
						- margin.right - margin.left, height = width * 1.6;
					var i = 0, duration = 750, root;

					var tree = d3.layout.tree().size([height, width]);

					var diagonal = d3.svg.diagonal().projection(
						function (d)
						{
							return [d.y, d.x];
						});

					var svg = d3
						.select("#traceability")
						.append("svg")
						.attr("width",
							width + margin.right + margin.left)
						.attr("height",
							height + margin.top + margin.bottom)
						.append("g").attr("perserveAspectRatio",
							"xMinYMid").attr("viewBox",
							"0 0 960 500").attr(
							"transform",
							"translate(" + margin.left + ","
							+ margin.top + ")");

					root = treeData[0];
					root.x0 = height / 2;
					root.y0 = 0;
					update(root);
					fixScroll(root.x0, root.y0);

					d3.select(self.frameElement).style("height",
						"500px");

					function update(source)
					{

						// Compute the new tree layout.
						nodes = tree.nodes(root).reverse(),
							links = tree.links(nodes);

						computeNodeLinks();

						// Normalize for fixed-depth.
						nodes.forEach(function (d)
						{
							d.y = d.depth * 450;
						});

						// Update the nodes?
						var node = svg.selectAll("g.node").data(nodes,
							function (d)
							{
								return d.id || (d.id = ++i);
							});

						// Enter any new nodes at the parent's previous position.
						var nodeEnter = node
							.enter()
							.append("g")
							.attr("class", "node")
							.attr(
								"transform",
								function (d)
								{
									return "translate("
										+ source.y0 + ","
										+ source.x0 + ")";
								})
							.on("click", highlight_node_links)
							.on(
								"mouseover",
								function (d)
								{
									var tooltipString = "<div id='heading'>"
										+ d.name
										+ "</div><div id='description'>"
										+ (d.description || 'No Description Provided')
										+ "<div>";
									var g = d3.select(this); // The node
									var div = d3
										.select("body")
										.append("div")
										.attr(
											'pointer-events',
											'none')
										.attr("class",
											"tooltip")
										.style("opacity", 1)
										.html(tooltipString)
										.style(
											"left",
											(currentMousePos.x - 200 + "px"))
										.style(
											"top",
											(currentMousePos.y + 20 + "px"));
								}).on(
								"mouseout",
								function ()
								{
									// Remove the info text on mouse out.
									d3.select("body").select(
										'div.tooltip')
										.remove()
								});

						nodeEnter
							.append("circle")
							.attr("r", 1e-6)
							.style(
								"fill",
								function (d)
								{
									return d._children ? "lightsteelblue"
										: "#fff";
								}).attr(
							"id",
							function (d, i)
							{
								return "circle-"
									+ d.id.replace(
										/ /g, "_");
								;
							});

						nodeEnter
							.append("text")
							.attr(
								"x",
								function (d)
								{
									return d.children
									|| d._children ? -13
										: 13;
								})
							.attr("dy", ".35em")
							.attr(
								"text-anchor",
								function (d)
								{
									return d.children
									|| d._children ? "end"
										: "start";
								}).text(function (d)
						{
							return d.name.trunc(25);
						}).style("fill-opacity", 1e-6);

						// Transition nodes to their new position.
						var nodeUpdate = node.transition().duration(
							duration).attr(
							"transform",
							function (d)
							{
								return "translate(" + d.y + ","
									+ d.x + ")";
							});

						nodeUpdate
							.select("circle")
							.attr("r", 10)
							.style(
								"fill",
								function (d)
								{
									return d._children ? "lightsteelblue"
										: "#fff";
								});

						nodeUpdate.select("text").style("fill-opacity",
							1);

						// Transition exiting nodes to the parent's new position.
						var nodeExit = node.exit().transition()
							.duration(duration).attr(
								"transform",
								function (d)
								{
									return "translate("
										+ source.y + ","
										+ source.x + ")";
								}).remove();

						nodeExit.select("circle").attr("r", 1e-6);

						nodeExit.select("text").style("fill-opacity",
							1e-6);

						// Update the links?
						var link = svg.selectAll("path.link").data(
							links, function (d)
							{
								return d.target.id;
							});

						// Enter any new links at the parent's previous position.
						link.enter().insert("path", "g").attr("class",
							"link").attr("d", function (d)
						{
							var o = {
								x: source.x0,
								y: source.y0
							};
							return diagonal({
								source: o,
								target: o
							});
						}).attr("id", function (d, i)
						{
							d.id = i;
							return "link-" + i;
						});

						// Transition links to their new position.
						link.transition().duration(duration).attr("d",
							diagonal);

						// Transition exiting nodes to the parent's new position.
						link.exit().transition().duration(duration)
							.attr("d", function (d)
							{
								var o = {
									x: source.x,
									y: source.y
								};
								return diagonal({
									source: o,
									target: o
								});
							}).remove();

						// Stash the old positions for transition.
						nodes.forEach(function (d)
						{
							d.x0 = d.x;
							d.y0 = d.y;
						});
					}

					// Toggle children on click.
					function click(d)
					{
						//console.log(d);
						if (d.children)
						{
							d._children = d.children;
							d.children = null;
						} else
						{
							d.children = d._children;
							d._children = null;
						}
						update(d);
					}

					$('#searchInput').keypress(function (e)
					{
						if (e.which == 13)
						{
							var searchString = $('#searchInput').val();
							if (searchString != "")
							{
								for (var i = 0; i < searchedNodes.length; i++) {
									highlight_node_links(searchedNodes[i], 597, false, true);
								}
								searchedNodes = [];
								findOaf(treeData[0], searchString);
							}
						}
					});

					$('#searchButton').click(function ()
					{
						var searchString = $('#searchInput').val();
						if (searchString != "")
						{
							for (var i = 0; i < searchedNodes.length; i++) {
									highlight_node_links(searchedNodes[i], 597, false, true);
								}
								searchedNodes = [];
							findOaf(treeData[0], searchString);
						}
					});

					$('#resetButton').click(function ()
					{
						$('#searchInput').val('');
						//root = treeData[0];
						//update(root);
						fixScroll(root.x0, root.y0);
						d3.selectAll("path").style('stroke', '#ccc');
						d3.selectAll("path").attr("data-clicked", "1");
						d3.selectAll("circle").style('fill', '#fff');
					});

					//Show Gaps/Overlap Button
					$("#showHideButton").click(function ()
					{
						if (isOriginal)
						{
							$(this).text("Show Gaps/Overlaps");
							d3.selectAll("circle").style('fill', '#fff');
							//keep search node still highlighted
							var searchString = $('#searchInput').val();
							if (searchString != "")
							{
								findOaf(treeData[0], searchString);
							}

						} else
						{
							$(this).text("Hide Gaps/Overlaps");
							//show the colors
							colorGapsandOverlaps();
						}
						isOriginal = !isOriginal;
					});

					function colorGapsandOverlaps()
					{
						//color in gaps
						$.getJSON("getGapNodesAndParents.exec", function (response)
						{
							if (response.children && response.children.length === 0)
							{
								return false;
							} else
							{
								var arrayLength = response.length;
								for (var i = 0; i < response.length; i++)
								{
									var skip = false; 
									for (var j = 0; j < searchedNodes.length; j++) {
										if (searchedNodes[j].id == response[i]) {
											skip = true;
										}
									}
									if (!skip){
									color_circle(response[i].replace(/ /g, "_"), "#990000");
									}
								}
							}
						});

						//color in overlaps
						$.getJSON("getOverlapAndParents.exec", function (response)
						{
							if (response.children && response.children.length === 0)
							{
								return false;
							} else
							{
								var arrayLength = response.length;
								for (var i = 0; i < response.length; i++)
								{
									var skip = false; 
									for (var j = 0; j < searchedNodes.length; j++) {
										if (searchedNodes[j].id == response[i]) {
											skip = true;
										}
									}
									if (!skip) {
										color_circle(response[i].replace(/ /g, "_"), "#FFA500");
									}
								}
							}
						});
					}

					function findOaf(node, searchString)
					{
						var finished = false;
						var nodeName = node.name.toLowerCase();


						if (nodeName
								.indexOf(searchString.toLowerCase()) >= 0)
						{
							console.log("found here");
							console.log(node);
							console.log(node.id);
							//node.on("click", highlight_node_links);
							highlight_node_links(node, 597, true, false);
							fixScroll(node.x0, node.y0);
							color_circle(node.id.replace(/ /g, "_"),
								"#3c763d");
							searchedNodes.push(node);

							if (node.children != null
								&& finished == false)
							{
								$.each(node.children, function (index,
																		  child)
								{
									console.log("checking children after found");
									findOaf(child, searchString);
								});
							}
							finished = true;

						}
						/*if (node.number.toLowerCase() == searchString.toLowerCase()) {
                     root = node;
                     root.x0 = height / 2;
                     root.y0 = 0;
                     finished = true;

                     //resize tree and scroll
                     var xcord = (root.children != null) ? root.children.length : 1;
                     tree = tree.size([xcord * 500, width])
                     update(root);
                     fixScroll(root.x0, root.y0);
                  }*/
						else
						{
							if (node.children != null
								&& finished == false)
							{
								$.each(node.children, function (index,
																		  child)
								{
									findOaf(child, searchString);
								});
							}
						}
					}

					function expand(d)
					{
						var children = (d.children) ? d.children
							: d._children;
						if (d._children)
						{
							d.children = d._children;
							d._children = null;
						}
						if (children)
							children.forEach(expand);
					}

					function expandAll()
					{
						expand(root);
						update(root);
					}

					function computeNodeLinks()
					{
						nodes.forEach(function (node)
						{
							node.sourceLinks = [];
							node.targetLinks = [];
						});
						links
							.forEach(function (link)
							{
								var source = link.source, target = link.target;
								if (typeof source === "number")
									source = link.source = nodes[link.source];
								if (typeof target === "number")
									target = link.target = nodes[link.target];
								source.sourceLinks.push(link);
								target.targetLinks.push(link);
							});
					}

					function highlight_node_links(node, i, highlight, unhighlight)
					{
						console.log(node);
						console.log(i);
						console.log("node is highlighted");
						var remainingNodes = [], nextNodes = [];
						var stroke = '#ccc';
						console.log(d3.select("#traceability"));
						if (unhighlight == true) {
							console.log("unhighlight in highlight if statement");
							console.log(node.name);
							d3.select("#traceability").attr("data-clicked", "0");
							color_circle(node.id.replace(/ /g, "_"),
								"#ffffff");
							stroke = '#ccc';
						} else if (d3.select("#traceability").attr("data-clicked") == "1" && highlight == false)
						{
							console.log("$$$$$$$$$$in highlight if statement");
							console.log(node.name);
							d3.select("#traceability").attr("data-clicked", "0");
							color_circle(node.id.replace(/ /g, "_"),
								"#ffffff");
							stroke = '#ccc';

						} 
						else
						{
							console.log("$$$$in highlight else");
							console.log(node.name);
							d3.select("#traceability").attr("data-clicked", "1");
							stroke = '#3ba938';
						}
						console.log("after statements"); 
						var traverse = [{
							linkType: "sourceLinks",
							nodeType: "target"
						}, {
							linkType: "targetLinks",
							nodeType: "source"
						}];
						traverse
							.forEach(function (step)
							{
								node[step.linkType]
									.forEach(function (link)
									{
										remainingNodes
											.push(link[step.nodeType]);
										highlight_link(link.id,
											stroke);
									});
								while (remainingNodes.length)
								{
									nextNodes = [];
									remainingNodes
										.forEach(function (node)
										{
											node[step.linkType]
												.forEach(function (link)
												{
													nextNodes
														.push(link[step.nodeType]);
													highlight_link(
														link.id,
														stroke);
												});
										});
									remainingNodes = nextNodes;
								}
							});
					}

					function highlight_link(id, stroke)
					{
						d3.select("#link-" + id)
							.style("stroke", stroke);
					}

					function color_circle(id, color)
					{
						console.log(id)
						d3.select("#circle-" + id).style("fill", color);
					}
				});

		// start: code to download the traceability tree
		var prefix = {
			xmlns: "http://www.w3.org/2000/xmlns/",
			xlink: "http://www.w3.org/1999/xlink",
			svg: "http://www.w3.org/2000/svg"
		};

		d3.select("#downloadPng").on("click", function ()
		{

			var w = 1000;
			var h = 8000;
			var img = new Image();
			img.width = w;
			img.height = h;

			img.onload = function ()
			{
				var canvas = document.createElement('canvas');
				canvas.width = w;
				canvas.height = h;

				var ctx = canvas.getContext('2d');
				ctx.drawImage(img, 0, 0, w, h);

				window.URL.revokeObjectURL(url);
				var canvasdata = canvas.toDataURL('image/png');
				var a = window.document.createElement("a");
				a.download = "traceability.png";
				a.href = canvasdata;
				document.body.appendChild(a);
				a.click();
				document.body.removeChild(a);
			}

			var blob = new Blob(getNewSource()[0].source, {
				type: 'image/svg+xml;charset=utf-8'
			});
			var url = window.URL.createObjectURL(blob);
			img.src = url
		});

		d3.select("#download").on("click", function ()
		{

			// create a file blob of our SVG.
			var a = window.document.createElement("a");
			downloadSVG(a, getNewSource(), "traceability.svg")
			document.body.appendChild(a);
			a.click();
			document.body.removeChild(a);
		});

		function getNewSource()
		{

			var emptySvg = window.document.createElementNS(prefix.svg, 'svg');
			window.document.body.appendChild(emptySvg);
			var emptySvgDeclarationComputed = getComputedStyle(emptySvg);
			return getSource(emptySvgDeclarationComputed);
		}

		function downloadSVG(anchorSelector, newSource, fileName)
		{
			if (window.navigator.msSaveOrOpenBlob)
			{ // IE11
				blobObject = new Blob(newSource[0].source, {
					"type": "text\/xml"
				});
				$(anchorSelector).click(function ()
				{
					window.navigator.msSaveOrOpenBlob(blobObject, fileName);
				});
			} else
			{ // Chrome
				var url = window.URL.createObjectURL(new Blob(
					newSource[0].source, {
						"type": "text\/xml"
					}));
				$(anchorSelector).attr("class", "svg-crowbar");
				$(anchorSelector).attr("download", fileName);
				$(anchorSelector).attr("href", url);
			}
		}

		function getSource(emptySvgDeclarationComputed)
		{

			var svgInfo = [], svgs = document.querySelectorAll("svg");

			svgs[0].setAttribute("version", "1.1");

			// removing attributes so they aren't doubled up
			svgs[0].removeAttribute("xmlns");
			svgs[0].removeAttribute("xlink");

			// These are needed for the svg
			if (!svgs[0].hasAttributeNS(prefix.xmlns, "xmlns"))
			{
				svgs[0].setAttributeNS(prefix.xmlns, "xmlns", prefix.svg);
			}

			if (!svgs[0].hasAttributeNS(prefix.xmlns, "xmlns:xlink"))
			{
				svgs[0].setAttributeNS(prefix.xmlns, "xmlns:xlink",
					prefix.xlink);
			}

			setInlineStyles(svgs[0], emptySvgDeclarationComputed);

			var doctype = '<?xml version="1.0" standalone="no"?>'
				+ '<!DOCTYPE svg PUBLIC "-//W3C//DTD SVG 1.1//EN" "http://www.w3.org/Graphics/SVG/1.1/DTD/svg11.dtd">';

			var source = (new XMLSerializer()).serializeToString(svgs[0]);
			var rect = svgs[0].getBoundingClientRect();
			svgInfo.push({
				top: rect.top,
				left: rect.left,
				width: rect.width,
				height: rect.height,
				class: svgs[0].getAttribute("class"),
				id: svgs[0].getAttribute("id"),
				childElementCount: svgs[0].childElementCount,
				source: [doctype + source]
			});

			return svgInfo;
		}

		function setInlineStyles(svg, emptySvgDeclarationComputed)
		{

			function explicitlySetStyle(element)
			{
				var cSSStyleDeclarationComputed = getComputedStyle(element);
				var i, len, key, value;
				var computedStyleStr = "";
				for (i = 0, len = cSSStyleDeclarationComputed.length; i < len; i++)
				{
					key = cSSStyleDeclarationComputed[i];
					value = cSSStyleDeclarationComputed.getPropertyValue(key);
					if (value !== emptySvgDeclarationComputed
							.getPropertyValue(key))
					{
						computedStyleStr += key + ":" + value + ";";
					}
				}

				element.setAttribute('style', computedStyleStr);
			}

			function traverse(obj)
			{
				var tree = [];
				tree.push(obj);
				visit(obj);

				function visit(node)
				{
					if (node && node.hasChildNodes())
					{
						var child = node.firstChild;
						while (child)
						{
							if (child.nodeType === 1
								&& child.nodeName != 'SCRIPT')
							{
								tree.push(child);
								visit(child);
							}
							child = child.nextSibling;
						}
					}
				}

				return tree;
			}

			// hardcode computed css styles inside svg
			var allElements = traverse(svg);
			var i = allElements.length;
			while (i--)
			{
				explicitlySetStyle(allElements[i]);
			}
		}

		//end: code to download traceability tree
	}


</script>

<body>
<div id="traceability-container" class="container">
   <div class="row">
      <div class="col-md-12" style="width: auto;">
         <h1 class="page-title">Traceability</h1>
      </div>

      <button id="download" class="svg-crowbar showDownload" type="button">Download</button>
      <button id="showHideButton" class="showDownload" type="button">Show
         Gaps/Overlaps
      </button>

      <div id="searchDiv" class="topbox"
           style="width: 40%; margin: 0 10px; padding: 0; border: none; background: transparent;">
         <input class="search-box" id="searchInput" type="text"
                name="searchInput" placeholder=" Search ">

         <button class="traceability-button" id="resetButton" title="Reset"
                 type="reset"></button>
         <button class="traceability-button" id="searchButton"
                 title="searchButton" type="button"></button>
      </div>
   </div>
   <div class="tab-content">
      <div id="traceability"></div>
      <div id="log"></div>
      <canvas></canvas>
   </div>
</div>
</body>

<script type="text/javascript">

</script>

</html>