<!DOCTYPE html>
<html lang="en">
<%@ taglib prefix="c" uri="http://java.sun.com/jstl/core_rt" %>
<%@ taglib prefix="sec" uri="http://www.springframework.org/security/tags" %>
<head>
   <meta charset="utf-8"/>
   <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no">
   <title>Attune</title>

   <link rel="stylesheet" href="css/bootstrap.css">
   <link rel="stylesheet" href="css/bootstrap-theme.css">
   <link rel="stylesheet" href="css/jquery.editable-select.css">
   <link rel="stylesheet" href="css/c3.css">
   <link rel="stylesheet" href="css/jquery-ui.min.css">
   <link href='https://fonts.googleapis.com/css?family=Open+Sans:400,300,700' rel='stylesheet' type='text/css'>
   <link rel="stylesheet" href="css/main.css">
   <link rel="stylesheet" href="css/vis.css">
   <link rel="stylesheet" href="css/jquery.dataTables.min.css">
   <link rel="stylesheet" href="css/dataTables.bootstrap.min.css">
   <link rel="stylesheet" href="css/fileinput.css" media="all" type="text/css"/>
   <link rel="stylesheet" href="css/bootstrap-editable.css" type="text/css"/>
   <link rel="stylesheet" href="css/nice-select.css" type="text/css"/>
   <%--<link rel="stylesheet" href="css/screen.css">--%>

   <script src="js/jquery-2.1.3.js"></script>
   <script src="js/fileinput.js" type="text/javascript"></script>
   <script src="js/bootstrap.js"></script>
   <script src="js/main.js"></script>
   <script src="js/inflection.js"></script>
   <script src="js/springy.js" type="text/javascript"></script>
   <script src="js/cytoscape.js" type="text/javascript"></script>
   <script src="js/networkView.js" type="text/javascript"></script>
   <script src="js/d3.js" charset="utf-8"></script>
   <script src="js/Sortable.js"></script>
   <script src="js/jquery.editable-select.js"></script>
   <script src="js/c3.js"></script>
   <script src="js/vis.js"></script>
   <script src="js/jit.js"></script>
   <script src="js/jquery-ui.min.js"></script>
   <script src="js/accounting.min.js"></script>
   <script src="js/excellentexport.min.js"></script>
   <script src="js/jquery.tablesorter.min.js"></script>
   <script src="js/timeline.js"></script>
   <script src="js/metadata.js" type="text/javascript"></script>
   <%--<script src="js/bootstrap-editable.min.js" type="text/javascript"></script>--%>
   <script src="js/jquery.nice-select.min.js" type="text/javascript"></script>
   <script src="js/bootbox.min.js" type="text/javascript"></script>
   <script src="js/jquery.validate.min.js" type="text/javascript"></script>
   <script src="js/additional-methods.min.js" type="text/javascript"></script>

   <style>

      #sidebar-footer
      {
         position: absolute;
         color: white;
         bottom: 100px;
         width: 100%;
         text-align: center;
      }

      #main-header
      {
         position: fixed;
         left: 0;
         top: 0;
            height:100px;

         width: 100%;
         z-index: 1050;
         background-color: #ffffff;
         background: url('../icons/attune_logo.png') no-repeat center center;
         background-size: 25%;
      }

      #header-items {
         list-style: none;
         float: right;
         margin: 0;
         padding: 0;
      }

      .sidebar-item-user
      {
         margin-left:-60px;
      }

      .dropdown-menu .divider
      {
         margin: 0px;
         padding: 1px;
      }

      .dropdown-menu > li > a
      {
         padding: 3px 0px;
      }

      .portfolio-button {
   float: right;
   height: 3.0em;
   width: 150px;
   background-color: #72a1b1 !important;
   border: 1px solid #cac9c9;
   color: #fff;
   font-size: 16px;
   margin-top: -7px;
   margin-bottom:5px;
}
   </style>

   <script>
		$(document).ready(function ()
		{
			$('#main').load('dashboard.exec', function (response, status, xhr)
			{
				if (status == "error")
					$('#main').append(response);
				else
					$('#main').fadeIn();
			});

			// Set the sidebar to be expanded or collapsed depending on the users device
			setSidebar();

			// Sidebar toggler for mobile devices
			$("#toggleSidebar").click(function (e)
			{
				e.preventDefault();
				$("#wrapper").toggleClass("toggled");
			});

			$('#configDashboard').click(function ()
			{
				$('#spinner').show();
				$('#main').load('configDashboard.exec', function ()
				{
					$('#main').fadeIn();
					$('#spinner').hide();
				});
			});

			$('.sidebar-item a').click(function ()
			{
				$('.sidebar-item').removeClass('sidebar-item-selected');
				$(this).parent().addClass('sidebar-item-selected');
			});

			$('.sidebar-item:first').addClass('sidebar-item-selected');

			// Appropriately open/close the sidebar if someone adjusts the browswer dimensions
			$(window).resize(function ()
			{
				setSidebar();
			});

			function setSidebar()
			{
				if ($(window).width() <= 1024)
					$('#wrapper').addClass('toggled');
				else
					$('#wrapper').removeClass('toggled');
			}

			// Sidebar listeners for styling the sidebar items
			$('.sidebar-item a').hover(function ()
			{
				if (!$(this).parent().hasClass('sidebar-item-selected'))
					$(this).parent().addClass('sidebar-item-hovered');
			}, function ()
			{
				$(this).parent().removeClass('sidebar-item-hovered');
			});

			$('#configButton').hover(function ()
			{
				$('.sidebar-item').removeClass('sidebar-item-hovered');
			});

			$('[data-toggle="tooltip"]').tooltip(
				{
					'container': 'body'
				});

			// If the user clicks/touches the main content, always close the sidebar
			$('#main').on('click', function ()
			{
				if ($(window).width() <= 1024)
					$('#wrapper').addClass('toggled');
			});

			pluralizeByCount = function (word, count)
			{
				if (count == 1)
					return word;
				else return word.pluralize();
			}

			$('.dropdown').hover(function ()
			{
				$(this).find('.profile-menu').stop(true, true).fadeIn(200)
			}, function ()
			{
				$(this).find('.profile-menu').stop(true, true).delay(400).fadeOut(200);
			});

			<c:if test="${not empty logo}">
			   $('#logo').css('background', "url(./icons/logo.png) no-repeat center center");
			</c:if>
		});

   </script>

</head>

<body class="base-color1">
<div id="wrapper">
   <header id="main-header" style="height:100px; background-image: url('images/AttuneBanner3800x200.png'); background-size: cover; width:100%">
   <div id="logo"></div>



   <ul id="header-items">
      <li>
         <div class="dropdown" style="margin-top:35px; margin-right: -71px;font-size: 18px; width:320px;">
         <div class="btn-group">
            <button type="button" class="btn btn-default" style="width:200px;">
               &nbsp;&nbsp;<span class="glyphicon glyphicon-user"></span>&nbsp;&nbsp;${username}</button>
            <button type="button" class="btn btn-default dropdown-toggle" data-toggle="dropdown">
               <span class="caret"></span>
               <span class="sr-only">Toggle Dropdown</span>
            </button>
            <ul class="dropdown-menu" role="menu">
               <li style="margin-left: -13px;"><i>Last Login: ${lastLogin}</i></li>
               <%--<li style="display: ${ reviewer? 'none':'block' }"><a href="loginHistory.exec" class="execLink">Login History</a></li>--%>
               <li style="display: ${ reviewer? 'none':'block' }" class="divider"></li>
               <li style="margin-left: -13px;"><a href='<c:url value="logout.exec" />'>Logout</a></li>
            </ul>
         </div>
      </div>
      </li>
   </ul>
   </header>
   <!-- Sidebar -->
   <div id="sidebar-wrapper" class="base-color2">

      <ul class="sidebar-nav" style="margin-top: 10px">
         <li class="sidebar-item"><a href="dashboard.exec" class="execLink"><span
                 class="glyphicon glyphicon-dashboard"></span>&nbsp;&nbsp;Dashboard</a>
         <li class="sidebar-item"><a href="metadata.exec" class="execLink"><span
                 class="glyphicon glyphicon glyphicon-star"></span>&nbsp;&nbsp;Metadata Model</a></li>
         <li class="sidebar-item"><a href="portfolio.exec" class="execLink"><span
                 class="glyphicon glyphicon-briefcase"></span>&nbsp;&nbsp;Portfolio</a></li>
         <li class="sidebar-item" id="timelineMenu" style="display: ${ timelineCreated? 'block':'none' }">
            <a href="timeline.exec?entity=Ship" class="execLink">
             <span class="glyphicon glyphicon-time"></span>&nbsp;&nbsp;Timeline</a>
         </li>
         <li class="sidebar-item"><a href="traceability.exec" class="execLink"><span
                 class="glyphicon glyphicon-random"></span>&nbsp;&nbsp;Traceability</a></li>
         <li class="sidebar-item"><a id="dataAnalysis" href="dataAnalysis.exec" class="execLink"><span
                 class="glyphicon glyphicon-list-alt"></span>&nbsp;&nbsp;Data Analysis</a></li>

         <%-- security ${security.comment}%>
         <sec:authorize access="hasRole('ADMIN')">
         <%${security.comment} security --%>

            <li class="sidebar-item"><a href="userAdmin.exec" class="execLink"><span
                 class="glyphicon glyphicon glyphicon-user"></span>&nbsp;&nbsp;User Admin</a></li>

         <li class="sidebar-item"><a href="auditTrail.exec" class="execLink"><span
                 class="glyphicon glyphicon glyphicon-list-alt"></span>&nbsp;&nbsp;Audit Trail</a></li>

            <li class="sidebar-item"><a href="dataImport.exec" class="execLink"><span
                 class="glyphicon glyphicon glyphicon-download-alt"></span>&nbsp;&nbsp;Data Import</a></li>

         <%-- security ${security.comment}%>
         </sec:authorize>
         <%${security.comment} security --%>

      </ul>

   </div>

   <div id="page-content-wrapper" style="padding-top: 1px; padding-left:10px;padding-right:10px">
      <div id="main" style="float:left;width: 100%; margin-top: 100px;"></div>
      <div id="spinner" class="spinner"></div>
   </div>

   <div class="modal fade" id="baseballCardModal" role="dialog">
      <div class="modal-dialog modal-sm">
         <div class="modal-content base-color3" id="baseballModalContent">
            <div class="modal-body" id="baseballContent">
            </div>
         </div>
      </div>
   </div>

</div>


</body>
</html>