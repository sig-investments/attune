<%@ page language="java" contentType="text/html; charset=ISO-8859-1" pageEncoding="ISO-8859-1" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jstl/core_rt" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<html>
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">

    <style>
        #dataAnalysis-container {
            margin-right: 0px;
            margin-left: 0px;
            width: 100%;
        }

        #filter-container {
            height: 210px;
            overflow-y: auto;
            overflow-x: hidden;
        }

        #filter-container label {
            float: left;
            padding-left: 15px;
            padding-right: 15px;
            padding-top: 10px;
            height: 40px;
            overflow: hidden;
            white-space: nowrap;
            text-overflow: ellipsis;
        }

        #parameter-container {
            transition: all 0.3s ease;
        }

        .parameter {
            padding-top: 20px;
            padding-bottom: 20px;
        }

        .panel-heading span.glyphicon {
            color: #666;
            float: right;
            top: -1.2em
        }

        .control-label {
            font-weight: normal;
            padding-top: 9px;
        }

        #submitAnalysis {
            transition: all 0.2s ease;
            margin-top: 20px;
            background: #9ac440;
            border-radius: 0px;
            border: 1px solid #9ac440;
            color: white;
            height: 40px;
        }

        #submitAnalysis:hover {
            transition: all 0.2s ease;
            background-color: transparent;
            color: #9ac440;
        }

        #results-container {
            transition: all 0.3s ease;
            height: 37em;
            overflow: auto;
            padding: 0px 15px 15px 15px;
        }

        #results {
            transition: all 0.3s ease;
        }

        #results table th {
            font-weight: normal;
            font-size: 15px;
            color: #333;
            cursor: pointer;
        }

        #results table th:active {
            outline: 0;
        }

        #results table th a {
            color: #807777;
        }

        #results table td {
            color: #807777;
        }

        #result-spinner {
            position: absolute;
            left: 48%;
            top: -140%;
        }

        .divider {
            position: relative;
            left: 15%;
            height: 1px;
        }

        #filter-container > form
        {
            background-color: #ddd;
            padding-top: 15px;
            margin-top: -20px;
        }

        #filter-container > form > div.form-group {
            margin-bottom: 0;
        }

        #parameter-container > div > form > div.form-group {
            margin-top: 10px;
            margin-bottom: 0;
        }

        #edit-entity-container {
            border: 1px solid #faebcc;
            margin-top: 1%;
        }

        #add-entity-div {
            border: 1px solid #337ab7;
            margin-top: 1%;
        }

        #edit-entity-name, #new-entity-name, #edit-entity-rels-name {
            margin: 0;
        }

        #is-root, .required-box {
            height: 60%;
            width: 10%;
        }

        .alert-error {
            padding: 15px;
            margin-bottom: 20px;
            border: 1px solid transparent;
            border-radius: 4px;
            background-color: #f2dede;
            border-color: #eed3d7;
            color: #b94a48;
        }

        .col-md-4 {
            height: 55px;
            width:30%;
        }
    </style>

    <script>
       $(document).ready(function () {
          var currentParameters = [];
          var currentFields = [];

          $('#results-container').hide();

          $('#result-spinner').hide();

          $('#edit-entity-container').hide();

          $('#add-entity-container').hide();

          $('#edit-button').hide();
          $('#delete-button').hide();

          $('#entityType').change(function () {
             $('#parameter-container').empty();
             $('#parameter-container').show();
             $('#results-container').show();
             $('#edit-entity-list').empty();
             $('#edit-entity-container').hide();
             $('#add-entity-container').hide();
             $('#edit-button').show();
             $('#delete-button').show();

             var entityListSelector = $('#entityList');

             $.getJSON("getFieldList.exec", {entity: $(this).val()}, function (list) {

                currentParameters.forEach(function (param) {
                   param.remove();
                });
                currentParameters = [];
                currentFields = list;

                list.forEach(function (param, index) {
                   addParameter(param, index);
                   populateEditList(param, $('#entityType').val(), index)
                });

//                buildRelationshipsTable($('#entityType').val());
//
                $('#add-entity-select').append(createEntitySelect('ACAT', '', 'add-entity-rel-select'));
                $('.nice-select').niceSelect();

                $('#result-spinner').show();

                getResults();

             });
          });

          function addParameter(values)
          {
             var id = currentParameters.length + 1;
             var newParam = new Parameter(values, id);
             newParam.init();
             currentParameters.push(newParam);
          }

          function removeParameter(id)
          {
             var newArray = []
             currentParameters.forEach(function (param) {
                if (param.id !== id)
                {
                   newArray.push(param)
                }
             });
             currentParameters = newArray;
          }

          var Parameter = function (param, id) {
             this.value = function () {
                if ($('#parameter-input-' + that.id)[0].value)
                   return $('#parameter-input-' + that.id)[0].value;
                else
                   return "Any";
             };
             this.property = param;
             this.id = id;
             var that = this;

             this.init = function () {
                html = '';

                html += '<div class="col-md-4">\n';
                html += ' <form id="parameter-' + that.id + '" class="form-horizontal">\n';
                html += '   <div class="form-group">\n';
                html += '     <label id="parameter-label-' + that.id + '" for="parameter-input-' +
                   that.id + '" class="control-label col-md-4"></label>\n';
                html += '     <div class="col-md-8">\n';
                html += '       <select id="parameter-input-' + that.id + '" class="form-control" style="background-color: white">\n';
                html += '       </select>\n';
                html += '     </div>\n'
                html += '   </div>\n';
                html += ' </form>\n';
                html += '</div>\n';


                $('#parameter-container').prepend(html);
                setParameter(that.property);
                setSuggestions();
                attachListeners();
                setDisabled(false);

                // Correctly set the styling of the parameters
                $('.parameter').css('padding-top', '20px');
                $('.parameter').first().css('padding-top', '0px');
                $('.parameter').last().css('margin-bottom', '20px');
             };

             var setParameter = function (param) {
                $('#parameter-label-' + id).text(param);
             }

             var attachListeners = function () {
                $('#parameter-input-' + that.id).change(function () {
                   getResults();
                });
             };

             this.remove = function () {
                $('#parameter-' + that.id).remove();
                removeParameter(that.id);
             }

             var setDisabled = function (disabled) {
                $('#parameter-input-' + id).attr('disabled', disabled);
             };

             this.setDisabled = function (disabled) {
                $('#parameter-select-' + id).attr('disabled', disabled);
                $('#parameter-input-' + id).attr('disabled', disabled);
             };
             var setSuggestions = function (suggestions) {
                html = '';

                html += '<option value="Any">Any</option>\n';
                suggestions.forEach(function (suggestion) {
                   html += '<option value="' + suggestion + '">' + suggestion + '</option>\n';
                });

                $('#parameter-input-' + that.id).html(html);
             };

             var setSuggestions = function () {
                var field = that.property;
                $.getJSON("getValueList.exec", {entity: $('#entityType').val(), field: field}, function (list) {
                   html = '';

                   html += '<option value="Any">Any</option>\n';
                   list.forEach(function (suggestion) {
                      html += '<option value="' + suggestion + '">' + suggestion + '</option>\n';
                   });

                   $('#parameter-input-' + that.id).html(html);
                });
             };
          }

          function populateEditList(param, entity)
          {

             //change the edit label to be the name of the selected entity
             $('#edit-entity-name').text('Edit ' + entity + ' Parameters');

             var editList = $('#edit-entity-list');
             item = '<li class="list-group-item">' +
                '<input id="' + entity + '-' + param + '" value="' + param + '" class="form-control edit-list-item"/>' +
                '</li>';
             editList.append(item);
          }

          function createEntitySelect(startNodeName, endNodeName, niceSelectId)
          {

             var html = '';
             html += '<select class="nice-select wide" data-id="' + niceSelectId + '">';
             $("#entityType option").each(function () {
                var entityType = $(this).val();
                if (entityType != startNodeName)
                {
                   html += '<option value="' + entityType + '"';
                   if (entityType == endNodeName)
                   {
                      html += ' selected="selected"'
                   }
                   html += '">' + entityType + '</option>';
                }
             });
             html += '</select>';
             return html;

          }

          function createInput(value, type, checked, cssClass, neo4jId)
          {
             var html = '<input type="' + type + '" data-id="' + neo4jId + '"';
             if (cssClass != '')
             {
                html += ' class="' + cssClass + '"';
             }
             if (value != '')
             {
                html += ' value="' + value + '"';
             }
             if (checked != '' && checked == 'true')
             {
                html += ' checked="' + checked + '"';
             }
             html += '" />';
             return html;
          }

          function createBtn(text, cssClass, neo4jId)
          {
             return '<button class="' + cssClass + '" data-id="' + neo4jId + '">' + text + '</button>';
          }

          $(document).on("click", ".updateRel", function (event) {

             var neo4jId = event.target.getAttribute('data-id');
             var $elements = $(event.target).closest('tr').find('[data-id="' + neo4jId + '"]');

             var inputParams = [];
             $elements.each(function (index, value) {
                if (!$(this).is("button"))
                {
                   inputParams.push(value);
                }
             });

             $.ajax({
                type: "POST",
                dataType: "text",
                contentType: 'application/json',
                url: "insertRelationship.exec",
                async: true,
                data: JSON.stringify(
                   {
                      startNodeType: $('#entityType').val(),
                      endNodeType: inputParams[0].value,
                      required: inputParams[2].checked.toString(),
                      relationshipType: inputParams[1].value
                   }
                ),
                success: function (data) {
                   deleteRelationship(neo4jId);
                },
                error: function (jqXHR, textStatus, errorThrown) {
                   success = false;
                   //alert(jqXHR.status + ' ' + jqXHR.responseText);
                }
             })

          });

          function deleteRelationship(neo4jId)
          {
             var success = true;
             $.ajax({
                type: "DELETE",
                dataType: "text",
                url: 'deleteEntityRelationship.exec',
                data: neo4jId,
                async: true,
                success: function (result) {
                   if (success != false)
                   {
                      alert("Update Of Entity Successful!");
                   }
                },
                error: function (jqXHR, textStatus, errorThrown) {
//              alert(jqXHR.status + ' ' + jqXHR.responseText);
                }

             });
          }

          function buildRelationshipsTable(entity)
          {

             $('#edit-entity-rels-name').text('Edit ' + entity + ' Relationships');
             $.ajax({
                type: 'POST',
                contentType: 'text/plain',
                dataType: 'json',
                async: true,
                url: 'getRelationships.exec',
                data: entity,
                success: function (result) {

                   var relsTable = $('#edit-entity-rels-table');
                   relsTable.empty();

                   if (result.length > 0)
                   {
                      var html = '';
                      html += '<thead>' +
                         '<tr><th colspan="4" style="border:none;"><labeL style="font-size: 18px;">' +
                         'Edit Existing Relationships</label></th></tr>' +
                         '<tr class="bg-info"><th>Name</th><th>Type</th><th>Required</th><th></th></tr>' +
                         '</thead><tbody>';
                      for (i = 0; i < result.length; i++)
                      {
                         if (result[i].endNodeName != entity)
                         {
                            var entitySelectHtml = createEntitySelect(result[i].startNodeName, result[i].endNodeName, result[i].neo4jId);
                            var relTypeInputHtml = createInput(result[i].relationshipType, "text", '', 'form-control', result[i].neo4jId);
                            var relRequiredHtml = createInput("", "checkbox", result[i].required, 'required-box', result[i].neo4jId);
                            var relSaveBtnHtml = createBtn('Save', 'updateRel btn btn-success', result[i].neo4jId);
                            var relDeleteBtnHtml = createBtn('Delete', 'deleteRel btn btn-primary', result[i].neo4jId);
                            html += '<tr><td style="width:450px;">' + entitySelectHtml + '</td>' +
                               '<td>' + relTypeInputHtml + '</td>' +
                               '<td>' + relRequiredHtml + '</td>' +
                               '<td>' + relSaveBtnHtml + '&nbsp;' + relDeleteBtnHtml + '</td></tr>';
                         }
                      }
                      html += '</tbody>';

                      relsTable.append(html);
                   }

                },
                error: function (jqXHR, textStatus, errorThrown) {
//              alert(jqXHR.status + ' ' + jqXHR.responseText);
                }
             });
          }

          $('#edit-button').click(function (event) {
             event.preventDefault();

             $('#parameter-container').hide();
             $('#add-entity-container').hide();
             $('#edit-entity-container').show();
             $('#results-container').hide();
          });

          $('#add-button').click(function (event) {
             event.preventDefault();
             $('#parameter-container').hide();
             $('#edit-entity-container').hide();
             $('#results-container').hide();
             $('#add-entity-container').show();
          });

          $('#new-row-button').click(function (event) {
             event.preventDefault();
             $('#new-entity-list').append(
                '<tr>' +
                '<td><input class="form-control add-parameter"/></td>' +
                '<td><input type="checkbox" class="required-box"/></td>' +
                '</tr>'
             );
          });

          $('#delete-button').click(function (event) {

             var result = confirm("Want to delete?");
             if (result)
             {

                $.ajax({
                   type: "DELETE",
                   dataType: "text",
                   url: 'deleteEntity.exec',
                   async: true,
                   data: JSON.stringify({property: 'entityType', value: $('#entityType').find(':selected').text()}),
                   success: function (result) {
                      alert('Delete was successful!');
                      $('#dataAnalysis').trigger("click");
                   },
                   error: function (jqXHR, textStatus, errorThrown) {
//                alert(jqXHR.status + ' ' + jqXHR.responseText);
                   }

                });

             }
             return false;
          });

          $('#edit-save-button').click(function (event) {
             var editList = $('.edit-list-item');
             var success = true;

             editList.each(function (index, li) {
                var listItem = $(li);

                $.ajax({
                   type: "POST",
                   url: "updateEntity.exec",
                   async: true,
                   data: JSON.stringify({property: listItem.attr('id'), value: listItem.val()}),
                   dataType: "text",
                   success: function (data) {
                   },
                   error: function (jqXHR, textStatus, errorThrown) {
                      console.log("Error, status = " + textStatus + ", " +
                         "error thrown: " + errorThrown
                      );
                      success = false;
                   }
                });
             });

             if (success != false)
             {
                alert("Update Of Entity Successful!")
                $('#entityType').trigger("change");
             }

          });

          $('#edit-cancel-button').click(function (event) {

             $('#parameter-container').show();
             $('#add-entity-container').show();
             $('#edit-entity-container').hide();
             $('#results-container').show();
          });

          $('#new-save-button').click(function (event) {
             event.preventDefault();
             var saveList = $('.add-parameter');
             var name = $('#new-parameter-name').val();
             var isRoot = $('#is-root').prop('checked');
             var propertiesList = [];
             var fieldListString = "";
             var success = true;

             //create properties list
             propertiesList.push({name: 'Name', value: name});
             propertiesList.push({name: 'isRoot', value: isRoot});

             saveList.each(function (index, li) {
                var listItem = $(li);
                if (fieldListString != '')
                {
                   fieldListString = fieldListString.concat(',')
                }
                fieldListString = fieldListString.concat(listItem.val());
             });

             propertiesList.push({name: 'fieldList', value: fieldListString});
             //save the new node
             $.ajax({
                type: "POST",
                url: "saveEntity.exec",
                contentType: 'application/json',
                async: true,
                data: JSON.stringify({label: 'Metadata', properties: propertiesList}),
                dataType: "text",
                success: function (data) {

                },
                error: function (jqXHR, textStatus, errorThrown) {
                   console.log("Error, status = " + textStatus + ", " +
                      "error thrown: " + errorThrown
                   );
                   success = false;
                }
             });

             if (success != false)
             {
                alert("Creation Of Entity Successful!");
                $('#dataAnalysis').trigger("click");
                //$('#entityType').val(name);
                //$('#entityType').trigger("change");
             }
          });

          function getResults()
          {
             //$('#results').hide();
             $('#result-spinner').show();

             var analysisParameters = {
                entityType: '',
                additionalParameters: []
             };
             var parameters = [];
             currentParameters.forEach(function (parameter) {
                var p = {
                   property: parameter.property,
                   value: parameter.value()
                }

                parameters.push(p);
             });

             analysisParameters.additionalParameters = parameters;
             analysisParameters.entityType = $('#entityType').val();

             $.ajaxSetup({dataType: 'json', type: 'POST', contentType: 'application/json; charset=utf-8'})
             $('#results').load('runAnalysis.exec', JSON.stringify(analysisParameters));
             $('#results').show();
          };

          $('#results').click('.baseballCardLink', function (event) {
             if (event.target.getAttribute('data-entity'))
             {
                var baseballEntity = event.target.getAttribute('data-entity');
                var baseballName = event.target.getAttribute('data-name');

                localStorage.setItem("originalEntity", baseballEntity);
                localStorage.setItem("originalName", baseballName);

                $('#baseballCardModal').modal('show');

                var url = "baseballCard.exec?entity=" + encodeURIComponent(baseballEntity) + "&name=" + encodeURIComponent(baseballName);

                $("#baseballContent").load(url);
             }
          });

          $('#results').on('reload', function () {
             getResults();
          });
       });
    </script>

</head>
<body>
<div id="dataAnalysis-container" class="container">
    <div class="row">
        <div class="col-md-12">
                        <p> If exiting tab without updates, you must refresh your browser </p>

            <h1 class="page-title">Data Analysis</h1>
        </div>
    </div>
    <div class="row">
        <div class="col-md-12">
            <div class="panel">
                <div id="msg" class="alert hide"></div>
                <!--Selection DIV -->
                <div id="filters" class="top-border-accent-color2">
                    <div class="panel-body">
                        <div class="row">
                            <div class="col-md-12">
                                <div id="filter-container">
                                    <form class="form-horizontal">
                                        <div class="form-group" style="padding-top:20px; margin-bottom: -20px;">
                                            <label for="entityType" class="control-label col-md-2"
                                                   style="margin-left:-65px; font-weight: bold;">       Select Entity</label>
                                            <div class="col-md-4" style="margin-left:-5px">
                                                <select id="entityType" class="form-control" style="width:220px; background-color: white">
                                                    <option value="" disabled selected hidden>Select Entity...</option>
                                                    <c:forEach items="${entities}" var="entity">
                                                        <option value="${entity.name}">${entity.name}</option>
                                                    </c:forEach>
                                                </select>
                                            </div>
                                            <!--
                                            <button id="edit-button" class="btn btn-warning">Edit</button>
                                            <button id="add-button" class="btn btn-primary">Add New</button>
                                            <button id="delete-button" class="btn btn-primary">Delete</button>
                                            -->
                                        </div>
                                    </form>
                                    <div id="parameter-container" class="row"></div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-md-12">
            <div class="panel">
                <div id="results-container" class="top-border-accent-color3" style="padding:15px; height:710px">
                    <div class="panel-body">
                        <div id="results">
                            <p>Run the analysis to display results.</p>
                        </div>
                        <div id="result-spinner" class="pulser"></div>
                    </div>
                </div>
                <div id="add-entity-container">
                    <div id="add-entity-div" class="panel panel-primary">
                        <div class="panel-heading"><h4 id="new-entity-name">Add New Entity</h4></div>
                        <div class="panel-body">
                            <table class="table table-bordered">
                                <thead>
                                <tr class="bg-info">
                                    <td>Entity Name</td>
                                    <td>Is Root?</td>
                                </tr>
                                </thead>
                                <tbody>
                                <tr>
                                    <td><input class="form-control" id="new-parameter-name"/></td>
                                    <td><input type="checkbox" id="is-root"/></td>
                                </tr>
                                </tbody>
                            </table>
                            <h4>Parameters:</h4>
                            <table class="table table-bordered table-sm" id="new-entity-list">
                                <thead>
                                <tr class="bg-info">
                                    <td>Name</td>
                                    <td>Required?</td>
                                </tr>
                                </thead>
                                <tbody>
                                <tr>
                                    <td><input class="form-control add-parameter"/></td>
                                    <td><input type="checkbox" class="required-box"/></td>
                                </tr>
                                </tbody>
                            </table>
                            <button class="btn btn-success" id="new-row-button"><i class="glyphicon glyphicon-plus"></i>
                            </button>
                        </div>
                        <div class="panel-footer" style="text-align: right">
                            <button class="btn btn-success" id="new-save-button">Save</button>
                        </div>
                    </div>
                </div>

                <div id="edit-entity-container">
                    <div id="edit-entity-div" class="panel panel-warning">
                        <div class="panel-heading"><h4 id="edit-entity-name">Selected Entity Columns</h4></div>
                        <div class="panel-body">
                            <ul class="list-group" id="edit-entity-list"></ul>
                            <div class="panel-footer" style="text-align: right">
                                <button class="btn btn-success" id="edit-save-button">Save</button>&nbsp;&nbsp;<button
                                    class="btn btn-success" id="edit-cancel-button">Cancel
                            </button>
                            </div>
                        </div>
                    </div>
                    <div id="edit-entity-rels-div" class="panel panel-warning">
                        <div class="panel-heading"><h4 id="edit-entity-rels-name"></h4></div>
                        <div class="panel-body">
                            <div id="edit-entity-rels" style="padding-bottom:10px;height:500px;">
                                <table id="edit-entity-rels-table" class="table table-bordered"></table>
                                <br>
                                <table id="add-entity-rels-table" class="table table-bordered">
                                    <thead>
                                    <tr>
                                        <th colspan="5" style="border:none;"><labeL style="font-size: 18px;">Add New
                                            Relationships</label></th>
                                    </tr>
                                    <tr class="bg-info">
                                        <th>Name</th>
                                        <th>Type</th>
                                        <th>Required</th>
                                        <th></th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    <tr>
                                        <td id="add-entity-select" style="width:450px;"></td>
                                        <td><input type="text" class="form-control" placeholder="Type"/></td>
                                        <td><input type="checkbox" class="required-box"/></td>
                                        <td>
                                            <button class="btn btn-success">Save</button>&nbsp;<button
                                                class="btn btn-primary">Delete
                                        </button>
                                        </td>
                                    </tr>
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
</body>
</html>
