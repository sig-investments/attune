<head>
    <style>
        #baseballCardModal div.modal-dialog {
            width: 850px;
            margin: 5% auto;
        }

        #baseballContent {
            padding: 0px;
        }

        #baseballCardModal div div div.modal-header {
            color: white;
            background-color: transparent;
            position: relative;
            border: 0;
            padding: 15px 15px 5px 15px;
        }

        #baseballCardModal div div div.modal-header:after {
            width: 100%;
            display: block;
            text-align: center;
            /* background-color: white; */
            height: 1px;
            margin-top: 10px;
            content: '';
        }

        #baseballCardModal .modal-header button.close {
            margin-top: -0.4em;
            text-shadow: 0 0;
            margin-right: 0.1em;
            font-weight: normal;
            color: white;
            opacity: 1;
        }

        .modal-header button.close:hover {
            opacity: 0.75;
        }

        #baseballHeader {
            text-align: left;
            background-color: #1F3E48;
            color: white;
            /* background: url('./icons/baseball_card.png'); */
            background-repeat: no-repeat;
            background-size: cover;
            background-position-y: 70%;
            position: relative;
            border-bottom: thin solid #1F3E48;
        }

        #baseballHeader:before {
            position: absolute;
            background-color: #1F3E48;
            opacity: 0;
            top: 0;
            left: 0;
            width: 100%;
            height: 100%;
            content: '';
        }

        #newTitle {
            color: #B6B8B5;
        }

        #entity {
            text-transform: uppercase;
            display: inline-block;
        }

        #name {
            text-transform: capitalize;
            display: inline-block;
        }

        #baseballTitle {
            padding-top: 10px;
            padding-bottom: 35px;
            padding-left: 25px;
            padding-right: 25px;
            font-size: 30px;
            position: relative;
            width: 100%;
            font-weight: 100;
            overflow: hidden;
            text-overflow: ellipsis;
            white-space: nowrap;
        }

        #baseballTabRow {
            /* padding-left: 225px; */
            background-color: #B6B8B5;
        }

        #baseballNav {
            text-align: left;
            line-height: 30px;
            background-color: #f1f1ef;
            height: 545px;
            width: 225px;
            float: left;
            padding: 10px 20px;
            overflow-x: hidden;
            font-size: 17px;
            position: relative;
        }

        #propertiesSection:before {
            content: '';
            height: 90%;
            display: block;
            width: 1px;
            background-color: #ddd;
            position: absolute;
            top: 5%;
            left: 0;
        }

        #propertiesSection {
            position: relative;
            text-align: left;
            height: 545px;
            width: 625px;
            padding: 10px 20px;
            overflow: scroll;
            overflow-x: hidden;
            font-size: 17px;
            background-color: #f1f1ef;
        }

        #baseballCardModal a:link {
            text-decoration: none;
        }

        #baseballCardModal a:visited {
            text-decoration: none;
        }

        #propertiesList, #longPropertiesList {
            padding-left: 10px;
            margin: 0;
        }

        #longPropertiesList {
            margin-bottom: 20px;
        }

        #propertiesList li {
            float: left;
            width: 50%;
        }

        #baseballCardModal ul {
            list-style-type: none;
        }

        #baseballCardModal #parentLinks, #childrenLinks {
            margin-bottom: 20px;
        }

        #baseballCardModal #metadata-container {
            padding: 15px 15px 15px 15px;
        }

        #baseballCardModal #metadata-container a {
            border-color: rgba(195, 99, 48, 1);
            border-width: 2px 2px 0px 2px;
            border-style: solid;
            color: white;
            font-size: 1.4em;
            margin-right: 0px;
        }

        #baseballCardModal #metadata {
            height: 515px;
        }

        #baseballCardModal #metadata div canvas {
            position: absolute;
            left: 0px;
        }

        #breadcrumbList {
            display: inline-block;
            color: #C46E40;
            font-size: 16px;
            background-color: transparent;
            padding: 5px;
            cursor: pointer;
            vertical-align: top;
            padding-bottom: 20px;
            width: 95%;
        }

        #breadcrumb {
            color: #C46E40;
            overflow: hidden;
            text-overflow: ellipsis;
            display: -webkit-box;
            -webkit-box-orient: vertical;
            -webkit-line-clamp: 3;
            line-height: 22px;
            max-height: 66px;
        }

        #backArrow {
            height: 20px;
            line-height: 20px;
            vertical-align: middle;
            float: left;
            padding-left: 10px;
        }

        #breadcrumbList a {
            color: #C46E40;
        }

        #breadcrumbList a:hover {
            text-decoration: underline;
        }

        #block {
            display: block;
        }

        @media ( max-width: 768px) {
            #metadata-container li {
                width: 100% !important;
            }
        }

        #baseballTabRow li {
            margin-bottom: 0px;
            border-radius: 0;
        }

        #baseballTabRow li a {
            margin: 0px;
            font-size: 1.3em;
            color: #1F3E48;
            border: 0 !important;
            border-radius: 0;
            text-decoration: none;
            cursor: pointer;
        }

        #detailsViewTab {
            text-align: center;
            /* border-left: 1px solid white;
              border-top: 1px solid white;
              border-right: 1px solid white; */
            background-color: transparent;
            width: 20%;
        }

        #detailsViewTab.active a {
            background-color: #44B3F7;
        }

        #detailsViewTab a {
            background-color: transparent;
        }

        #detailsViewTab:not (.active ) a:hover {
            background-color: transparent;
        }

        #networkViewTab {
            text-align: center;
            width: 20%;
            /* border-left: 1px solid white;
              border-top: 1px solid white;
              border-right: 1px solid white; */
            background-color: transparent;
        }

        #networkViewTab.active a {
            background-color: #44B3F7;
        }

        #networkViewTab a {
            background-color: transparent;
        }

        #networkViewTab:not (.active ) a:hover {
            background-color: transparent;
        }

        #originalViewTab {
            padding-top: 5px;
            padding-bottom: 5px;
            padding-left: 15px;
            padding-right: 15px;
        }

        #originalViewButton {
            width: 1.75em;
            height: 1.75em;
            position: relative;
            float: right;
            margin-top: -2.1em;
            margin-right: 1.4em;
            background-color: transparent;
            padding-top: 5px;
            padding-right: 6px;
            padding-bottom: 1px;
            padding-left: 6px;
            cursor: pointer;
        }

        h3.baseball-property {
            font-size: 22px;
            width: 100%;
            overflow: hidden;
            white-space: nowrap;
            color: #444;
            font-weight: bold;
            text-overflow: ellipsis;
            /* border-bottom: thin solid #1F3E48; */
        }

        h3.baseball-property + p {
            font-size: 16px;
            color: #666;
        }

        h3.baseball-property + a {
            font-size: 16px;
        }

    </style>

    <script>
       var nodeList = null;
       var relationshipList = null;
       var entity = '${entity}';
       var name = '${name}';

       $(document).ready(function ()
       {
          buildBreadcrumbLink("${breadcrumbText}");

          $.getJSON("getAttributesMap.exec", {entity: entity, name: name}, function (map) {
             $.each(map, function (key, val) {
                var output = '';
                output += '<li><h3 class="baseball-property">';
                output += key;
                output += '</h3>';
                if (key == 'Attachments')
                {
                   output = '';
                   output += '<h3 class="baseball-property">';
                   output += key;
                   output += '</h3>';
                   output += '<a href="/attune/attachments/' + val + '" target="_blank">';
                   output += val;
                   output += '</a><br>';
                   output += '';

                   $("#attachmentsLinks").append(output);
                }
                else
                {
                   output += '<p>' + val + '</p>';
                   output += '</li>';
                   if (val.length > 30)
                      $("#longPropertiesList").append(output);
                   else
                   {
                      $("#propertiesList").append(output);
                   }

                }
             });
          });

          $.getJSON("getParentMap.exec", {entity: entity, name: name}, function (map) {
             if (!jQuery.isEmptyObject(map))
             {
                $.each(map, function (key, val) {
                   var output = '';
                   output += '<h3 class="baseball-property">';
                   output += key;
                   output += '</h3>';
                   $.each(val, function (key2, item) {
                      output += '<a href="#"  onclick="baseballCardLink(&apos;' + key.replace(/'/g, "\\'") + '&apos;, &apos;' + item.replace(/'/g, "\\'") + '&apos;)">';
                      output += item;
                      output += '</a><br>';
                      output += '';
                   });

                   $("#parentLinks").append(output);
                });
             }
          });

          $.getJSON("getChildrenMap.exec", {entity: entity, name: name}, function (map) {
             if (!jQuery.isEmptyObject(map))
             {
                $.each(map, function (key, val) {
                   var output = '';
                   output += '<h3 class="baseball-property">';
                   output += key;
                   output += '</h3>';
                   for (i = 0; i < val.length; i++)
                   {
                      output += '<a href="#" onclick="baseballCardLink(&apos;' + key.replace(/'/g, "\\'") + '&apos;, &apos;' + val[i].replace(/'/g, "\\'") + '&apos;)">';
                      output += val[i];
                      output += '</a><br>';
                   }
                   $("#childrenLinks").append(output);
                });
             }
          });


          $("#metadata-container").hide();
          $("#networkViewTab").click(function () {
             $("#baseballSection").hide();
             $.getJSON("getNetworkView.exec", {entity: entity, name: name}, function (data) {
                nodeList = titleizeArray(data.nodeList);
                relationshipList = titleizeArray(data.relationshipList);

                drawNetworkView();
             });

             // Title-izes each string in array.
             // Ex. old[0] = "abc def" -> new[0] = "Abc Def"
             function titleizeArray(strArray)
             {
                for (var i = 0; i < strArray.length; i++)
                {
                   strArray[i].data.name = strArray[i].data.name.titleize();
                }
                return strArray;
             }

             $("#metadata-container").show();
          });

          $("#detailsViewTab").click(function () {
             var url = "baseballCard.exec?entity=" + encodeURIComponent(entity) + "&name=" + encodeURIComponent(name)
                + "&breadcrumbText=" + encodeURIComponent("${breadcrumbText}");

             $("#baseballContent").load(url);
             $("#baseballSection").show();
             $("#metadata-container").hide();
          });

          $('[data-toggle="tooltip"]').tooltip()
       });

       function baseballCardLink(entity1, name1)
       {
          var url = "baseballCard.exec?entity=" + encodeURIComponent(entity1) + "&name=" + encodeURIComponent(name1)
             + "&breadcrumbText=" + encodeURIComponent("${breadcrumbText}");

          $("#baseballContent").load(url);
       }

       function originalLink()
       {

          var url = "baseballCard.exec?entity=" + encodeURIComponent(localStorage.getItem("originalEntity")) + "&name=" + encodeURIComponent(localStorage.getItem("originalName"));

          $("#baseballContent").load(url);
       }

       function buildBreadcrumbLink(text)
       {
          //split all the links by the seperators, >> and :, to create an array of entity, name, entity, name
          var allLinks = text.split('>>').join('>>').split(": ").join('>>').split('>>');

          var breadcrumbLink = "";
          for (var i = 0; i < allLinks.length; i += 2)
          {
             var entity = allLinks[i];
             var name = allLinks[i + 1];

             if (i == 0)
             {
                breadcrumbLink += "<a href='#' class=\"breadcrumbLink\" onclick=\"baseballCardLink('" + entity + "','" + name + "')\">"
                   + entity.toUpperCase() + ": " + name + "</a>";
             }
             else
             {
                breadcrumbLink += " > <a href='#' class=\"breadcrumbLink\" onclick=\"baseballCardLink('" + entity + "','" + name + "')\">"
                   + entity.toUpperCase() + ": " + name + "</a>";
             }
          }

          $("#breadcrumb").html(breadcrumbLink);
       }

       function goBack()
       {
          $(".breadcrumbLink").get($(".breadcrumbLink").length - 2).click();
       }

    </script>
</head>

<body>
<div id="baseballHeader">
    <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal"
                aria-label="Close">
            <span aria-hidden="true">&#x2573;</span>
        </button>
        <h4 id="newTitle" class="modal-title" align="left">
            <div id="entity">${entity}:</div>
            <div id="name">${name}</div>
        </h4>
    </div>

    <div id="breadcrumbList">
        <span id="backArrow" class="glyphicon glyphicon-triangle-left" onclick="goBack()"></span>
        <div class='col-md-11'>
            <div id="breadcrumb"></div>
        </div>
    </div>

    <ul class="nav nav-tabs" role="tablist" id="baseballTabRow">
        <li id="originalViewTab"><a id="originalViewButton" onclick="originalLink()" data-toggle="tooltip"
                                    data-placement="right" title="Original View"><span
                class="glyphicon glyphicon-home"></span></a></li>
        <li class='active' id="detailsViewTab" role="presentation"><a data-toggle="tab">Details View</a></li>
        <li class='inactive' id="networkViewTab" role="presentation"><a data-toggle="tab" aria-expanded="false">Network
            View</a></li>
    </ul>
</div>

<div class="tab-content">
    <div id="baseballSection">
        <div id="baseballNav">
            <h4><b>Supporting Entities</b></h4>
            <hr>
            <div id="parentLinks"></div>
            <div id="childrenLinks"></div>
            <div id="attachmentsLinks"></div>
        </div>
        <div id="propertiesSection">
            <ul id="longPropertiesList"></ul>
            <ul id="propertiesList"></ul>
        </div>
    </div>
    <div id="metadata-container">
        <div class="metadata-content">
            <div id="metadata" class="base-color1"></div>
        </div>
    </div>
</div>
</body>
