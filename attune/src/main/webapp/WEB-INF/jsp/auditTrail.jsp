<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jstl/core_rt"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Dashboard</title>

<style>
/** Main container **/
#dashboard-container {
	margin-right: 0px;
	margin-left: 0px;
	margin-top: 20px;
	width: 100%;
}

#dashboard-panel {
	margin-top: 10px;
}

.dashboard-tile {
	padding-bottom: 20px;
}

.tile-header {
	width: 100%;
	padding-top: 10px;
	padding-bottom: 10px;
	display: table;
	background-color: #f1f1ef;
}

.entityTitle {
	vertical-align: middle;
	margin: 0 auto;
	display: inline;
}

.entityTitle span {
	position: relative;
	vertical-align: middle;
	font-size: 3.1em;
	color: #252d32;
	margin-left: 25px;
	white-space: nowrap;
}

.entityTitle span small {
	position: absolute;
	top: 25%;
	font-size: 50%;
	color: #666;
}

.tile-content {
	margin-top: 20px;
	height: 20em;
}

.tile-content-title {
	display: table;
	height: 15%;
	width: 100.0668%;
	padding-top: 10px;
	padding-bottom: 10px;
	padding-left: 25px;
}

.tile-content-title h3 {
	color: #343f47;
	display: table-cell;
	vertical-align: middle;
	font-size: 1.3em;
}

.tile-content-graph {
	width: 100%;
	height: 85%;
	overflow: hidden;
	background-color: #f1f1ef;
}

#tabRow li {
	width: 16.6666667%;
	margin-bottom: 0px;
}

#tabRow li a {
	margin: 0px;
	font-size: 1.3em;
	color: #252d32;
	text-overflow: ellipsis;
	white-space: nowrap;
	overflow: hidden;
	text-align: center;
	border-radius: 0px;
	border-left: 0px;
	border-right: 0px;
}

#tabRow li a:focus {
	outline: 0;
}

#tab1 {
	background-color: #f1f1ef;
}

#tab1.active a {
	border-top-style: solid;
	border-top-color: #aec9df;
	border-top-width: 10px;
	border-right-color: #ddd;
	opacity: 1;
}

#tab1 a {
	border-top-style: solid;
	border-top-color: #aec9df;
	border-top-width: 10px;
	border-right-color: #ddd;
	opacity: 0.7;
}

#detailed-view-container div.tab-content {
	overflow-y: scroll;
	overflow-x: scroll;
	height: 22em;
	transition: all 0.5s ease;
	background-color: white;
}

#detailed-view-container [role="tabpanel"] {
	padding: 15px;
}

@media ( max-width : 992px) {
	#tabRow li {
		width: 33.333333333%
	}
	.entityTitle {
		width: 50%;
	}
}

@media ( max-width : 1024px) {
	.entityTitle {
		width: 70%;
	}
}

/** Target IE 10+ **/
@media screen and (-ms-high-contrast: active) , ( -ms-high-contrast :
	none) {
	.tile-content {
		height: 22em;
	}
	.entityTitle {
		vertical-align: middle;
		width: 80%;
		margin: 0 auto;
	}
}

/** Target Firefox **/
@
-moz-document url-prefix () { .tile-content {
	height: 20em;
}

.entityTitle {
	vertical-align: middle;
	width: 70%;
	margin: 0 auto;
}

}
#config {
	color: #666;
	float: right;
	font-size: 16px;
	height: 4em;
	width: 120px;
	background-color: #f1f1ef;
	border: 1px solid #cac9c9;
	margin-top: -7px;
}

tr.isAlert * {
	color: #D62728;
}

tr.entity-summary-table * {
	font-size: 15px;
}
</style>
<script type="application/javascript">
	$(document).ready(function ()
	{
		$('table').tablesorter();
	});
</script>

</head>
<body>

	<!------------------------------------------ Beginning of Login History page ----------------------------->
	<div id="dashboard-container" class="container">

		<div class="row">
			<div class="col-md-12">
				<h1 class="page-title-inline" style="position: relative">Audit Trail</h1>
			</div>
		</div>

		<div id="userList" class="top-border-accent-color1"
			  style="height:800px; background-color: #f1f1ef; padding: 25px; overflow-y: auto; overflow-x:hidden; margin-top:20px">
	
			<div class="row" style="margin-top: 15px; margin-bottom: 15px;">
				<div id="detailed-view-container" class="col-sm-12 ">

					<!-- Tab panes -->
					<div class="tab-content" style="height:100%;">
						<table class="table table-bordered table-striped">
							<thead>
								<tr>
									<th>User<div class="table-sorter-icon"></div></th>
									<th style="width:100px">Time<div class="table-sorter-icon"></div></th>
									<th>IP<div class="table-sorter-icon"></div></th>
									<th>Action<div class="table-sorter-icon"></div></th>
									<th>Original Value/Target User<div class="table-sorter-icon"></div></th>
									<th>Updated Value<div class="table-sorter-icon"></div></th>
								</tr>
							</thead>
							<tbody>
								<c:forEach items="${auditTrailList}" var="auditTrail">
									<tr>
										<td>${auditTrail.username}</td>
										<td>${auditTrail.time}</td>
										<td>${auditTrail.IP}</td>
										<td>${auditTrail.action}</td>
										<td>${auditTrail.originalValue}</td>
										<td>${auditTrail.updatedValue}</td>
									</tr>
								</c:forEach>
							</tbody>
						</table>

					</div>
				</div>
			</div>
		</div>
	</div>
	</div>
</body>
</html>