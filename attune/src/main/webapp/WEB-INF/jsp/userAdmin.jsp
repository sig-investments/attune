<%@ page language="java" contentType="text/html; charset=ISO-8859-1" pageEncoding="ISO-8859-1" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jstl/core_rt" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>

<html>
<head>
<style type="text/css">
   form
   {
      margin: 20px 0;
   }

   form input, button
   {
      padding: 5px;
   }

   table
   {
      width: 100%;
      margin-bottom: 20px;
      border-collapse: collapse;
   }

   table th, table td
   {
      padding: 8px;
      text-align: left;
   }

   #addUser
   {
      color: #666;
      float: right;
      font-size: 16px;
      height: 3.5em;
      width: 125px;
      background-color: #f1f1ef;
      border: 1px solid #cac9c9;
      margin-top: -7px;
      margin-bottom: 5px;
   }

   .form-button
   {
      color: #666;
      float: right;
      font-size: 16px;
      height: 3.5em;
      width: 125px;
      background-color: #f1f1ef;
      border: 1px solid #cac9c9;
      margin-top: -7px;
      margin-bottom: 5px;
   }

   #feedbackModal .modal-dialog
   {
      width: 300px;
      height: 400px;
   }

   /*form .highlight{*/
      /*background-color:#e2e2e2;*/
   /*}*/


   form .error{
      color: #c00;
   }

   form label{
      display:inline-block;
   }

   .col-md-6
   {
      width: 600px;
   }


   .col-md-8
   {
      width:450px;
   }

   .form-control
   {
      display: inline-block;
   }

   .control-label
   {
      width:120px;
   }

</style>

<script>

   var validator;

   $(document).ready(function ()
   {
      $('table').tablesorter();

      $("#save").click(function ()
      {
         validator = $("#userForm").validate(
         {
            rules:
            {
               username:
               {
                  remote: "usernameAvailable.exec?uuid=" + $("#uuid").val() + "&" + $("#username").val()
               },
               password:
               {
                  required:true,
						minlength: 8,
                  password:true,
						remote: "passwordAvailable.exec?&uuid=" + $("#uuid").val() +  "&" + $("#password").val()
               },
					phone:
               {
                  phoneUS: true
               },
               role:
               {
                  required:true
               },
               isActive:
               {
                  required:true
               },
            },
            messages:
            {
               username:
               {
                   required: "&nbsp;&nbsp;Username is required",
                   remote: "&nbsp;&nbsp;Username already taken"
               },
               password:
               {
                  required: "&nbsp;&nbsp;Password is required",
                  password: "&nbsp;&nbsp;Password must have both lower/upper case letters",
						remote: "&nbsp;&nbsp;Password has been used before"
               },
					phone:
               {
                  phoneUS: "&nbsp;&nbsp;Invalid phone number"
               },
               email:
               {
                  email: "&nbsp;&nbsp;Invalid email address"
               },
               role:
               {
                  required: "&nbsp;&nbsp;Role is required",
               },
               isActive:
               {
                  required: "&nbsp;&nbsp;Active Status is required",
               },

            },


            submitHandler: function()
            {
               var username;
               var data = {};
               $.each($('#userForm').serializeArray(), function (i, v)
               {
                  data[v.name] = v.value
                  if (v.name == 'username')
                     username = v.value;
               });

               $.ajax(
               {
                  type: "POST",
                  url: "saveUser.exec",
                  dataType: "json",
                  contentType: "application/json",
                  data: JSON.stringify(data),

                  success: function ()
                  {
                     userSaved(username);
                  },
               });
            }
         });
      });


      $( "#userForm" ).submit(function( event ) {
         event.preventDefault();
      });

      $("#addUser").click(function ()
      {
         if ( validator )
            validator.resetForm();

         $("#editHeader").html("Add New User");
         $("#userForm")[0].reset()
         $("#editPanel").show();
      });


      $("#cancel").click(function ()
      {
         $("#editPanel").hide();
      });


      $(".delete").click(function ()
      {
         var uuid = $(this).attr("uuid");
         var username = $(this).attr("username");

         bootbox.confirm(
         {
            size: "small",
            title: "Delete",
            message: "Are you sure you want to delete user " + username + "?",
            callback: function (result)
            {
               if (result)
               {
                  deleteUser(uuid, username);
               }
            }
         });
      });


      $(".edit").click(function ()
      {
			if ( validator )
				validator.resetForm();

			var uuid = $(this).attr("uuid");
         $("#editHeader").html("Edit User");
         $.get("getUser.exec?uuid=" + uuid, function (data)
         {
            $("#uuid").val(data.uuid);
            $("#username").val(data.username);
            $("#password").val(data.password);
            $("#firstName").val(data.firstName);
            $("#lastName").val(data.lastName);
            $("#phone").val(data.phone);
            $("#email").val(data.email);
            $("#role").val(data.role);
            if ( data.isActive )
               $("#isActive").val('true');
            else
               $("#isActive").val('false');

            $("#editPanel").show();
         });
      });

		$.validator.methods.email = function( value, element ) {
			return this.optional( element ) || /[a-z]+@[a-z]+\.[a-z]+/.test( value );
		};


		$.validator.methods.password = function( value, element ) {
			return this.optional(element) || /[a-z]+[A-Z]+/.test(value) || /[A-Z]+[a-z]+/.test(value);;
		};

//		jQuery.validator.addMethod("password", function(value, element) {
//			return this.optional(element) || /[a-z]+[A-Z]+/.test(value);
//		});

   });

   function deleteUser(uuid, username)
   {
      $.get("deleteUser.exec?uuid=" + uuid, function ()
      {
         bootbox.alert(
         {
            size: "small",
            title: "Delete",
            message: "AttuneUser " + username + " deleted.",
            callback: function ()
            {
               $('#main').empty().load("userAdmin.exec", function ()
               {
                  $('#main').fadeIn();
               });
            }
         })
      });
   }

   function userSaved(username)
   {
      bootbox.alert(
      {
         size: "small",
         title: "Save",
         message: "AttuneUser " + username + " saved.",
         callback: function ()
         {
            $('#main').empty().load("userAdmin.exec", function ()
            {
               $('#main').fadeIn();
            });
         }
      })
   }
</script>
</head>
<body>

<div id="userAdmin-container" class="container" style="margin-top:20px; width:100%">

   <div class="row" style="margin-bottom: 10px">
      <div class="col-md-12">
         <h1 class="page-title-inline">User Admin</h1>
         <button type="button" id="addUser">Add</button>
      </div>
   </div>

   <div id="userList" class="top-border-accent-color2"
        style="height:330px; background-color: #f1f1ef;padding: 25px; overflow-y: auto; overflow-x:hidden">
      <table class="table table-bordered" id="edit-entity-list">
         <caption style="margin-top:-10px">Total: ${fn:length(userList)}</caption>
         <thead style="font-weight: bold;">
         <tr>
            <td>Username
               <div class="table-sorter-icon"></div>
            </td>
            <td>First Name
               <div class="table-sorter-icon"></div>
            </td>
            <td>Last Name
               <div class="table-sorter-icon"></div>
            </td>
            <td>Phone
               <div class="table-sorter-icon"></div>
            </td>
            <td>Email
               <div class="table-sorter-icon"></div>
            </td>
            <td>Role
               <div class="table-sorter-icon"></div>
            </td>
            <td>Active
               <div class="table-sorter-icon"></div>
            </td>
            <td>Action</td>
         </tr>
         </thead>
         <tbody>
         <c:forEach items="${userList}" var="user">
            <tr>
               <td>${user.username}</td>
               <td>${user.firstName}</td>
               <td>${user.lastName}</td>
               <td>${user.phone}</td>
               <td>${user.email}</td>
               <td>${user.role}</td>
               <td>${user.isActive? 'Yes': 'No'}</td>
               <td>
                  <img src="images/edit.png" class="edit" uuid="${user.uuid}"
                       style="height: 15px;width: 15px; opacity: 0.5"> &nbsp;&nbsp;
                  <img src="images/clear.png" class="delete" uuid="${user.uuid}" username="${user.username}"></td>
            </tr>
         </c:forEach>
         </tbody>
      </table>
   </div>


   <div class="panel" id="editPanel" style="display: none;">
      <div id="edit-container" class="top-border-accent-color1" style="padding:15px; height:340px;margin-top:30px">
         <form id="userForm" class="form-horizontal">
            <input type="hidden" id="uuid" name="uuid" value="${user.uuid}">
            <h3 style="margin-top:-15px" id="editHeader"></h3>
            <div class="form-group" style="margin-bottom: -20px;">
               <div class="row" style="margin:10px 0px">
                  <div class="col-md-6" >
                     <label class="control-label col-md-4">Username</label>
                     <div class="col-md-8">
                        <input id="username" name="username" class="form-control"
                               style="background-color: white;width:220px;" required>
                     </div>
                  </div>
                  <div class="col-md-6">
                     <label class="control-label col-md-4">Password</label>
                     <div class="col-md-8">
                        <input id="password" name="password" class="form-control"
                               style="background-color: white;width:220px;" required>
                     </div>
                  </div>

               </div>

               <div class="row" style="margin:10px 0px">
                  <div class="col-md-6">
                     <label class="control-label col-md-4">First Name</label>
                     <div class="col-md-8">
                        <input id="firstName" class="form-control" name="firstName"
                               style="background-color: white;width:220px">
                     </div>
                  </div>
                  <div class="col-md-6">
                     <label class="control-label col-md-4">Last Name</label>
                     <div class="col-md-8">
                        <input id="lastName" class="form-control" name="lastName"
                               style="background-color: white;width:220px">
                     </div>
                  </div>
               </div>

               <div class="row" style="margin:10px 0px">
                  <div class="col-md-6">
                     <label class="control-label col-md-4">Phone</label>
                     <div class="col-md-8">
                        <input id="phone" class="form-control" name="phone" style="background-color: white;width:220px">
                     </div>
                  </div>
                  <div class="col-md-6">
                     <label class="control-label col-md-4">Email</label>
                     <div class="col-md-8">
                        <input id="email" class="form-control text email" name="email"
                               style="background-color: white;width:220px;">
                     </div>

                  </div>
               </div>

               <div class="row" style="margin:10px 0px">
                  <div class="col-md-6">
                     <label class="control-label col-md-4">Role</label>
                     <div class="col-md-8">
                        <select id="role" name="role" class="form-control"
                                style="width:220px; background-color: white" required>
                           <option value="">--Select One--</option>
                           <option value="admin">Admin</option>
                           <option value="user">User</option>
                        </select>
                        <span class="status"></span>
                     </div>
                  </div>

                  <div class="col-md-6">
                     <label class="control-label col-md-4">Active</label>
                     <div class="col-md-8">
                        <select id="isActive" name="isActive" class="form-control"
                                style="width:220px; background-color: white" required>
                           <option value="">--Select One--</option>
                           <option value=true>Yes</option>
                           <option value=false>No</option>
                        </select>
                     </div>
                  </div>
               </div>
            </div>

            <div class="row" style="margin:20px 0px; float:right">
               <div class="col-md-12" style="float:right">
                  <button type="submit" id="save" class="btn btn-default" style="width:120px">Save</button>
                  <button type="button" id="cancel" class="btn btn-default" style="width:120px">Cancel</button>
               </div>
            </div>

         </form>
      </div>
   </div>
</div>

<div class="modal fade" id="feedbackModal" role="dialog">
   <div class="modal-dialog" style="margin: 10% auto;">
      <div class="modal-content" id="compareModalContent">
         <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
               <span aria-hidden="true">&#x2573;</span>
            </button>
            <h4 class="modal-title" align="left">Save</h4>
         </div>
         <div class="modal-body">
            User information saved.
         </div>
         <div class="modal-footer" style="text-align: center;">
            <button style="color: white; width: 100px" type="button"
                    class="accent-color2" data-dismiss="modal" id="close">OK
            </button>
         </div>
      </div>

   </div>

</div>

</div>

</body>
</html>