<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
    <%@ taglib prefix="c" uri="http://java.sun.com/jstl/core_rt"%>
    <%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title></title>
</head>
<style>

#crud-container {
	margin-right: 0px;
    margin-left: 0px;
    margin-top: 0px;
    margin-bottom: 20px;
    width: 100%;
}

#crud {
	position: relative;
    width: 100%;
    height: 53em;
    margin: auto;
    padding: 20px;
    overflow: hidden;
    background-color: #f1f1ef;
    border-top: 15px solid #aec9df;
    border-bottom: 15px solid #aec9df;
}

.alert-error {
  padding: 15px;
  margin-bottom: 20px;
  border: 1px solid transparent;
  border-radius: 4px;
  background-color: #f2dede;
  border-color: #eed3d7;
  color: #b94a48;
}

</style>

<script>

$(document).ready(function()
{ 
	// change to inline mode
	$.fn.editable.defaults.mode = 'inline';
	$('.myeditable').editable();
    
  	// validation
  	$('#name').editable('option', 'validate', function(v) {if(!v) return 'This is a required field!';});
  	$('#fieldlist').editable('option', 'validate', function(v) {if(!v) return 'This is a required field!';});
  	$('#fieldlist').editable('option', 'validate', function(v) {if(v.indexOf(' ') > 0) return 'Remove spaces. Format is a comma separated list with no spaces.';});
  	$('#fieldlistreq').editable('option', 'validate', function(v) {if(v.indexOf(' ') > 0) return 'Remove spaces. Format is a comma separated list with no spaces.';});
  	
  	getEntities();
  	$('.entity').editable({disabled: true});
	  	
	$('#save-btn').click(function() {
	    
	  	var name = $('#name').text();
	  	var fieldlist = $('#fieldlist').text();
	  	var fieldlistreq = $('#fieldlistreq').text();
	  	var isroot = $('#isroot').text(); 
	  	
	    $('.myeditable').editable('submit', {
	    	url: 'insertEntity.exec',
	        ajaxOptions: {
	        	dataType: 'json',
	            data: { data:[name, fieldlist, fieldlistreq, isroot]},
	       	  	traditional: true,
	        },
	        success: function(result) {
  		    	var entityId = result["neo4j.id"];
  		    	$(this).editable('option', 'pk', entityId);
  		    	$(this).removeClass('editable-unsaved');
                //show messages
                var msg = 'Save was successful!';
                $('#msg').addClass('alert-success').removeClass('alert-error').removeClass('hide').html(msg).show();
                getEntities();                   
  		    },
  		  	error: function (errors) {
  			  	var msg = '';
              	if(errors && errors.responseText) { //ajax error, errors = xhr object
              		msg = errors.responseText;
              	} else { //validation error (client-side or server-side)
              		$.each(errors, function(k, v) { msg += k+": "+v+"<br>"; });
              	} 
              	$('#msg').removeClass('alert-success').addClass('alert-error').html(msg).show();
          	}
		});
	});
	  	
  	$('#reset-btn').click(function() {
  	    $('.myeditable').editable('setValue', null)  		//clear values
  	                    .editable('option', 'pk', null)     //clear pk
  	                    .removeClass('editable-unsaved');   //remove bold css
  	    $('#msg').hide();                
  	});
  	
  	$('#delete-btn').click(function() {
  		
  		var name = $('#name').text();
  		if (name) {
  			$.ajax({
  				dataType: "json",
  			  	url: 'deleteEntity.exec',
  			  	data: { type: name},
  			  	success: function(result) {
  			  		var msg = 'Delete was successful!';
                	$('#msg').addClass('alert-success').removeClass('alert-error').removeClass('hide').html(msg).show();
                	getEntities();
  				},
  				error: function (errors) {
  					debugger;
  					var msg = '';
  		           	if(errors && errors.responseText) { //ajax error, errors = xhr object
  		           		msg = errors.responseText;
  		           	} else { //validation error (client-side or server-side)
  		           		$.each(errors, function(k, v) { msg += k+": "+v+"<br>"; });
  		           	} 
  		           	$('#msg').removeClass('alert-success').addClass('alert-error').html(msg).show();
  		       	}
  			});     
  		}
  		else {
  			var msg = 'Select an entity to delete!';
        	$('#msg').addClass('alert-success').removeClass('alert-success').removeClass('hide').html(msg).show();
  		}
  		
  	});
		  	
	function getEntities() {
		$.ajax({
			dataType: "json",
		  	url: 'getEntities.exec',
		  	success: function(result) {
	        	showEntities(result);
			},
			error: function (errors) {
				var msg = '';
	           	if(errors && errors.responseText) { //ajax error, errors = xhr object
	           		msg = errors.responseText;
	           	} else { //validation error (client-side or server-side)
	           		$.each(errors, function(k, v) { msg += k+": "+v+"<br>"; });
	           	} 
	           	$('#msg').removeClass('alert-success').addClass('alert-error').html(msg).show();
	       	}
		});
	}
	
	function showDataByType(type, result) {

		$("#entityData").empty();
	
	   	var table = document.getElementById('entityData');
	   	var tbody = document.createElement("tbody");
	   	var tr = document.createElement("tr");
	   	
		$.ajax({
		  dataType: "json",
		  url: 'getEntityProperties.exec',
		  data: { type: type },
		  success: function(result) {
	         	if (result) {
	         		$.each(result, function (index, entity) {

	         		});
	         	}
			},
			error: function (errors) {
				var msg = '';
	           	if(errors && errors.responseText) { //ajax error, errors = xhr object
	           		msg = errors.responseText;
	           	} else { //validation error (client-side or server-side)
	           		$.each(errors, function(k, v) { msg += k+": "+v+"<br>"; });
	           	} 
	           	$('#msg').removeClass('alert-success').addClass('alert-error').html(msg).show();
	       	}
		});
	
		
	}
	  
	function showEntities(result) {
		
		$("#entities").empty();
		
       	var table = document.getElementById('entities');
       	var tbody = document.createElement("tbody");
       	var tr = document.createElement("tr");
       	
       	var tdNameHdr = document.createElement("td");
       	tdNameHdr.appendChild(document.createTextNode("Name"));
       	tdNameHdr.style.cssText = "font-weight:bold;";
       	tr.appendChild(tdNameHdr);
       	
       	var tdFieldsHdr = document.createElement("td");
       	tdFieldsHdr.appendChild(document.createTextNode("Fields"));
       	tdFieldsHdr.style.cssText = "font-weight:bold;";
       	tr.appendChild(tdFieldsHdr);
       	
       	var tdFieldsReqHdr = document.createElement("td");
       	tdFieldsReqHdr.appendChild(document.createTextNode("Required Fields"));
       	tdFieldsReqHdr.style.cssText = "font-weight:bold;";
       	tr.appendChild(tdFieldsReqHdr);
       	
       	var tdIsRootHdr = document.createElement("td");
       	tdIsRootHdr.appendChild(document.createTextNode("Root"));
       	tdIsRootHdr.style.cssText = "font-weight:bold;";
       	tr.appendChild(tdIsRootHdr);
       	
       	tbody.appendChild(tr);
   	    table.appendChild(tbody);
       	
       	$.each(result, function (index, entity) {
       		
       		var tr = document.createElement("tr");
       		
       		var tdName = document.createElement("td");
       		tdName.appendChild(createAnchor(entity["neo4j.id"], entity.Name, entity.Name));
       		tr.appendChild(tdName);
       		
       		var tdFields = document.createElement("td");
       		tdFields.appendChild(createAnchor(entity["neo4j.id"], entity.fieldList, entity.Name));
       		tr.appendChild(tdFields);
       		
       		var tdFieldsReq = document.createElement("td");
       		if (entity.requiredFields) {
       			tdFieldsReq.appendChild(createAnchor(entity["neo4j.id"], entity.requiredFields, entity.Name));
       		} else {
       			tdFieldsReq.appendChild(document.createTextNode(""));
       		}
       		tr.appendChild(tdFieldsReq);
       		
       		var tdIsRoot = document.createElement("td");
       		if (entity.isRoot) {	         			
       			tdIsRoot.appendChild(createAnchor(entity["neo4j.id"], entity.isRoot, entity.Name));		         			
       		} else {
       			tdFieldsReq.appendChild(document.createTextNode(""));
       		}
       		tr.appendChild(tdIsRoot);
       		
       		tbody.appendChild(tr);
       	    table.appendChild(tbody);
       	    
    	});
	}
		
	function createAnchor(id, text, entityType) {
		var anchor = document.createElement("a");
        var anchorText = document.createTextNode(text);
        anchor.setAttribute("name", entityType)
        anchor.setAttribute('href', "#insert");
        anchor.setAttribute('id', id); //entity id
        anchor.setAttribute('class', 'entity editable editable-click')
        anchor.setAttribute('data-type', 'text');
        anchor.onclick = function() { selectEntity(this) };
        anchor.appendChild(anchorText);
        return anchor;
	}
	
	function selectEntity(anchor) {
		
		$('#msg').hide(); 
		
		// get entity
		$.ajax({
		  dataType: "json",
		  url: 'getEntity.exec',
		  data: { type: anchor.name },
		  success: function(result) {
	         	if (result) {
	         		$('#name').editable('option', 'pk', anchor.id)    
	         		$('#name').editable('setValue',result.Name);
	         		$('#fieldlist').editable('setValue',result.fieldList);
	        	  	if (result.requiredFields) { $('#fieldlistreq').editable('setValue',result.requiredFields); } else { $('#fieldlistreq').editable('setValue',""); }
	        	  	if (result.isRoot) { $('#isroot').editable('setValue',result.isRoot); } else { $('#isroot').editable('setValue',""); }
	         	}
			},
			error: function (errors) {
				var msg = '';
	           	if(errors && errors.responseText) { //ajax error, errors = xhr object
	           		msg = errors.responseText;
	           	} else { //validation error (client-side or server-side)
	           		$.each(errors, function(k, v) { msg += k+": "+v+"<br>"; });
	           	} 
	           	$('#msg').removeClass('alert-success').addClass('alert-error').html(msg).show();
	       	}
		});
		
		// get data for selected entity type
		$.ajax({
		  dataType: "json",
		  url: 'getNodesByType.exec',
		  data: { type: anchor.name },
		  success: function(result) {
	         	if (result) {
	         		debugger;
	         		showDataByType(anchor.name, result);
	         	}
			},
			error: function (errors) {
				var msg = '';
	           	if(errors && errors.responseText) { //ajax error, errors = xhr object
	           		msg = errors.responseText;
	           	} else { //validation error (client-side or server-side)
	           		$.each(errors, function(k, v) { msg += k+": "+v+"<br>"; });
	           	} 
	           	$('#msg').removeClass('alert-success').addClass('alert-error').html(msg).show();
	       	}
		});
	}

	
});
</script>
<body>
	<div id="crud-container" class="container">
		<div class="row">
			<div class="tab-content">
				<div id="crud" style="height: 100%">
					<div id="insert" style="width: 75%;">
						<div id="msg" class="alert hide"></div>
						<h1 class="page-title">Add/Edit</h1>
						<table id="entity" class="table table-bordered table-striped">
							<tbody>
								<tr>
									<td width="25%" style="font-weight: bold">Name</td>
									<td><a href="#" class="myeditable" id="name"
										data-type="text" data-name="name"
										data-original-title="Entity name"></a></td>
								</tr>
								<tr>
									<td style="font-weight: bold">Fields</td>
									<td><a href="#" class="myeditable" id="fieldlist"
										data-type="textarea" data-name="fieldList"
										data-original-title="Fields"></a></td>
								</tr>
								<tr>
									<td style="font-weight: bold">Required Fields</td>
									<td><a href="#" class="myeditable" id="fieldlistreq"
										data-type="textarea" data-name="requiredFields"
										data-original-title="Required fields"></a></td>
								</tr>
								<tr>
									<td style="font-weight: bold">Root</td>
									<td><a href="#" class="myeditable" id="isroot"
										data-type="text" data-name="isRoot"
										data-original-title="Is root entity?"></a></td>
								</tr>
							</tbody>
						</table>
						<div style="padding-top: 5px;">
							<button id="save-btn" class="save-btn btn btn-primary">Save</button>
							<button id="delete-btn" class="save-btn btn pull-right"
								style="margin-left: 5px;">Delete</button>
							<button id="reset-btn" class="reset-btn btn pull-right">New</button>
						</div>
					</div>
					<div id="update" style="width: 75%;">
						<h1 class="page-title">Entities</h1>
						<table id="entities" class="table table-bordered table-striped"></table>
					</div>
					<br />
					<div id="view" style="width: 75%;">
						<h1 class="page-title">Data</h1>
						<table id="entityData" class="table table-bordered table-striped"></table>
					</div>
				</div>
			</div>
		</div>
	</div>
</body>
</html>