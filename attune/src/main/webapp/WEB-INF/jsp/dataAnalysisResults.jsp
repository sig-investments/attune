<%@ page language="java" contentType="text/html; charset=ISO-8859-1" pageEncoding="ISO-8859-1" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jstl/core_rt" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>

<html>
<head>
    <style type="text/css">
        form {
            margin: 20px 0;
        }

        form input, button {
            padding: 5px;
        }

        table {
            width: 100%;
            margin-bottom: 20px;
            border-collapse: collapse;
        }

        table th, table td {
            padding: 8px;
            text-align: left;
        }

    </style>
    <script>
       $(document).ready(function () {
          $('table').tablesorter();

          $('.editable-td').hide();

          $('#add-new-button').hide();

          $('#update-node-button').hide();
          $('#delete-node-button').hide();
          $('#addNewEntDiv').hide();
          $('#cancel').hide();
          $('#updateExistingDiv').hide();



          var showInsertFrm = true;
          // selected values of parentSelect
          var parentSelVals = "";
          var childSelVals = "";

          var parentLabel = "";
          var childLabel = "";

          $('#edit-property-rows').click(function (event) {
             event.preventDefault();

             $('.noneditable-td').hide();
             $('.editable-td').show();

             $('#add-new-button').show();
             $('#update-node-button').show();
             $('#delete-node-button').show();

             $('#edit-entity-container').hide();

             if (showInsertFrm == true)
             {
                $.ajax({
                   type: 'POST',
                   dataType: 'json',
                   url: 'getEntityProperties.exec',
                   async: true,
                   data: $(entityType).val(),
                   success: function (result) {
                      createNewEntityForm(result);
                   },
                   error: function (jqXHR, textStatus, errorThrown) {
                      alert(jqXHR.status + ' ' + jqXHR.responseText);
                   }
                });
             }
          });


          $('#addNew').click(function() {
            $('#addNewEntDiv').show();
            $('#cancel').show();
            $('#updateRelationship').hide()
            $('#addNew').hide();
          });

          $('#cancel').click(function() {
            $('#addNewEntDiv').hide();
            $('#cancel').hide();
            $('#updateRelationship').show()
            $('#addNew').show();
            var node = document.getElementById('modalBody');

            while (node.firstChild) {
              node.removeChild(node.firstChild);
            }
          });

          $('#relUpdateCancel').click(function() {
            //$('#relUpdateCancel').hide();
            //$('#updateRelSelect').hide();
            $('#updateExistingDiv').hide();
            $('#editButtons').show();
          });

          $('#updateRelationship').click(function() {
            //$('#updateRelSelect').show();
            //$('#relUpdateCancel').show();
            $('#updateExistingDiv').show();
            $('#editButtons').hide();

            var table = document.getElementById('edit-entity-list');
            var newtbl = document.getElementById('addNewRow');
            var addnew = false;
            var duplicate = false;
            var notEmpty = false;
            var newEntFieldVals = "";
            var nid = "";
            var nidArr = [];
            var ents = "${analysisResult.entities}";
            var select = document.getElementById("updateRelSelect");

            var opt = document.createElement("option");
            opt.text = "----";
            opt.label = "----";
            select.add(opt);
            for (var i = 1, row; row = table.rows[i]; i++) {
                  var option = document.createElement("option");
                  option.text = table.rows[i].cells[0].children[0].value;
                  option.label = table.rows[i].cells[0].children[0].value;
                  select.add(option);

            }

            $('#updateRelSelect').change(function() {
              var selVal = select.options[select.selectedIndex].value;
              if (selVal != "----"){
                updateRelationToEntity(selVal);
              }
            });
          });

          // EDIT RELATIONSHIPS
          function updateRelationToEntity(nameOfEntityInstance) {
            $('#myModal').modal('show');
            $.getJSON("getMetadata.exec", function(data) {
              nodeList = titleizeArray(data.nodeList);
              relationshipList = titleizeArray(data.relationshipList);
              var lab = $(entityType).val();
              parentList = [];
              childList = [];

              // Get relationships of Entity Labels from Metadata view
              for (var i = 0; i < relationshipList.length; i++) {
/*
                console.log("name: " + relationshipList[i].data.name);
                console.log("entity: " + relationshipList[i].data.entity);
                console.log("source: " + relationshipList[i].data.source);
                console.log("target: " + relationshipList[i].data.target);
*/
                if (relationshipList[i].data.source == lab) {
                  childList.push(relationshipList[i].data.target);
                } else if(relationshipList[i].data.target == lab) {
                  parentList.push(relationshipList[i].data.source);
                }
              }

              // Add parentSelect
              var node = document.getElementById('modalBody');

              var newNode = document.createElement('p');
              while (node.firstChild) {
                node.removeChild(node.firstChild);
              }

              // Only Show Parent Select if there is a parent relationship
              if (parentList.length > 0) {
                var parentSelect = document.createElement('select');
                parentSelect.multiple = "multiple";
                parentSelect.style.width = "250px";
                newNode.appendChild(document.createTextNode('Add Parent relationship'));
                node.appendChild(newNode);

              // For each parent Label, Get list of instances
              for (var i = 0; i < parentList.length; i++) {
                  // Send request to get ent list for label
                  var retData = "";
                  $.ajax({
                     type: "POST",
                     url: "getEntNames.exec",
                     async: false,
                     data: JSON.stringify({name: "label", value: parentList[i]}),
                     dataType: "text",
                     success: function (data) {
                        retData = data;
                        console.log(retData);
                     },
                     error: function (jqXHR, textStatus, errorThrown) {
                        console.log("Error, status = " + textStatus + ", " +
                           "error thrown: " + errorThrown
                        );
                        success = false;
                     }
                  });

                  var retArr = retData.split(',');
                  var optgrp = document.createElement('optgroup');
                  optgrp.text = parentList[i];
                  optgrp.label = parentList[i];

                  var relationships = "";

                  // Get parent relationship list
                  $.ajax({
                     type: "POST",
                     url: "getParentRelationshipsOfNode.exec",
                     async: false,
                     data: JSON.stringify({name: lab, value: nameOfEntityInstance}),
                     dataType: "text",
                     success: function (data) {
                        relationships = data;
                     },
                     error: function (jqXHR, textStatus, errorThrown) {
                        console.log("Error, status = " + textStatus + ", " +
                           "error thrown: " + errorThrown
                        );
                        success = false;
                     }
                  });
                  var relationships = relationships.slice(0, -1);
                  var parentRelArr = relationships.split(",");
                  // add options to optgroup
                  // Pre-select/highlight existing relationships
                  for (var j = 0; j < retArr.length; j++) {
                    var option = document.createElement('option');
                    option.text = retArr[j];
                    option.label = retArr[j];
                    for (var k = 0; k < parentRelArr.length; k++) {
                      if (retArr[j] == parentRelArr[k]) {
                        option.selected = "selected";
                      }
                    }
                    optgrp.appendChild(option);
                  }

                  parentSelect.add(optgrp);

              }
              node.appendChild(parentSelect);
            }

              // Add Child Select if Children exist
              // The rest is the same logic for parent above
              if (childList.length > 0) {
                var cnode = document.getElementById('modalBody');
                var cnewNode = document.createElement('p');
                var childSelect = document.createElement('select');
                childSelect.multiple = 'multiple';
                childSelect.style.width = "250px"
                cnewNode.appendChild(document.createTextNode('Add Child relationship'));
                cnode.appendChild(cnewNode);

              for (var i = 0; i < childList.length; i++) {
                var retData = "";
                $.ajax({
                   type: "POST",
                   url: "getEntNames.exec",
                   async: false,
                   data: JSON.stringify({name: "label", value: childList[i]}),
                   dataType: "text",
                   success: function (data) {
                      retData = data;
                      console.log(retData);
                   },
                   error: function (jqXHR, textStatus, errorThrown) {
                      console.log("Error, status = " + textStatus + ", " +
                         "error thrown: " + errorThrown
                      );
                      success = false;
                   }
                });

                  var retArr = retData.split(',');
                  var optgrp = document.createElement('optgroup');
                  optgrp.text = childList[i];
                  optgrp.label = childList[i];

                  var relationships = "";

                  $.ajax({
                     type: "POST",
                     url: "getChildRelationshipsOfNode.exec",
                     async: false,
                     data: JSON.stringify({name: lab, value: nameOfEntityInstance}),
                     dataType: "text",
                     success: function (data) {
                        relationships = data;
                        //alert("got child relationships");
                     },
                     error: function (jqXHR, textStatus, errorThrown) {
                        console.log("Error, status = " + textStatus + ", " +
                           "error thrown: " + errorThrown
                        );
                        //console.log("${entNamesByLabel}");
                        success = false;
                     }
                  });
                  var relationships = relationships.slice(0, -1);
                  var childRelArr = relationships.split(",");
                  // add options to child menu
                  for (var j = 0; j < retArr.length; j++) {
                     var option = document.createElement('option');
                     option.text = retArr[j];
                     option.label = retArr[j];
                     for (var k = 0; k < childRelArr.length; k++) {
                       console.log("##comparing");
                       console.log("ret: " + retArr[j]); console.log(childRelArr[k]);
                       if (retArr[j] == childRelArr[k]) {
                         option.selected = "selected";
                       }
                     }
                     optgrp.appendChild(option);
                  }
                  childSelect.add(optgrp);
              }
                cnode.appendChild(childSelect);
              }


              // Get Selected relationships
              // Title-izes each string in array.
              // Ex. old[0] = "abc def" -> new[0] = "Abc Def"
              function titleizeArray(strArray) {
                for (var i = 0; i < strArray.length; i++) {
                  strArray[i].data.name = strArray[i].data.name.titleize();
                }
                return strArray;
              }

              // Ok button is for the edit relationships modal
              $('#okButton').click(function() {
                // Get Selected relationships for parent and child
                var label = $(entityType).val();
                if (parentList.length > 0) {
                  parentSelVals = "";
                for (var i = 0; i < parentSelect.length; i++) {
                  if (parentSelect.options[i].selected) {
                    parentSelVals = parentSelVals.concat(parentSelect.options[i].value + ",");
                    parentLabel = parentSelect.options[i].parentNode.label;
                  }
                }
              }
                if (childList.length > 0){
                childSelVals = "";
                for (var i = 0; i < childSelect.length; i++) {
                  if (childSelect.options[i].selected) {
                    childSelVals = childSelVals.concat(childSelect.options[i].value + ",");
                    childLabel = childSelect.options[i].parentNode.label;
                  }
                }
              }
                // add Parent relationships-- make ajax callRestService
                // Make post call to update parent relationships
                if (parentList.length > 0 && parentSelVals) {
                parentSelVals = parentSelVals.slice(0, -1);
                $.ajax({
                   type: "POST",
                   dataType: "text",
                   contentType: 'application/json',
                   url: 'addEntRelationships.exec',
                   async: false,
                   data: JSON.stringify(
                      {
                         name: nameOfEntityInstance + "," + label + "," + parentLabel,
                         value: parentSelVals
                      }),
                   success: function (result) {
                      $('#results').trigger('reload');
                   },
                   error: function (jqXHR, textStatus, errorThrown) {
                      alert(jqXHR.status + ' ' + jqXHR.responseText);
                   }
                 });
               }

                 //make call to add child relationship
                 if (childList.length > 0 && childSelVals) {
                 childSelVals = childSelVals.slice(0, -1);
                 $.ajax({
                    type: "POST",
                    dataType: "text",
                    contentType: 'application/json',
                    url: 'addChildRelationships.exec',
                    async: false,
                    data: JSON.stringify(
                       {
                          name: nameOfEntityInstance + "," + label + "," + childLabel,
                          value: childSelVals
                       }),
                    success: function (result) {
                       $('#results').trigger('reload');
                    },
                    error: function (jqXHR, textStatus, errorThrown) {
                       alert(jqXHR.status + ' ' + jqXHR.responseText);
                    }
                  });
                }
                $('#updateExistingDiv').hide();
                $('#editButtons').show();
                //location.reload(true);

              });

              function getResults(entLabel) {
                var analysisParameters = {
                   entityType: '',
                   additionalParameters: []
                };
                var parameters = [];
                analysisParameters.additionalParameters = parameters;
                analysisParameters.entityType = entLabel;
                console.log("entlabel in results: " + entLabel);
                $.ajaxSetup({dataType: 'json', type: 'POST', contentType: 'application/json; charset=utf-8'})
                $('#modalBody').load('runAnalysis2.exec', JSON.stringify(analysisParameters));
              }
            });
          }

          $('#entRelationships').click(function() {
            $('#myModal').modal('show');
            $.getJSON("getMetadata.exec", function(data) {
              nodeList = titleizeArray(data.nodeList);
              relationshipList = titleizeArray(data.relationshipList);
              var lab = $(entityType).val();
              parentList = [];
              childList = [];
              for (var i = 0; i < relationshipList.length; i++) {
                if (relationshipList[i].data.source == lab) {
                  childList.push(relationshipList[i].data.target);
                } else if(relationshipList[i].data.target == lab) {
                  parentList.push(relationshipList[i].data.source);
                }
              }

              // Add parentSelect
              var node = document.getElementById('modalBody');
              var newNode = document.createElement('p');
              while (node.firstChild) {
                node.removeChild(node.firstChild);
              }

              if (parentList.length > 0) {
                var parentSelect = document.createElement('select');
                parentSelect.multiple = "multiple";
                parentSelect.style.width = "250px";
                newNode.appendChild(document.createTextNode('Add Parent relationship'));
                node.appendChild(newNode);

              for (var i = 0; i < parentList.length; i++) {
                  // Send request to get ent list for label
                  var retData = "";
                  $.ajax({
                     type: "POST",
                     url: "getEntNames.exec",
                     async: false,
                     data: JSON.stringify({name: "label", value: parentList[i]}),
                     dataType: "text",
                     success: function (data) {
                        retData = data;
                        console.log(retData);
                     },
                     error: function (jqXHR, textStatus, errorThrown) {
                        console.log("Error, status = " + textStatus + ", " +
                           "error thrown: " + errorThrown
                        );
                        success = false;
                     }
                  });
                  var retArr = retData.split(',');
                  var optgrp = document.createElement('optgroup');
                  optgrp.text = parentList[i];
                  optgrp.label = parentList[i];

                  // add options to optgroup
                  for (var j = 0; j < retArr.length; j++) {
                    var option = document.createElement('option');
                    option.text = retArr[j];
                    option.label = retArr[j];
                    optgrp.appendChild(option);
                  }

                  parentSelect.add(optgrp);

                  // add options to optgroup




              }
              node.appendChild(parentSelect);
            }

              // Add Child Select
              if (childList.length > 0) {
                var cnode = document.getElementById('modalBody');
                var cnewNode = document.createElement('p');
                var childSelect = document.createElement('select');
                childSelect.multiple = 'multiple';
                childSelect.style.width = "250px";
                cnewNode.appendChild(document.createTextNode('Add Child relationship'));
                cnode.appendChild(cnewNode);

              for (var i = 0; i < childList.length; i++) {
                var retData = "";
                $.ajax({
                   type: "POST",
                   url: "getEntNames.exec",
                   async: false,
                   data: JSON.stringify({name: "label", value: childList[i]}),
                   dataType: "text",
                   success: function (data) {
                      retData = data;
                      console.log(retData);
                   },
                   error: function (jqXHR, textStatus, errorThrown) {
                      console.log("Error, status = " + textStatus + ", " +
                         "error thrown: " + errorThrown
                      );
                      success = false;
                   }
                });

                var retArr = retData.split(',');

                console.log("adding option : " + childList[i]);
                  var optgrp = document.createElement('optgroup');
                  optgrp.text = childList[i];
                  optgrp.label = childList[i];

                  // add options to child menu
                  for (var j = 0; j < retArr.length; j++) {
                     var option = document.createElement('option');
                     option.text = retArr[j];
                     option.label = retArr[j];
                     optgrp.appendChild(option);
                  }
                  childSelect.add(optgrp);
              }
              cnode.appendChild(childSelect);
            }


              // Get Selected relationships
              // Title-izes each string in array.
              // Ex. old[0] = "abc def" -> new[0] = "Abc Def"
              function titleizeArray(strArray) {
                for (var i = 0; i < strArray.length; i++) {
                  strArray[i].data.name = strArray[i].data.name.titleize();
                }
                return strArray;
              }

              $('#okButton').click(function() {
                if (parentList.length > 0) {
                  parentSelVals = "";
                for (var i = 0; i < parentSelect.length; i++) {
                  if (parentSelect.options[i].selected) {
                    parentSelVals = parentSelVals.concat(parentSelect.options[i].value + ",");
                    parentLabel = parentSelect.options[i].parentNode.label;
                  }
                }
              }
                if (childList.length > 0) {
                  childSelVals = "";
                for (var i = 0; i < childSelect.length; i++) {
                  if (childSelect.options[i].selected) {
                    childSelVals = childSelVals.concat(childSelect.options[i].value + ",");
                    childLabel = childSelect.options[i].parentNode.label;
                  }
                }
              }
              });

              function getResults(entLabel) {
                var analysisParameters = {
                   entityType: '',
                   additionalParameters: []
                };
                var parameters = [];

                analysisParameters.additionalParameters = parameters;
                analysisParameters.entityType = entLabel;
                $.ajaxSetup({dataType: 'json', type: 'POST', contentType: 'application/json; charset=utf-8'})
                $('#modalBody').load('runAnalysis2.exec', JSON.stringify(analysisParameters));
              }

            });
          });




          $('#save').click(function() {
              console.log("parent selvals: " + parentSelVals);
              var properties = [];
              var propDict = [];
              var table = document.getElementById('edit-entity-list');
              var newtbl = document.getElementById('addNewRow');
              var addnew = false;
              var duplicate = false;
              var notEmpty = false;
              var newEntFieldVals = "";
              var nid = "";
              var nidArr = [];
              var ents = "${analysisResult.entities}";
              for (var i = 0, row; row = table.rows[i]; i++) {
                //iterate through rows
                //rows would be accessed using the "row" variable assigned in the for loop
                for (var j = 0, col; col = row.cells[j]; j++) {
                  //iterate through columns
                  //columns would be accessed using the "col" variable assigned in the for loop
                var inp =  col.children[0];
                  if (i ==0) {
                    properties.push(inp.innerText); //offset button
                    if (j == 0) {
                      // must check j+1 to offset relationship button
                     if (newtbl.rows[0].cells[j+1].children[0].value){
                       notEmpty = true;
                       newEntFieldVals = newEntFieldVals.concat(newtbl.rows[0].cells[j+1].children[0].value);
                     } else {
                       notEmpty = false;
                     }
                    } else {
                       newEntFieldVals = newEntFieldVals.concat("," + newtbl.rows[0].cells[j+1].children[0].value);
                    }
                  } else {
                    var val = inp.value;

                    if (j == 0) {
                      nid = val;
                      nidArr.push(nid);
                    }

                    var label = $(entityType).val();

                    var prop = label + "," + val + "," + nid + "," + properties[j] + "," + col.id;
                    var upsuccess = true;
                    $.ajax({
                       type: "POST",
                       url: "updateEntity.exec",
                       async: true,
                       data: JSON.stringify({property: prop, value: properties[0]}),
                       dataType: "text",
                       success: function (data) {
                       },
                       error: function (jqXHR, textStatus, errorThrown) {
                          console.log("Error, status = " + textStatus + ", " +
                             "error thrown: " + errorThrown
                          );
                          success = false;
                          upsuccess = false;
                       }
                    });

                  }
                }
              }
              if (upsuccess) alert("Updating Entity: Successful");
              //Check if add new div is visible
              for (var i = 1; i < nidArr.length; i++) {
                nent = newEntFieldVals.split(",");
                if (nidArr[i] == nent[0]) {
                  console.log(nidArr[i]);
                  console.log("nent: " + nent[0]);
                  duplicate = true;
                }
              }
              if (!$('addNewEntDiv').is(":hidden") && !duplicate && notEmpty) {
                // add new node to the db
                var lab = $(entityType).val();
                var propValArr = newEntFieldVals.split(",");
                var propNameArr = properties;
                var propList = [];

                for (var idx = 0; idx < propNameArr.length; idx++) {
                  propList.push({name: propNameArr[idx], value: propValArr[idx]});
                }

              $.ajax({
                 type: "POST",
                 dataType: "text",
                 contentType: 'application/json',
                 url: 'addEntity.exec',
                 async: false,
                 data: JSON.stringify(
                    {
                       neo4jId: "",
                       label: lab,
                       properties: propList
                    }),
                 success: function (result) {
                    $('#results').trigger('reload');
                 },
                 error: function (jqXHR, textStatus, errorThrown) {
                    alert(jqXHR.status + ' ' + jqXHR.responseText);
                 }
               });

               if (parentList.length > 0 && parentSelVals) {
               parentSelVals = parentSelVals.slice(0, -1);
               $.ajax({
                  type: "POST",
                  dataType: "text",
                  contentType: 'application/json',
                  url: 'addEntRelationships.exec',
                  async: false,
                  data: JSON.stringify(
                     {
                        name: propValArr[0] + "," + label + "," + parentLabel,
                        value: parentSelVals
                     }),
                  success: function (result) {
                     $('#results').trigger('reload');
                  },
                  error: function (jqXHR, textStatus, errorThrown) {
                     alert("addEntRelationships: " + jqXHR.status + ' ' + jqXHR.responseText);
                  }
                });
              }

                if (childList.length > 0 && childSelVals) {
                childSelVals = childSelVals.slice(0, -1);
                //make call to add child relationship
                console.log("childSelVals: " + childSelVals);
                console.log("name &&&&: " + propValArr[0] + "," + label + "," + childLabel)
                $.ajax({
                   type: "POST",
                   dataType: "text",
                   contentType: 'application/json',
                   url: 'addChildRelationships.exec',
                   async: false,
                   data: JSON.stringify(
                      {
                         name: propValArr[0] + "," + label + "," + childLabel,
                         value: childSelVals
                      }),
                   success: function (result) {
                      $('#results').trigger('reload');
                   },
                   error: function (jqXHR, textStatus, errorThrown) {
                      alert("addChildRelationships: " + jqXHR.status + ' ' + jqXHR.responseText);
                   }
                 });
               }
             } else {
                if (!notEmpty && $('#updateExistingDiv').is(":visible")) {
                  alert("Error: Name field value must not be empty!");
                } else if (duplicate) {
                  alert("Error: Name field value must be unique!");
                }
                $('#addNewEntDiv').hide();
             }
              //console.log(newEntFieldVals);
              // Reload the page
              //location.reload(true);
              //location.href = "maincac.exec";
           });


          $('#edit-save-button').click(function (event) {
             var editList = $('.edit-list-item');
             var success = true;

             editList.each(function (index, li) {
                var listItem = $(li);

                $.ajax({
                   type: "POST",
                   url: "updateEntity.exec",
                   async: true,
                   data: JSON.stringify({property: listItem.attr('id'), value: listItem.val()}),
                   dataType: "text",
                   success: function (data) {
                   },
                   error: function (jqXHR, textStatus, errorThrown) {
                      console.log("Error, status = " + textStatus + ", " +
                         "error thrown: " + errorThrown
                      );
                      success = false;
                   }
                });
             });

             if (success != false)
             {
                alert("Update Of Entity Successful!")
                $('#entityType').trigger("change");
             }

          });

          function createNewEntityForm(result)
          {

             showInsertFrm = false;

             var fieldList = result.fieldList;
             var lastChar = fieldList.slice(-1);
             if (lastChar == ',')
             {
                fieldList = fieldList.substring(0, fieldList.length - 1);
             }
             var props = fieldList.split(",");
             var numRows = props.length;

             var form = document.createElement("form");
             var table = document.createElement("table");

             var titleRow = document.createElement("tr");
             var titleCol = document.createElement("td");
             titleCol.setAttribute("colSpan", numRows + 1);
             var title = document.createElement("label");
             title.style = "font-size: 18px;";
             title.innerHTML = "Add New Data";
             titleCol.appendChild(title);
             titleRow.appendChild(titleCol);
             table.appendChild(titleRow);

             var row = document.createElement("tr");
             for (i = 0; i < numRows; i++)
             {

                var input = document.createElement("input");
                input.className = "form-control";
                input.id = $(entityType).val() + '_' + i;
                input.type = "text";
                input.name = "new-data-input";
                input.placeholder = props[i];

                var inputCol = document.createElement("td");
                inputCol.appendChild(input);
                row.appendChild(inputCol);
             }

             var insertBtn = document.createElement('button');
             insertBtn.id = "insertBtn";
             insertBtn.innerHTML = "Save";
             insertBtn.className = "insert-node btn btn-success";
             insertBtn.setAttribute("data-names", fieldList);
             insertBtn.onclick = function () {
                insertDataNode();
                return false;
             };

             var btnCol = document.createElement("td");
             btnCol.append(insertBtn);

             row.appendChild(btnCol);
             table.appendChild(row);

             form.appendChild(table);

             document.getElementById("insertDiv").appendChild(form);
          }

          function insertDataNode()
          {

             var names = event.target.getAttribute('data-names');
             var namesArray = names.split(',');

             var label = $(entityType).val();

             var propertiesList = [];
             $("[name^='new-data-input']").each(function () {
                var inputId = $(this).attr('id');
                var inputIdx = inputId.substring(inputId.lastIndexOf("_") + 1);
                propertiesList.push({name: namesArray[inputIdx], value: $(this).val()});
             });

             $.ajax({
                type: "POST",
                dataType: "text",
                contentType: 'application/json',
                url: 'insertNode.exec',
                async: true,
                data: JSON.stringify(
                   {
                      neo4jId: "",
                      label: label,
                      properties: propertiesList
                   }),
                success: function (result) {
                   alert('Update was successful!');
                   $('#results').trigger('reload');
                },
                error: function (jqXHR, textStatus, errorThrown) {
                   alert(jqXHR.status + ' ' + jqXHR.responseText);
                }

             });

          }

          $('.update-node').click(function (event) {
             var neo4jId = event.target.getAttribute('data-id');
             var label = event.target.getAttribute('data-label');
             var names = event.target.getAttribute('data-names');

             var propertiesList = [];
             var namesArray = names.split(',');
             for (var i = 0; i < namesArray.length; i++)
             {
                var fieldvalue = $('#' + label + '_' + namesArray[i] + '_' + neo4jId).val();
                propertiesList.push({name: namesArray[i], value: fieldvalue});
             }

             $.ajax({
                type: "PUT",
                dataType: "text",
                contentType: 'application/json',
                url: 'updateNode.exec',
                async: true,
                data: JSON.stringify(
                   {
                      neo4jId: event.target.getAttribute('data-id'),
                      label: event.target.getAttribute('data-label'),
                      properties: propertiesList
                   }),
                success: function (result) {
                   alert('Update was successful!');
                   $('#results').trigger('reload');
                },
                error: function (jqXHR, textStatus, errorThrown) {
                   alert(jqXHR.status + ' ' + jqXHR.responseText);
                }

             });
          });

          $('.delete-node').click(function (event) {

             var result = confirm("Want to delete?");
             if (result)
             {

                $.ajax({
                   type: "DELETE",
                   dataType: "text",
                   url: 'deleteNode.exec',
                   async: true,
                   data: JSON.stringify({property: 'neo4jId', value: event.target.getAttribute('data-id')}),
                   success: function (result) {
                      alert('Delete was successful!');
                      $('#results').trigger('reload');
                   },
                   error: function (jqXHR, textStatus, errorThrown) {
                      success = false;
                      alert(jqXHR.status + ' ' + jqXHR.responseText);
                   }

                });

             }

          });
       });

    </script>
</head>
<body>

<%--<input type="button" class="btn btn-warning" style="float: right" value="Edit" id="edit-property-rows"/>--%>
<table class="table table-bordered" id="edit-entity-list">
    <caption>Total: ${fn:length(analysisResult.entities)}</caption>
    <thead>
    <tr>
        <c:set var="firstEntity" value="${analysisResult.entities[0]}"/>
        <c:forEach items="${firstEntity.properties}" var="property">
            <th>${property.name}
                <div class="table-sorter-icon"></div>
            </th>
        </c:forEach>
    </tr>
    </thead>
    <tbody>
    <c:forEach items="${analysisResult.entities}" var="entity">
        <tr id="entRows">
            <%--<th scope="row">
                <c:forEach items="${entity.properties}" var="property">
                    <c:if test="${property.name == 'name'}">
                        <a href="#" data-name="${property.value}" data-entity="${entity.label}"
                           class="baseballCardLink noneditable-td">${property.value}</a>
                        <input type="text" class="form-control editable-td" value="${property.value}"
                               id="${entity.label}_${property.name}_${entity.neo4jId}"/>
                    </c:if>
                </c:forEach>
            </th>--%>

            <c:forEach items="${entity.properties}" var="property">
                <c:if test="${property.name != 'name'}">
                    <td id="${entity.neo4jId}"><input type="text" id="test" value="${property.value}"></input></td><%--
                    <td class="editable-td"><input class="form-control" type="text" value="${property.value}"
                                                   id="${entity.label}_${property.name}_${entity.neo4jId}"/></td>--%>
                </c:if>
            </c:forEach><%--
            <td class="editable-td">
                <button id="update-node-button" data-id="${entity.neo4jId}" data-label="${entity.label}"
                        data-names="${entity.names}" class="update-node btn btn-success">Save
                </button>
                <button id="delete-node-button" data-id="${entity.neo4jId}" class="delete-node btn btn-primary">
                    Delete
                </button>

            </td>--%>
        </tr>

    </c:forEach>
    </tbody>
</table>
<br>
<div id="addNewEntDiv">
<p> Add New </p>
<table id="addNewRow">
  <tr>
      <td><input type="button" value="    Relationships   " id="entRelationships"/></td>
      <c:set var="firstEntity" value="${analysisResult.entities[0]}"/>
      <c:forEach items="${firstEntity.properties}" var="property">
        <td><input type="text" value=""></input></td>

      </c:forEach>
  </tr>
</table>
</div>

<!-- Modal -->
<div id="myModal" class="modal fade" role="dialog">
  <div class="modal-dialog">

    <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title">Add Relationships</h4>
      </div>
      <div class="modal-body" id="modalBody">
      </div>
      <div class="modal-footer" style="text-align: center;">
        <button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>
        <button type="button" class="btn btn-default" data-dismiss="modal" id="okButton">Ok</button>
      </div>
    </div>

  </div>
</div>

<br>
<div id="updateExistingDiv">
<p>Select Entity to Edit</p>
<br>
<select id="updateRelSelect" class ="updaterelS"></select>
<input type="button" value=" Cancel " id="relUpdateCancel">
</div>
<br>
<div id="editButtons">
<input type="button" value="  Save  " id="save"/>
<input type="button" value=" Add New " id="addNew">
<input type="button" value=" Update Relationship " id="updateRelationship"/>
<input type="button" value=" Cancel " id="cancel">
</div>
<div id="insertDiv"></div>
<br>

</body>
</html>
