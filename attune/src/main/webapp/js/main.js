$(document).ready(function ()
{
	$('#spinner').hide();

	$('body').on('click', '.execLink', function()
	{
		var mainScreen = $(this).attr('href');

		$('#main').fadeOut(function()
		{
			$('#spinner').show();

			$.get(mainScreen, function(data)
			{
				if ( data.indexOf("This system is the property of Booz Allen Hamilton and may only be accessed by authorized users.") != -1)
				{
					document.location.href=mainScreen;
				}
				else
				{

					$('#spinner').hide();

					$("#main").html(data);
					$('#main').fadeIn();

					// Remove the editable selector elements on config page that cause height issues on other pages
					if (mainScreen != "configDashboard.exec")
						$('.es-list').remove();

					// If the user is on mobile device, toggle the sidebar
					if ($(window).width() <= 1024)
						$('#wrapper').addClass('toggled');
					}
			});

		});

		return false;
	});
});

