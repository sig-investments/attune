#!/bin/sh

exec docker inspect --format '{{ .Id }}' "$@"