#!/bin/bash
#
# This script starts up a Tomcat Docker Container using the appropriate parameters. 
# If different parameters are needed for different versions (tags), then this file can be updated and 
# placed inside the tag-specific sub-folder and it will be used instead.
#
# Useage:  ./start-docker-container.sh --image IMAGE [REGISTRY]
#
#   REGISTRY: The container registry, for example 918792412995.dkr.ecr.us-west-2.amazonaws.com
#   IMAGE: The image repo name and tag, for example attune/tomcat:tomcat7-jre8. The default is attune/tomcat:tomcat7-jre8 
#
IMAGE="attune/tomcat:tomcat7-jre8"
REGISTRY=

while [[ ${1} ]]; do
        case "${1}" in
            --image)
                IMAGE=${2}
                shift
                ;;
            --registry)
                REGISTRY=${2}
                shift
                ;;
            *)
                echo "Unknown parameter: ${1}" >&2
                exit 1
        esac

        if ! shift; then
            echo 'Missing parameter argument --image.' >&2
            return 1
        fi
done

if [ -z "$REGISTRY" ]; then
  REGISTRY=""
else
  if [ "${REGISTRY: -1}" == "/" ]; then
    echo "Using Registry $REGISTRY"
  else
    REGISTRY="$REGISTRY/"
    echo "Using Registry $REGISTRY"
  fi
fi

if [ -z "$WORKSPACE" ]; then
  PRESENT_DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd )"
  WORKSPACE="$(readlink -f "${PRESENT_DIR}"/../../../../../../)"
fi

if [ -f "${WORKSPACE}"/docker.properties ]; then
  #echo "File ${WORKSPACE}/docker.properties not found!"
  #exit 1
  echo "Reading docker.properties to set environment"
  while read -r line; 
  do
    if [[ ${line:0:1} == '#' ]]
    then
      echo 'ignoring comment in properties file'
    else
      declare $line;
      IFS='=' read -r -a array <<< "$line"
      key=$(echo "${array[0]}" | sed 's/\\./_/g')
      value="${array[1]}"
      echo "Exporting ${key}=${value} to environment"
      export "${key}"="${value}"
    fi    
  done < "${WORKSPACE}"/docker.properties 
fi

if [ -z "$SSL_WORKING_DIR_TOMCAT" ]; then
  SSL_WORKING_DIR_TOMCAT=/home/jenkins
fi

if [ -z "$tomcat_https_port" ]; then
  tomcat_https_port=8543
fi

if [ -z "$tomcat_http_port" ]; then
  tomcat_http_port=8180
fi

if [ -z "$tomcat_container_https_port" ]; then
  tomcat_container_https_port=8443
fi

if [ -z "$tomcat_container_http_port" ]; then
  tomcat_container_http_port=8080
fi

if [ -z "$tomcat_debug_port" ]; then
  tomcat_debug_port=8787
fi

if [ -z "$tomcat_container_debug_port" ]; then
  tomcat_container_debug_port=8787
fi

if [ -z "$CONTAINER_NAME_TOMCAT" ]; then
  CONTAINER_NAME_TOMCAT="master-tomcat"
fi

if [ "${1}" == "--help" ]; then
  echo
  echo "This script starts up a Neo4j Docker Container using the appropriate parameters. "
  echo "If different parameters are needed for different versions (tags), then this file can be updated and "
  echo "placed inside the tag-specific sub-folder and it will be used instead."
  echo 
  echo "Useage:  ./start-docker-container.sh [--registry REGISTRY] [--image IMAGE]"
  echo 
  echo "IMAGE: The full image repo name and tag, for example --registry 918792412995.dkr.ecr.us-west-2.amazonaws.com --image attune/tomcat:tomcat7-jre8"
  echo
  exit 0  
fi

TAG_PATH=$(echo "${IMAGE}" | sed 's/\(.*\):/\1\//')
TAG="$(basename $TAG_PATH)"
PRESENT_DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd )"
#
#Clear previous logs
#
sudo rm "${PRESENT_DIR}"/"${TAG}"/logs -rf
echo "Starting Docker Container using: "
if [ ! -z "$HOST_NAME_NEO4J" ] && [ ! -z "$IP_NEO4J" ]; then
  echo "sudo docker run -d --name=${CONTAINER_NAME_TOMCAT} --hostname=${HOST_NAME_TOMCAT} --add-host ${HOST_NAME_NEO4J}:${IP_NEO4J} -p ${tomcat_http_port}:${tomcat_container_http_port} -p ${tomcat_https_port}:${tomcat_container_https_port} -p ${tomcat_debug_port}:${tomcat_container_debug_port} -v ${PRESENT_DIR}/${TAG}/logs:/usr/local/tomcat/logs -v $WORKSPACE/attune/src/main/docker/ssl:$SSL_WORKING_DIR_TOMCAT/ssl $REGISTRY$IMAGE"
  echo
  sudo docker run -d --name="${CONTAINER_NAME_TOMCAT}" --hostname="${HOST_NAME_TOMCAT}" --add-host "${HOST_NAME_NEO4J}":"${IP_NEO4J}" -p "${tomcat_http_port}":"${tomcat_container_http_port}" -p "${tomcat_https_port}":"${tomcat_container_http_port}" -p "${tomcat_debug_port}":"${tomcat_container_debug_port}" -v "${PRESENT_DIR}/${TAG}/logs:/usr/local/tomcat/logs" -v "$WORKSPACE/attune/src/main/docker/ssl:$SSL_WORKING_DIR_TOMCAT/ssl" $REGISTRY$IMAGE
else
  echo "sudo docker run -d --name=${CONTAINER_NAME_TOMCAT} --hostname=${HOST_NAME_TOMCAT} -p ${tomcat_http_port}:${tomcat_container_http_port} -p ${tomcat_https_port}:${tomcat_container_https_port} -p ${tomcat_debug_port}:${tomcat_container_debug_port} -v ${PRESENT_DIR}/${TAG}/logs:/usr/local/tomcat/logs -v $WORKSPACE/attune/src/main/docker/ssl:$SSL_WORKING_DIR_TOMCAT/ssl $REGISTRY$IMAGE"
  echo
  sudo docker run -d --name="${CONTAINER_NAME_TOMCAT}" --hostname="${HOST_NAME_TOMCAT}" -p "${tomcat_http_port}":"${tomcat_container_http_port}" -p "${tomcat_https_port}":"${tomcat_container_http_port}" -p "${tomcat_debug_port}":"${tomcat_container_debug_port}" -v "${PRESENT_DIR}/${TAG}/logs:/usr/local/tomcat/logs" -v "$WORKSPACE/attune/src/main/docker/ssl:$SSL_WORKING_DIR_TOMCAT/ssl" $REGISTRY$IMAGE
fi

#Wait for ports
if [ -z "$jenkins_ip" ]; then
  if [ -z "$host_ip" ]; then
    HOST="localhost"
  else
    HOST="${host_ip}"
  fi  
else
  HOST="${jenkins_ip}"
fi

#first sleep for 5 seconds for docker to start
echo "Waiting 5 seconds for Docker to start"
sleep 5

PORTS=${tomcat_http_port},${tomcat_https_port}
for PORT in $(echo $PORTS | sed "s/,/ /g")
do
  echo "Waiting for $HOST:$PORT ...."
  PORT_STATUS=$([ "$(curl -sm5 --retry 5 $HOST:$PORT >/dev/null; echo $?)" != 7 ] && echo OK || echo FAIL)
  echo "PORT_STATUS is $PORT_STATUS"
  if [ "$PORT_STATUS" == "OK" ]; then
    echo "Port $PORT Ready."
  else
    echo "Port $PORT Unavailable."
  fi
done