#!/bin/bash
# Write Tomcat Docker Image Id to File for later use
#
IMAGE=
REGISTRY=
REGION=us-west-2
WORKSPACE=
CONTAINER_NAME=

while [[ ${1} ]]; do
        case "${1}" in
            --image)
                IMAGE=${2}
                shift
                ;;
            --registry)
                REGISTRY=${2}
                shift
                ;;
            --region)
                REGION=${2}
                shift
                ;;
            --workspace)
                WORKSPACE=${2}
                shift
                ;;
            --container)
                CONTAINER_NAME=${2}
                shift
                ;;
            *)
                echo "Unknown parameter: ${1}" >&2
                exit 1
        esac

        if ! shift; then
            echo 'Missing parameter argument --registry and --image.' >&2
            return 1
        fi
done

if [ -z "$IMAGE" ]; then
  echo "Environment variable IMAGE must be set, for example IMAGE=attune/tomcat:tomcat7-jre8"
  exit 1
fi

if [ -z "$WORKSPACE" ]; then
  PRESENT_DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd )"
  WORKSPACE="$(readlink -f "${PRESENT_DIR}"/../../../../../../)"
  echo "Defaulted WORKSPACE to ${WORKSPACE}"
fi

if [ -z "$CONTAINER_NAME" ]; then
  CONTAINER_NAME="master-tomcat"
  echo "Defaulted CONTAINER_NAMEto ${CONTAINER_NAME}"
fi

if [ "$REGISTRY" == "aws" ]; then
#
# It seems that the AWS ECR instances are named based off of the AWS Account number and region.
# The AWS account number for the 'attunebah' account seems to be 918792412995, so we just have to
# plug in the right region
#
  REGISTRY="918792412995.dkr.ecr.$REGION.amazonaws.com"  
fi

if [ -z "$REGISTRY" ]; then
  REGISTRY=""
  #echo "Environment variable REGISTRY must be set, for example REGISTRY=918792412995.dkr.ecr.us-west-2.amazonaws.com"
  #exit 1
else
  REGISTRY=$REGISTRY/
fi

IMAGE_ID_TOMCAT=$( "${WORKSPACE}"/attune/src/main/docker/get-docker-image-id.sh "${REGISTRY}${IMAGE}" )
export IMAGE_ID_TOMCAT=$IMAGE_ID_TOMCAT
export CONTAINER_NAME_TOMCAT="${CONTAINER_NAME}"
export HOST_NAME_TOMCAT="tomcat.attune.bah.com"
export SSL_WORKING_DIR_TOMCAT="/home/jenkins"
export SERVER_CERT_TOMCAT="${HOST_NAME_TOMCAT}.cer"
export CLIENT_CERT_TOMCAT="${HOST_NAME_TOMCAT}-client.cer"
export CLIENT_KEY_TOMCAT="${HOST_NAME_TOMCAT}-clientkey.pkcs8"
export OUT_CLIENT_KEY_TOMCAT="${HOST_NAME_TOMCAT}-clientkey.p12"
export TRUSTSTORE_TOMCAT="${JAVA_HOME}/jre/lib/security/truststore.jks"
export TRUSTSTORE_PASSWORD_TOMCAT="changeit"
export KEYSTORE_TOMCAT="${JAVA_HOME}/jre/lib/security/keystore.jks"
export KEYSTORE_PASSWORD_TOMCAT="changeit"
export tomcat_http_port="8180"
export tomcat_https_port="8543"
export tomcat_container_http_port="8080"
export tomcat_container_https_port="8443"
export tomcat_debug_port="8787"
export tomcat_container_debug_port="8787"
export SSL_PORT_TOMCAT=$tomcat_container_https_port

echo "IMAGE_ID_TOMCAT=${IMAGE_ID_TOMCAT}" >> "$WORKSPACE"/docker.properties
echo "CONTAINER_NAME_TOMCAT=${CONTAINER_NAME_TOMCAT}" >>  "$WORKSPACE"/docker.properties
echo "HOST_NAME_TOMCAT=${HOST_NAME_TOMCAT}" >>  "$WORKSPACE"/docker.properties
echo "SSL_WORKING_DIR_TOMCAT=${SSL_WORKING_DIR_TOMCAT}" >>  "$WORKSPACE"/docker.properties
echo "SERVER_CERT_TOMCAT=${SERVER_CERT_TOMCAT}" >>  "$WORKSPACE"/docker.properties
echo "CLIENT_CERT_TOMCAT=${CLIENT_CERT_TOMCAT}" >>  "$WORKSPACE"/docker.properties
echo "CLIENT_KEY_TOMCAT=${CLIENT_KEY_TOMCAT}" >>  "$WORKSPACE"/docker.properties
echo "OUT_CLIENT_KEY_TOMCAT=${OUT_CLIENT_KEY_TOMCAT}" >>  "$WORKSPACE"/docker.properties
echo "TRUSTSTORE_TOMCAT=${TRUSTSTORE_TOMCAT}" >>  "$WORKSPACE"/docker.properties
echo "TRUSTSTORE_PASSWORD_TOMCAT=${TRUSTSTORE_PASSWORD_TOMCAT}" >>  "$WORKSPACE"/docker.properties
echo "KEYSTORE_TOMCAT=${KEYSTORE_TOMCAT}" >>  "$WORKSPACE"/docker.properties
echo "KEYSTORE_PASSWORD_TOMCAT=${KEYSTORE_PASSWORD_TOMCAT}" >>  "$WORKSPACE"/docker.properties
echo "tomcat_http_port=${tomcat_http_port}" >>  "$WORKSPACE"/docker.properties
echo "tomcat_https_port=${tomcat_https_port}" >>  "$WORKSPACE"/docker.properties
echo "tomcat_container_http_port=${tomcat_container_http_port}" >>  "$WORKSPACE"/docker.properties
echo "tomcat_container_https_port=${tomcat_container_https_port}" >>  "$WORKSPACE"/docker.properties
echo "SSL_PORT_TOMCAT=${SSL_PORT_TOMCAT}" >>  "$WORKSPACE"/docker.properties