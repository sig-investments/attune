#!/bin/bash
# Write Neo4j Docker Image Id to File for later use
#
IMAGE=
REGISTRY=
REGION=us-west-2
WORKSPACE=
CONTAINER_NAME=

while [[ ${1} ]]; do
        case "${1}" in
            --image)
                IMAGE=${2}
                shift
                ;;
            --registry)
                REGISTRY=${2}
                shift
                ;;
            --region)
                REGION=${2}
                shift
                ;;
            --workspace)
                WORKSPACE=${2}
                shift
                ;;
            --container)
                CONTAINER_NAME=${2}
                shift
                ;;
            *)
                echo "Unknown parameter: ${1}" >&2
                exit 1
        esac

        if ! shift; then
            echo 'Missing parameter argument --registry and --image.' >&2
            return 1
        fi
done

if [ -z "$IMAGE" ]; then
  echo "Environment variable IMAGE must be set, for example IMAGE=attune/neo4j:neo4j-2.3"
  exit 1
fi

if [ -z "$WORKSPACE" ]; then
  PRESENT_DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd )"
  WORKSPACE="$(readlink -f "${PRESENT_DIR}"/../../../../../../)"
  echo "Defaulted WORKSPACE to ${WORKSPACE}"
fi

if [ -z "$CONTAINER_NAME" ]; then
  CONTAINER_NAME=master-neo4j
  echo "Defaulted CONTAINER_NAME to ${CONTAINER_NAME}"
fi

if [ "$REGISTRY" == "aws" ]; then
#
# It seems that the AWS ECR instances are named based off of the AWS Account number and region.
# The AWS account number for the 'attunebah' account seems to be 918792412995, so we just have to
# plug in the right region
#
  REGISTRY="918792412995.dkr.ecr.$REGION.amazonaws.com"  
fi

if [ -z "$REGISTRY" ]; then
  REGISTRY=""
  #echo "Environment variable REGISTRY must be set, for example REGISTRY=918792412995.dkr.ecr.us-west-2.amazonaws.com"
  #exit 1
else
  REGISTRY=$REGISTRY/
fi

IMAGE_ID_NEO4J=$( "${WORKSPACE}"/attune/src/main/docker/get-docker-image-id.sh "${REGISTRY}${IMAGE}" )
export IMAGE_ID_NEO4J=$IMAGE_ID_NEO4J
export CONTAINER_NAME_NEO4J="${CONTAINER_NAME}"
export HOST_NAME_NEO4J="neo4j.attune.bah.com"
export SSL_WORKING_DIR_NEO4J="/home/jenkins"
export SERVER_CERT_NEO4J="${HOST_NAME_NEO4J}.cer"
export CLIENT_CERT_NEO4J="${HOST_NAME_NEO4J}-client.cer"
export CLIENT_KEY_NEO4J="${HOST_NAME_NEO4J}-clientkey.pkcs8"
export OUT_CLIENT_KEY_NEO4J="${HOST_NAME_NEO4J}-clientkey.p12"
export TRUSTSTORE_NEO4J="${JAVA_HOME}/jre/lib/security/truststore.jks"
export TRUSTSTORE_PASSWORD_NEO4J="changeit"
export KEYSTORE_NEO4J="${JAVA_HOME}/jre/lib/security/keystore.jks"
export KEYSTORE_PASSWORD_NEO4J="changeit"
export neo4j_http_port="7474"
export neo4j_https_port="7687"
export neo4j_container_http_port="7474"
export neo4j_container_https_port="7687"
export SSL_PORT_NEO4J=$neo4j_container_https_port

echo "IMAGE_ID_NEO4J=${IMAGE_ID_NEO4J}" >> "$WORKSPACE"/docker.properties
echo "CONTAINER_NAME_NEO4J=${CONTAINER_NAME_NEO4J}" >> "$WORKSPACE"/docker.properties
echo "HOST_NAME_NEO4J=${HOST_NAME_NEO4J}" >>  "$WORKSPACE"/docker.properties
echo "SSL_WORKING_DIR_NEO4J=${SSL_WORKING_DIR_NEO4J}" >>  "$WORKSPACE"/docker.properties
echo "SERVER_CERT_NEO4J=${SERVER_CERT_NEO4J}" >>  "$WORKSPACE"/docker.properties
echo "CLIENT_CERT_NEO4J=${CLIENT_CERT_NEO4J}" >>  "$WORKSPACE"/docker.properties
echo "CLIENT_KEY_NEO4J=${CLIENT_KEY_NEO4J}" >>  "$WORKSPACE"/docker.properties
echo "OUT_CLIENT_KEY_NEO4J=${OUT_CLIENT_KEY_NEO4J}" >>  "$WORKSPACE"/docker.properties
echo "TRUSTSTORE_NEO4J=${TRUSTSTORE_NEO4J}" >>  "$WORKSPACE"/docker.properties
echo "TRUSTSTORE_PASSWORD_NEO4J=${TRUSTSTORE_PASSWORD_NEO4J}" >>  "$WORKSPACE"/docker.properties
echo "KEYSTORE_NEO4J=${KEYSTORE_NEO4J}" >>  "$WORKSPACE"/docker.properties
echo "KEYSTORE_PASSWORD_NEO4J=${KEYSTORE_PASSWORD_NEO4J}" >>  "$WORKSPACE"/docker.properties
echo "neo4j_http_port=${neo4j_http_port}" >>  "$WORKSPACE"/docker.properties
echo "neo4j_https_port=${neo4j_https_port}" >>  "$WORKSPACE"/docker.properties
echo "neo4j_container_http_port=${neo4j_container_http_port}" >>  "$WORKSPACE"/docker.properties
echo "SSL_PORT_NEO4J=${SSL_PORT_NEO4J}" >>  "$WORKSPACE"/docker.properties
echo "neo4j_container_https_port=${neo4j_container_https_port}" >>  "$WORKSPACE"/docker.properties