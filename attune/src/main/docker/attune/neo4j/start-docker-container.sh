#!/bin/bash
#
# This script starts up a Neo4j Docker Container using the appropriate parameters. 
# If different parameters are needed for different versions (tags), then this file can be updated and 
# placed inside the tag-specific sub-folder and it will be used instead.
#
# Useage:  ./start-docker-container.sh --image IMAGE [REGISTRY]
#
#   REGISTRY: The container registry, for example 918792412995.dkr.ecr.us-west-2.amazonaws.com
#   IMAGE: The image repo name and tag, for example attune/neo4j:neo4j-2.3. The default is attune/neo4j:neo4j-2.3 
#
IMAGE="attune/neo4j:neo4j-3.0.4"
REGISTRY=

while [[ ${1} ]]; do
        case "${1}" in
            --image)
                IMAGE=${2}
                shift
                ;;
            --registry)
                REGISTRY=${2}
                shift
                ;;
            *)
                echo "Unknown parameter: ${1}" >&2
                exit 1
        esac

        if ! shift; then
            echo 'Missing parameter argument --image.' >&2
            return 1
        fi
done

if [ -z "$REGISTRY" ]; then
  REGISTRY=""
else
  if [ "${REGISTRY: -1}" == "/" ]; then
    echo "Using Registry $REGISTRY"
  else
    REGISTRY="$REGISTRY/"
    echo "Using Registry $REGISTRY"
  fi
fi

if [ -z "$WORKSPACE" ]; then
  PRESENT_DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd )"
  WORKSPACE="$(readlink -f "${PRESENT_DIR}"/../../../../../../)"
fi

if [ -f "${WORKSPACE}"/docker.properties ]; then
  #echo "File ${WORKSPACE}/docker.properties not found!"
  #exit 1
  echo "Reading docker.properties to set environment"
  while read -r line; 
  do
    if [[ ${line:0:1} == '#' ]]
    then
      echo 'ignoring comment in properties file'
    else
      declare $line;
      IFS='=' read -r -a array <<< "$line"
      key=$(echo "${array[0]}" | sed 's/\\./_/g')
      value="${array[1]}"
      echo "Exporting ${key}=${value} to environment"
      export "${key}"="${value}"
    fi    
  done < "${WORKSPACE}"/docker.properties 
fi

if [ -z "$SSL_WORKING_DIR_NEO4J" ]; then
  SSL_WORKING_DIR_NEO4J=/home/jenkins
fi

if [ -z "$neo4j_https_port" ]; then
  neo4j_https_port=7687
fi

if [ -z "$neo4j_http_port" ]; then
  neo4j_http_port=7474
fi

if [ -z "$neo4j_container_https_port" ]; then
  neo4j_container_https_port=7687
fi

if [ -z "$neo4j_container_http_port" ]; then
  neo4j_container_http_port=7474
fi

if [ -z "$CONTAINER_NAME_NEO4J" ]; then
  CONTAINER_NAME_NEO4J=master-neo4j
fi

if [ "${1}" == "--help" ]; then
  echo
  echo "This script starts up a Neo4j Docker Container using the appropriate parameters. "
  echo "If different parameters are needed for different versions (tags), then this file can be updated and "
  echo "placed inside the tag-specific sub-folder and it will be used instead."
  echo 
  echo "Useage:  ./start-docker-container.sh [--registry REGISTRY] [--image IMAGE]"
  echo 
  echo "IMAGE: The full image repo name and tag, for example --registry 918792412995.dkr.ecr.us-west-2.amazonaws.com --image attune/neo4j:neo4j-2.3"
  echo
  exit 0  
fi

TAG_PATH=$(echo "${IMAGE}" | sed 's/\(.*\):/\1\//')
TAG="$(basename $TAG_PATH)"
PRESENT_DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd )"
#
#Clear previous data and use what's in the image
#
sudo rm "${PRESENT_DIR}"/"${TAG}"/data -rf
echo "Starting Docker Container using: "
if [ ! -z "$HOST_NAME_TOMCAT" ] && [ ! -z "$IP_TOMCAT" ]; then
  echo "sudo docker run -d --name=${CONTAINER_NAME_NEO4J} --hostname=${HOST_NAME_NEO4J} --add-host ${HOST_NAME_TOMCAT}:${IP_TOMCAT} -p ${neo4j_http_port}:${neo4j_container_http_port} -p ${neo4j_https_port}:${neo4j_container_https_port} -v ${PRESENT_DIR}/${TAG}/data:/data -v $WORKSPACE/attune/src/main/docker/ssl:$SSL_WORKING_DIR_NEO4J/ssl $REGISTRY$IMAGE"
  echo
  sudo docker run -d --name="${CONTAINER_NAME_NEO4J}" --hostname="${HOST_NAME_NEO4J}" --add-host "${HOST_NAME_TOMCAT}":"${IP_TOMCAT}" -p "${neo4j_http_port}":"${neo4j_container_http_port}" -p "${neo4j_https_port}":"${neo4j_container_https_port}" -v "${PRESENT_DIR}/${TAG}/data:/data" -v "$WORKSPACE/attune/src/main/docker/ssl:$SSL_WORKING_DIR_NEO4J/ssl" $REGISTRY$IMAGE
else
  echo "sudo docker run -d --name=${CONTAINER_NAME_NEO4J} --hostname=${HOST_NAME_NEO4J} -p ${neo4j_http_port}:${neo4j_container_http_port} -p ${neo4j_https_port}:${neo4j_container_https_port} -v ${PRESENT_DIR}/${TAG}/data:/data -v $WORKSPACE/attune/src/main/docker/ssl:$SSL_WORKING_DIR_NEO4J/ssl $REGISTRY$IMAGE"
  echo
  sudo docker run -d --name="${CONTAINER_NAME_NEO4J}" --hostname="${HOST_NAME_NEO4J}" -p "${neo4j_http_port}":"${neo4j_container_http_port}" -p "${neo4j_https_port}":"${neo4j_container_https_port}" -v "${PRESENT_DIR}/${TAG}/data:/data" -v "$WORKSPACE/attune/src/main/docker/ssl:$SSL_WORKING_DIR_NEO4J/ssl" $REGISTRY$IMAGE
fi

#Wait for ports
if [ -z "$jenkins_ip" ]; then
  if [ -z "$host_ip" ]; then
    HOST="localhost"
  else
    HOST="${host_ip}"
  fi  
else
  HOST="${jenkins_ip}"
fi

#first sleep for 5 seconds for docker to start
echo "Waiting 5 seconds for Docker to start"
sleep 5

PORTS=${neo4j_http_port},${neo4j_https_port}
for PORT in $(echo $PORTS | sed "s/,/ /g")
do
  echo "Waiting for $HOST:$PORT ...."
  PORT_STATUS=$([ "$(curl -sm5 --retry 5 $HOST:$PORT >/dev/null; echo $?)" != 7 ] && echo OK || echo FAIL)
  echo "PORT_STATUS is $PORT_STATUS"
  if [ "$PORT_STATUS" == "OK" ]; then
    echo "Port $PORT Ready."
    if [ "$PORT" == "$neo4j_http_port" ]; then
      RESP=$(curl --write-out %{http_code} --silent --output /dev/null -X POST http://neo4j:neo4j@localhost:$neo4j_http_port/user/neo4j/password -d 'password=password')
      echo "cURL response code from Neo4J password change was $RESP"
      if [ "$RESP" == "200" ]; then
        echo "Updated Neo4J password."
      else
        echo "Default Neo4J password could not be updated."
        exit 1
      fi
    fi
  else
    echo "Port $PORT Unavailable."
  fi
done
