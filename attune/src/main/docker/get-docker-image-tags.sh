#!/bin/sh

exec docker inspect --format '{{ .RepoTags }}' "$@"
