#!/bin/bash
#
# This script enables you to Build, Pull, Start, Stop, Commit, and Push Docker Container images to a container registry, such as  
# AWS Elastic Container Registry (ECR).
#
# Useage:  ./execute-docker-cmd.sh.sh --cmd COMMAND --image IMAGE [--region REGION] [--msg MSG] [--auth AUTH]
#
#   COMMAND: The command that you want to execute (build, pull, start, stop, commit, push). 
#        build: Builds a a local Docker Container image, which can then be pushed to an AWS Elastic Container Registry (ECR).
#        pull: Pulls an image from a registry such as the AWS Elastic Container Registry (ECR).
#        start:  Starts and runs a Container based on the image.
#        commit: Commits all changes to a running container based on the passed in image. If commit is used, the --msg and --auth arguments must be supplied to provide a commit message and the author.
#        push:  Pushes all committed changes in a running container to a registry such as AWS Elastic Container Registry (ECR).
#        stop:  Stops the container based on the container id or name.
#        stop-all:  Stops all containers.
#        remove:  Removes the container based on the container id or name.
#        remove-all:  Removes all containers.
#        remove-image:  Removes an image based on the image tag.
#        remove-all-images:  Removes all images.
#   IMAGE: The image repo name and tag, for example attune/tomcat:tomcat7-jre8
#   REGISTRY: The docker container registry. If a value of 'aws' is supplied, then the default container registry 918792412995.dkr.ecr.us-west-2.amazonaws.com will be used. If the AWS registry is not used then the --user and --password options must be supplied.
#   REGION: The AWS region, the default is us-west-2
#   MSG:  The commit message, describing the changes, example --msg "Updated the JDK"
#   AUTH: The author's name that made the changes, example --auth "John Smith" 
#
CMD=
IMAGE=
#REGION=us-west-2  #Old Region
REGION=us-east-1
REGISTRY=
MSG=
AUTH=
USER=
PASSWORD=
CONTAINER=

if [ "${1}" == "--help" ]; then
  echo
  echo "This script enables you to Build, Pull, Start, Stop, Commit, and Push Docker Container images to a container registry, such as AWS Elastic Container Registry (ECR)."
  echo 
  echo "Useage:  ./execute-docker-cmd.sh.sh --cmd COMMAND --image IMAGE [--registry REGISTRY] [--region REGION] [--user USER] [--password PASSWORD] [--msg MSG] [--auth AUTH]"
  echo 
  echo "COMMAND: The command that you want to execute (build, pull, start, stop, commit, push). "
  echo     "build: Builds a a local Docker Container image, which can then be pushed to an AWS Elastic Container Registry (ECR)."
  echo     "pull: Pulls an image from a registry such as the AWS Elastic Container Registry (ECR)."
  echo     "start:  Starts and runs a Container based on the image."
  echo     "commit: Commits all changes to a running container based on the passed in image. If commit is used, the --msg and --auth arguments must be supplied to provide a commit message and the author."
  echo     "push:  Pushes all committed changes in a running container to a registry such as the AWS Elastic Container Registry (ECR)."
  echo     "stop:  Stops the container based on the container id or name."
  echo     "stop-all:  Stops all containers."
  echo     "remove:  Removes the container based on the container id or name."
  echo     "remove-all:  Removes all containers."
  echo     "remove-image:  Removes an image based on the image tag."
  echo     "remove-all-images:  Removes all images."
  echo "IMAGE: The image repo name and tag, for example attune/tomcat:tomcat7-jre8"
  echo "REGISTRY: The docker container registry. If a value of 'aws' is supplied, then the default container registry 918792412995.dkr.ecr.us-west-2.amazonaws.com will be used."
  echo "          If the AWS registry is not used then the --user and --password options must be supplied" 
  echo "REGION: The AWS region, the default is us-west-2"
  echo "USER: The user name to log into the container registry."
  echo "PASSWORD: The password to log into the container registry."
  echo "MSG:  The commit message, describing the changes, example --msg \"Updated the JDK\""
  echo "AUTH: The author's name that made the changes, example --auth \"John Smith\" "
  echo
  exit 0
fi

while [[ ${1} ]]; do
        case "${1}" in
            --cmd)
                CMD=${2}
                shift
                ;;
            --image)
                IMAGE=${2}
                shift
                ;;
            --container)
                CONTAINER=${2}
                shift
                ;;
            --registry)
                REGISTRY=${2}
                shift
                ;;
            --user)
                USER=${2}
                shift
                ;;
            --password)
                PASSWORD=${2}
                shift
                ;;
            --msg)
                MSG=${2}
                shift
                ;;
            --auth)
                AUTH=${2}
                shift
                ;;
            --region)
                REGION=${2}
                shift
                ;;
            *)
                echo "Unknown parameter: ${1}" >&2
                exit 1
        esac

        if ! shift; then
            echo 'Missing parameter argument --cmd and/or --image.' >&2
            return 1
        fi
done

if [ -z "$CMD" ]; then
  echo "Missing --cmd argument, for example: ./execute-docker-cmd.sh --cmd pull --image attune/apimgmt/api-umbrella:0.12.0"
  exit 1
fi

if [ -z "$IMAGE" ] && [ "$CMD" != "remove" ] && [ "$CMD" != "remove-all" ] && [ "$CMD" != "remove-all-images" ] && [ "$CMD" != "stop" ] && [ "$CMD" != "stop-all" ]; then
  echo "Missing --image argument, for example: ./execute-docker-cmd.sh --cmd ${CMD} --image attune/apimgmt/api-umbrella:0.12.0"
  exit 1
fi

if [[ "$CMD" != "build" && "$CMD" != "pull" && "$CMD" != "start" && "$CMD" != "stop" && "$CMD" != "stop-all" && "$CMD" != "commit" && "$CMD" != "push" && "$CMD" != "remove" && "$CMD" != "remove-all" && "$CMD" != "remove-image" && "$CMD" != "remove-all-images" && "$CMD" != "search" ]]; then
  echo "Invalid --cmd value. Must be one of build, pull, start, stop, commit, push, stop-all, remove, remove-all, remove-image, or remove-all-images"
  exit 1
fi

if [[ "$CMD" == "commit" && -z "$MSG" ]]; then
  echo "The --msg argument is required for a commit, for example: ./execute-docker-cmd.sh --cmd commit --msg "Updated JDK" --auth "Joe Smith" --image attune/apimgmt/api-umbrella:0.12.0"
  exit 1
fi

if [[ "$CMD" == "commit" && -z "$AUTH" ]]; then
  echo "The --auth argument is required for a commit, for example: ./execute-docker-cmd.sh --cmd commit --msg "Updated JDK" --auth "Joe Smith" --image attune/apimgmt/api-umbrella:0.12.0"
  exit 1
fi

if [[ "$CMD" == "commit" && -z "$CONTAINER" ]]; then
  echo "The --container argument is required to commit changes to a single container and the --image argument is used to specify the new image and tag with the changes, for example: ./execute-docker-cmd.sh --cmd commit --container master-tomcat  --image attune/tomcat:tomcat9"
  exit 1
fi

if [[ "$CMD" == "stop" && -z "$CONTAINER" ]]; then
  echo "The --container argument is required to stop a single container, for example: ./execute-docker-cmd.sh --cmd stop --container master-tomcat"
  exit 1
fi

if [[ "$CMD" == "remove" && -z "$CONTAINER" ]]; then
  echo "The --container argument is required to remove a single container, for example: ./execute-docker-cmd.sh --cmd remove --container master-tomcat"
  exit 1
fi

if [ "$REGISTRY" == "aws" ]; then
#
# It seems that the AWS ECR instances are named based off of the AWS Account number and region.
# The AWS account number for the 'attunebah' account seems to be 918792412995, so we just have to
# plug in the right region
#
  REGISTRY="918792412995.dkr.ecr.$REGION.amazonaws.com"  
fi

if [ -z "$REGISTRY" ]; then
  REGISTRY=""
else
  if [[ $REGISTRY == *"amazonaws.com"* ]]; then
    echo "Using Region $REGION"
    #
    #Log into AWS Elastic Container Registry, and get Registry
    #
    aws_ecr_login_command=$(aws ecr get-login --region "${REGION}" )
    sudo ${aws_ecr_login_command}
    REGISTRY=$(aws ecr get-login --region "${REGION}" | sed 's#.*https://##g')
    echo "Using Registry $REGISTRY"
    REGISTRY="$REGISTRY/"
  else
    if [ -z "$USER" ]; then
      echo "Missing --user argument"
    fi
    
    if [ -z "$PASSWORD" ]; then
      echo "Missing --password argument"
    fi
    sudo docker login -u $USER -p $PASSWORD $REGISTRY
    REGISTRY=$(echo $REGISTRY | awk -F/ '{print $3}')
    REGISTRY=$(echo $REGISTRY | awk -F: '{print $1}')
    if [ "$REGISTRY" == "localhost" ]; then
      REGISTRY=""
      echo "Using local Registry"
    else
      echo "Using Registry $REGISTRY"
      REGISTRY="$REGISTRY/"
    fi 
  fi
fi

#
#Build local Image from Dockerfile so that we can push updates later
#
if [ "$CMD" == "build" ]; then
  DOCKERFILE_PATH=$(echo "${IMAGE}" | sed 's/\(.*\):/\1\//')
  echo
  echo "Building Image with $PWD/$DOCKERFILE_PATH/Dockerfile"
  echo "Image will be tagged as $REGISTRY$IMAGE" 
  PRESENT_DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd )"
  echo "PRESENT_DIR is ${PRESENT_DIR}" 
  cd "${PRESENT_DIR}"/"${DOCKERFILE_PATH}"
  sudo docker build -t "${REGISTRY}""${IMAGE}" .
  sudo docker tag "${REGISTRY}""${IMAGE}" "${REGISTRY}""${IMAGE}"
fi

#
#Pull Image from AWS Elastic Container Registry and Tag Image so that we can push updates later
#
if [ "$CMD" == "pull" ]; then
  sudo docker pull "${REGISTRY}""${IMAGE}"
  sudo docker tag "${REGISTRY}""${IMAGE}" "${REGISTRY}""${IMAGE}"
fi

#
#start an image that's been previously built or pulled 
#
if [ "$CMD" == "start" ]; then
  PRESENT_DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd )"
  echo "PRESENT_DIR is ${PRESENT_DIR}"
  cd "${PRESENT_DIR}"
  echo "PWD is $PWD"
  RUN_CMD_PATH=$(echo "${IMAGE}" | sed 's/\(.*\):/\1\//')
  RUN_CMD_SCRIPT="start-docker-container.sh"  
  if [ -f "${PRESENT_DIR}/${RUN_CMD_PATH}/${RUN_CMD_SCRIPT}" ]; then
    if [ -z "$REGISTRY" ]; then
      echo "Runing ${PRESENT_DIR}/${RUN_CMD_PATH}/${RUN_CMD_SCRIPT} --image ${IMAGE}"      
      bash -c "./${RUN_CMD_PATH}/${RUN_CMD_SCRIPT} --image ${IMAGE}"      
    else
      echo "Runing ${PRESENT_DIR}/${RUN_CMD_PATH}/${RUN_CMD_SCRIPT} --registry ${REGISTRY} --image ${IMAGE}"      
      bash -c "./${RUN_CMD_PATH}/${RUN_CMD_SCRIPT} --registry ${REGISTRY} --image ${IMAGE}"    
    fi
  else
    if [[ $IMAGE == *":"* ]]; then
      REPO=$(echo "${IMAGE}" | sed 's/:.*//')
    else
      REPO="${IMAGE}"
    fi
    
    echo "File ${PRESENT_DIR}/${RUN_CMD_PATH}/${RUN_CMD_SCRIPT} not found, trying $(readlink -f "${PRESENT_DIR}"/"${REPO}"/"${RUN_CMD_SCRIPT}")"
    if [ -f "${PRESENT_DIR}/${REPO}/${RUN_CMD_SCRIPT}" ]; then
      if [ -z "$REGISTRY" ]; then
        echo "Runing $(readlink -f "${PRESENT_DIR}"/${REPO}/${RUN_CMD_SCRIPT}) --image ${IMAGE}"      
        bash -c "./${REPO}/${RUN_CMD_SCRIPT} --image ${IMAGE}"      
      else
        echo "Runing $(readlink -f "${PRESENT_DIR}"/${REPO}/${RUN_CMD_SCRIPT}) --registry ${REGISTRY} --image ${IMAGE}"      
        bash -c "./${REPO}/${RUN_CMD_SCRIPT} --registry ${REGISTRY} --image ${IMAGE}"
      fi
    else
      echo "File ${PRESENT_DIR}/${REPO}/${RUN_CMD_SCRIPT} not found"
      exit 1
    fi
  fi
fi

#
#Commit changes
#
if [ "$CMD" == "commit" ]; then  
  PRESENT_DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd )"
  echo "Getting container id for ${CONTAINER}"
  CONTAINER_ID=$(sudo docker ps -aqf "name=${CONTAINER}")
  sudo docker commit -m "${MSG}" -a "${AUTH}" "${CONTAINER_ID}" "${REGISTRY}""${IMAGE}"
fi

#
#Push changes
#
if [ "$CMD" == "push" ]; then
  sudo docker push "${REGISTRY}""${IMAGE}"
fi

#
#Stop a container
#
if [ "$CMD" == "stop" ]; then
  echo "Stopping container ${CONTAINER} ..."
  echo
  sudo docker stop ${CONTAINER}
fi

#
#Stop all containers
#
if [ "$CMD" == "stop-all" ]; then
  for i in $(sudo docker ps -aq)
  do
    echo "Stopping container ($i)..."
    echo
    sudo docker stop $i
  done
fi

#
#Remove a container
#
if [ "$CMD" == "remove" ]; then
  echo "Removing container ${CONTAINER} ..."
  echo
  sudo docker rm -v ${CONTAINER}
  if [ -z "$(sudo docker volume ls -qf dangling=true)" ]; then
    echo
  else
    sudo docker volume rm $(docker volume ls -qf dangling=true)
  fi
fi

#
#Remove all containers
#
if [ "$CMD" == "remove-all" ]; then
  for i in $(sudo docker ps -aq)
  do
    echo "Removing container ($i)..."
    echo
    sudo docker rm -v $i
  done
  if [ -z "$(sudo docker volume ls -qf dangling=true)" ]; then
    echo
  else
    sudo docker volume rm $(docker volume ls -qf dangling=true)
  fi  
fi

#
#Remove image
#
if [ "$CMD" == "remove-image" ]; then
  PRESENT_DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd )"
  for i in $(sudo docker images -q)
  do
    IMAGE_TAGS=$( "${PRESENT_DIR}"/get-docker-image-tags.sh $i )
    if [[ $IMAGE_TAGS == *"${REGISTRY}${IMAGE}"* ]]; then
      echo "Removing image ${REGISTRY}${IMAGE} ($i)..."
      echo
      sudo docker rmi $i
    fi
  done
  if [ -z "$(sudo docker images -f "dangling=true" -q)" ]; then
    echo
  else
    sudo docker rmi $(docker images -f "dangling=true" -q)
  fi
fi

#
#Remove all images
#
if [ "$CMD" == "remove-all-images" ]; then
  for i in $(sudo docker images -q)
  do
    echo "Removing image ($i)..."
    echo
    sudo docker rmi -f $i    
  done
  if [ -z "$(sudo docker images -f "dangling=true" -q)" ]; then
    echo
  else
    sudo docker rmi -f $(docker images -f "dangling=true" -q)
  fi  
fi

#
#Search for images
#
if [ "$CMD" == "search" ]; then
  if [[ $IMAGE == *":"* ]]; then
    TAG=$(echo "${IMAGE}" | sed 's/\(.*\)://')
    REPO=$(echo "${IMAGE}" | sed 's/:.*//')
  else
    TAG="latest"
    REPO="${IMAGE}"
  fi
  if [[ $REGISTRY == *"amazonaws.com"* ]]; then    
    RESULT=$(aws ecr list-images --no-paginate --region "${REGION}" --repository-name "${REPO}")
    RESULT=$( echo $RESULT | grep -Po '(?<="imageTag": ")[^"]*')    
  else
    RESULT=$(curl -X GET http://"${REGISTRY}"/v1/search?q="${REPO}")
    RESULT=$( echo $RESULT | grep -Po '(?<="name": ")[^"]*')
  fi
  PRESENT_DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd )"
  SEARCH_RESULTS_FILE="$(echo "${IMAGE}" | sed 's/:/-/g' | sed 's/\//-/g')-searchImageResults.txt"
  sudo rm "${PRESENT_DIR}"/../../../../"${SEARCH_RESULTS_FILE}" -rf
  if [[ $RESULT == *"${TAG}"* ]]; then
    MSG="$IMAGE Found in $REGISTRY"
  else
    MSG="$IMAGE Not Found in $REGISTRY"
  fi
  echo "${MSG}"
  echo -n "${MSG}" > "${PRESENT_DIR}"/../../../../"${SEARCH_RESULTS_FILE}"
fi

echo