#!/bin/bash
if [ "$#" -ne 2 ]; then
  echo "Two Property name suffixes must be provided! For example ./configureSSLCerts.sh TOMCAT API_UMBRELLA"
  exit 1
fi

if [ -z "$WORKSPACE" ]; then
  PRESENT_DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd )"
  WORKSPACE="$(readlink -f "${PRESENT_DIR}"/../../../../../)"
fi

cd "${WORKSPACE}"

if [ ! -f "${WORKSPACE}"/docker.properties ]; then
  echo "File ${WORKSPACE}/docker.properties not found!"
  exit 1 
fi

echo "Reading docker.properties to set environment"
while read -r line; 
do
  if [[ ${line:0:1} == '#' ]]
  then
    echo 'ignoring comment in properties file'
  else
    declare $line;
    IFS='=' read -r -a array <<< "$line"
    key=$(echo "${array[0]}" | sed 's/\\./_/g')
    value="${array[1]}"
    echo "Exporting ${key}=${value} to environment"
    export "${key}"="${value}"
  fi    
done < "${WORKSPACE}"/docker.properties

CONTAINER_ID_SERVER1="CONTAINER_ID_$1"
IMAGE_ID_SERVER1="IMAGE_ID_$1"
HOST_NAME_SERVER1="HOST_NAME_$1"
SSL_PORT_SERVER1="SSL_PORT_$1"
IP_SERVER1="IP_$1"
SSL_WORKING_DIR_SERVER1="SSL_WORKING_DIR_$1"
SERVER_CERT_SERVER1="SERVER_CERT_$1"
CLIENT_CERT_SERVER1="CLIENT_CERT_$1"
CLIENT_KEY_SERVER1="CLIENT_KEY_$1"
OUT_CLIENT_KEY_SERVER1="OUT_CLIENT_KEY_$1"
TRUSTSTORE_SERVER1="TRUSTSTORE_$1"
TRUSTSTORE_PASSWORD_SERVER1="TRUSTSTORE_PASSWORD_$1"
KEYSTORE_SERVER1="KEYSTORE_$1"
KEYSTORE_PASSWORD_SERVER1="KEYSTORE_PASSWORD_$1"

CONTAINER_ID_SERVER2="CONTAINER_ID_$2"
IMAGE_ID_SERVER2="IMAGE_ID_$2"
HOST_NAME_SERVER2="HOST_NAME_$2"
SSL_PORT_SERVER2="SSL_PORT_$2"
IP_SERVER2="IP_$2"
SSL_WORKING_DIR_SERVER2="SSL_WORKING_DIR_$2"
SERVER_CERT_SERVER2="SERVER_CERT_$2"
CLIENT_CERT_SERVER2="CLIENT_CERT_$2"
CLIENT_KEY_SERVER2="CLIENT_KEY_$2"
OUT_CLIENT_KEY_SERVER2="OUT_CLIENT_KEY_$2"
TRUSTSTORE_SERVER2="TRUSTSTORE_$2"
TRUSTSTORE_PASSWORD_SERVER2="TRUSTSTORE_PASSWORD_$2"
KEYSTORE_SERVER2="KEYSTORE_$2"
KEYSTORE_PASSWORD_SERVER2="KEYSTORE_PASSWORD_$2"

echo
echo "Values in configureSSLCerts.sh: "
echo
echo "CONTAINER_ID_SERVER1 is ${!CONTAINER_ID_SERVER1}"
echo "IMAGE_ID_SERVER1 is ${!IMAGE_ID_SERVER1}"
echo "HOST_NAME_SERVER1 is ${!HOST_NAME_SERVER1}"
echo "IP_SERVER1 is ${!IP_SERVER1}"
echo "SSL_WORKING_DIR_SERVER1 is ${!SSL_WORKING_DIR_SERVER1}"
echo "SERVER_CERT_SERVER1 is ${!SERVER_CERT_SERVER1}"
echo "CLIENT_CERT_SERVER1 is ${!CLIENT_CERT_SERVER1}"
echo "CLIENT_KEY_SERVER1 is ${!CLIENT_KEY_SERVER1}"
echo "OUT_CLIENT_KEY_SERVER1 is ${!OUT_CLIENT_KEY_SERVER1}"
echo "TRUSTSTORE_SERVER1 is ${!TRUSTSTORE_SERVER1}"
echo "TRUSTSTORE_PASSWORD_SERVER1 is ${!TRUSTSTORE_PASSWORD_SERVER1}"
echo "KEYSTORE_SERVER1 is ${!KEYSTORE_SERVER1}"
echo "KEYSTORE_PASSWORD_SERVER1 is ${!KEYSTORE_PASSWORD_SERVER1}"
echo
echo "CONTAINER_ID_SERVER2 is ${!CONTAINER_ID_SERVER2}"
echo "IMAGE_ID_SERVER2 is ${!IMAGE_ID_SERVER2}"
echo "HOST_NAME_SERVER2 is ${!HOST_NAME_SERVER2}"
echo "IP_SERVER2 is ${!IP_SERVER2}"
echo "SSL_WORKING_DIR_SERVER2 is ${!SSL_WORKING_DIR_SERVER2}"
echo "SERVER_CERT_SERVER2 is ${!SERVER_CERT_SERVER2}"
echo "CLIENT_CERT_SERVER2 is ${!CLIENT_CERT_SERVER2}"
echo "CLIENT_KEY_SERVER2 is ${!CLIENT_KEY_SERVER2}"
echo "OUT_CLIENT_KEY_SERVER2 is ${!OUT_CLIENT_KEY_SERVER2}"
echo "TRUSTSTORE_SERVER2 is ${!TRUSTSTORE_SERVER2}"
echo "TRUSTSTORE_PASSWORD_SERVER2 is ${!TRUSTSTORE_PASSWORD_SERVER2}"
echo "KEYSTORE_SERVER2 is ${!KEYSTORE_SERVER2}"
echo "KEYSTORE_PASSWORD_SERVER2 is ${!KEYSTORE_PASSWORD_SERVER2}"
echo

#  Example, Reference $HOST_NAME_SERVER1 for property name
#  or ${!HOST_NAME_SERVER1} for property value
echo "Creating SSL Certificates between ${!HOST_NAME_SERVER1} and ${!HOST_NAME_SERVER2}"

#
# Create Server and client certificates for SSL
#
echo
echo "Creating ${!SSL_WORKING_DIR_SERVER1} on $1 docker container..."
#docker exec -d ${!CONTAINER_ID_SERVER1} mkdir -p ${!SSL_WORKING_DIR_SERVER1}
sudo docker exec ${!CONTAINER_ID_SERVER1} chmod -R 777 ${!SSL_WORKING_DIR_SERVER1}/*
sudo docker exec ${!CONTAINER_ID_SERVER1} chmod -R 777 ${!SSL_WORKING_DIR_SERVER1}/ssl/*

echo
echo "Creating ${!SSL_WORKING_DIR_SERVER2} on $2 docker container..."
#docker exec -d ${!CONTAINER_ID_SERVER2} mkdir -p ${!SSL_WORKING_DIR_SERVER2}
sudo docker exec ${!CONTAINER_ID_SERVER2} chmod -R 777 ${!SSL_WORKING_DIR_SERVER2}/*
sudo docker exec ${!CONTAINER_ID_SERVER2} chmod -R 777 ${!SSL_WORKING_DIR_SERVER2}/ssl/*

# Generate Server Certificates and copy to appropriate containers
echo
echo "Creating $1 Server Cert ${!SSL_WORKING_DIR_SERVER1}/${!SERVER_CERT_SERVER1} on $1 docker container..."
sudo docker exec ${!CONTAINER_ID_SERVER1} bash -c "${!SSL_WORKING_DIR_SERVER1}/ssl/createServerCert.sh --server ${!HOST_NAME_SERVER1} --servercert ${!SSL_WORKING_DIR_SERVER1}/${!SERVER_CERT_SERVER1} --keystore ${!KEYSTORE_SERVER1} --password ${!KEYSTORE_PASSWORD_SERVER1}"

echo
echo "Downloading $1 Server Cert ${!SSL_WORKING_DIR_SERVER1}/${!SERVER_CERT_SERVER1} to local workspace $WORKSPACE/${!SERVER_CERT_SERVER1}..."
sudo docker cp ${!CONTAINER_ID_SERVER1}:${!SSL_WORKING_DIR_SERVER1}/${!SERVER_CERT_SERVER1} "${WORKSPACE}"/${!SERVER_CERT_SERVER1}
sudo chmod 640 "${WORKSPACE}"/${!SERVER_CERT_SERVER1}

echo
echo "Uploading $1 Server Cert $WORKSPACE/${!SERVER_CERT_SERVER1} to $2 docker container..."
sudo docker cp "${WORKSPACE}"/${!SERVER_CERT_SERVER1} ${!CONTAINER_ID_SERVER2}:${!SSL_WORKING_DIR_SERVER2}/${!SERVER_CERT_SERVER1}

echo
echo "Creating $2 Server Cert ${!SSL_WORKING_DIR_SERVER2}/${!SERVER_CERT_SERVER2} on $2 docker container..."
sudo docker exec ${!CONTAINER_ID_SERVER2} bash -c "${!SSL_WORKING_DIR_SERVER2}/ssl/createServerCert.sh --server ${!HOST_NAME_SERVER2} --servercert ${!SSL_WORKING_DIR_SERVER2}/${!SERVER_CERT_SERVER2} --keystore ${!KEYSTORE_SERVER2} --password ${!KEYSTORE_PASSWORD_SERVER2}"

echo
echo "Downloading $2 Server Cert ${!SSL_WORKING_DIR_SERVER2}/${!SERVER_CERT_SERVER2} to local workspace $WORKSPACE/${!SERVER_CERT_SERVER2}..."
sudo docker cp ${!CONTAINER_ID_SERVER2}:${!SSL_WORKING_DIR_SERVER2}/${!SERVER_CERT_SERVER2} "${WORKSPACE}"/${!SERVER_CERT_SERVER2}
sudo chmod 640 "${WORKSPACE}"/${!SERVER_CERT_SERVER2}

echo
echo "Uploading $2 Server Cert $WORKSPACE/${!SERVER_CERT_SERVER2} to $1 docker container..."
sudo docker cp "${WORKSPACE}"/${!SERVER_CERT_SERVER2} ${!CONTAINER_ID_SERVER1}:${!SSL_WORKING_DIR_SERVER1}/${!SERVER_CERT_SERVER2}

# Import Server certificates that were just uploaded
echo
echo "Importing $2 Server Cert ${!SSL_WORKING_DIR_SERVER1}/${!SERVER_CERT_SERVER2} on $1 docker container..."
sudo docker exec ${!CONTAINER_ID_SERVER1} bash -c "${!SSL_WORKING_DIR_SERVER1}/ssl/importServerCert.sh --servercert ${!SSL_WORKING_DIR_SERVER1}/${!SERVER_CERT_SERVER2} --remote ${!HOST_NAME_SERVER2} --remoteport ${!SSL_PORT_SERVER2} --truststore ${!TRUSTSTORE_SERVER1} --password ${!TRUSTSTORE_PASSWORD_SERVER1}"

echo
echo "Importing $1 Server Cert ${!SSL_WORKING_DIR_SERVER2}/${!SERVER_CERT_SERVER1} on $2 docker container..."
sudo docker exec ${!CONTAINER_ID_SERVER2} bash -c "${!SSL_WORKING_DIR_SERVER2}/ssl/importServerCert.sh --servercert ${!SSL_WORKING_DIR_SERVER2}/${!SERVER_CERT_SERVER1} --remote ${!HOST_NAME_SERVER1} --remoteport ${!SSL_PORT_SERVER1} --truststore ${!TRUSTSTORE_SERVER2} --password ${!TRUSTSTORE_PASSWORD_SERVER2}"

# Generate Client Certificates and copy to appropriate containers
echo
echo "Creating $1 Client Cert ${!SSL_WORKING_DIR_SERVER1}/${!CLIENT_CERT_SERVER1} on $1 docker container..."
sudo docker exec ${!CONTAINER_ID_SERVER1} bash -c "${!SSL_WORKING_DIR_SERVER1}/ssl/createClientCert.sh --server ${!HOST_NAME_SERVER1} --remote ${!HOST_NAME_SERVER2} --clientcert ${!SSL_WORKING_DIR_SERVER1}/${!CLIENT_CERT_SERVER1} --servercert ${!SSL_WORKING_DIR_SERVER1}/${!SERVER_CERT_SERVER2} --clientkey ${!SSL_WORKING_DIR_SERVER1}/${!CLIENT_KEY_SERVER1} --keystore ${!KEYSTORE_SERVER1} --password ${!KEYSTORE_PASSWORD_SERVER1}"

echo
echo "Downloading $1 Client Cert ${!SSL_WORKING_DIR_SERVER1}/${!CLIENT_CERT_SERVER1} to local workspace $WORKSPACE/${!CLIENT_CERT_SERVER1}..."
sudo docker cp ${!CONTAINER_ID_SERVER1}:${!SSL_WORKING_DIR_SERVER1}/${!CLIENT_CERT_SERVER1} "${WORKSPACE}"/${!CLIENT_CERT_SERVER1}
sudo chmod 640 "${WORKSPACE}"/${!CLIENT_CERT_SERVER1}

echo
echo "Uploading $1 Client Cert $WORKSPACE/${!CLIENT_CERT_SERVER1} to $2 docker container..."
sudo docker cp "${WORKSPACE}"/${!CLIENT_CERT_SERVER1} ${!CONTAINER_ID_SERVER2}:${!SSL_WORKING_DIR_SERVER2}/${!CLIENT_CERT_SERVER1}

echo
echo "Downloading $1 Client Key ${!SSL_WORKING_DIR_SERVER1}/${!CLIENT_KEY_SERVER1} to local workspace $WORKSPACE/${!CLIENT_KEY_SERVER1}..."
sudo docker cp ${!CONTAINER_ID_SERVER1}:${!SSL_WORKING_DIR_SERVER1}/${!CLIENT_KEY_SERVER1} "${WORKSPACE}"/${!CLIENT_KEY_SERVER1}
sudo chmod 640 "${WORKSPACE}"/${!CLIENT_KEY_SERVER1}

echo
echo "Uploading $1 Client Key $WORKSPACE/${!CLIENT_KEY_SERVER1} to $2 docker container..."
sudo docker cp "${WORKSPACE}"/${!CLIENT_KEY_SERVER1} ${!CONTAINER_ID_SERVER2}:${!SSL_WORKING_DIR_SERVER2}/${!CLIENT_KEY_SERVER1}

echo
echo "Creating $2 Client Cert ${!SSL_WORKING_DIR_SERVER2}/${!CLIENT_CERT_SERVER2} on $2 docker container..."
sudo docker exec ${!CONTAINER_ID_SERVER2} bash -c "${!SSL_WORKING_DIR_SERVER2}/ssl/createClientCert.sh --server ${!HOST_NAME_SERVER2} --remote ${!HOST_NAME_SERVER1} --clientcert ${!SSL_WORKING_DIR_SERVER2}/${!CLIENT_CERT_SERVER2} --servercert ${!SSL_WORKING_DIR_SERVER2}/${!SERVER_CERT_SERVER1} --clientkey ${!SSL_WORKING_DIR_SERVER2}/${!CLIENT_KEY_SERVER2} --keystore ${!KEYSTORE_SERVER2} --password ${!KEYSTORE_PASSWORD_SERVER2}"

echo
echo "Downloading $2 Client Cert ${!SSL_WORKING_DIR_SERVER2}/${!CLIENT_CERT_SERVER2} to local workspace $WORKSPACE/${!CLIENT_CERT_SERVER2}..."
sudo docker cp ${!CONTAINER_ID_SERVER2}:${!SSL_WORKING_DIR_SERVER2}/${!CLIENT_CERT_SERVER2} "${WORKSPACE}"/${!CLIENT_CERT_SERVER2}
sudo chmod 640 "${WORKSPACE}"/${!CLIENT_CERT_SERVER2}

echo
echo "Uploading $2 Client Cert $WORKSPACE/${!CLIENT_CERT_SERVER2} to $1 docker container..."
sudo docker cp "${WORKSPACE}"/${!CLIENT_CERT_SERVER2} ${!CONTAINER_ID_SERVER1}:${!SSL_WORKING_DIR_SERVER1}/${!CLIENT_CERT_SERVER2}

echo
echo "Downloading $2 Client Key ${!SSL_WORKING_DIR_SERVER2}/${!CLIENT_KEY_SERVER2} to local workspace $WORKSPACE/${!CLIENT_KEY_SERVER2}..."
sudo docker cp ${!CONTAINER_ID_SERVER2}:${!SSL_WORKING_DIR_SERVER2}/${!CLIENT_KEY_SERVER2} "${WORKSPACE}"/${!CLIENT_KEY_SERVER2}
sudo chmod 640 "${WORKSPACE}"/${!CLIENT_KEY_SERVER2}

echo
echo "Uploading $2 Client Key $WORKSPACE/${!CLIENT_KEY_SERVER2} to $1 docker container..."
sudo docker cp "${WORKSPACE}"/${!CLIENT_KEY_SERVER2} ${!CONTAINER_ID_SERVER1}:${!SSL_WORKING_DIR_SERVER1}/${!CLIENT_KEY_SERVER2}

# Import Client certificates that were just uploaded
echo
echo "Importing $2 Client Cert ${!SSL_WORKING_DIR_SERVER1}/${!CLIENT_CERT_SERVER2} on $1 docker container..."
sudo docker exec ${!CONTAINER_ID_SERVER1} bash -c "${!SSL_WORKING_DIR_SERVER1}/ssl/importClientCert.sh --clientcert ${!SSL_WORKING_DIR_SERVER1}/${!CLIENT_CERT_SERVER2} --remote ${!HOST_NAME_SERVER2} --inclientkey ${!SSL_WORKING_DIR_SERVER1}/${!CLIENT_KEY_SERVER2} --outclientkey ${!SSL_WORKING_DIR_SERVER1}/${!OUT_CLIENT_KEY_SERVER2} --keystore ${!KEYSTORE_SERVER1} --password ${!KEYSTORE_PASSWORD_SERVER1}"
#sudo docker cp ${!CONTAINER_ID_SERVER1}:${!SSL_WORKING_DIR_SERVER1}/${!OUT_CLIENT_KEY_SERVER2} "${WORKSPACE}"/${!OUT_CLIENT_KEY_SERVER2}

echo
echo "Importing $1 Client Cert ${!SSL_WORKING_DIR_SERVER2}/${!CLIENT_CERT_SERVER1} on $2 docker container..."
sudo docker exec ${!CONTAINER_ID_SERVER2} bash -c "${!SSL_WORKING_DIR_SERVER2}/ssl/importClientCert.sh --clientcert ${!SSL_WORKING_DIR_SERVER2}/${!CLIENT_CERT_SERVER1} --remote ${!HOST_NAME_SERVER1} --inclientkey ${!SSL_WORKING_DIR_SERVER2}/${!CLIENT_KEY_SERVER1} --outclientkey ${!SSL_WORKING_DIR_SERVER2}/${!OUT_CLIENT_KEY_SERVER1} --keystore ${!KEYSTORE_SERVER2} --password ${!KEYSTORE_PASSWORD_SERVER2}"
#sudo docker cp ${!CONTAINER_ID_SERVER2}:${!SSL_WORKING_DIR_SERVER2}/${!OUT_CLIENT_KEY_SERVER1} "${WORKSPACE}"/${!OUT_CLIENT_KEY_SERVER1}

##############################################################

#
# Create Server and client certificates for SSL
#
#docker exec -d $NEO4J_CONTAINER_ID mkdir /home/jenkins
#docker exec -d $TOMCAT_CONTAINER_ID mkdir /home/jenkins
#docker exec -d $API_UMBRELLA_CONTAINER_ID mkdir /home/jenkins
#
# Generate Server Certificates and copy to appropriate containers
#docker exec -d $API_UMBRELLA_CONTAINER_ID /home/jenkins/ssl/createServerCert.sh --server "attune.api-umbrella" --servercert /home/jenkins/attune.api-umbrella.cer --keystore ${JAVA_HOME}/jre/lib/security/keystore.jks --password changeit
#docker cp $API_UMBRELLA_CONTAINER_ID:/home/jenkins/attune.api-umbrella.cer $WORKSPACE/attune.api-umbrella.cer
#docker cp $WORKSPACE/attune.api-umbrella.cer $TOMCAT_CONTAINER_ID:/home/jenkins/attune.api-umbrella.cer
#
#docker exec -d $TOMCAT_CONTAINER_ID /home/jenkins/ssl/createServerCert.sh --server "attune.tomcat" --servercert /home/jenkins/attune.tomcat.cer --keystore ${JAVA_HOME}/jre/lib/security/keystore.jks --password changeit
#docker cp $TOMCAT_CONTAINER_ID:/home/jenkins/attune.tomcat.cer $WORKSPACE/attune.tomcat.cer
#docker cp $WORKSPACE/attune.tomcat.cer $API_UMBRELLA_CONTAINER_ID:/home/jenkins/attune.tomcat.cer
#
# Import Server certificates that were just uploaded
#docker exec -d $API_UMBRELLA_CONTAINER_ID /home/jenkins/ssl/importServerCert.sh --servercert /home/jenkins/attune.tomcat.cer --remote attune.tomcat --truststore ${JAVA_HOME}/jre/lib/security/cacerts --password changeit 
#docker exec -d $TOMCAT_CONTAINER_ID /home/jenkins/ssl/importServerCert.sh --servercert /home/jenkins/attune.api-umbrella.cer --remote attune.api-umbrella --truststore ${JAVA_HOME}/jre/lib/security/cacerts --password changeit
#
# Generate Client Certificates and copy to appropriate containers
#docker exec -d $API_UMBRELLA_CONTAINER_ID /home/jenkins/ssl/createClientCert.sh --server attune.api-umbrella --remote attune.tomcat --clientcert /home/jenkins/attune.api-umbrella-client.cer --servercert /home/jenkins/attune.tomcat.cer --clientkey /home/jenkins/attune.api-umbrella-clientkey.pkcs8 --keystore ${JAVA_HOME}/jre/lib/security/keystore.jks --password changeit
#docker cp $API_UMBRELLA_CONTAINER_ID:/home/jenkins/attune.api-umbrella-client.cer $WORKSPACE/attune.api-umbrella-client.cer
#docker cp $WORKSPACE/attune.api-umbrella-client.cer $TOMCAT_CONTAINER_ID:/home/jenkins/attune.api-umbrella-client.cer
#docker cp $API_UMBRELLA_CONTAINER_ID:/home/jenkins/attune.api-umbrella-clientkey.pkcs8 $WORKSPACE/attune.api-umbrella-clientkey.pkcs8
#docker cp $WORKSPACE/attune.api-umbrella-clientkey.pkcs8 $TOMCAT_CONTAINER_ID:/home/jenkins/attune.api-umbrella-clientkey.pkcs8
#
#
#docker exec -d $TOMCAT_CONTAINER_ID /home/jenkins/ssl/createClientCert.sh --server attune.tomcat --remote attune.api-umbrella --clientcert /home/jenkins/attune.tomcat-client.cer --servercert /home/jenkins/attune.api-umbrella.cer --clientkey /home/jenkins/attune.tomcat-clientkey.pkcs8 --keystore ${JAVA_HOME}/jre/lib/security/keystore.jks --password changeit
#docker cp $TOMCAT_CONTAINER_ID:/home/jenkins/attune.tomcat-client.cer $WORKSPACE/attune.tomcat-client.cer
#docker cp $WORKSPACE/attune.tomcat-client.cer $API_UMBRELLA_CONTAINER_ID:/home/jenkins/attune.tomcat-client.cer
#docker cp $TOMCAT_CONTAINER_ID:/home/jenkins/attune.tomcat-clientkey.pkcs8 $WORKSPACE/attune.tomcat-clientkey.pkcs8
#docker cp $WORKSPACE/attune.tomcat-clientkey.pkcs8 $API_UMBRELLA_CONTAINER_ID:/home/jenkins/attune.tomcat-clientkey.pkcs8
#
# Import Client certificates that were just uploaded
#docker exec -d $API_UMBRELLA_CONTAINER_ID /home/jenkins/ssl/importClientCert.sh --clientcert /home/jenkins/attune.tomcat-client.cer --remote attune.tomcat --inclientkey /home/jenkins/attune.tomcat-clientkey.pkcs8 --outclientkey /home/jenkins/attune.tomcat-clientkey.p12 --keystore ${JAVA_HOME}/jre/lib/security/keystore.jks --password changeit
#docker exec -d $TOMCAT_CONTAINER_ID /home/jenkins/ssl/importClientCert.sh --clientcert /home/jenkins/attune.api-umbrella-client.cer --remote api-umbrella --inclientkey /home/jenkins/attune.api-umbrella-clientkey.pkcs8 --outclientkey /home/jenkins/attune.api-umbrella-clientkey.p12 --keystore ${JAVA_HOME}/jre/lib/security/keystore.jks --password changeit

