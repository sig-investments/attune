#!/bin/bash
#set -e
#
REMOTECLIENT=ec2-52-38-173-43.us-west-2.compute.amazonaws.com
WORKINGDIR=/home/jenkins
KEYSTORE=$WORKINGDIR/keystore.jks
KEYSTOREPASSWORD=changeit
CLIENTCERT=$WORKINGDIR/client.cer
INCLIENTKEY=$WORKINGDIR/clientkey.pkcs8
OUTCLIENTKEY=$WORKINGDIR/client.p12


while [[ ${1} ]]; do
        case "${1}" in
            --remote)
                REMOTECLIENT=${2}
                shift
                ;;
            --password)
                KEYSTOREPASSWORD=${2}
                shift
                ;;
            --wd)
                WORKINGDIR=${2}
                shift
                ;;
            --keystore)
                KEYSTORE=${2}
                shift
                ;;
            --clientcert)
                CLIENTCERT=${2}
                shift
                ;;
            --inclientkey)
                INCLIENTKEY=${2}
                shift
                ;;
            --outclientkey)
                OUTCLIENTKEY=${2}
                shift
                ;;
            *)
                echo "Unknown parameter: ${1}" >&2
                return 1
        esac

        if ! shift; then
            echo 'Missing parameter argument.' >&2
            return 1
        fi
done

if [ -z "$JAVA_HOME" ]; then
  JAVA_HOME=$(readlink -e /usr/bin/java | sed �s/\(.*\)bin\/java$/\1/�)
fi

if [ ! -d "$WORKINGDIR" ]; then
  mkdir $WORKINGDIR
fi

cd $WORKINGDIR

if [ ! -f "$KEYSTORE" ]; then
    echo
    echo "Creating Server Keystore $KEYSTORE to import client cert..."
    DIR=${KEYSTORE%/*}
    if [ ! -d "$DIR" ]; then
        mkdir -p $DIR
    fi
    $JAVA_HOME/bin/keytool -genkey -alias $REMOTECLIENT -keyalg RSA -keysize 2048 -dname "CN=$REMOTECLIENT,OU=SIG,O=BAH,L=McLean,ST=VA,C=US" -keystore $KEYSTORE -storepass $KEYSTOREPASSWORD -keypass $KEYSTOREPASSWORD
fi

#if [ -z "$JAVA_HOME/bin/keytool -list -alias $REMOTECLIENT" ]; then
#  echo
#  echo "Importing Client Cert $CLIENTCERT into Keystore $KEYSTORE ..."  
#  openssl pkcs12 -export -in $CLIENTCERT -inkey $INCLIENTKEY -password pass:$KEYSTOREPASSWORD -out $OUTCLIENTKEY
#  $JAVA_HOME/bin/keytool -delete -alias $REMOTECLIENT -keystore $KEYSTORE -storepass $KEYSTOREPASSWORD -noprompt
#  $JAVA_HOME/bin/keytool -importcert -alias $REMOTECLIENT -file $CLIENTCERT -keystore $KEYSTORE -storepass $KEYSTOREPASSWORD -noprompt
#fi


echo
echo "Importing Client Cert $CLIENTCERT into Keystore $KEYSTORE ..."  
#openssl pkcs12 -export -in $CLIENTCERT -inkey $INCLIENTKEY -password pass:$KEYSTOREPASSWORD -out $OUTCLIENTKEY
$JAVA_HOME/bin/keytool -delete -alias $REMOTECLIENT -keystore $KEYSTORE -storepass $KEYSTOREPASSWORD -noprompt
$JAVA_HOME/bin/keytool -importcert -alias $REMOTECLIENT -file $CLIENTCERT -keystore $KEYSTORE -storepass $KEYSTOREPASSWORD -noprompt


#rm $CLIENTCERT
echo