#!/bin/bash
#set -e
#
REMOTESERVER=ec2-52-42-37-45.us-west-2.compute.amazonaws.com
REMOTESERVER_SSL_PORT=443
WORKINGDIR=/home/jenkins
TRUSTSTORE=${JAVA_HOME}/jre/lib/security/cacerts
TRUSTSTOREPASSWORD=changeit
SERVERCERT=$WORKINGDIR/server.cer
#EC2PRIVATEKEY=/home/ec2-user/Api-Management.pem
#EC2USER=ec2-user

while [[ ${1} ]]; do
        case "${1}" in
            --remote)
                REMOTESERVER=${2}
                shift
                ;;
            --remoteport)
                REMOTESERVER_SSL_PORT=${2}
                shift
                ;;
            --password)
                TRUSTSTOREPASSWORD=${2}
                shift
                ;;
            --truststore)
                TRUSTSTORE=${2}
                shift
                ;;
            --wd)
                WORKINGDIR=${2}
                shift
                ;;
            --servercert)
                SERVERCERT=${2}
                shift
                ;;
            --ec2key)
                EC2PRIVATEKEY=${2}
                shift
                ;;
            --ec2user)
                EC2USER=${2}
                shift
                ;;
            *)
                echo "Unknown parameter: ${1}" >&2
                return 1
        esac

        if ! shift; then
            echo 'Missing parameter argument.' >&2
            return 1
        fi
done

if [ -z "$JAVA_HOME" ]; then
  JAVA_HOME=$(readlink -e /usr/bin/java | sed �s/\(.*\)bin\/java$/\1/�)
fi

if [ ! -d "$WORKINGDIR" ]; then
  mkdir $WORKINGDIR
fi

cd $WORKINGDIR

if [ -z ${EC2PRIVATEKEY+x} ] && [ -z ${EC2USER+x} ];
then 
  echo "EC2PRIVATEKEY or EC2USER is unset"; 
else 
  scp -i $EC2PRIVATEKEY $EC2USER@$REMOTESERVER:$SERVERCERT $WORKINGDIR
fi

if [ ! -f "$TRUSTSTORE" ]; then
    $JAVA_HOME/bin/javac -classpath . $WORKINGDIR/ssl/InstallCert.java
    $JAVA_HOME/bin/java -classpath ./ssl InstallCert $REMOTESERVER:$REMOTESERVER_SSL_PORT $TRUSTSTORE $TRUSTSTOREPASSWORD

    echo
    echo "Creating Client Truststore $TRUSTSTORE to import server cert..."
    DIR=${TRUSTSTORE%/*}
    if [ ! -d "$DIR" ]; then
        mkdir -p $DIR
    fi    
    $JAVA_HOME/bin/keytool -genkey -alias $REMOTESERVER -keyalg RSA -dname "CN=$REMOTESERVER,OU=SIG,O=BAH,L=McLean,ST=VA,C=US" -keysize 2048 -keystore $TRUSTSTORE -storepass $TRUSTSTOREPASSWORD -keypass $TRUSTSTOREPASSWORD
fi

#export JAVA_HOME=$(/usr/libexec/java_home -v 1.7)
#if [ -z "$JAVA_HOME/bin/keytool -list -alias $REMOTESERVER" ]; then
#  $JAVA_HOME/bin/keytool -delete -alias $REMOTESERVER -keystore $TRUSTSTORE -storepass $TRUSTSTOREPASSWORD
#  $JAVA_HOME/bin/keytool -import -alias $REMOTESERVER -file $SERVERCERT -keystore $TRUSTSTORE -storepass $TRUSTSTOREPASSWORD
#fi

echo
echo "Importing Server Cert $SERVERCERT into Truststore $TRUSTSTORE ..."
$JAVA_HOME/bin/keytool -delete -alias $REMOTESERVER -keystore $TRUSTSTORE -storepass $TRUSTSTOREPASSWORD -noprompt
$JAVA_HOME/bin/keytool -import -alias $REMOTESERVER -file $SERVERCERT -keystore $TRUSTSTORE -storepass $TRUSTSTOREPASSWORD -noprompt

#rm -f $SERVERCERT
echo