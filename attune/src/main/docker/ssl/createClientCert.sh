#!/bin/bash
#set -e
#
SERVER=ec2-52-38-173-43.us-west-2.compute.amazonaws.com
REMOTESERVER=ec2-52-42-37-45.us-west-2.compute.amazonaws.com
PASSWORD=changeit
OU=SIG
O=BAH
L=McLean
ST=VA
C=US
WORKINGDIR=/home/jenkins
KEYSTORE=$WORKINGDIR/keystore.jks
SERVERCERT=$WORKINGDIR/server.cer
CLIENTCERT=$WORKINGDIR/client.cer
CLIENTKEY=$WORKINGDIR/clientkey.pkcs8
#EC2PRIVATEKEY=$WORKINGDIR/Api-Management.pem
#EC2USER=ec2-user
LOCAL_CONTAINER_ID=
REMOTE_CONTAINER_ID=

while [[ ${1} ]]; do
        case "${1}" in
            --server)
                SERVER=${2}
                shift
                ;;
            --remote)
                REMOTESERVER=${2}
                shift
                ;;
            --password)
                PASSWORD=${2}
                shift
                ;;
            --ou)
                OU=${2}
                shift
                ;;
            --o)
                O=${2}
                shift
                ;;
            --l)
                L=${2}
                shift
                ;;
            --st)
                ST=${2}
                shift
                ;;
            --c)
                C=${2}
                shift
                ;;
            --wd)
                WORKINGDIR=${2}
                shift
                ;;
            --keystore)
                KEYSTORE=${2}
                shift
                ;;
            --servercert)
                SERVERCERT=${2}
                shift
                ;;
            --clientcert)
                CLIENTCERT=${2}
                shift
                ;;
            --clientkey)
                CLIENTKEY=${2}
                shift
                ;;
            --ec2key)
                EC2PRIVATEKEY=${2}
                shift
                ;;
            --ec2user)
                EC2USER=${2}
                shift
                ;;
            --remotecontainerid)
                REMOTE_CONTAINER_ID=${2}
                shift
                ;;
            --localcontainerid)
                LOCAL_CONTAINER_ID=${2}
                shift
                ;;
            *)
                echo "Unknown parameter: ${1}" >&2
                return 1
        esac

        if ! shift; then
            echo 'Missing parameter argument.' >&2
            return 1
        fi
done

if [ -f "$CLIENTCERT" ] && [ -f "$CLIENTKEY" ]; then
  exit 0
fi

if [ -z "$JAVA_HOME" ]; then
  echo
  echo "Seting JAVA_HOME ..."
  JAVA_HOME=$(readlink -e /usr/bin/java | sed �s/\(.*\)bin\/java$/\1/�)
fi

if [ ! -d "$WORKINGDIR" ]; then
  mkdir $WORKINGDIR
fi

cd $WORKINGDIR

#sudo rm -f $KEYSTORE
#sudo rm -f $SERVERCERT
#sudo rm -f $CLIENTCERT
#sudo rm -f $CLIENTKEY
#sudo rm -f $WORKINGDIR/DumpPrivateKey.java

if [ -z ${EC2PRIVATEKEY+x} ] && [ -z ${EC2USER+x} ];
then 
  echo "EC2PRIVATEKEY or EC2USER is unset"; 
else 
  scp -i $EC2PRIVATEKEY $EC2USER@$REMOTESERVER:$SERVERCERT $WORKINGDIR 
fi

if [ ! -f "$KEYSTORE" ]; then
    #$JAVA_HOME/bin/keytool -genkey -alias $SERVER -keyalg RSA -keystore $KEYSTORE -keypass $PASSWORD -keysize 2048
    echo
    echo "Creating Client Keystore $KEYSTORE"
    DIR=${KEYSTORE%/*}
    if [ ! -d "$DIR" ]; then
        mkdir -p $DIR
    fi
    $JAVA_HOME/bin/keytool -genkey -alias $SERVER -keyalg RSA -dname "CN=$SERVER,OU=$OU,O=$O,L=$L,ST=$ST,C=$C" -keystore $KEYSTORE -storepass $PASSWORD -keypass $PASSWORD -keysize 2048
fi

echo
echo "Importing Server Cert $SERVERCERT into keystore $KEYSTORE ..."
$JAVA_HOME/bin/keytool -importcert -alias $REMOTESERVER -noprompt -file $SERVERCERT -keystore $KEYSTORE -storepass $PASSWORD -keypass $PASSWORD

echo
echo "Generating key pair..."
$JAVA_HOME/bin/keytool -delete -alias $SERVER -keystore $KEYSTORE -storepass $PASSWORD -keypass $PASSWORD -noprompt
$JAVA_HOME/bin/keytool -genkeypair -alias $SERVER -keyalg RSA -keysize 2048 -dname "CN=$SERVER,OU=$OU,O=$O,L=$L,ST=$ST,C=$C" -keypass $PASSWORD -keystore $KEYSTORE -storepass $PASSWORD -validity 999

$JAVA_HOME/bin/keytool -list -keystore $KEYSTORE -storepass $PASSWORD

echo
echo "Creating Client Cert $CLIENTCERT ..."
$JAVA_HOME/bin/keytool -rfc -exportcert -alias $SERVER -file $CLIENTCERT -keypass $PASSWORD -keystore $KEYSTORE -storepass $PASSWORD

if [ -z ${EC2PRIVATEKEY+x} ] && [ -z ${EC2USER+x} ];
then 
  echo "EC2PRIVATEKEY or EC2USER is unset"; 
else 
  scp -i $EC2PRIVATEKEY -o StrictHostKeyChecking=no $CLIENTCERT $EC2USER@$REMOTESERVER:$WORKINGDIR 
fi

cat > $WORKINGDIR/DumpPrivateKey.java << DUMPPRIVATEKEY_JAVA
public class DumpPrivateKey { 
  public static void main(String[] args) throws Exception { 
  final String keystoreName = args[0];
    final String keystorePassword = args[1];
    final String alias = args[2];
    java.security.KeyStore ks = java.security.KeyStore.getInstance("jks");
    ks.load(new java.io.FileInputStream(keystoreName), keystorePassword.toCharArray());
    System.out.println("-----BEGIN PRIVATE KEY-----");
    System.out.println(java.util.Base64.getEncoder().encode(ks.getKey(alias, keystorePassword.toCharArray()).getEncoded()));
    System.out.println("-----END PRIVATE KEY-----");
  }
}

DUMPPRIVATEKEY_JAVA

$JAVA_HOME/bin/javac -classpath . $WORKINGDIR/DumpPrivateKey.java

echo
echo "Creating Client Key $CLIENTKEY ..."
$JAVA_HOME/bin/java -classpath . DumpPrivateKey $KEYSTORE $PASSWORD $SERVER > $CLIENTKEY

if [ -z ${EC2PRIVATEKEY+x} ] && [ -z ${EC2USER+x} ];
then 
  echo "EC2PRIVATEKEY or EC2USER is unset"; 
else 
  scp -i $EC2PRIVATEKEY -o StrictHostKeyChecking=no $CLIENTKEY $EC2USER@$REMOTESERVER:$WORKINGDIR 
fi

#sudo rm -f $CLIENTCERT
#sudo rm -f $CLIENTKEY
rm -f $WORKINGDIR/DumpPrivateKey.class
rm -f $WORKINGDIR/DumpPrivateKey.java
#sudo rm -f $SERVERCERT