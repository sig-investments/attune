#!/bin/bash
#set -e
#
SERVER=ec2-52-42-37-45.us-west-2.compute.amazonaws.com
PASSWORD=changeit
OU=SIG
O=BAH
L=McLean
ST=VA
C=US
WORKINGDIR=/home/jenkins
KEYSTORE=$WORKINGDIR/keystore.jks
SERVERCERT=$WORKINGDIR/server.cer

while [[ ${1} ]]; do
        case "${1}" in
            --server)
                SERVER=${2}
                shift
                ;;
            --password)
                PASSWORD=${2}
                shift
                ;;
            --ou)
                OU=${2}
                shift
                ;;
            --o)
                O=${2}
                shift
                ;;
            --l)
                L=${2}
                shift
                ;;
            --st)
                ST=${2}
                shift
                ;;
            --c)
                C=${2}
                shift
                ;;
            --wd)
                WORKINGDIR=${2}
                shift
                ;;
            --keystore)
                KEYSTORE=${2}
                shift
                ;;
            --servercert)
                SERVERCERT=${2}
                shift
                ;;
            *)
                echo "Unknown parameter: ${1}" >&2
                return 1
        esac

        if ! shift; then
            echo 'Missing parameter argument.' >&2
            return 1
        fi
done

if [ -z "$JAVA_HOME" ]; then
  JAVA_HOME=$(readlink -e /usr/bin/java | sed �s/\(.*\)bin\/java$/\1/�)
fi

if [ ! -d "$WORKINGDIR" ]; then
  mkdir $WORKINGDIR
fi

cd $WORKINGDIR

#sudo rm -f $KEYSTORE
if [ ! -f "$KEYSTORE" ]; then
    echo
    echo "Creating Server Keystore $KEYSTORE ...."
    DIR=${KEYSTORE%/*}
    if [ ! -d "$DIR" ]; then
        echo
        echo "Creating Directory $DIR ...."
        mkdir -p -m 775 $DIR
    else
        echo "Directory $DIR exists"
    fi
    $JAVA_HOME/bin/keytool -genkey -alias $SERVER -keyalg RSA -dname "CN=$SERVER,OU=SIG,O=BAH,L=McLean,ST=VA,C=US" -keystore $KEYSTORE -storepass $PASSWORD -keypass $PASSWORD -keysize 2048
else
  echo
  echo "$KEYSTORE already created."
fi

#sudo rm -f $SERVERCERT
if [ ! -f "$SERVERCERT" ]; then
  echo
  echo "Creating Server Cert $SERVERCERT ...."
  $JAVA_HOME/bin/keytool -delete -alias $SERVER -keystore $KEYSTORE -storepass $PASSWORD -noprompt  
  $JAVA_HOME/bin/keytool -genkeypair -alias $SERVER -keyalg RSA -keysize 2048 -dname "CN=$SERVER,OU=$OU,O=$O,L=$L,ST=$ST,C=$C" -keystore $KEYSTORE -keypass $PASSWORD -storepass $PASSWORD -validity 999
  $JAVA_HOME/bin/keytool -exportcert -alias $SERVER -file $SERVERCERT -keystore $KEYSTORE -storepass $PASSWORD
else
  echo
  echo "$SERVERCERT already created."
fi
echo 