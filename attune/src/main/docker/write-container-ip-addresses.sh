#!/bin/bash
#
# Determine individual Docker Container IP addresses for Tomcat, Neo4j, and Api Umbrella and then write them to a file for later use
#
if [ -z "$1" ]; then
  CONTAINER_NAME=
  echo "Getting All Container IP Addresses ....."
else
  CONTAINER_NAME="${1}"
  echo "Getting ${1} Container IP Address ....."
fi

if [ -z "$WORKSPACE" ]; then
  PRESENT_DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd )"
  WORKSPACE="$(readlink -f "${PRESENT_DIR}"/../../../../)"
fi

if [ ! -f "${WORKSPACE}"/docker.properties ]; then
  echo "File ${WORKSPACE}/docker.properties not found!"
  exit 1 
fi

echo "Reading docker.properties to set environment"
while read -r line; 
do
  if [[ ${line:0:1} == '#' ]]
  then
    echo 'ignoring comment in properties file'
  else
    declare $line;
    IFS='=' read -r -a array <<< "$line"
    key=$(echo "${array[0]}" | sed 's/\\./_/g')
    value="${array[1]}"
    echo "Exporting ${key}=${value} to environment"
    export "${key}"="${value}"
  fi    
done < "${WORKSPACE}"/docker.properties

if [[ $(sudo docker ps -aq) ]]; then
  for i in $(sudo docker ps -aq)
  do
      CURRENT_CONTAINER_NAME=$( "${WORKSPACE}"/attune/src/main/docker/get-docker-container-name.sh $i )
      IP_ADDRESS=$( "${WORKSPACE}"/attune/src/main/docker/get-docker-container-ip.sh $i )
      
      echo "Current Container Name = $CURRENT_CONTAINER_NAME"
      echo "Current Container Id = $i"
      echo "Current Container IP Address = $IP_ADDRESS"
    
      if [ -z "$CONTAINER_NAME" ]; then
        if [ "$i" = "$CONTAINER_ID_TOMCAT" ];
        then
          echo "Writting Container IP Address $IP_ADDRESS for $CURRENT_CONTAINER_NAME to $WORKSPACE/docker.properties"
          echo "IP_TOMCAT=$IP_ADDRESS" >> "$WORKSPACE"/docker.properties
        fi
            
        if [ "$i" = "$CONTAINER_ID_NEO4J" ];
        then
          echo "Writting Container IP Address $IP_ADDRESS for $CURRENT_CONTAINER_NAME to $WORKSPACE/docker.properties"
          IP_NEO4J=$( "${WORKSPACE}"/attune/src/main/docker/get-docker-container-ip.sh $i )
          echo "IP_NEO4J=$IP_NEO4J" >> "$WORKSPACE"/docker.properties  
        fi
      else
        if [ "$CURRENT_CONTAINER_NAME" = "$CONTAINER_NAME" ]; then
          if [ "$i" = "$CONTAINER_ID_TOMCAT" ];
          then
            echo "Writting Container IP Address $IP_ADDRESS for $CURRENT_CONTAINER_NAME to $WORKSPACE/docker.properties"
            echo "IP_TOMCAT=$IP_ADDRESS" >> "$WORKSPACE"/docker.properties
          fi
              
          if [ "$i" = "$CONTAINER_ID_NEO4J" ];
          then
            echo "Writting Container IP Address $IP_ADDRESS for $CURRENT_CONTAINER_NAME to $WORKSPACE/docker.properties"
            IP_NEO4J=$( "${WORKSPACE}"/attune/src/main/docker/get-docker-container-ip.sh $i )
            echo "IP_NEO4J=$IP_NEO4J" >> "$WORKSPACE"/docker.properties  
          fi          
        fi
      fi      
  done
else
  echo "No Docker containers are running!"
  exit 1
fi
echo