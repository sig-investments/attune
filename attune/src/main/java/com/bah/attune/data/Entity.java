package com.bah.attune.data;

import java.util.List;

import javax.validation.constraints.NotNull;

import com.fasterxml.jackson.annotation.JsonIgnore;

public class Entity
{
	@JsonIgnore		//To pass Fortify Scan
	private String dummyVariableForJsonIgnore;	//To pass Fortify Scan

	private String neo4jId;
    private String label;
    private List<NameValuePair> properties;


    public String getNeo4jId()
    {
        return neo4jId;
    }


    public void setNeo4jId(String neo4jId)
    {
        this.neo4jId = neo4jId;
    }
    
    public String getLabel()
    {
        return label;
    }


    public void setLabel(String label)
    {
        this.label = label;
    }


    public List<NameValuePair> getProperties()
    {
        return properties;
    }


    public void setProperties(List<NameValuePair> properties)
    {
        this.properties = properties;
    }

    public String getNames()
    {
    	StringBuilder buffer = new StringBuilder();
    	for (NameValuePair pair : getProperties())
    	{
    		buffer.append(pair.getName()).append(",");
    	}
    	if (buffer.length() > 0)
    	{
    		buffer.setLength(buffer.length() - 1); // remove last , character
    	}
    	return buffer.toString();
    }
    
    public String getPropertyValue(String property)
    {
        for (NameValuePair pair : properties)
        {
            if ( property.equals(pair.getName()) )
                return pair.getValue();
        }

        return "";
    }
}
