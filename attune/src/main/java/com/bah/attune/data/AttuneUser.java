package com.bah.attune.data;

import lombok.Data;
import lombok.ToString;

import java.util.Date;

@Data
@ToString(exclude={"uuid", "password", "accountCreateDate", "lastLoginDate", "passwordHistory",
        "lastLoginFailDate", "lastLoginFailCount" })
public class AttuneUser
{
   public String uuid;
   public String username;
   public String password;
   public String firstName;
   public String lastName;
   public String phone;
   public String email;
   public String role;
   public Boolean isActive;
   public Date accountCreateDate;
   public Date lastLoginDate;
   public String passwordHistory;
   public Date lastLoginFailDate;
   public int lastLoginFailCount;

}
