package com.bah.attune.data;

import com.fasterxml.jackson.annotation.JsonIgnore;

public class EntityRelationship {
	
	@JsonIgnore		//To pass Fortify Scan
	private String dummyVariableForJsonIgnore;	//To pass Fortify Scan

	private String neo4jId;
	private String startNodeId;
	private String startNodeType;
	private String startNodeName;
	private String endNodeId;
	private String endNodeType;
	private String endNodeName;
	private String required;
	private String relationshipType;
	
	public String getNeo4jId() {
		return neo4jId;
	}
	
	public void setNeo4jId(String neo4jId) {
		this.neo4jId = neo4jId;
	}
	
	public String getStartNodeId() {
		return startNodeId;
	}
	
	public void setStartNodeId(String startNodeId) {
		this.startNodeId = startNodeId;
	}
	
	public String getStartNodeType() {
		return startNodeType;
	}
	
	public void setStartNodeType(String startNodeType) {
		this.startNodeType = startNodeType;
	}
	
	public String getStartNodeName() {
		return startNodeName;
	}
	
	public void setStartNodeName(String startNodeName) {
		this.startNodeName = startNodeName;
	}
	
	public String getRelationshipType() {
		return relationshipType;
	}
	
	public void setRelationshipType(String relationshipType) {
		this.relationshipType = relationshipType;
	}
	
	public String getEndNodeId() {
		return endNodeId;
	}
	
	public void setEndNodeId(String endNodeId) {
		this.endNodeId = endNodeId;
	}
	
	public String getEndNodeType() {
		return endNodeType;
	}
	
	public void setEndNodeType(String endNodeType) {
		this.endNodeType = endNodeType;
	}
	
	public String getEndNodeName() {
		return endNodeName;
	}
	
	public void setEndNodeName(String endNodeName) {
		this.endNodeName = endNodeName;
	}
	
	public String getRequired() {
		return required;
	}
	
	public void setRequired(String required) {
		this.required = required;
	}
	
}
