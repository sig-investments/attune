package com.bah.attune.data;

import org.springframework.http.HttpStatus;

public class AttuneException extends RuntimeException
{
    private static final long serialVersionUID = 1L;
    
    private HttpStatus statusCode = null;

	public AttuneException() {
		super();
	}
	
	public AttuneException(String message, Throwable cause, HttpStatus statusCode) {
		super(message, cause);
		this.statusCode = statusCode;
	}
	
	public AttuneException(String message, HttpStatus statusCode) {
		super(message);
		this.statusCode = statusCode;
	}
	
	public AttuneException(Throwable cause, HttpStatus statusCode) {
		super(cause.getMessage(), cause);
		this.statusCode = statusCode;
	}

	public AttuneException(String message, Throwable cause) {
		super(message, cause);
	}

	public AttuneException(String message) {
		super(message);
	}

	public AttuneException(Throwable cause) {
		super(cause.getMessage(), cause);
	}

	public HttpStatus getStatusCode() {
		return statusCode;
	}	
}
