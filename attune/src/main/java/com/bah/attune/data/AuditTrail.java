package com.bah.attune.data;

import lombok.Data;

@Data
public class AuditTrail
{
	private String username;
	private String time;
	private String IP;
	private String action;
	private String originalValue;
	private String updatedValue;
}
