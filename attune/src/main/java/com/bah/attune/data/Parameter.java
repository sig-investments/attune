package com.bah.attune.data;

import com.fasterxml.jackson.annotation.JsonIgnore;

public class Parameter
{
	@JsonIgnore		//To pass Fortify Scan
	private String dummyVariableForJsonIgnore;	//To pass Fortify Scan

	private String property;
    private String value;


    public String getProperty()
    {
        return property;
    }


    public void setProperty(String property)
    {
        this.property = property;
    }


    public String getValue()
    {
        return value;
    }


    public void setValue(String value)
    {
        this.value = value;
    }
}
