package com.bah.attune.rest;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.Reader;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.commons.io.IOUtils;
import org.json.simple.JSONArray;
import org.json.simple.JSONAware;
import org.json.simple.JSONObject;
import org.json.simple.JSONValue;
import org.json.simple.parser.JSONParser;

import com.bah.attune.data.AttuneException;

public class Neo4JUtils {

	private static JSONParser parser = new JSONParser();
	private static final String METADATA = "metadata"; 
	
	private Neo4JUtils() {

	}

	public static boolean isEmpty(String json) {
		if (json == null || json.isEmpty()) {
			return true;
		}
		return Neo4JUtils.isEmpty(new InputStreamReader(new ByteArrayInputStream(json.getBytes())));
	}

	public static boolean isEmpty(JSONAware json) {
		if (json == null) {
			return true;
		}
		return Neo4JUtils.isEmpty(new InputStreamReader(new ByteArrayInputStream(json.toJSONString().getBytes())));
	}

	public static int getSize(String json) {
		if (json == null || json.isEmpty()) {
			return 0;
		}
		return Neo4JUtils.getSize(new InputStreamReader(new ByteArrayInputStream(json.getBytes())));
	}

	public static int getSize(JSONAware json) {
		if (json == null) {
			return 0;
		}
		return Neo4JUtils.getSize(new InputStreamReader(new ByteArrayInputStream(json.toJSONString().getBytes())));
	}

	public static String getId(String json) {
		if (json == null || json.isEmpty()) {
			return null;
		}
		return Neo4JUtils.getId(new InputStreamReader(new ByteArrayInputStream(json.getBytes())));
	}

	public static String getId(JSONAware json) {
		if (json == null) {
			return null;
		}
		return Neo4JUtils.getId(new InputStreamReader(new ByteArrayInputStream(json.toJSONString().getBytes())));
	}

	public static Map<String, Object> getProperties(String json) {
		try {
			if (json == null || json.isEmpty()) {
				return new HashMap<>();
			}
			return Neo4JUtils.getProperties(new InputStreamReader(new ByteArrayInputStream(json.getBytes())));
		} catch (Exception e) {
			throw new AttuneException(e.getMessage(), e);
		}
	}

	public static Map<String, Object> getProperties(JSONAware json) {
		try {
			if (json == null) {
				return new HashMap<>();
			}
			return Neo4JUtils
					.getProperties(new InputStreamReader(new ByteArrayInputStream(json.toJSONString().getBytes())));
		} catch (Exception e) {
			throw new AttuneException(e.getMessage(), e);
		}
	}

	public static Map<String, Object> getMetadataProperties(String json) {
		try {
			if (json == null || json.isEmpty()) {
				return new HashMap<>();
			}
			return Neo4JUtils.getMetadataProperties(new InputStreamReader(new ByteArrayInputStream(json.getBytes())));
		} catch (Exception e) {
			throw new AttuneException(e.getMessage(), e);
		}
	}

	public static Map<String, Object> getMetadataProperties(JSONAware json) {
		try {
			if (json == null) {
				return new HashMap<>();
			}
			return Neo4JUtils.getMetadataProperties(
					new InputStreamReader(new ByteArrayInputStream(json.toJSONString().getBytes())));
		} catch (Exception e) {
			throw new AttuneException(e.getMessage(), e);
		}
	}

	public static String getMetadataValue(String json, String propName) {
		try {
			if (json == null || json.isEmpty()) {
				return null;
			}
			return Neo4JUtils.getMetadataValue(new InputStreamReader(new ByteArrayInputStream(json.getBytes())),
					propName);
		} catch (Exception e) {
			throw new AttuneException(e.getMessage(), e);
		}
	}

	public static String getMetadataValue(JSONAware json, String propName) {
		try {
			if (json == null) {
				return null;
			}
			return Neo4JUtils.getMetadataValue(
					new InputStreamReader(new ByteArrayInputStream(json.toJSONString().getBytes())), propName);
		} catch (Exception e) {
			throw new AttuneException(e.getMessage(), e);
		}
	}

	public static Map<String, Object> getDataProperties(String json) {
		try {
			if (json == null || json.isEmpty()) {
				return new HashMap<>();
			}
			return Neo4JUtils.getDataProperties(new InputStreamReader(new ByteArrayInputStream(json.getBytes())));
		} catch (Exception e) {
			throw new AttuneException(e.getMessage(), e);
		}
	}

	public static Map<String, Object> getDataProperties(JSONAware json) {
		try {
			if (json == null) {
				return new HashMap<>();
			}
			return Neo4JUtils
					.getDataProperties(new InputStreamReader(new ByteArrayInputStream(json.toJSONString().getBytes())));
		} catch (Exception e) {
			throw new AttuneException(e.getMessage(), e);
		}
	}

	public static String getDataValue(String json, String propName) {
		try {
			if (json == null || json.isEmpty()) {
				return null;
			}
			return Neo4JUtils.getDataValue(new InputStreamReader(new ByteArrayInputStream(json.getBytes())), propName);
		} catch (Exception e) {
			throw new AttuneException(e.getMessage(), e);
		}
	}

	public static String getDataValue(JSONAware json, String propName) {
		try {
			if (json == null) {
				return null;
			}
			return Neo4JUtils.getDataValue(
					new InputStreamReader(new ByteArrayInputStream(json.toJSONString().getBytes())), propName);
		} catch (Exception e) {
			throw new AttuneException(e.getMessage(), e);
		}
	}

	public static String getValue(String json, String propName) {
		try {
			if (json == null || json.isEmpty()) {
				return null;
			}
			return Neo4JUtils.getValue(new InputStreamReader(new ByteArrayInputStream(json.getBytes())), propName);
		} catch (Exception e) {
			throw new AttuneException(e.getMessage(), e);
		}
	}

	public static String getValue(JSONAware json, String propName) {
		try {
			if (json == null) {
				return null;
			}
			return Neo4JUtils.getValue(new InputStreamReader(new ByteArrayInputStream(json.toJSONString().getBytes())),
					propName);
		} catch (Exception e) {
			throw new AttuneException(e.getMessage(), e);
		}
	}

	public static Object getValue(String json) {
		try {
			if (json == null || json.isEmpty()) {
				return null;
			}
			return Neo4JUtils.getValue(new InputStreamReader(new ByteArrayInputStream(json.getBytes())));
		} catch (Exception e) {
			throw new AttuneException(e.getMessage(), e);
		}
	}

	public static Object getValue(JSONAware json) {
		try {
			if (json == null) {
				return null;
			}
			return Neo4JUtils.getValue(new InputStreamReader(new ByteArrayInputStream(json.toJSONString().getBytes())));
		} catch (Exception e) {
			throw new AttuneException(e.getMessage(), e);
		}
	}

	public static List<String> getLabels(String json) {
		try {
			if (json == null || json.isEmpty()) {
				return new ArrayList<>();
			}
			return Neo4JUtils.getLabels(new InputStreamReader(new ByteArrayInputStream(json.getBytes())));
		} catch (Exception e) {
			throw new AttuneException(e.getMessage(), e);
		}
	}

	public static List<String> getLabels(JSONAware json) {
		try {
			if (json == null) {
				return new ArrayList<>();
			}
			return Neo4JUtils
					.getLabels(new InputStreamReader(new ByteArrayInputStream(json.toJSONString().getBytes())));
		} catch (Exception e) {
			throw new AttuneException(e.getMessage(), e);
		}
	}

	public static String getRelationshipType(String json) {
		try {
			if (json == null || json.isEmpty()) {
				return null;
			}
			return Neo4JUtils.getRelationshipType(new InputStreamReader(new ByteArrayInputStream(json.getBytes())));
		} catch (Exception e) {
			throw new AttuneException(e.getMessage(), e);
		}
	}

	public static String getRelationshipType(JSONAware json) {
		try {
			if (json == null) {
				return null;
			}
			return Neo4JUtils.getRelationshipType(
					new InputStreamReader(new ByteArrayInputStream(json.toJSONString().getBytes())));
		} catch (Exception e) {
			throw new AttuneException(e.getMessage(), e);
		}
	}

	public static String getRelationshipStartNode(String json) {
		try {
			if (json == null || json.isEmpty()) {
				return null;
			}
			return Neo4JUtils
					.getRelationshipStartNode(new InputStreamReader(new ByteArrayInputStream(json.getBytes())));
		} catch (Exception e) {
			throw new AttuneException(e.getMessage(), e);
		}
	}

	public static String getRelationshipStartNode(JSONAware json) {
		try {
			if (json == null) {
				return null;
			}
			return Neo4JUtils.getRelationshipStartNode(
					new InputStreamReader(new ByteArrayInputStream(json.toJSONString().getBytes())));
		} catch (Exception e) {
			throw new AttuneException(e.getMessage(), e);
		}
	}

	public static String getRelationshipEndNode(String json) {
		try {
			if (json == null || json.isEmpty()) {
				return null;
			}
			return Neo4JUtils.getRelationshipEndNode(new InputStreamReader(new ByteArrayInputStream(json.getBytes())));
		} catch (Exception e) {
			throw new AttuneException(e.getMessage(), e);
		}
	}

	public static String getRelationshipEndNode(JSONAware json) {
		try {
			if (json == null) {
				return null;
			}
			return Neo4JUtils.getRelationshipEndNode(
					new InputStreamReader(new ByteArrayInputStream(json.toJSONString().getBytes())));
		} catch (Exception e) {
			throw new AttuneException(e.getMessage(), e);
		}
	}

	public static boolean isEmpty(InputStream json) {
		try {
			if (json == null || json.available() <= 0) {
				return true;
			}
			ByteArrayOutputStream baos = new ByteArrayOutputStream();
			IOUtils.copy(json, baos);
			InputStream clone = new ByteArrayInputStream(baos.toByteArray());
			return Neo4JUtils.isEmpty(new InputStreamReader(clone));
		} catch (Exception e) {
			throw new AttuneException(e.getMessage(), e);
		}
	}

	public static int getSize(InputStream json) {
		try {
			ByteArrayOutputStream baos = new ByteArrayOutputStream();
			IOUtils.copy(json, baos);
			InputStream clone = new ByteArrayInputStream(baos.toByteArray());
			return Neo4JUtils.getSize(new InputStreamReader(clone));
		} catch (Exception e) {
			throw new AttuneException(e.getMessage(), e);
		}
	}

	public static String getId(InputStream json) {
		try {
			ByteArrayOutputStream baos = new ByteArrayOutputStream();
			IOUtils.copy(json, baos);
			InputStream clone = new ByteArrayInputStream(baos.toByteArray());
			return Neo4JUtils.getId(new InputStreamReader(clone));
		} catch (Exception e) {
			throw new AttuneException(e.getMessage(), e);
		}
	}

	public static Map<String, Object> getProperties(InputStream json) {
		try {
			ByteArrayOutputStream baos = new ByteArrayOutputStream();
			IOUtils.copy(json, baos);
			InputStream clone = new ByteArrayInputStream(baos.toByteArray());
			return Neo4JUtils.getProperties(new InputStreamReader(clone));
		} catch (Exception e) {
			throw new AttuneException(e.getMessage(), e);
		}
	}

	public static Map<String, Object> getMetadataProperties(InputStream json) {
		try {
			ByteArrayOutputStream baos = new ByteArrayOutputStream();
			IOUtils.copy(json, baos);
			InputStream clone = new ByteArrayInputStream(baos.toByteArray());
			return Neo4JUtils.getMetadataProperties(new InputStreamReader(clone));
		} catch (Exception e) {
			throw new AttuneException(e.getMessage(), e);
		}
	}

	public static String getMetadataValue(InputStream json, String propName) {
		try {
			ByteArrayOutputStream baos = new ByteArrayOutputStream();
			IOUtils.copy(json, baos);
			InputStream clone = new ByteArrayInputStream(baos.toByteArray());
			return Neo4JUtils.getMetadataValue(new InputStreamReader(clone), propName);
		} catch (Exception e) {
			throw new AttuneException(e.getMessage(), e);
		}
	}

	public static Map<String, Object> getDataProperties(InputStream json) {
		try {
			ByteArrayOutputStream baos = new ByteArrayOutputStream();
			IOUtils.copy(json, baos);
			InputStream clone = new ByteArrayInputStream(baos.toByteArray());
			return Neo4JUtils.getDataProperties(new InputStreamReader(clone));
		} catch (Exception e) {
			throw new AttuneException(e.getMessage(), e);
		}
	}

	public static String getDataValue(InputStream json, String propName) {
		try {
			ByteArrayOutputStream baos = new ByteArrayOutputStream();
			IOUtils.copy(json, baos);
			InputStream clone = new ByteArrayInputStream(baos.toByteArray());
			return Neo4JUtils.getDataValue(new InputStreamReader(clone), propName);
		} catch (Exception e) {
			throw new AttuneException(e.getMessage(), e);
		}
	}

	public static String getValue(InputStream json, String propName) {
		try {
			ByteArrayOutputStream baos = new ByteArrayOutputStream();
			IOUtils.copy(json, baos);
			InputStream clone = new ByteArrayInputStream(baos.toByteArray());
			return Neo4JUtils.getValue(new InputStreamReader(clone), propName);
		} catch (Exception e) {
			throw new AttuneException(e.getMessage(), e);
		}
	}

	public static Object getValue(InputStream json) {
		try {
			ByteArrayOutputStream baos = new ByteArrayOutputStream();
			IOUtils.copy(json, baos);
			InputStream clone = new ByteArrayInputStream(baos.toByteArray());
			return Neo4JUtils.getValue(new InputStreamReader(clone));
		} catch (Exception e) {
			throw new AttuneException(e.getMessage(), e);
		}
	}

	public static List<String> getLabels(InputStream json) {
		try {
			ByteArrayOutputStream baos = new ByteArrayOutputStream();
			IOUtils.copy(json, baos);
			InputStream clone = new ByteArrayInputStream(baos.toByteArray());
			return Neo4JUtils.getLabels(new InputStreamReader(clone));
		} catch (Exception e) {
			throw new AttuneException(e.getMessage(), e);
		}
	}

	public static String getRelationshipType(InputStream json) {
		try {
			ByteArrayOutputStream baos = new ByteArrayOutputStream();
			IOUtils.copy(json, baos);
			InputStream clone = new ByteArrayInputStream(baos.toByteArray());
			return Neo4JUtils.getRelationshipType(new InputStreamReader(clone));
		} catch (Exception e) {
			throw new AttuneException(e.getMessage(), e);
		}
	}

	public static String getRelationshipStartNode(InputStream json) {
		try {
			ByteArrayOutputStream baos = new ByteArrayOutputStream();
			IOUtils.copy(json, baos);
			InputStream clone = new ByteArrayInputStream(baos.toByteArray());
			return Neo4JUtils.getRelationshipStartNode(new InputStreamReader(clone));
		} catch (Exception e) {
			throw new AttuneException(e.getMessage(), e);
		}
	}

	public static String getRelationshipEndNode(InputStream json) {
		try {
			ByteArrayOutputStream baos = new ByteArrayOutputStream();
			IOUtils.copy(json, baos);
			InputStream clone = new ByteArrayInputStream(baos.toByteArray());
			return Neo4JUtils.getRelationshipEndNode(new InputStreamReader(clone));
		} catch (Exception e) {
			throw new AttuneException(e.getMessage(), e);
		}
	}

	public static synchronized boolean isEmpty(Reader json) {
		try {
			boolean isEmpty = true;
			if (json != null) {
				Object jsonObj = parser.parse(json);
				JSONObject jsonObject;
				if (jsonObj instanceof JSONArray) {
					JSONArray jsonArray = (JSONArray) jsonObj;
					isEmpty = jsonArray.isEmpty();
				} else if (jsonObj instanceof JSONObject) {
					jsonObject = (JSONObject) jsonObj;
					isEmpty = jsonObject.isEmpty();
				}
			}
			return isEmpty;
		} catch (Exception e) {
			throw new AttuneException(e.getMessage(), e);
		}
	}

	public static synchronized int getSize(Reader json) {
		try {
			int size = 0;
			if (json != null) {
				Object jsonObj = parser.parse(json);
				JSONObject jsonObject = null;
				if (jsonObj instanceof JSONArray) {
					JSONArray jsonArray = (JSONArray) jsonObj;
					size = jsonArray.size();
				} else if (jsonObj instanceof JSONObject) {
					jsonObject = (JSONObject) jsonObj;
					size = jsonObject.size();
				}
			}
			return size;
		} catch (Exception e) {
			throw new AttuneException(e.getMessage(), e);
		}
	}

	public static synchronized String getId(Reader json) {
		try {
			String id = null;
			if (json != null) {
				Object jsonObj = parser.parse(json);
				JSONObject jsonObject = null;
				if (jsonObj instanceof JSONArray) {
					JSONArray jsonArray = (JSONArray) jsonObj;
					if (!jsonArray.isEmpty()) {
						jsonObject = (JSONObject) jsonArray.get(0);
					}
				} else if (jsonObj instanceof JSONObject) {
					jsonObject = (JSONObject) jsonObj;
				}
				if (jsonObject != null && !jsonObject.isEmpty()) {
					id = (String) jsonObject.get("neo4j.id");
					if (id == null) {
						id = String.valueOf((Long) ((JSONObject) jsonObject.get(METADATA)).get("id"));
					}
				}
			}
			return id;
		} catch (Exception e) {
			throw new AttuneException(e.getMessage(), e);
		}
	}

	public static synchronized Map<String, Object> getProperties(Reader json) {
		try {
			Map<String, Object> properties = new HashMap<>();
			if (json != null) {
				Object jsonObj = parser.parse(json);
				JSONObject jsonObject = (JSONObject) jsonObj;
				for (Object key : jsonObject.keySet()) {
					String propName = (String) key;
					Object propValue = jsonObject.get(propName);
					properties.put(propName, propValue);
				}
			}
			return properties;
		} catch (Exception e) {
			throw new AttuneException(e.getMessage(), e);
		}
	}

	public static synchronized Map<String, Object> getMetadataProperties(Reader json) {
		try {
			Map<String, Object> props = new HashMap<>();
			if (json != null) {
				Object jsonObj = parser.parse(json);
				JSONObject jsonObject = null;
				if (jsonObj instanceof JSONArray) {
					JSONArray jsonArray = (JSONArray) jsonObj;
					if (!jsonArray.isEmpty()) {
						jsonObject = (JSONObject) jsonArray.get(0);
					}
				} else if (jsonObj instanceof JSONObject) {
					jsonObject = (JSONObject) jsonObj;
				}
				if (jsonObject != null && !jsonObject.isEmpty()) {
					if (jsonObject.get(METADATA) != null) {
						props = getProperties(((JSONObject) jsonObject.get(METADATA)).toJSONString());
					}
				}
			}
			return props;
		} catch (Exception e) {
			throw new AttuneException(e.getMessage(), e);
		}
	}

	public static synchronized String getMetadataValue(Reader json, String propName) {
		try {
			String value = null;
			if (json != null) {
				Object jsonObj = parser.parse(json);
				JSONObject jsonObject = null;
				if (jsonObj instanceof JSONArray) {
					JSONArray jsonArray = (JSONArray) jsonObj;
					if (!jsonArray.isEmpty()) {
						jsonObject = (JSONObject) jsonArray.get(0);
					}
				} else if (jsonObj instanceof JSONObject) {
					jsonObject = (JSONObject) jsonObj;
				}
				if (jsonObject != null && !jsonObject.isEmpty()) {
					if (jsonObject.get(METADATA) != null) {
						value = getValue(((JSONObject) jsonObject.get(METADATA)).toJSONString(), propName);
					}
				}
			}
			return value;
		} catch (Exception e) {
			throw new AttuneException(e.getMessage(), e);
		}
	}

	public static synchronized Map<String, Object> getDataProperties(Reader json) {
		try {
			Map<String, Object> props = new HashMap<>();
			if (json != null) {
				Object jsonObj = parser.parse(json);
				JSONObject jsonObject = null;
				if (jsonObj instanceof JSONArray) {
					JSONArray jsonArray = (JSONArray) jsonObj;
					if (!jsonArray.isEmpty()) {
						jsonObject = (JSONObject) jsonArray.get(0);
					}
				} else if (jsonObj instanceof JSONObject) {
					jsonObject = (JSONObject) jsonObj;
				}
				if (jsonObject != null && !jsonObject.isEmpty()) {
					if (jsonObject.get("data") != null) {
						props = getProperties(((JSONObject) jsonObject.get("data")).toJSONString());
					} else {
						props = getProperties(jsonObject.toJSONString());
					}
				}
			}
			return props;
		} catch (Exception e) {
			throw new AttuneException(e.getMessage(), e);
		}
	}

	public static synchronized String getDataValue(Reader json, String propName) {
		try {
			String value = null;
			if (json != null) {
				Object jsonObj = parser.parse(json);
				JSONObject jsonObject = null;
				if (jsonObj instanceof JSONArray) {
					JSONArray jsonArray = (JSONArray) jsonObj;
					if (!jsonArray.isEmpty()) {
						jsonObject = (JSONObject) jsonArray.get(0);
					}
				} else if (jsonObj instanceof JSONObject) {
					jsonObject = (JSONObject) jsonObj;
				}
				if (jsonObject != null && !jsonObject.isEmpty()) {
					Object objVal = jsonObject.get(propName);
					if (objVal != null) {
						if (Boolean.class.isAssignableFrom(objVal.getClass())) {
							value = ((Boolean) jsonObject.get(propName)).toString();
						} else if (Number.class.isAssignableFrom(objVal.getClass())) {
							value = ((Number) jsonObject.get(propName)).toString();
						} else {
							value = (String) jsonObject.get(propName);
						}
						if (value == null && jsonObject.get("data") != null) {
							value = getValue(((JSONObject) jsonObject.get("data")).toJSONString(), propName);
						}
					}
				}
			}
			return value;
		} catch (Exception e) {
			throw new AttuneException(e.getMessage(), e);
		}
	}

	public static synchronized String getValue(Reader json, String propName) {
		try {
			String value = null;
			if (json != null) {
				Object jsonObj = parser.parse(json);
				JSONObject jsonObject = null;
				if (jsonObj instanceof JSONArray) {
					JSONArray jsonArray = (JSONArray) jsonObj;
					if (!jsonArray.isEmpty()) {
						jsonObject = (JSONObject) jsonArray.get(0);
					}
				} else if (jsonObj instanceof JSONObject) {
					jsonObject = (JSONObject) jsonObj;
				}

				if (jsonObject != null && !jsonObject.isEmpty() && jsonObject.containsKey(propName)) {
					Object val = jsonObject.get(propName);
					if (val != null) {
						if (val instanceof Boolean) {
							value = ((Boolean) val).toString();
						} else if (val instanceof Number) {
							if (val instanceof Double) {
								value = String.valueOf((Double) val);
							}
							if (val instanceof Float) {
								value = String.valueOf((Float) val);
							}
							if (val instanceof Integer) {
								value = String.valueOf((Integer) val);
							}
							if (val instanceof Long) {
								value = String.valueOf((Long) val);
							}
							if (val instanceof Short) {
								value = String.valueOf((Short) val);
							}
						} else {
							value = (String) val;
						}
					}
				}
			}
			return value;
		} catch (Exception e) {
			throw new AttuneException(e.getMessage(), e);
		}
	}

	public static Object getValue(Reader json) {
		try {
			Object value = null;
			if (json != null) {
				Object jsonObj = JSONValue.parse(json);
				if (jsonObj instanceof JSONArray) {
					JSONArray jsonArray = (JSONArray) jsonObj;
					if (jsonArray.size() == 1) {
						jsonObj = jsonArray.get(0);
						if (jsonObj instanceof JSONObject) {
							JSONObject jsonObject = (JSONObject) jsonObj;
							value = getProperties(jsonObject.toJSONString());
						} else if (jsonObj instanceof String) {
							value = (String) jsonObj;
						} else if (jsonObj instanceof Boolean) {
							value = (Boolean) jsonObj;
						} else if (jsonObj instanceof Number) {
							value = (Number) jsonObj;
						}
					} else {
						value = new ArrayList<>();
						for (int i = 0; i < jsonArray.size(); i++) {
							jsonObj = jsonArray.get(i);
							if (jsonObj instanceof JSONObject) {
								JSONObject jsonObject = (JSONObject) jsonObj;
								((List<Object>) value).add(getProperties(jsonObject.toJSONString()));
							} else if (jsonObj instanceof String) {
								((List<Object>) value).add((String) jsonObj);
							} else if (jsonObj instanceof Boolean) {
								((List<Object>) value).add((Boolean) jsonObj);
							} else if (jsonObj instanceof Number) {
								((List<Object>) value).add((Number) jsonObj);
							}
						}
					}
				} else if (jsonObj instanceof JSONObject) {
					JSONObject jsonObject = (JSONObject) jsonObj;
					value = getProperties(jsonObject.toJSONString());
				} else if (jsonObj instanceof String) {
					value = (String) jsonObj;
				} else if (jsonObj instanceof Boolean) {
					value = (Boolean) jsonObj;
				} else if (jsonObj instanceof Number) {
					value = (Number) jsonObj;
				}
			}
			return value;
		} catch (Exception e) {
			throw new AttuneException(e.getMessage(), e);
		}
	}

	public static synchronized List<String> getLabels(Reader json) {
		try {
			List<String> labels = new ArrayList<>();
			if (json != null) {
				Object jsonObj = parser.parse(json);
				JSONObject jsonObject = null;
				if (jsonObj instanceof JSONArray) {
					JSONArray jsonArray = (JSONArray) jsonObj;
					if (!jsonArray.isEmpty()) {
						jsonObject = (JSONObject) jsonArray.get(0);
					}
				} else if (jsonObj instanceof JSONObject) {
					jsonObject = (JSONObject) jsonObj;
				}
				if (jsonObject != null && !jsonObject.isEmpty()) {
					if (jsonObject.containsKey("neo4j.label")) {
						labels.add((String) jsonObject.get("neo4j.label"));
					} else if (jsonObject.get(METADATA) != null) {
						JSONArray jsonArray = (JSONArray) ((JSONObject) jsonObject.get(METADATA)).get("labels");
						for (int i = 0; i < jsonArray.size(); i++) {
							String label = (String) jsonArray.get(i);
							labels.add(label);
						}
					}
				}
			}
			return labels;
		} catch (Exception e) {
			throw new AttuneException(e.getMessage(), e);
		}
	}

	public static synchronized String getRelationshipType(Reader json) {
		try {
			String relType = null;
			if (json != null) {
				Object jsonObj = parser.parse(json);
				JSONObject jsonObject = null;
				if (jsonObj instanceof JSONArray) {
					JSONArray jsonArray = (JSONArray) jsonObj;
					if (!jsonArray.isEmpty()) {
						jsonObject = (JSONObject) jsonArray.get(0);
					}
				} else if (jsonObj instanceof JSONObject) {
					jsonObject = (JSONObject) jsonObj;
				}
				if (jsonObject != null && !jsonObject.isEmpty()) {
					relType = (String) jsonObject.get("relationshipType");
					if (relType == null || relType.isEmpty()) {
						if (jsonObject.get(METADATA) != null) {
							relType = (String) ((JSONObject) jsonObject.get(METADATA)).get("type");
						}
					}
				}
			}
			return relType;
		} catch (Exception e) {
			throw new AttuneException(e.getMessage(), e);
		}
	}

	public static synchronized String getRelationshipStartNode(Reader json) {
		try {
			String start = null;
			if (json != null) {
				Object jsonObj = parser.parse(json);
				JSONObject jsonObject = null;
				if (jsonObj instanceof JSONArray) {
					JSONArray jsonArray = (JSONArray) jsonObj;
					if (!jsonArray.isEmpty()) {
						jsonObject = (JSONObject) jsonArray.get(0);
					}
				} else if (jsonObj instanceof JSONObject) {
					jsonObject = (JSONObject) jsonObj;
				}
				if (jsonObject != null && !jsonObject.isEmpty()) {
					start = (String) jsonObject.get("startNodeId");
					if (start == null || start.isEmpty()) {
						start = (String) jsonObject.get("start");
						if (start.lastIndexOf("/") > -1) {
							return start.substring(start.lastIndexOf("/") + 1);
						}
					}
				}
			}
			return start;
		} catch (Exception e) {
			throw new AttuneException(e.getMessage(), e);
		}
	}

	public static synchronized String getRelationshipEndNode(Reader json) {
		try {
			String end = null;
			if (json != null) {
				Object jsonObj = parser.parse(json);
				JSONObject jsonObject = null;
				if (jsonObj instanceof JSONArray) {
					JSONArray jsonArray = (JSONArray) jsonObj;
					if (!jsonArray.isEmpty()) {
						jsonObject = (JSONObject) jsonArray.get(0);
					}
				} else if (jsonObj instanceof JSONObject) {
					jsonObject = (JSONObject) jsonObj;
				}
				if (jsonObject != null && !jsonObject.isEmpty()) {
					end = (String) jsonObject.get("endNodeId");
					if (end == null || end.isEmpty()) {
						end = (String) jsonObject.get("end");
						if (end.lastIndexOf("/") > -1) {
							return end.substring(end.lastIndexOf("/") + 1);
						}
					}
				}
			}
			return end;
		} catch (Exception e) {
			throw new AttuneException(e.getMessage(), e);
		}
	}
}

