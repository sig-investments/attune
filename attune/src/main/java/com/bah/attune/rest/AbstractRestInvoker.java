package com.bah.attune.rest;

import com.bah.attune.data.AttuneException;
import com.bah.attune.service.SecurityService;
import org.apache.commons.codec.binary.Base64;
import org.json.simple.JSONAware;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.springframework.context.ApplicationContext;
import org.springframework.context.support.AbstractApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;
import org.springframework.core.io.Resource;
import org.springframework.http.*;
import org.springframework.http.client.HttpComponentsClientHttpRequestFactory;
import org.springframework.web.client.HttpStatusCodeException;
import org.springframework.web.client.RestTemplate;

import java.net.URL;
import java.nio.charset.Charset;
import java.util.*;
import java.util.Map.Entry;

public abstract class AbstractRestInvoker {

	// HttpComponentsClientHttpRequestFactory is used for Connection Pooling
	// and releasing connections
	protected RestTemplate restTemplate = new RestTemplate(new HttpComponentsClientHttpRequestFactory());

	protected static final String LOOKUP_PROP_BASEBALLCARD_SERVICE = "baseballcard.service.url";
	protected static final String LOOKUP_PROP_DASHBOARD_SERVICE = "dashboard.service.url";
	protected static final String LOOKUP_PROP_DATA_ANALYSIS_SERVICE = "data.analysis.service.url";
	protected static final String LOOKUP_PROP_DATA_SERVICE = "data.service.url";
	protected static final String LOOKUP_PROP_IMPORT_SERVICE = "import.service.url";
	protected static final String LOOKUP_PROP_METADATA_SERVICE = "metadata.service.url";
	protected static final String LOOKUP_PROP_PORTFOLIO_SERVICE = "portfolio.service.url";
	protected static final String LOOKUP_PROP_TIMELINE_SERVICE = "timeline.service.url";
	protected static final String LOOKUP_PROP_TRACEABILITY_SERVICE = "traceability.service.url";
	protected static final String LOOKUP_PROP_USER_SERVICE = "user.service.url";
	protected static final String LOOKUP_PROP_VALIDATION_SERVICE = "validation.service.url";

	protected static Properties registry = null; //GC- Pg 138,139 on 3-27-2017 report
	//protected Properties registry = null; //Original Code
	protected JSONParser parser = new JSONParser();

	static {									//GC- Pg 138,139 on 3-27-2017 report
		if (registry==null) {					//GC- Pg 138,139 on 3-27-2017 report
			getURLForService("validation.service.url");	//GC
		}		//GC
	}			//GC
	
	
	protected HttpHeaders createHeaders(final String url, final HttpMethod httpMethod) {
		HttpHeaders headers = new HttpHeaders();
		String auth = null;
		JSONObject currentUser = SecurityService.getCurrentUser();
		if (currentUser != null) {
			auth = new StringBuilder().append(currentUser.get(SecurityService.USERNAME_FIELD)).append(":")
					.append(currentUser.get(SecurityService.PSW_FIELD)).toString();
		} else {
			auth = new StringBuilder().append(SecurityService.USER_ATTUNE).append(":")
					.append(SecurityService.PASSWORD_ATTUNE).toString();
		}
		byte[] encodedAuth = Base64.encodeBase64(auth.getBytes(Charset.forName("US-ASCII")));
		String authHeader = "Basic " + new String(encodedAuth);
		headers.set("Authorization", authHeader);
		headers.setContentType(MediaType.APPLICATION_JSON);
		return headers;
	}

	protected static String getURLForService(String serviceLookupProperty) throws AttuneException { //GC- Pg 138-139 of 3-27-2017 report
	//protected String getURLForService(String serviceLookupProperty) throws AttuneException {		//Original Code
		try {
			if (registry == null) {
				registry = new Properties();
				ApplicationContext appContext = new ClassPathXmlApplicationContext();
				Resource resource = appContext.getResource("classpath:registry.properties");
				//registry.load(resource.getInputStream());										//Original Code
				//((AbstractApplicationContext) appContext).close();							//Original Code
//-------------------START OF GENE'S TEST CODE FOR Unreleased Resource: Streams, pg 643-645 of 3-30-2017 report----------------		
				registry.load(resource.getInputStream());
				((AbstractApplicationContext) appContext).close();
				resource.getInputStream().close();
//-------------------END OF GENE'S TEST CODE FOR Unreleased Resource: Streams, pg 643-645 of 3-30-2017 report----------------		
			}
			String url = registry.getProperty(serviceLookupProperty);
			return url;
		} catch (Exception e) {
			throw new AttuneException(e);
		}
	}

	protected String getURLForService(String serviceLookupProperty, Map<String, String> queryParams)
			throws AttuneException {
		try {
			StringBuilder url = new StringBuilder(this.getURLForService(serviceLookupProperty));
			if (queryParams != null) {
				url.append(this.getQueryParamString(queryParams));
			}
			return url.toString();
		} catch (Exception e) {
			throw new AttuneException(e);
		}
	}

	protected String getURLForService(String serviceLookupProperty, String name, List<String> values)
			throws AttuneException {
		try {
			StringBuilder url = new StringBuilder(this.getURLForService(serviceLookupProperty));
			if (name != null && values != null) {
				url.append(this.getQueryParamString(name, values));
			}
			return url.toString();
		} catch (Exception e) {
			throw new AttuneException(e);
		}
	}

	@SuppressWarnings("rawtypes")
	protected ResponseEntity callRestService(String url, HttpMethod httpMethod, Object body, Class returnType)
			throws AttuneException {
		return this.callRestService(url, httpMethod, body, returnType, null);
	}

	@SuppressWarnings("rawtypes")
	protected ResponseEntity callRestService(String url, HttpMethod httpMethod, Object body, Class returnType,
			Map<String, ?> uriVariables) throws AttuneException {
		try {
			if (uriVariables == null) {
				uriVariables = new HashMap<String, String>();
			}
// -----------START of Gene's Test code for Server-Side Request Forgery Error---------------------------------------------------
			// Page 137, 139, 140, 141 of 3-27-2017 report
			boolean validURL = false;
			for (Entry<Object, Object> entry : registry.entrySet()) {
				URL value = new URL((String) entry.getValue());
                if (new URL(url).getPath().contains((CharSequence) value.getPath())) {
					validURL = true;
					break;
				}
			}

			if (!validURL) {
				throw new AttuneException("Base URL not found in registry, blocking call to prevent security issues!");
			}
			
			@SuppressWarnings("unchecked")
			ResponseEntity response = restTemplate.exchange(url, 
					httpMethod, new HttpEntity(body, this.createHeaders(url, httpMethod)), returnType, uriVariables);
			
			
// -----------END of Gene's Test code for Server-Side Request
			// Forgery Error---------------------------------------------------

			//ResponseEntity response = restTemplate.exchange(url, httpMethod,									//Original Code
			//		new HttpEntity(body, this.createHeaders(url, httpMethod)), returnType, uriVariables);		//Original Code
			//Code below was original code but it was already commented out.
			
			// if(response != null){		
			// return returnType.cast(response.getBody());
			// } else {
			// return null;
			// }
			return response;
		} catch (HttpStatusCodeException hsce) {
			StringBuilder sb = new StringBuilder().append(String.valueOf(hsce.getStatusCode().value())).append(" ")
					.append(hsce.getStatusText()).append(" - ").append(hsce.getResponseBodyAsString());
			throw new AttuneException(sb.toString(), hsce, hsce.getStatusCode());
		} catch (Exception e) {
			throw new AttuneException(e);
		}
	}

	protected String getQueryParamString(Map<String, String> queryParams) throws AttuneException {
		StringBuilder queryParamString = new StringBuilder();
		if (!queryParams.isEmpty()) {
			queryParamString.append("?");
		}
		Iterator<String> keySet = queryParams.keySet().iterator();
		while (keySet.hasNext()) {
			String key = keySet.next();
			String value = queryParams.get(key);
			queryParamString.append(key).append("=").append(value);
			if (keySet.hasNext()) {
				queryParamString.append("&");
			}
		}
		return queryParamString.toString();
	}

	protected String getQueryParamString(String name, List<String> values) throws AttuneException {
		StringBuilder queryParamString = new StringBuilder();
		if (!values.isEmpty()) {
			queryParamString.append("?");
		}
		Iterator<String> valueSet = values.iterator();
		while (valueSet.hasNext()) {
			String value = valueSet.next();
			queryParamString.append(name).append("=").append(value);
			if (valueSet.hasNext()) {
				queryParamString.append("&");
			}
		}
		return queryParamString.toString();
	}

	protected JSONAware parseJSONResponse(String json) throws AttuneException {
		try {
			JSONAware obj = (JSONAware) parser.parse(json);
			return obj;
		} catch (Exception e) {
			throw new AttuneException(e);
		}
	}
}
