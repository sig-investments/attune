package com.bah.attune.web;

import java.nio.charset.Charset;

import javax.servlet.ServletContext;

import org.apache.commons.codec.binary.Base64;
import org.json.simple.JSONObject;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Controller;
import org.springframework.web.context.ServletContextAware;

import com.bah.attune.rest.AbstractRestInvoker;
import com.bah.attune.service.SecurityService;

@Controller
public abstract class AbstractRestController extends AbstractRestInvoker implements ServletContextAware {
	
    protected static ServletContext servletContext;
        
	@Override
	public void setServletContext(ServletContext servletContext) {
		setContext(servletContext);
	}
	
	//Needed for SonarQube
	private static void setContext(ServletContext context) {
		servletContext = context;
	}
		
	@Override
	protected HttpHeaders createHeaders(final String url, final HttpMethod httpMethod){
		HttpHeaders headers = new HttpHeaders();
		String auth;
    	JSONObject currentUser = SecurityService.getCurrentUser();
    	if(currentUser != null){
    		auth = new StringBuilder().append(currentUser.get(SecurityService.USERNAME_FIELD)).append(":").append(currentUser.get(SecurityService.PSW_FIELD)).toString();
    	} else {
    		auth = servletContext.getInitParameter("attune.credentials");
    	}
        byte[] encodedAuth = Base64.encodeBase64(auth.getBytes(Charset.forName("US-ASCII")));
        String authHeader = "Basic " + new String( encodedAuth );
        headers.set( "Authorization", authHeader );
        headers.setContentType(MediaType.APPLICATION_JSON);
        return headers;
    }
}
