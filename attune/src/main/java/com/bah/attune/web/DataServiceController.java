package com.bah.attune.web;

import java.io.UnsupportedEncodingException;
import java.net.URLDecoder;
import java.nio.charset.Charset;
import java.nio.charset.StandardCharsets;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.StringTokenizer;

import javax.servlet.http.HttpServletRequest;

import org.apache.commons.codec.binary.Base64;
import org.json.simple.JSONArray;
import org.json.simple.JSONAware;
import org.json.simple.JSONObject;
import org.json.simple.JSONValue;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

import com.bah.attune.data.AttuneException;
import com.bah.attune.rest.Neo4JUtils;
import com.bah.attune.service.SecurityService;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;

@Controller
@RequestMapping("/api/data/*")
@Api(value = "/api/data")
public class DataServiceController extends AbstractRestController {

	@Autowired
	private HttpServletRequest httpServletRequest;
	private static final Logger LOGGER = LoggerFactory.getLogger(DataServiceController.class);
	private static String neo4jUrlPrefix = null;

	static {
		if (neo4jUrlPrefix == null) {
			neo4jUrlPrefix = servletContext.getInitParameter("neo4j.url");
		}
	}

	public DataServiceController() {
		super();
	}

	@SuppressWarnings("unchecked")
	@RequestMapping(value = "/node/{id}", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
	@PreAuthorize("hasRole('" + SecurityService.ROLE_USER + "')")
	@ApiOperation(value = "getNode", notes = "Returns a single-level JSON object that contains the properties for the specified node id.", response = String.class, httpMethod = "GET")
	@ApiResponses(value = {
			@ApiResponse(code = 200, message = "JSON object containing the properties for the node with the given node id. It will also contain the transient fields called neo4j.id and neo4j.label that contains it's Neo4J node id and Metadata entity type."),
			@ApiResponse(code = 400, message = "Invalid request if the id doesn't exist."),
			@ApiResponse(code = 500, message = "An Error has Occurred") })
	public ResponseEntity<String> getNode(
			@ApiParam(value = "The Node id.", required = true) @PathVariable(value = "id") Long id) {
		String result = null;
		try {
			String url = new StringBuilder(neo4jUrlPrefix).append("db/data/node/").append(String.valueOf(id))
					.toString();
			result = this.getNeo4JData(url);
			if (result != null) {
				JSONObject node;
				synchronized (parser) {
					node = (JSONObject) parser.parse(result);
				}
				JSONObject nodeJson = new JSONObject(Neo4JUtils.getDataProperties(node.toJSONString()));
				nodeJson.put("neo4j.id", String.valueOf(id));
				nodeJson.put("neo4j.label", Neo4JUtils.getLabels(node.toJSONString()).get(0));
				result = nodeJson.toJSONString();
			}
			final HttpHeaders httpHeaders = new HttpHeaders();
			httpHeaders.setContentType(MediaType.APPLICATION_JSON);
			if (result != null) {
				return new ResponseEntity<>(((JSONAware) parser.parse(result)).toJSONString(), httpHeaders,
						HttpStatus.OK);
			} else {
				return new ResponseEntity<>(null, httpHeaders, HttpStatus.OK);
			}
		} catch (AttuneException ae) {
			LOGGER.error(ae.getMessage(), ae);
			if (ae.getStatusCode() != null) {
				return new ResponseEntity<>(ae.getMessage(), ae.getStatusCode());
			}
			throw ae;
		} catch (Exception e) {
			LOGGER.error(e.getMessage(), e);
			throw new AttuneException(e);
		}
	}

	@RequestMapping(value = "/node/{type}", method = RequestMethod.POST, consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
	@PreAuthorize("hasRole('" + SecurityService.ROLE_USER + "')")
	@ApiOperation(value = "insertNode", notes = "Inserts a new node using the specified Metadata entity type, passing in a single-level JSON object that contains the properties for the node.", response = String.class, httpMethod = "POST")
	@ApiResponses(value = {
			@ApiResponse(code = 200, message = "Node was successfully inserted, JSON object is returned containing the properties for the node with the given node id. It will also contain the transient fields called neo4j.id and neo4j.label that contains it's Neo4J node id and Metadata entity type."),
			@ApiResponse(code = 400, message = "Invalid request if there is no Metadata definition for the given Metadata entity type."),
			@ApiResponse(code = 500, message = "An Error has Occurred") })
	public ResponseEntity<String> insertNode(
			@ApiParam(value = "Path parameter indicating the Metadata entity type name.", required = true) @PathVariable(value = "type") String type,
			@ApiParam(value = "A single-level JSON object that contains the properties for the node, values can be String, boolean, or a number."
					+ " There must be at least a Name property.", required = true) @RequestBody(required = true) Map<String, Object> nodeProperties) {
		String result = null;
		try {
			Map<String, Object> properties = nodeProperties;
			if (properties == null) {
				properties = new HashMap<>();
			}
			String metadataJson = this.validateNode(type, properties);
			String existingNodeId = this.getExistingNodeId(metadataJson, type, properties);
			if (existingNodeId != null) {
				Map<String, Object> props = Neo4JUtils
						.getProperties(this.getNodeProperties(Long.parseLong(existingNodeId)).getBody());
				if (props != null && !props.isEmpty()) {
					props.putAll(properties);
				} else {
					props = properties;
				}
				this.updateNodeProperties(Long.parseLong(existingNodeId), props);
				return this.getNode(Long.parseLong(existingNodeId));
			}

			String url = new StringBuilder(neo4jUrlPrefix).append("db/data/node").toString();
			String json = null;
			if (!properties.isEmpty()) {
				properties = defaultNullPropertyValues(properties);
				JSONObject jsonObj = new JSONObject(properties);
				json = jsonObj.toJSONString();
			}
			result = this.insertNeo4JData(json, url);
			// Add Label
			String nodeId = Neo4JUtils.getId(result);
			url = new StringBuilder(neo4jUrlPrefix).append("db/data/node/").append(nodeId).append("/labels").toString();
			this.insertNeo4JData(JSONValue.toJSONString(type), url);
			// Re-retrieve the updated data
			result = this.getNode(Long.parseLong(nodeId)).getBody();
			final HttpHeaders httpHeaders = new HttpHeaders();
			httpHeaders.setContentType(MediaType.APPLICATION_JSON);
			if (result != null) {
				return new ResponseEntity<>(((JSONAware) parser.parse(result)).toJSONString(), httpHeaders,
						HttpStatus.OK);
			} else {
				return new ResponseEntity<>(null, httpHeaders, HttpStatus.OK);
			}
		} catch (AttuneException ae) {
			LOGGER.error(ae.getMessage(), ae);
			if (ae.getStatusCode() != null) {
				return new ResponseEntity<>(ae.getMessage(), ae.getStatusCode());
			}
			throw ae;
		} catch (Exception e) {
			LOGGER.error(e.getMessage(), e);
			throw new AttuneException(e);
		}
	}

	@RequestMapping(value = "/node/{id}", method = RequestMethod.DELETE)
	@PreAuthorize("hasRole('" + SecurityService.ROLE_USER + "')")
	@ApiOperation(value = "deleteNode", notes = "Deletes the node with the passed in node id.", response = String.class, httpMethod = "DELETE")
	@ApiResponses(value = {
	@ApiResponse(code = 204, message = "If the Node was successfully deleted, null or empty response."),
	@ApiResponse(code = 400, message = "Invalid request, if relationships still exist for the node."),
	@ApiResponse(code = 500, message = "An Error has Occurred") })
	public ResponseEntity<String> deleteNode(@ApiParam(value = "The Node id.", required = true) @PathVariable(value = "id") Long id) {	
		try {
			if (this.getNodeTypes(id).getBody().contains("Metadata")) {
				StringBuilder msg = new StringBuilder("Can not modify Metadata through this API. Please use ");
				msg.append(this.httpServletRequest.getContextPath()).append(this.httpServletRequest.getServletPath());
				msg.append("/metadata/entity/{entityType}").append(" instead.");
				throw new AttuneException(msg.toString(), HttpStatus.BAD_REQUEST);
			}
			String url = new StringBuilder(neo4jUrlPrefix).append("db/data/node/").append(String.valueOf(id))
					.toString();
			this.deleteNeo4JData(url);
			return new ResponseEntity<>(HttpStatus.OK);
		} catch (AttuneException ae) {
			LOGGER.error(ae.getMessage(), ae);
			if (ae.getStatusCode() != null) {
				return new ResponseEntity<>(ae.getMessage(), ae.getStatusCode());
			}
			throw ae;
		} catch (Exception e) {
			LOGGER.error(e.getMessage(), e);
			throw new AttuneException(e);
		}
	}

	@RequestMapping(value = "/nodes", method = RequestMethod.DELETE)
	@PreAuthorize("hasRole('" + SecurityService.ROLE_USER + "')")
	@ApiOperation(value = "deleteAllNodes", notes = "Deletes all Nodes.", response = String.class, httpMethod = "DELETE")
	@ApiResponses(value = {
			@ApiResponse(code = 204, message = "If the Nodes were successfully deleted, null or empty response."),
			@ApiResponse(code = 400, message = "Invalid request, if relationships still exist for any nodes."),
			@ApiResponse(code = 500, message = "An Error has Occurred") })
	public ResponseEntity<String> deleteAllNodes() {
		try {
			// First Get all Node Types
			String allNodeTypesJson = this.getAllNodeTypes().getBody();
			if (allNodeTypesJson != null) {
				JSONArray nodeTypes;
				synchronized (parser) {
					nodeTypes = (JSONArray) parser.parse(allNodeTypesJson);
				}
				// For each type, get all nodes
				for (int i = 0; i < nodeTypes.size(); i++) {
					String nodeType = (String) nodeTypes.get(i);
					if (nodeType == null || "Metadata".equals(nodeType)) {
						continue;
					}
					this.deleteNodesForType(nodeType);
				}
			}
			return new ResponseEntity<>(HttpStatus.OK);
		} catch (AttuneException ae) {
			LOGGER.error(ae.getMessage(), ae);
			if (ae.getStatusCode() != null) {
				return new ResponseEntity<>(ae.getMessage(), ae.getStatusCode());
			}
			throw ae;
		} catch (Exception e) {
			LOGGER.error(e.getMessage(), e);
			throw new AttuneException(e);
		}
	}

	@RequestMapping(value = "/nodes/{type}", method = RequestMethod.DELETE)
	@PreAuthorize("hasRole('" + SecurityService.ROLE_USER + "')")
	@ApiOperation(value = "deleteNodesForType", notes = "Deletes all Nodes for the given Metadata entity type.", response = String.class, httpMethod = "DELETE")
	@ApiResponses(value = {
			@ApiResponse(code = 204, message = "If the Nodes were successfully deleted, null or empty response."),
			@ApiResponse(code = 400, message = "Invalid request, if relationships still exist for any nodes."),
			@ApiResponse(code = 500, message = "An Error has Occurred") })
	public ResponseEntity<String> deleteNodesForType(
			@ApiParam(value = "Path parameter indicating the Metadata entity type name.", required = true) @PathVariable(value = "type") String type) {
		try {
			// First get all nodes of this type
			String nodesJson = this.getNodes(type, null, null).getBody();
			if (nodesJson != null) {
				JSONArray nodes;
				synchronized (parser) {
					nodes = (JSONArray) parser.parse(nodesJson);
				}
				// For each node get all of the relationships
				for (int x = 0; x < nodes.size(); x++) {
					JSONObject nodeJsonObj = (JSONObject) nodes.get(x);
					String nodeId = Neo4JUtils.getId(nodeJsonObj.toJSONString());
					this.deleteNode(Long.parseLong(nodeId));
				}
			}
			return new ResponseEntity<>(HttpStatus.OK);
		} catch (AttuneException ae) {
			LOGGER.error(ae.getMessage(), ae);
			if (ae.getStatusCode() != null) {
				return new ResponseEntity<>(ae.getMessage(), ae.getStatusCode());
			}
			throw ae;
		} catch (Exception e) {
			LOGGER.error(e.getMessage(), e);
			throw new AttuneException(e);
		}
	}

	@SuppressWarnings("unchecked")
	@RequestMapping(value = "/nodes/{type}", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
	@PreAuthorize("hasRole('" + SecurityService.ROLE_USER + "')")
	@ApiOperation(value = "getNodes", notes = "Returns all of the nodes for the given Metadata entity type. Each item in the returned JSON array will be a single-level JSON object containing it's properties.", response = String.class, httpMethod = "GET")
	@ApiResponses(value = {
			@ApiResponse(code = 200, message = "A JSON array containing node instances of the given Metadata entity type, or an empty array if none exist. Each item will also contain the transient fields called neo4j.id and neo4j.label that contains it's Neo4J node id and Metadata entity type."),
			@ApiResponse(code = 400, message = "Invalid request if the entity type is invalid."),
			@ApiResponse(code = 500, message = "An Error has Occurred") })
	public ResponseEntity<String> getNodes(
			@ApiParam(value = "Path parameter indicating the node types to return.", required = true) @PathVariable(value = "type") String type,
			@ApiParam(value = "Optional Query parameter to return Nodes with this property name. If specified, the propValue query parameter must be specified as well.", required = false) @RequestParam(value = "propName", required = false) String propName,
			@ApiParam(value = "Optional Query parameter to return Metadata entities with this property value. If specified, the propName query parameter must be specified as well.", required = false) @RequestParam(value = "propValue", required = false) String propValue) {
		String result = null;
		try {
			StringBuilder sb = new StringBuilder(neo4jUrlPrefix);
			String url;
			if (type != null) {
				sb.append("db/data/label/").append(type).append("/nodes");
			} else {
				StringBuilder msg = new StringBuilder("type parameter is mandatory");
				throw new AttuneException(msg.toString(), HttpStatus.BAD_REQUEST);
			}
			if (propName != null) {
				String name = URLDecoder.decode(propName, StandardCharsets.UTF_8.toString());
				String value = JSONValue.toJSONString(URLDecoder.decode(propValue, StandardCharsets.UTF_8.toString()));
				sb.append("?").append(name).append("=").append(value);
			}
			url = sb.toString();
			result = this.getNeo4JData(url);
			if (result != null) {
				JSONArray resultArray = new JSONArray();
				JSONArray array;
				synchronized (parser) {
					array = (JSONArray) parser.parse(result);
				}
				for (int i = 0; i < array.size(); i++) {
					JSONObject jsonObject = (JSONObject) array.get(i);
					String id = Neo4JUtils.getId(jsonObject.toJSONString());
					JSONObject nodeJson = new JSONObject(Neo4JUtils.getDataProperties(jsonObject.toJSONString()));
					nodeJson.put("neo4j.id", String.valueOf(id));
					nodeJson.put("neo4j.label", Neo4JUtils.getLabels(jsonObject.toJSONString()).get(0));
					resultArray.add(nodeJson);
				}
				result = resultArray.toJSONString();
			}
			final HttpHeaders httpHeaders = new HttpHeaders();
			httpHeaders.setContentType(MediaType.APPLICATION_JSON);
			if (result != null) {
				return new ResponseEntity<>(((JSONAware) parser.parse(result)).toJSONString(), httpHeaders,
						HttpStatus.OK);
			} else {
				return new ResponseEntity<>(null, httpHeaders, HttpStatus.OK);
			}
		} catch (AttuneException ae) {
			LOGGER.error(ae.getMessage(), ae);
			if (ae.getStatusCode() != null) {
				return new ResponseEntity<>(ae.getMessage(), ae.getStatusCode());
			}
			throw ae;
		} catch (Exception e) {
			LOGGER.error(e.getMessage(), e);
			throw new AttuneException(e);
		}
	}

	@RequestMapping(value = "/node/{id}/properties", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
	@PreAuthorize("hasRole('" + SecurityService.ROLE_USER + "')")
	@ApiOperation(value = "getNodeProperties", notes = "Returns all properties for the Node type as a single-level JSON Object", response = String.class, httpMethod = "GET")
	@ApiResponses(value = { @ApiResponse(code = 200, message = "A JSON object containing the properties for the Node"),
			@ApiResponse(code = 400, message = "Invalid request, if the Node id doesn't exist"),
			@ApiResponse(code = 500, message = "An Error has Occurred") })
	public ResponseEntity<String> getNodeProperties(
			@ApiParam(value = "The Node id.", required = true) @PathVariable(value = "id") Long id) {
		String result = null;
		try {
			String url = new StringBuilder(neo4jUrlPrefix).append("db/data/node/").append(String.valueOf(id))
					.append("/properties").toString();
			result = this.getNeo4JData(url);
			final HttpHeaders httpHeaders = new HttpHeaders();
			httpHeaders.setContentType(MediaType.APPLICATION_JSON);
			if (result != null) {
				return new ResponseEntity<>(((JSONAware) parser.parse(result)).toJSONString(), httpHeaders,
						HttpStatus.OK);
			} else {
				return new ResponseEntity<>(null, httpHeaders, HttpStatus.OK);
			}
		} catch (AttuneException ae) {
			LOGGER.error(ae.getMessage(), ae);
			if (ae.getStatusCode() != null) {
				return new ResponseEntity<>(ae.getMessage(), ae.getStatusCode());
			}
			throw ae;
		} catch (Exception e) {
			LOGGER.error(e.getMessage(), e);
			throw new AttuneException(e);
		}
	}

	@RequestMapping(value = "/node/{id}/properties", method = RequestMethod.PUT, consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
	@PreAuthorize("hasRole('" + SecurityService.ROLE_USER + "')")
	@ApiOperation(value = "updateNodeProperties", notes = "Overwrites all properties on a Node, passing in a single-level JSON object that contains the properties for the Node.", response = String.class, httpMethod = "PUT")
	@ApiResponses(value = {
			@ApiResponse(code = 204, message = "Node properties were successfully updated, empty response"),
			@ApiResponse(code = 400, message = "Invalid request, if the node id represents a Metadata entity, doesn't exist, or if the passed in properties don't contain all of the required fields defined in it's Metadata definition."),
			@ApiResponse(code = 500, message = "An Error has Occurred") })
	public ResponseEntity<String> updateNodeProperties(
			@ApiParam(value = "The Node id.", required = true) @PathVariable(value = "id") Long id,
			@ApiParam(value = "A single-level JSON object that contains the properties for the node, values can be String, boolean, or a number.", required = true) @RequestBody(required = true) Map<String, Object> properties) {
		try {
			String type = (String) Neo4JUtils.getValue(this.getNodeTypes(id).getBody());
			if (type.contains("Metadata")) {
				StringBuilder msg = new StringBuilder("Can not modify Metadata through this API. Please use ");
				msg.append(this.httpServletRequest.getContextPath()).append(this.httpServletRequest.getServletPath());
				msg.append("/metadata/entity/{entityType}/properties").append(" instead.");
				throw new AttuneException(msg.toString(), HttpStatus.BAD_REQUEST);
			}
			for (String requiredField : this.getRequiredFields(type)) {
				if (!properties.containsKey(requiredField)) {
					StringBuilder msg = new StringBuilder("Property " + requiredField + " is required!");
					throw new AttuneException(msg.toString(), HttpStatus.BAD_REQUEST);
				}
			}
			String url = new StringBuilder(neo4jUrlPrefix).append("db/data/node/").append(String.valueOf(id))
					.append("/properties").toString();
			String json;
			if (properties != null && !properties.isEmpty()) {
				properties = defaultNullPropertyValues(properties);
				JSONObject jsonObj = new JSONObject(properties);
				json = jsonObj.toJSONString();
				this.updateNeo4JData(json, url);
			}
			return new ResponseEntity<>(HttpStatus.OK);
		} catch (AttuneException ae) {
			LOGGER.error(ae.getMessage(), ae);
			if (ae.getStatusCode() != null) {
				return new ResponseEntity<>(ae.getMessage(), ae.getStatusCode());
			}
			throw ae;
		} catch (Exception e) {
			LOGGER.error(e.getMessage(), e);
			throw new AttuneException(e);
		}
	}

	@RequestMapping(value = "/node/{id}/properties", method = RequestMethod.DELETE)
	@PreAuthorize("hasRole('" + SecurityService.ROLE_USER + "')")
	@ApiOperation(value = "deleteNodeProperties", notes = "Deletes all properties on a Node except the required fields as defined by it's Metadata entity definition.", response = String.class, httpMethod = "DELETE")
	@ApiResponses(value = {
			@ApiResponse(code = 204, message = "Node properties were successfully deleted, empty response"),
			@ApiResponse(code = 400, message = "Invalid request if the node id doesn't exist."),
			@ApiResponse(code = 500, message = "An Error has Occurred") })
	public ResponseEntity<String> deleteNodeProperties(
			@ApiParam(value = "The Node id.", required = true) @PathVariable(value = "id") Long id) {
		try {
			String type = (String) Neo4JUtils.getValue(this.getNodeTypes(id).getBody());
			if (type.contains("Metadata")) {
				StringBuilder msg = new StringBuilder("Can not modify Metadata through this API. Please use ");
				msg.append(this.httpServletRequest.getContextPath()).append(this.httpServletRequest.getServletPath());
				msg.append("/metadata/entity/{entityType}/properties").append(" instead.");
				throw new AttuneException(msg.toString(), HttpStatus.BAD_REQUEST);
			}
			Map<String, Object> props = Neo4JUtils.getProperties(this.getNodeProperties(id).getBody());
			Set<String> requiredFields = this.getRequiredFields(type);
			for (String propName : props.keySet()) {
				if (!requiredFields.contains(propName)) {
					this.deleteNodeProperty(id, propName);
				}
			}
			return new ResponseEntity<>(HttpStatus.OK);
		} catch (AttuneException ae) {
			LOGGER.error(ae.getMessage(), ae);
			if (ae.getStatusCode() != null) {
				return new ResponseEntity<>(ae.getMessage(), ae.getStatusCode());
			}
			throw ae;
		} catch (Exception e) {
			LOGGER.error(e.getMessage(), e);
			throw new AttuneException(e);
		}
	}

	@RequestMapping(value = "/node/{id}/property/{propName:.+}", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
	@PreAuthorize("hasRole('" + SecurityService.ROLE_USER + "')")
	@ApiOperation(value = "getNodeProperty", notes = "Returns the value for the specified property name and Node id. The body of the response is a single String, boolean, or number value which is still valid JSON.", response = String.class, httpMethod = "GET")
	@ApiResponses(value = {
			@ApiResponse(code = 200, message = "A a single String, boolean, or number for the specified property on the Node"),
			@ApiResponse(code = 400, message = "Invalid request, if the Node id or property name doesn't exist"),
			@ApiResponse(code = 500, message = "An Error has Occurred") })
	public ResponseEntity<String> getNodeProperty(
			@ApiParam(value = "The Node id.", required = true) @PathVariable(value = "id") Long id,
			@ApiParam(value = "Path parameter for which property's value should be returned.", required = true) @PathVariable(value = "propName") String propName) {
		String result = null;
		try {
			String url = new StringBuilder(neo4jUrlPrefix).append("db/data/node/").append(String.valueOf(id))
					.append("/properties/").append(propName).toString();
			result = this.getNeo4JData(url);
			final HttpHeaders httpHeaders = new HttpHeaders();
			httpHeaders.setContentType(MediaType.APPLICATION_JSON);
			if (result != null) {
				return new ResponseEntity<>(JSONValue.toJSONString(parser.parse(result)), httpHeaders, HttpStatus.OK);
			} else {
				return new ResponseEntity<>(null, httpHeaders, HttpStatus.OK);
			}
		} catch (AttuneException ae) {
			LOGGER.error(ae.getMessage(), ae);
			if (ae.getStatusCode() != null) {
				return new ResponseEntity<>(ae.getMessage(), ae.getStatusCode());
			}
			throw ae;
		} catch (Exception e) {
			LOGGER.error(e.getMessage(), e);
			throw new AttuneException(e);
		}
	}

	@RequestMapping(value = "/node/{id}/property/{propName:.+}", method = RequestMethod.PUT, consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
	@PreAuthorize("hasRole('" + SecurityService.ROLE_USER + "')")
	@ApiOperation(value = "updateNodeProperty", notes = "Adds or updates the specified property on the Node. The body of the request should be a single String, boolean, or number value which is still valid JSON.", response = String.class, httpMethod = "PUT")
	@ApiResponses(value = {
			@ApiResponse(code = 204, message = "Node property was successfully added or updated, empty response"),
			@ApiResponse(code = 400, message = "Invalid request  if the node id does not exist or property value is null or not a String, boolean, or number."),
			@ApiResponse(code = 500, message = "An Error has Occurred") })
	public ResponseEntity<String> updateNodeProperty(
			@ApiParam(value = "The Node id.", required = true) @PathVariable(value = "id") Long id,
			@ApiParam(value = "Path parameter for which property's value should be returned.", required = true) @PathVariable(value = "propName") String propName,
			@ApiParam(value = "The property's value which should be a single String, boolean, or number value", required = true) @RequestBody(required = true) Object propValue) {
		try {
			if (propValue == null) {
				StringBuilder msg = new StringBuilder("Property values can not be null!");
				throw new AttuneException(msg.toString(), HttpStatus.BAD_REQUEST);
			}
			String type = (String) Neo4JUtils.getValue(this.getNodeTypes(id).getBody());
			if (type.contains("Metadata")) {
				StringBuilder msg = new StringBuilder("Can not modify Metadata through this API. Please use ");
				msg.append(this.httpServletRequest.getContextPath()).append(this.httpServletRequest.getServletPath());
				msg.append("/metadata/entity/{entityType}/property/{propName}").append(" instead.");
				throw new AttuneException(msg.toString(), HttpStatus.BAD_REQUEST);
			}
			String url = new StringBuilder(neo4jUrlPrefix).append("db/data/node/").append(String.valueOf(id))
					.append("/properties/").append(propName).toString();
			this.updateNeo4JData(JSONValue.toJSONString(propValue), url);
			return new ResponseEntity<>(HttpStatus.OK);
		} catch (AttuneException ae) {
			LOGGER.error(ae.getMessage(), ae);
			if (ae.getStatusCode() != null) {
				return new ResponseEntity<>(ae.getMessage(), ae.getStatusCode());
			}
			throw ae;
		} catch (Exception e) {
			LOGGER.error(e.getMessage(), e);
			throw new AttuneException(e);
		}
	}

	@RequestMapping(value = "/node/{id}/property/{propName:.+}", method = RequestMethod.DELETE)
	@PreAuthorize("hasRole('" + SecurityService.ROLE_USER + "')")
	@ApiOperation(value = "deleteNodeProperty", notes = "Delete the specified property from the Node.", response = String.class, httpMethod = "DELETE")
	@ApiResponses(value = {
			@ApiResponse(code = 204, message = "Node property was successfully deleted, empty response"),
			@ApiResponse(code = 400, message = "Invalid request, if the node id represents a Metadata entity, doesn't exist, or if the passed in property name is one of the required fields defined in it's Metadata definition."),
			@ApiResponse(code = 500, message = "An Error has Occurred") })
	public ResponseEntity<String> deleteNodeProperty(
			@ApiParam(value = "The Node id.", required = true) @PathVariable(value = "id") Long id,
			@ApiParam(value = "Path parameter for which property's value should be returned.", required = true) @PathVariable(value = "propName") String propName) {
		try {
			String type = (String) Neo4JUtils.getValue(this.getNodeTypes(id).getBody());
			if (type.contains("Metadata")) {
				StringBuilder msg = new StringBuilder("Can not modify Metadata through this API. Please use ");
				msg.append(this.httpServletRequest.getContextPath()).append(this.httpServletRequest.getServletPath());
				msg.append("/metadata/entity/{entityType}/property/{propName}").append(" instead.");
				throw new AttuneException(msg.toString(), HttpStatus.BAD_REQUEST);
			}
			for (String requiredField : this.getRequiredFields(type)) {
				if (propName.equals(requiredField)) {
					StringBuilder msg = new StringBuilder("Property " + requiredField + " is required!");
					throw new AttuneException(msg.toString(), HttpStatus.BAD_REQUEST);
				}
			}
			String url = new StringBuilder(neo4jUrlPrefix).append("db/data/node/").append(String.valueOf(id))
					.append("/properties/").append(propName).toString();
			this.deleteNeo4JData(url);
			return new ResponseEntity<>(HttpStatus.OK);
		} catch (AttuneException ae) {
			LOGGER.error(ae.getMessage(), ae);
			if (ae.getStatusCode() != null) {
				return new ResponseEntity<>(ae.getMessage(), ae.getStatusCode());
			}
			throw ae;
		} catch (Exception e) {
			LOGGER.error(e.getMessage(), e);
			throw new AttuneException(e);
		}
	}

	@RequestMapping(value = "/nodeTypes", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
	@PreAuthorize("hasRole('" + SecurityService.ROLE_USER + "')")
	@ApiOperation(value = "getAllNodeTypes", notes = "Returns all of the metadata entity type names for all nodes.", response = String.class, httpMethod = "GET")
	@ApiResponses(value = {
			@ApiResponse(code = 200, message = "A JSON array of Strings is returned that contains the Metadata entity type names, or an empty JSON array if none exist."),
			@ApiResponse(code = 500, message = "An Error has Occurred") })
	public ResponseEntity<String> getAllNodeTypes() {
		String result = null;
		try {
			String url = new StringBuilder(neo4jUrlPrefix).append("db/data/labels").toString();
			result = this.getNeo4JData(url);
			final HttpHeaders httpHeaders = new HttpHeaders();
			httpHeaders.setContentType(MediaType.APPLICATION_JSON);
			if (result != null) {
				return new ResponseEntity<>(((JSONAware) parser.parse(result)).toJSONString(), httpHeaders,
						HttpStatus.OK);
			} else {
				return new ResponseEntity<>(null, httpHeaders, HttpStatus.OK);
			}
		} catch (AttuneException ae) {
			LOGGER.error(ae.getMessage(), ae);
			if (ae.getStatusCode() != null) {
				return new ResponseEntity<>(ae.getMessage(), ae.getStatusCode());
			}
			throw ae;
		} catch (Exception e) {
			LOGGER.error(e.getMessage(), e);
			throw new AttuneException(e);
		}
	}

	@RequestMapping(value = "/nodeTypes/{id}", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
	@PreAuthorize("hasRole('" + SecurityService.ROLE_USER + "')")
	@ApiOperation(value = "getNodeTypes", notes = "Returns the metadata entity type name for a node.", response = String.class, httpMethod = "GET")
	@ApiResponses(value = {
			@ApiResponse(code = 200, message = "A JSON array with a single String is returned that contains the Metadata entity type name for the Node."),
			@ApiResponse(code = 400, message = "Invalid request if the node id doesn't exist."),
			@ApiResponse(code = 500, message = "An Error has Occurred") })
	public ResponseEntity<String> getNodeTypes(
			@ApiParam(value = "The Node id.", required = true) @PathVariable(value = "id") Long id) {
		String result = null;
		try {
			String url = new StringBuilder(neo4jUrlPrefix).append("db/data/node/").append(String.valueOf(id))
					.append("/labels").toString();
			result = this.getNeo4JData(url);
			final HttpHeaders httpHeaders = new HttpHeaders();
			httpHeaders.setContentType(MediaType.APPLICATION_JSON);
			if (result != null) {
				return new ResponseEntity<>(((JSONAware) parser.parse(result)).toJSONString(), httpHeaders,
						HttpStatus.OK);
			} else {
				return new ResponseEntity<>(null, httpHeaders, HttpStatus.OK);
			}
		} catch (AttuneException ae) {
			LOGGER.error(ae.getMessage(), ae);
			if (ae.getStatusCode() != null) {
				return new ResponseEntity<>(ae.getMessage(), ae.getStatusCode());
			}
			throw ae;
		} catch (Exception e) {
			LOGGER.error(e.getMessage(), e);
			throw new AttuneException(e);
		}
	}

	@RequestMapping(value = "/relationship/types", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
	@PreAuthorize("hasRole('" + SecurityService.ROLE_USER + "')")
	@ApiOperation(value = "getRelationshipTypes", notes = "Returns all relationship types that have been defined for all Nodes.", response = String.class, httpMethod = "GET")
	@ApiResponses(value = {
			@ApiResponse(code = 200, message = "A JSON array of Strings that contains all relationship types, or an empty array if none exist"),
			@ApiResponse(code = 500, message = "An Error has Occurred") })
	public ResponseEntity<String> getRelationshipTypes() {
		String result = null;
		try {
			String url = new StringBuilder(neo4jUrlPrefix).append("db/data/relationship/types").toString();
			result = this.getNeo4JData(url);
			final HttpHeaders httpHeaders = new HttpHeaders();
			httpHeaders.setContentType(MediaType.APPLICATION_JSON);
			if (result != null) {
				return new ResponseEntity<>(((JSONAware) parser.parse(result)).toJSONString(), httpHeaders,
						HttpStatus.OK);
			} else {
				return new ResponseEntity<>(null, httpHeaders, HttpStatus.OK);
			}
		} catch (AttuneException ae) {
			LOGGER.error(ae.getMessage(), ae);
			if (ae.getStatusCode() != null) {
				return new ResponseEntity<>(ae.getMessage(), ae.getStatusCode());
			}
			throw ae;
		} catch (Exception e) {
			LOGGER.error(e.getMessage(), e);
			throw new AttuneException(e);
		}
	}

	@SuppressWarnings("unchecked")
	@RequestMapping(value = "/relationship/{id}", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
	@PreAuthorize("hasRole('" + SecurityService.ROLE_USER + "')")
	@ApiOperation(value = "getRelationship", notes = "Returns the relationship for the specificed Node.", response = String.class, httpMethod = "GET")
	@ApiResponses(value = {
			@ApiResponse(code = 200, message = "Returns a JSON object representing the relationship, which contains the neo4j.id, relationshipType, startNodeId, startNodeType, startNodeName, endNodeId, endNodeType, and endNodeName"),
			@ApiResponse(code = 400, message = "Invalid request if the id is invalid"),
			@ApiResponse(code = 500, message = "An Error has Occurred") })
	public ResponseEntity<String> getRelationship(
			@ApiParam(value = "The Relationship id.", required = true) @PathVariable(value = "id") Long id) {
		String result = null;
		try {
			String url = new StringBuilder(neo4jUrlPrefix).append("db/data/relationship/").append(String.valueOf(id))
					.toString();
			result = this.getNeo4JData(url);
			if (result != null) {
				JSONObject apiMgmtRelationshipJson;
				synchronized (parser) {
					apiMgmtRelationshipJson = (JSONObject) parser.parse(result);
				}
				Map<String, Object> data = Neo4JUtils.getDataProperties(apiMgmtRelationshipJson.toJSONString());
				JSONObject relationshipJson = new JSONObject(data);
				relationshipJson.put("neo4j.id", Neo4JUtils.getId(apiMgmtRelationshipJson.toJSONString()));

				String relationshipStartId = Neo4JUtils
						.getRelationshipStartNode(apiMgmtRelationshipJson.toJSONString());
				String relationshipEndId = Neo4JUtils.getRelationshipEndNode(apiMgmtRelationshipJson.toJSONString());
				relationshipJson.put("relationshipType",
						Neo4JUtils.getRelationshipType(apiMgmtRelationshipJson.toJSONString()));
				relationshipJson.put("startNodeId", relationshipStartId);
				relationshipJson.put("startNodeType",
						Neo4JUtils.getLabels(this.getNode(Long.parseLong(relationshipStartId)).getBody()).get(0));
				relationshipJson.put("startNodeName",
						Neo4JUtils.getDataValue(this.getNode(Long.parseLong(relationshipStartId)).getBody(), "Name"));
				relationshipJson.put("endNodeId", relationshipEndId);
				relationshipJson.put("endNodeType",
						Neo4JUtils.getLabels(this.getNode(Long.parseLong(relationshipEndId)).getBody()).get(0));
				relationshipJson.put("endNodeName",
						Neo4JUtils.getDataValue(this.getNode(Long.parseLong(relationshipEndId)).getBody(), "Name"));
				result = relationshipJson.toJSONString();
			}
			final HttpHeaders httpHeaders = new HttpHeaders();
			httpHeaders.setContentType(MediaType.APPLICATION_JSON);
			if (result != null) {
				return new ResponseEntity<>(((JSONAware) parser.parse(result)).toJSONString(), httpHeaders,
						HttpStatus.OK);
			} else {
				return new ResponseEntity<>(null, httpHeaders, HttpStatus.OK);
			}
		} catch (AttuneException ae) {
			LOGGER.error(ae.getMessage(), ae);
			if (ae.getStatusCode() != null) {
				return new ResponseEntity<>(ae.getMessage(), ae.getStatusCode());
			}
			throw ae;
		} catch (Exception e) {
			LOGGER.error(e.getMessage(), e);
			throw new AttuneException(e);
		}
	}

	@SuppressWarnings("unchecked")
	@RequestMapping(value = "/node/{startId}/relationships/{endId}", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
	@PreAuthorize("hasRole('" + SecurityService.ROLE_USER + "')")
	@ApiOperation(value = "getRelationships", notes = "Returns all relationships for the specified starting Node id and ending Node id.", response = String.class, httpMethod = "GET")
	@ApiResponses(value = {
			@ApiResponse(code = 200, message = "A JSON array containing the relationships, or an empty array if none exist"),
			@ApiResponse(code = 400, message = "Invalid request if the Node ids are invalid"),
			@ApiResponse(code = 500, message = "An Error has Occurred") })
	public ResponseEntity<String> getRelationships(
			@ApiParam(value = "The starting node's id.", required = true) @PathVariable(value = "startId") Long startId,
			@ApiParam(value = "The ending node's id.", required = true) @PathVariable(value = "endId") Long endId,
			@ApiParam(value = "Optional query parameter indicating the relationship types to get. Multiple values can be specified by repeating the parameter in the URL, "
					+ "for example http://localhost:8080/attune/api/data/node/{startId}/relationships/{endId}?relType=has&relType=likes&relType=uses"
					+ ", would return all 'has', 'likes', and 'uses' relationships.", required = false) @RequestParam(value = "relType", required = false) List<String> relType,
			@ApiParam(value = "Optional Query parameter indicating the relationship direction. Valid values are \"in\", \"out\", and \"all\".", required = false) @RequestParam(value = "relDir", required = false) String relDir) {
		String result = null;
		try {
			JSONArray results = new JSONArray();
			String allRelsJson;
			if (relDir != null) {
				if ("all".equals(relDir)) {
					allRelsJson = this.getRelationshipsAll(startId, relType).getBody();
				} else if ("out".equals(relDir)) {
					allRelsJson = this.getRelationshipsOut(startId, relType).getBody();
				} else if ("in".equals(relDir)) {
					allRelsJson = this.getRelationshipsIn(startId, relType).getBody();
				} else {
					StringBuilder msg = new StringBuilder("Query parameter relDir must equal all, out, or in.");
					throw new AttuneException(msg.toString(), HttpStatus.BAD_REQUEST);
				}
			} else {
				allRelsJson = this.getRelationshipsAll(startId, relType).getBody();
			}

			if (allRelsJson != null) {
				JSONArray jsonArray;
				synchronized (parser) {
					jsonArray = (JSONArray) parser.parse(allRelsJson);
				}
				for (int i = 0; i < jsonArray.size(); i++) {
					JSONObject jsonObject = (JSONObject) jsonArray.get(i);
					String relEndEntityId = Neo4JUtils.getRelationshipEndNode(jsonObject.toJSONString());
					if (endId.equals(Long.parseLong(relEndEntityId))) {
						results.add(jsonObject);
					}
				}
				result = results.toJSONString();
			}
			final HttpHeaders httpHeaders = new HttpHeaders();
			httpHeaders.setContentType(MediaType.APPLICATION_JSON);
			if (result != null) {
				return new ResponseEntity<>(((JSONAware) parser.parse(result)).toJSONString(), httpHeaders,
						HttpStatus.OK);
			} else {
				return new ResponseEntity<>(null, httpHeaders, HttpStatus.OK);
			}
		} catch (AttuneException ae) {
			LOGGER.error(ae.getMessage(), ae);
			if (ae.getStatusCode() != null) {
				return new ResponseEntity<>(ae.getMessage(), ae.getStatusCode());
			}
			throw ae;
		} catch (Exception e) {
			LOGGER.error(e.getMessage(), e);
			throw new AttuneException(e);
		}
	}

	@SuppressWarnings("unchecked")
	@RequestMapping(value = "/node/{id}/relationships/all", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
	@PreAuthorize("hasRole('" + SecurityService.ROLE_USER + "')")
	@ApiOperation(value = "getRelationshipsAll", notes = "Gets all relationships for the specified Node.", response = String.class, httpMethod = "GET")
	@ApiResponses(value = {
			@ApiResponse(code = 200, message = "JSON array of relationships for the Node, or an empty JSON array if none exist"),
			@ApiResponse(code = 400, message = "Invalid request if the Node id doesn't exist"),
			@ApiResponse(code = 500, message = "An Error has Occurred") })
	public ResponseEntity<String> getRelationshipsAll(
			@ApiParam(value = "The Node id.", required = true) @PathVariable(value = "id") Long id,
			@ApiParam(value = "Optional query parameter indicating the relationship types to get. Multiple values can be specified by repeating the parameter in the URL, "
					+ "for example http://localhost:8080/attune/api/data/node/{id}/relationships/all?relType=has&relType=likes&relType=uses"
					+ ", would return all 'has', 'likes', and 'uses' relationships.", required = false) @RequestParam(value = "relType", required = false) List<String> relTypes) {
		String result = null;
		try {
			StringBuilder sb = new StringBuilder(neo4jUrlPrefix).append("db/data/node/").append(String.valueOf(id))
					.append("/relationships/all");
			String url = this.getUrlForRelTypes(sb, relTypes);
			result = this.getNeo4JData(url);
			if (result != null) {
				JSONArray resultArray = new JSONArray();
				JSONArray array;
				synchronized (parser) {
					array = (JSONArray) parser.parse(result);
				}
				for (int i = 0; i < array.size(); i++) {
					JSONObject apiMgmtRelationshipJson = (JSONObject) array.get(i);
					Map<String, Object> data = Neo4JUtils.getDataProperties(apiMgmtRelationshipJson.toJSONString());
					JSONObject relationshipJson = new JSONObject(data);
					relationshipJson.put("neo4j.id", Neo4JUtils.getId(apiMgmtRelationshipJson.toJSONString()));

					String relationshipStartId = Neo4JUtils
							.getRelationshipStartNode(apiMgmtRelationshipJson.toJSONString());
					String relationshipEndId = Neo4JUtils
							.getRelationshipEndNode(apiMgmtRelationshipJson.toJSONString());
					relationshipJson.put("relationshipType",
							Neo4JUtils.getRelationshipType(apiMgmtRelationshipJson.toJSONString()));
					relationshipJson.put("startNodeId", relationshipStartId);
					relationshipJson.put("startNodeType",
							Neo4JUtils.getLabels(this.getNode(Long.parseLong(relationshipStartId)).getBody()).get(0));
					relationshipJson.put("startNodeName", Neo4JUtils
							.getDataValue(this.getNode(Long.parseLong(relationshipStartId)).getBody(), "Name"));
					relationshipJson.put("endNodeId", relationshipEndId);
					relationshipJson.put("endNodeType",
							Neo4JUtils.getLabels(this.getNode(Long.parseLong(relationshipEndId)).getBody()).get(0));
					relationshipJson.put("endNodeName",
							Neo4JUtils.getDataValue(this.getNode(Long.parseLong(relationshipEndId)).getBody(), "Name"));
					resultArray.add(relationshipJson);
				}
				result = resultArray.toJSONString();
			}
			final HttpHeaders httpHeaders = new HttpHeaders();
			httpHeaders.setContentType(MediaType.APPLICATION_JSON);
			if (result != null) {
				return new ResponseEntity<>(((JSONAware) parser.parse(result)).toJSONString(), httpHeaders,
						HttpStatus.OK);
			} else {
				return new ResponseEntity<>(null, httpHeaders, HttpStatus.OK);
			}
		} catch (AttuneException ae) {
			LOGGER.error(ae.getMessage(), ae);
			if (ae.getStatusCode() != null) {
				return new ResponseEntity<>(ae.getMessage(), ae.getStatusCode());
			}
			throw ae;
		} catch (Exception e) {
			LOGGER.error(e.getMessage(), e);
			throw new AttuneException(e);
		}
	}

	@RequestMapping(value = "/node/{id}/relationships/all", method = RequestMethod.DELETE)
	@PreAuthorize("hasRole('" + SecurityService.ROLE_USER + "')")
	@ApiOperation(value = "deleteRelationshipsAll", notes = "Deletes all relationships for the specified Node.", response = String.class, httpMethod = "DELETE")
	@ApiResponses(value = { @ApiResponse(code = 204, message = "Node relationships deleted, empty response"),
			@ApiResponse(code = 400, message = "Invalid request if the Node id doesn't exist"),
			@ApiResponse(code = 500, message = "An Error has Occurred") })
	public ResponseEntity<String> deleteRelationshipsAll(
			@ApiParam(value = "The Node id.", required = true) @PathVariable(value = "id") Long id,
			@ApiParam(value = "Optional query parameter indicating the relationship types to delete. Multiple values can be specified by repeating the parameter in the URL, "
					+ "for example http://localhost:8080/attune/api/data/node/{id}/relationships/all?relType=has&relType=likes&relType=uses"
					+ ", would delete all 'has', 'likes', and 'uses' relationships.", required = false) @RequestParam(value = "relType", required = false) List<String> relTypes) {
		try {
			String relsJson = this.getRelationshipsAll(id, relTypes).getBody();
			if (relsJson != null) {
				JSONArray relationships;
				synchronized (parser) {
					relationships = (JSONArray) parser.parse(relsJson);
				}
				if (relationships != null) {
					// Delete each of the relationships
					for (int y = 0; y < relationships.size(); y++) {
						JSONObject relationshipJsonObj = (JSONObject) relationships.get(y);
						String relationshipNodeId = Neo4JUtils.getId(relationshipJsonObj.toJSONString());
						String url = new StringBuilder(neo4jUrlPrefix).append("db/data/relationship/")
								.append(relationshipNodeId).toString();
						this.deleteNeo4JData(url);
					}
				}
			}
			return new ResponseEntity<>(HttpStatus.OK);
		} catch (AttuneException ae) {
			LOGGER.error(ae.getMessage(), ae);
			if (ae.getStatusCode() != null) {
				return new ResponseEntity<>(ae.getMessage(), ae.getStatusCode());
			}
			throw ae;
		} catch (Exception e) {
			LOGGER.error(e.getMessage(), e);
			throw new AttuneException(e);
		}
	}

	@SuppressWarnings("unchecked")
	@RequestMapping(value = "/node/{id}/relationships/in", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
	@PreAuthorize("hasRole('" + SecurityService.ROLE_USER + "')")
	@ApiOperation(value = "getRelationshipsIn", notes = "Gets all inbound relationships for the specified Node.", response = String.class, httpMethod = "GET")
	@ApiResponses(value = {
			@ApiResponse(code = 200, message = "JSON array of relationships for the Node, or an empty JSON array if none exist"),
			@ApiResponse(code = 400, message = "Invalid request if the Node id doesn't exist"),
			@ApiResponse(code = 500, message = "An Error has Occurred") })
	public ResponseEntity<String> getRelationshipsIn(
			@ApiParam(value = "The Node id.", required = true) @PathVariable(value = "id") Long id,
			@ApiParam(value = "Optional query parameter indicating the relationship types to get. Multiple values can be specified by repeating the parameter in the URL, "
					+ "for example http://localhost:8080/attune/api/data/node/{id}/relationships/in?relType=has&relType=likes&relType=uses"
					+ ", would return all 'has', 'likes', and 'uses' inbound relationships.", required = false) @RequestParam(value = "relType", required = false) List<String> relTypes) {
		String result = null;
		try {
			StringBuilder sb = new StringBuilder(neo4jUrlPrefix).append("db/data/node/").append(String.valueOf(id))
					.append("/relationships/in");
			String url = this.getUrlForRelTypes(sb, relTypes);
			result = this.getNeo4JData(url);
			if (result != null) {
				JSONArray resultArray = new JSONArray();
				JSONArray array;
				synchronized (parser) {
					array = (JSONArray) parser.parse(result);
				}
				for (int i = 0; i < array.size(); i++) {
					JSONObject apiMgmtRelationshipJson = (JSONObject) array.get(i);
					Map<String, Object> data = Neo4JUtils.getDataProperties(apiMgmtRelationshipJson.toJSONString());
					JSONObject relationshipJson = new JSONObject(data);
					relationshipJson.put("neo4j.id", Neo4JUtils.getId(apiMgmtRelationshipJson.toJSONString()));

					String relationshipStartId = Neo4JUtils
							.getRelationshipStartNode(apiMgmtRelationshipJson.toJSONString());
					String relationshipEndId = Neo4JUtils
							.getRelationshipEndNode(apiMgmtRelationshipJson.toJSONString());
					relationshipJson.put("relationshipType",
							Neo4JUtils.getRelationshipType(apiMgmtRelationshipJson.toJSONString()));
					relationshipJson.put("startNodeId", relationshipStartId);
					relationshipJson.put("startNodeType",
							Neo4JUtils.getLabels(this.getNode(Long.parseLong(relationshipStartId)).getBody()).get(0));
					relationshipJson.put("startNodeName", Neo4JUtils
							.getDataValue(this.getNode(Long.parseLong(relationshipStartId)).getBody(), "Name"));
					relationshipJson.put("endNodeId", relationshipEndId);
					relationshipJson.put("endNodeType",
							Neo4JUtils.getLabels(this.getNode(Long.parseLong(relationshipEndId)).getBody()).get(0));
					relationshipJson.put("endNodeName",
							Neo4JUtils.getDataValue(this.getNode(Long.parseLong(relationshipEndId)).getBody(), "Name"));
					resultArray.add(relationshipJson);
				}
				result = resultArray.toJSONString();
			}
			final HttpHeaders httpHeaders = new HttpHeaders();
			httpHeaders.setContentType(MediaType.APPLICATION_JSON);
			if (result != null) {
				return new ResponseEntity<>(((JSONAware) parser.parse(result)).toJSONString(), httpHeaders,
						HttpStatus.OK);
			} else {
				return new ResponseEntity<>(null, httpHeaders, HttpStatus.OK);
			}
		} catch (AttuneException ae) {
			LOGGER.error(ae.getMessage(), ae);
			if (ae.getStatusCode() != null) {
				return new ResponseEntity<>(ae.getMessage(), ae.getStatusCode());
			}
			throw ae;
		} catch (Exception e) {
			LOGGER.error(e.getMessage(), e);
			throw new AttuneException(e);
		}
	}

	@RequestMapping(value = "/node/{id}/relationships/in", method = RequestMethod.DELETE)
	@PreAuthorize("hasRole('" + SecurityService.ROLE_USER + "')")
	@ApiOperation(value = "deleteRelationshipsIn", notes = "Deletes all inbound relationships for the specified Node.", response = String.class, httpMethod = "DELETE")
	@ApiResponses(value = { @ApiResponse(code = 204, message = "Node relationships deleted, empty response"),
			@ApiResponse(code = 400, message = "Invalid request if the Node id doesn't exist"),
			@ApiResponse(code = 500, message = "An Error has Occurred") })
	public ResponseEntity<String> deleteRelationshipsIn(
			@ApiParam(value = "The Node id.", required = true) @PathVariable(value = "id") Long id,
			@ApiParam(value = "Optional query parameter indicating the relationship types to delete. Multiple values can be specified by repeating the parameter in the URL, "
					+ "for example http://localhost:8080/attune/api/data/node/{id}/relationships/in?relType=has&relType=likes&relType=uses"
					+ ", would delete all 'has', 'likes', and 'uses' inbound relationships.", required = false) @RequestParam(value = "relType", required = false) List<String> relTypes) {
		try {
			String relsJson = this.getRelationshipsIn(id, relTypes).getBody();
			if (relsJson != null) {
				JSONArray relationships;
				synchronized (parser) {
					relationships = (JSONArray) parser.parse(relsJson);
				}
				// Delete each of the relationships
				for (int y = 0; y < relationships.size(); y++) {
					JSONObject relationshipJsonObj = (JSONObject) relationships.get(y);
					String relationshipNodeId = Neo4JUtils.getId(relationshipJsonObj.toJSONString());
					String url = new StringBuilder(neo4jUrlPrefix).append("db/data/relationship/")
							.append(relationshipNodeId).toString();
					this.deleteNeo4JData(url);
				}
			}
			return new ResponseEntity<>(HttpStatus.OK);
		} catch (AttuneException ae) {
			LOGGER.error(ae.getMessage(), ae);
			if (ae.getStatusCode() != null) {
				return new ResponseEntity<>(ae.getMessage(), ae.getStatusCode());
			}
			throw ae;
		} catch (Exception e) {
			LOGGER.error(e.getMessage(), e);
			throw new AttuneException(e);
		}
	}

	@SuppressWarnings("unchecked")
	@RequestMapping(value = "/node/{id}/relationships/out", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
	@PreAuthorize("hasRole('" + SecurityService.ROLE_USER + "')")
	@ApiOperation(value = "getRelationshipsOut", notes = "Gets all outbound relationships for the specified Node.", response = String.class, httpMethod = "GET")
	@ApiResponses(value = {
			@ApiResponse(code = 200, message = "JSON array of relationships for the Node, or an empty JSON array if none exist"),
			@ApiResponse(code = 400, message = "Invalid request if the Node id doesn't exist"),
			@ApiResponse(code = 500, message = "An Error has Occurred") })
	public ResponseEntity<String> getRelationshipsOut(
			@ApiParam(value = "The Node id.", required = true) @PathVariable(value = "id") Long id,
			@ApiParam(value = "Optional query parameter indicating the relationship types to get. Multiple values can be specified by repeating the parameter in the URL, "
					+ "for example http://localhost:8080/attune/api/data/node/{id}/relationships/out?relType=has&relType=likes&relType=uses"
					+ ", would return all 'has', 'likes', and 'uses' outbound relationships.", required = false) @RequestParam(value = "relType", required = false) List<String> relTypes) {
		String result = null;
		try {
			StringBuilder sb = new StringBuilder(neo4jUrlPrefix).append("db/data/node/").append(String.valueOf(id))
					.append("/relationships/out");
			String url = this.getUrlForRelTypes(sb, relTypes);
			result = this.getNeo4JData(url);
			if (result != null) {
				JSONArray resultArray = new JSONArray();
				JSONArray array;
				synchronized (parser) {
					array = (JSONArray) parser.parse(result);
				}
				for (int i = 0; i < array.size(); i++) {
					JSONObject apiMgmtRelationshipJson = (JSONObject) array.get(i);
					Map<String, Object> data = Neo4JUtils.getDataProperties(apiMgmtRelationshipJson.toJSONString());
					JSONObject relationshipJson = new JSONObject(data);
					relationshipJson.put("neo4j.id", Neo4JUtils.getId(apiMgmtRelationshipJson.toJSONString()));

					String relationshipStartId = Neo4JUtils
							.getRelationshipStartNode(apiMgmtRelationshipJson.toJSONString());
					String relationshipEndId = Neo4JUtils
							.getRelationshipEndNode(apiMgmtRelationshipJson.toJSONString());
					relationshipJson.put("relationshipType",
							Neo4JUtils.getRelationshipType(apiMgmtRelationshipJson.toJSONString()));
					relationshipJson.put("startNodeId", relationshipStartId);
					relationshipJson.put("startNodeType",
							Neo4JUtils.getLabels(this.getNode(Long.parseLong(relationshipStartId)).getBody()).get(0));
					relationshipJson.put("startNodeName", Neo4JUtils
							.getDataValue(this.getNode(Long.parseLong(relationshipStartId)).getBody(), "Name"));
					relationshipJson.put("endNodeId", relationshipEndId);
					relationshipJson.put("endNodeType",
							Neo4JUtils.getLabels(this.getNode(Long.parseLong(relationshipEndId)).getBody()).get(0));
					relationshipJson.put("endNodeName",
							Neo4JUtils.getDataValue(this.getNode(Long.parseLong(relationshipEndId)).getBody(), "Name"));
					resultArray.add(relationshipJson);
				}
				result = resultArray.toJSONString();
			}
			final HttpHeaders httpHeaders = new HttpHeaders();
			httpHeaders.setContentType(MediaType.APPLICATION_JSON);
			if (result != null) {
				return new ResponseEntity<>(((JSONAware) parser.parse(result)).toJSONString(), httpHeaders,
						HttpStatus.OK);
			} else {
				return new ResponseEntity<>(null, httpHeaders, HttpStatus.OK);
			}
		} catch (AttuneException ae) {
			LOGGER.error(ae.getMessage(), ae);
			if (ae.getStatusCode() != null) {
				return new ResponseEntity<>(ae.getMessage(), ae.getStatusCode());
			}
			throw ae;
		} catch (Exception e) {
			LOGGER.error(e.getMessage(), e);
			throw new AttuneException(e);
		}
	}

	@RequestMapping(value = "/node/{id}/relationships/out", method = RequestMethod.DELETE)
	@PreAuthorize("hasRole('" + SecurityService.ROLE_USER + "')")
	@ApiOperation(value = "deleteRelationshipsOut", notes = "Deletes all outbound relationships for the specified Node.", response = String.class, httpMethod = "DELETE")
	@ApiResponses(value = { @ApiResponse(code = 204, message = "Node relationships deleted, empty response"),
			@ApiResponse(code = 400, message = "Invalid request if the Node id doesn't exist"),
			@ApiResponse(code = 500, message = "An Error has Occurred") })
	public ResponseEntity<String> deleteRelationshipsOut(
			@ApiParam(value = "The Node id.", required = true) @PathVariable(value = "id") Long id,
			@ApiParam(value = "Optional query parameter indicating the relationship types to delete. Multiple values can be specified by repeating the parameter in the URL, "
					+ "for example http://localhost:8080/attune/api/data/node/{id}/relationships/out?relType=has&relType=likes&relType=uses"
					+ ", would delete all 'has', 'likes', and 'uses' outbound relationships.", required = false) @RequestParam(value = "relType", required = false) List<String> relTypes) {
		try {
			String relsJson = this.getRelationshipsOut(id, relTypes).getBody();
			JSONArray relationships;
			synchronized (parser) {
				relationships = (JSONArray) parser.parse(relsJson);
			}
			if (relationships != null) {
				// Delete each of the relationships
				for (int y = 0; y < relationships.size(); y++) {
					JSONObject relationshipJsonObj = (JSONObject) relationships.get(y);
					String relationshipNodeId = Neo4JUtils.getId(relationshipJsonObj.toJSONString());
					String url = new StringBuilder(neo4jUrlPrefix).append("db/data/relationship/")
							.append(relationshipNodeId).toString();
					this.deleteNeo4JData(url);
				}
			}
			return new ResponseEntity<>(HttpStatus.OK);
		} catch (AttuneException ae) {
			LOGGER.error(ae.getMessage(), ae);
			if (ae.getStatusCode() != null) {
				return new ResponseEntity<>(ae.getMessage(), ae.getStatusCode());
			}
			throw ae;
		} catch (Exception e) {
			LOGGER.error(e.getMessage(), e);
			throw new AttuneException(e);
		}
	}

	@SuppressWarnings("unchecked")
	@RequestMapping(value = "/node/{id}/relationships/{relationshipType}/{endNodeId}", method = RequestMethod.POST, consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
	@PreAuthorize("hasRole('" + SecurityService.ROLE_USER + "')")
	@ApiOperation(value = "insertRelationship", notes = "Creates a relationship between the specified starting and ending Node ids. "
			+ "The body of the request may optionally contain a single-level JSON object that contains properties for the relationships, "
			+ "such as {\"required\": true} to indicate that this is a required relationships. Other user-defined properties may be specified as well.", response = String.class, httpMethod = "POST")
	@ApiResponses(value = {
			@ApiResponse(code = 200, message = "Returns a JSON object representing the relationship that was just created, which contains the neo4j.id, relationshipType, startNodeId, startNodeType, startNodeName, endNodeId, endNodeType, and endNodeName"),
			@ApiResponse(code = 400, message = "Invalid request if the node ids are invalid"),
			@ApiResponse(code = 500, message = "An Error has Occurred") })
	public ResponseEntity<String> insertRelationship(
			@ApiParam(value = "The starting Node id.", required = true) @PathVariable(value = "id") Long id,
			@ApiParam(value = "The type of relationship that the starting node has with the ending node.", required = true) @PathVariable(value = "relationshipType") String relationshipType,
			@ApiParam(value = "The ending Node id.", required = true) @PathVariable(value = "endNodeId") Long endNodeId,
			@ApiParam(value = "A single-level JSON object that contains the properties for the relationship, values can be String, boolean, or a number.", required = false) @RequestBody(required = false) Map<String, Object> relationshipProperties) {
		String result = null;
		try {
			Map<String, Object> properties = relationshipProperties;
			if (properties == null) {
				properties = new HashMap<>();
			}
			this.validateRelationship(id, relationshipType, endNodeId, properties);
			String existingRelationshipId = this.getExistingRelationshipId(id, relationshipType, endNodeId);
			if (existingRelationshipId != null) {
				Map<String, Object> props = Neo4JUtils
						.getDataProperties(this.getRelationship(Long.parseLong(existingRelationshipId)).getBody());
				if (props != null && !props.isEmpty()) {
					props.putAll(properties);
				} else {
					props = properties;
				}
				this.updateRelationshipProperties(Long.parseLong(existingRelationshipId), props);
				return this.getRelationship(Long.parseLong(existingRelationshipId));
			}

			JSONObject jsonObj = new JSONObject();
			jsonObj.put("to", new StringBuilder(neo4jUrlPrefix).append("db/data/node/").append(endNodeId).toString());
			jsonObj.put("type", relationshipType);
			if (!properties.isEmpty()) {
				properties = defaultNullPropertyValues(properties);
				jsonObj.put("data", properties);
			}
			String json = jsonObj.toJSONString();
			String url = new StringBuilder(neo4jUrlPrefix).append("db/data/node/").append(String.valueOf(id))
					.append("/relationships").toString();
			result = this.insertNeo4JData(json, url);
			if (result != null) {
				JSONObject resultRelationshipJson;
				synchronized (parser) {
					resultRelationshipJson = (JSONObject) parser.parse(result);
				}
				Map<String, Object> data = Neo4JUtils.getDataProperties(resultRelationshipJson.toJSONString());
				JSONObject relationshipJson = new JSONObject(data);
				relationshipJson.put("neo4j.id", Neo4JUtils.getId(resultRelationshipJson.toJSONString()));

				String relationshipStartId = Neo4JUtils.getRelationshipStartNode(resultRelationshipJson.toJSONString());
				String relationshipEndId = Neo4JUtils.getRelationshipEndNode(resultRelationshipJson.toJSONString());
				relationshipJson.put("relationshipType",
						Neo4JUtils.getRelationshipType(resultRelationshipJson.toJSONString()));
				relationshipJson.put("startNodeId", relationshipStartId);
				relationshipJson.put("startNodeType",
						Neo4JUtils.getLabels(this.getNode(Long.parseLong(relationshipStartId)).getBody()).get(0));
				relationshipJson.put("startNodeName",
						Neo4JUtils.getDataValue(this.getNode(Long.parseLong(relationshipStartId)).getBody(), "Name"));
				relationshipJson.put("endNodeId", relationshipEndId);
				relationshipJson.put("endNodeType",
						Neo4JUtils.getLabels(this.getNode(Long.parseLong(relationshipEndId)).getBody()).get(0));
				relationshipJson.put("endNodeName",
						Neo4JUtils.getDataValue(this.getNode(Long.parseLong(relationshipEndId)).getBody(), "Name"));
				result = relationshipJson.toJSONString();
			}
			final HttpHeaders httpHeaders = new HttpHeaders();
			httpHeaders.setContentType(MediaType.APPLICATION_JSON);
			if (result != null) {
				return new ResponseEntity<>(((JSONAware) parser.parse(result)).toJSONString(), httpHeaders,
						HttpStatus.OK);
			} else {
				return new ResponseEntity<>(null, httpHeaders, HttpStatus.OK);
			}
		} catch (AttuneException ae) {
			LOGGER.error(ae.getMessage(), ae);
			if (ae.getStatusCode() != null) {
				return new ResponseEntity<>(ae.getMessage(), ae.getStatusCode());
			}
			throw ae;
		} catch (Exception e) {
			LOGGER.error(e.getMessage(), e);
			throw new AttuneException(e);
		}
	}

	@RequestMapping(value = "/relationship/{id}", method = RequestMethod.DELETE)
	@PreAuthorize("hasRole('" + SecurityService.ROLE_USER + "')")
	@ApiOperation(value = "deleteRelationship", notes = "Deletes the Node relationship for the specificed Neo4J relationship id.", response = String.class, httpMethod = "DELETE")
	@ApiResponses(value = {
			@ApiResponse(code = 204, message = "If the relationship was successfully deleted, null or empty response."),
			@ApiResponse(code = 400, message = "Invalid request if the id is invalid"),
			@ApiResponse(code = 500, message = "An Error has Occurred") })
	public ResponseEntity<String> deleteRelationship(
			@ApiParam(value = "The Relationship id.", required = true) @PathVariable(value = "id") Long id) {
		try {
			String url = new StringBuilder(neo4jUrlPrefix).append("db/data/relationship/").append(String.valueOf(id))
					.toString();
			this.deleteNeo4JData(url);
			return new ResponseEntity<>(HttpStatus.OK);
		} catch (AttuneException ae) {
			LOGGER.error(ae.getMessage(), ae);
			if (ae.getStatusCode() != null) {
				return new ResponseEntity<>(ae.getMessage(), ae.getStatusCode());
			}
			throw ae;
		} catch (Exception e) {
			LOGGER.error(e.getMessage(), e);
			throw new AttuneException(e);
		}
	}

	@RequestMapping(value = "/relationships/{type}", method = RequestMethod.DELETE)
	@PreAuthorize("hasRole('" + SecurityService.ROLE_USER + "')")
	@ApiOperation(value = "deleteRelationshipsForType", notes = "Deletes all relationships for all Nodes of the given Metadata entity type.", response = String.class, httpMethod = "DELETE")
	@ApiResponses(value = {
			@ApiResponse(code = 204, message = "If the relationships were successfully deleted, null or empty response."),
			@ApiResponse(code = 400, message = "Invalid request if the entity type name is invalid"),
			@ApiResponse(code = 500, message = "An Error has Occurred") })
	public ResponseEntity<String> deleteRelationshipsForType(
			@ApiParam(value = "Path parameter indicating the node metadata entity type", required = true) @PathVariable(value = "type") String type) {
		try {
			// First get all nodes of this type
			String nodesJson = this.getNodes(type, null, null).getBody();
			if (nodesJson != null) {
				JSONArray nodes;
				synchronized (parser) {
					nodes = (JSONArray) parser.parse(nodesJson);
				}
				// For each node get all of the relationships
				for (int x = 0; x < nodes.size(); x++) {
					JSONObject nodeJsonObj = (JSONObject) nodes.get(x);
					String nodeId = Neo4JUtils.getId(nodeJsonObj.toJSONString());
					String nodeRelationshipsJson = this.getRelationshipsAll(Long.parseLong(nodeId), null).getBody();
					if (nodeRelationshipsJson != null) {
						JSONArray relationships;
						synchronized (parser) {
							relationships = (JSONArray) parser.parse(nodeRelationshipsJson);
						}
						// Delete each of the relationships
						for (int y = 0; y < relationships.size(); y++) {
							JSONObject relationshipJsonObj = (JSONObject) relationships.get(y);
							String relationshipNodeId = Neo4JUtils.getId(relationshipJsonObj.toJSONString());
							String url = new StringBuilder(neo4jUrlPrefix).append("db/data/relationship/")
									.append(relationshipNodeId).toString();
							this.deleteNeo4JData(url);
						}
					}
				}
			}
			return new ResponseEntity<>(HttpStatus.OK);
		} catch (AttuneException ae) {
			LOGGER.error(ae.getMessage(), ae);
			if (ae.getStatusCode() != null) {
				return new ResponseEntity<>(ae.getMessage(), ae.getStatusCode());
			}
			throw ae;
		} catch (Exception e) {
			LOGGER.error(e.getMessage(), e);
			throw new AttuneException(e);
		}
	}

	@RequestMapping(value = "/relationships", method = RequestMethod.DELETE)
	@PreAuthorize("hasRole('" + SecurityService.ROLE_USER + "')")
	@ApiOperation(value = "deleteAllRelationships", notes = "Deletes all relationships from all Nodes.", response = String.class, httpMethod = "DELETE")
	@ApiResponses(value = {
			@ApiResponse(code = 204, message = "If the relationships were successfully deleted, null or empty response."),
			@ApiResponse(code = 400, message = "Invalid request if there are any required relationships"),
			@ApiResponse(code = 500, message = "An Error has Occurred") })
	public ResponseEntity<String> deleteAllRelationships() {
		try {
			// First Get all Node Types
			String allNodeTypesJson = this.getAllNodeTypes().getBody();
			if (allNodeTypesJson != null) {
				JSONArray nodeTypes;
				synchronized (parser) {
					nodeTypes = (JSONArray) parser.parse(allNodeTypesJson);
				}
				// For each type, get all nodes
				for (int i = 0; i < nodeTypes.size(); i++) {
					String nodeType = (String) nodeTypes.get(i);
					this.deleteRelationshipsForType(nodeType);
				}
			}
			return new ResponseEntity<>(HttpStatus.OK);
		} catch (AttuneException ae) {
			LOGGER.error(ae.getMessage(), ae);
			if (ae.getStatusCode() != null) {
				return new ResponseEntity<>(ae.getMessage(), ae.getStatusCode());
			}
			throw ae;
		} catch (Exception e) {
			LOGGER.error(e.getMessage(), e);
			throw new AttuneException(e);
		}
	}

	@RequestMapping(value = "/relationship/{id}/properties", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
	@PreAuthorize("hasRole('" + SecurityService.ROLE_USER + "')")
	@ApiOperation(value = "getRelationshipProperties", notes = "Returns all properties for the specified relationship as a single-level JSON Object", response = String.class, httpMethod = "GET")
	@ApiResponses(value = {
			@ApiResponse(code = 200, message = "A JSON object containing the properties for a Node relationship"),
			@ApiResponse(code = 400, message = "Invalid request, if the id is invalid"),
			@ApiResponse(code = 500, message = "An Error has Occurred") })
	public ResponseEntity<String> getRelationshipProperties(
			@ApiParam(value = "The Relationship id.", required = true) @PathVariable(value = "id") Long id) {
		String result = null;
		try {
			String url = new StringBuilder(neo4jUrlPrefix).append("db/data/relationship/").append(String.valueOf(id))
					.append("/properties").toString();
			result = this.getNeo4JData(url);
			final HttpHeaders httpHeaders = new HttpHeaders();
			httpHeaders.setContentType(MediaType.APPLICATION_JSON);
			if (result != null) {
				return new ResponseEntity<>(((JSONAware) parser.parse(result)).toJSONString(), httpHeaders,
						HttpStatus.OK);
			} else {
				return new ResponseEntity<>(null, httpHeaders, HttpStatus.OK);
			}
		} catch (AttuneException ae) {
			LOGGER.error(ae.getMessage(), ae);
			if (ae.getStatusCode() != null) {
				return new ResponseEntity<>(ae.getMessage(), ae.getStatusCode());
			}
			throw ae;
		} catch (Exception e) {
			LOGGER.error(e.getMessage(), e);
			throw new AttuneException(e);
		}
	}

	@RequestMapping(value = "/relationship/{id}/property/{propName:.+}", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
	@PreAuthorize("hasRole('" + SecurityService.ROLE_USER + "')")
	@ApiOperation(value = "getRelationshipProperty", notes = "Returns the value for the specified property name and Node relationship id. The body of the response is a single String, boolean, or number value which is still valid JSON.", response = String.class, httpMethod = "GET")
	@ApiResponses(value = {
			@ApiResponse(code = 200, message = "A a single String, boolean, or number for the specified property on the Node relationship"),
			@ApiResponse(code = 400, message = "Invalid request, if the relationship id or property name doesn't exist"),
			@ApiResponse(code = 500, message = "An Error has Occurred") })
	public ResponseEntity<String> getRelationshipProperty(
			@ApiParam(value = "The relationship ID.", required = true) @PathVariable(value = "id") Long id,
			@ApiParam(value = "Path parameter with the property name who's value should be returned.", required = true) @PathVariable(value = "propName") String propName) {
		String result = null;
		try {
			String url = new StringBuilder(neo4jUrlPrefix).append("db/data/relationship/").append(String.valueOf(id))
					.append("/properties/").append(propName).toString();
			result = this.getNeo4JData(url);
			final HttpHeaders httpHeaders = new HttpHeaders();
			httpHeaders.setContentType(MediaType.APPLICATION_JSON);
			if (result != null) {
				return new ResponseEntity<>(JSONValue.toJSONString(parser.parse(result)), httpHeaders, HttpStatus.OK);
			} else {
				return new ResponseEntity<>(null, httpHeaders, HttpStatus.OK);
			}
		} catch (AttuneException ae) {
			LOGGER.error(ae.getMessage(), ae);
			if (ae.getStatusCode() != null) {
				return new ResponseEntity<>(ae.getMessage(), ae.getStatusCode());
			}
			throw ae;
		} catch (Exception e) {
			LOGGER.error(e.getMessage(), e);
			throw new AttuneException(e);
		}
	}

	@RequestMapping(value = "/relationship/{id}/properties", method = RequestMethod.PUT, consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
	@PreAuthorize("hasRole('" + SecurityService.ROLE_USER + "')")
	@ApiOperation(value = "updateRelationshipProperties", notes = "Overwrites all properties on a Node relationship, passing in a single-level JSON Object that contains all of the properties..", response = String.class, httpMethod = "PUT")
	@ApiResponses(value = {
			@ApiResponse(code = 204, message = "Relationship properties were successfully updated, empty response"),
			@ApiResponse(code = 400, message = "Invalid request, if the id doesn't exist."),
			@ApiResponse(code = 500, message = "An Error has Occurred") })
	public ResponseEntity<String> updateRelationshipProperties(
			@ApiParam(value = "The relationship ID.", required = true) @PathVariable(value = "id") Long id,
			@ApiParam(value = "A single-level JSON object that contains the properties for the relationship, values can be String, boolean, or a number.", required = true) @RequestBody(required = true) Map<String, Object> relationshipProperties) {
		try {
			String url = new StringBuilder(neo4jUrlPrefix).append("db/data/relationship/").append(String.valueOf(id))
					.append("/properties").toString();
			String json;
			Map<String, Object> properties = relationshipProperties;
			if (properties != null && !properties.isEmpty()) {
				properties = defaultNullPropertyValues(properties);
				JSONObject jsonObj = new JSONObject(properties);
				json = jsonObj.toJSONString();
				this.updateNeo4JData(json, url);
			}
			return new ResponseEntity<>(HttpStatus.OK);
		} catch (AttuneException ae) {
			LOGGER.error(ae.getMessage(), ae);
			if (ae.getStatusCode() != null) {
				return new ResponseEntity<>(ae.getMessage(), ae.getStatusCode());
			}
			throw ae;
		} catch (Exception e) {
			LOGGER.error(e.getMessage(), e);
			throw new AttuneException(e);
		}
	}

	@RequestMapping(value = "/relationship/{id}/properties", method = RequestMethod.DELETE)
	@PreAuthorize("hasRole('" + SecurityService.ROLE_USER + "')")
	@ApiOperation(value = "deleteRelationshipProperties", notes = "Deletes all properties on a relationship.", response = String.class, httpMethod = "DELETE")
	@ApiResponses(value = {
			@ApiResponse(code = 204, message = "Relationship properties were successfully deleted, null or empty response"),
			@ApiResponse(code = 400, message = "Invalid request if the id doesn't exist."),
			@ApiResponse(code = 500, message = "An Error has Occurred") })
	public ResponseEntity<String> deleteRelationshipProperties(
			@ApiParam(value = "The relationship ID.", required = true) @PathVariable(value = "id") Long id) {
		try {
			String url = new StringBuilder(neo4jUrlPrefix).append("db/data/relationship/").append(String.valueOf(id))
					.append("/properties").toString();
			this.deleteNeo4JData(url);
			return new ResponseEntity<>(HttpStatus.OK);
		} catch (AttuneException ae) {
			LOGGER.error(ae.getMessage(), ae);
			if (ae.getStatusCode() != null) {
				return new ResponseEntity<>(ae.getMessage(), ae.getStatusCode());
			}
			throw ae;
		} catch (Exception e) {
			LOGGER.error(e.getMessage(), e);
			throw new AttuneException(e);
		}
	}

	@RequestMapping(value = "/relationship/{id}/property/{propName:.+}", method = RequestMethod.PUT, consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
	@PreAuthorize("hasRole('" + SecurityService.ROLE_USER + "')")
	@ApiOperation(value = "updateRelationshipProperty", notes = "Adds or updates the specified property on the relationship. The body of the request should be a single String, boolean, or number value which is still valid JSON.", response = String.class, httpMethod = "PUT")
	@ApiResponses(value = {
			@ApiResponse(code = 204, message = "Relationship property was successfully added or updated, empty response"),
			@ApiResponse(code = 400, message = "Invalid request if the id doesn't exist; or the property value is null or not a String, boolean, or number."),
			@ApiResponse(code = 500, message = "An Error has Occurred") })
	public ResponseEntity<String> updateRelationshipProperty(
			@ApiParam(value = "The relationship ID.", required = true) @PathVariable(value = "id") Long id,
			@ApiParam(value = "Path parameter with the property name who's value should be returned.", required = true) @PathVariable(value = "propName") String propName,
			@ApiParam(value = "The property's value which should be a single String, boolean, or number value", required = true) @RequestBody(required = true) Object propValue) {
		try {
			if (propValue == null) {
				StringBuilder msg = new StringBuilder("Property values can not be null!");
				throw new AttuneException(msg.toString(), HttpStatus.BAD_REQUEST);
			}
			String url = new StringBuilder(neo4jUrlPrefix).append("db/data/relationship/").append(String.valueOf(id))
					.append("/properties/").append(propName).toString();
			this.updateNeo4JData(JSONValue.toJSONString(propValue), url);
			return new ResponseEntity<>(HttpStatus.OK);
		} catch (AttuneException ae) {
			LOGGER.error(ae.getMessage(), ae);
			if (ae.getStatusCode() != null) {
				return new ResponseEntity<>(ae.getMessage(), ae.getStatusCode());
			}
			throw ae;
		} catch (Exception e) {
			LOGGER.error(e.getMessage(), e);
			throw new AttuneException(e);
		}
	}

	@RequestMapping(value = "/relationship/{id}/property/{propName:.+}", method = RequestMethod.DELETE)
	@PreAuthorize("hasRole('" + SecurityService.ROLE_USER + "')")
	@ApiOperation(value = "deleteRelationshipProperty", notes = "Delete the specified property from the relationship.", response = String.class, httpMethod = "DELETE")
	@ApiResponses(value = {
			@ApiResponse(code = 204, message = "Relationship property was successfully deleted, empty response"),
			@ApiResponse(code = 400, message = "Invalid request if the id or property name doesn't exist"),
			@ApiResponse(code = 500, message = "An Error has Occurred") })
	public ResponseEntity<String> deleteRelationshipProperty(
			@ApiParam(value = "The relationship ID.", required = true) @PathVariable(value = "id") Long id,
			@ApiParam(value = "Path parameter for which property should be deleted.", required = true) @PathVariable(value = "propName") String propName) {
		try {
			String url = new StringBuilder(neo4jUrlPrefix).append("db/data/relationship/").append(String.valueOf(id))
					.append("/properties/").append(propName).toString();
			this.deleteNeo4JData(url);
			return new ResponseEntity<>(HttpStatus.OK);
		} catch (AttuneException ae) {
			LOGGER.error(ae.getMessage(), ae);
			if (ae.getStatusCode() != null) {
				return new ResponseEntity<>(ae.getMessage(), ae.getStatusCode());
			}
			throw ae;
		} catch (Exception e) {
			LOGGER.error(e.getMessage(), e);
			throw new AttuneException(e);
		}
	}

	@RequestMapping(value = "/node/{id}/relationshipCount/all", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
	@PreAuthorize("hasRole('" + SecurityService.ROLE_USER + "')")
	@ApiOperation(value = "getRelationshipCountAll", notes = "Returns the total number of all relationships for the specified Node. The body of the response is a single number value which is still valid JSON.", response = String.class, httpMethod = "GET")
	@ApiResponses(value = {
			@ApiResponse(code = 200, message = "A a single number for the total number of relationships for the specified Node id."),
			@ApiResponse(code = 400, message = "Invalid request, if the node id doesn't exist"),
			@ApiResponse(code = 500, message = "An Error has Occurred") })
	public ResponseEntity<String> getRelationshipCountAll(
			@ApiParam(value = "Path parameter indicating the Node id.", required = true) @PathVariable(value = "id") Long id,
			@ApiParam(value = "Optional query parameter indicating the relationship types to filter by. Multiple values can be specified by repeating the parameter in the URL, "
					+ "for example http://localhost:8080/attune/api/data/node/{id}/relationshipCount/all?relType=has&relType=likes&relType=uses"
					+ ", would count all 'has', 'likes', and 'uses' relationships.", required = false) @RequestParam(value = "relType", required = false) List<String> relTypes) {
		String result = null;
		try {
			StringBuilder sb = new StringBuilder(neo4jUrlPrefix).append("db/data/node/").append(String.valueOf(id))
					.append("/degree/all");
			String url = this.getUrlForRelTypes(sb, relTypes);
			result = this.getNeo4JData(url);
			final HttpHeaders httpHeaders = new HttpHeaders();
			httpHeaders.setContentType(MediaType.APPLICATION_JSON);
			if (result != null) {
				return new ResponseEntity<>(JSONValue.toJSONString(parser.parse(result)), httpHeaders, HttpStatus.OK);
			} else {
				return new ResponseEntity<>(null, httpHeaders, HttpStatus.OK);
			}
		} catch (AttuneException ae) {
			LOGGER.error(ae.getMessage(), ae);
			if (ae.getStatusCode() != null) {
				return new ResponseEntity<>(ae.getMessage(), ae.getStatusCode());
			}
			throw ae;
		} catch (Exception e) {
			LOGGER.error(e.getMessage(), e);
			throw new AttuneException(e);
		}
	}

	@RequestMapping(value = "/node/{id}/relationshipCount/in", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
	@PreAuthorize("hasRole('" + SecurityService.ROLE_USER + "')")
	@ApiOperation(value = "getRelationshipCountIn", notes = "Returns the total number of all inbound relationships for the specified Node. The body of the response is a single number value which is still valid JSON.", response = String.class, httpMethod = "GET")
	@ApiResponses(value = {
			@ApiResponse(code = 200, message = "A single number for the total number of inbound relationships for the specified Node id."),
			@ApiResponse(code = 400, message = "Invalid request, if the node id doesn't exist"),
			@ApiResponse(code = 500, message = "An Error has Occurred") })
	public ResponseEntity<String> getRelationshipCountIn(
			@ApiParam(value = "Path parameter indicating the Node id.", required = true) @PathVariable(value = "id") Long id,
			@ApiParam(value = "Optional query parameter indicating the relationship types to filter by. Multiple values can be specified by repeating the parameter in the URL, "
					+ "for example http://localhost:8080/attune/api/data/node/{id}/relationshipCount/in?relType=has&relType=likes&relType=uses"
					+ ", would count all 'has', 'likes', and 'uses' inbound relationships.", required = false) @RequestParam(value = "relType", required = false) List<String> relTypes) {
		String result = null;
		try {
			StringBuilder sb = new StringBuilder(neo4jUrlPrefix).append("db/data/node/").append(String.valueOf(id))
					.append("/degree/in");
			String url = this.getUrlForRelTypes(sb, relTypes);
			result = this.getNeo4JData(url);
			final HttpHeaders httpHeaders = new HttpHeaders();
			httpHeaders.setContentType(MediaType.APPLICATION_JSON);
			if (result != null) {
				return new ResponseEntity<>(JSONValue.toJSONString(parser.parse(result)), httpHeaders, HttpStatus.OK);
			} else {
				return new ResponseEntity<>(null, httpHeaders, HttpStatus.OK);
			}
		} catch (AttuneException ae) {
			LOGGER.error(ae.getMessage(), ae);
			if (ae.getStatusCode() != null) {
				return new ResponseEntity<>(ae.getMessage(), ae.getStatusCode());
			}
			throw ae;
		} catch (Exception e) {
			LOGGER.error(e.getMessage(), e);
			throw new AttuneException(e);
		}
	}

	@RequestMapping(value = "/node/{id}/relationshipCount/out", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
	@PreAuthorize("hasRole('" + SecurityService.ROLE_USER + "')")
	@ApiOperation(value = "getRelationshipCountOut", notes = "Returns the total number of all outbound relationships for the specified Node. The body of the response is a single number value which is still valid JSON.", response = String.class, httpMethod = "GET")
	@ApiResponses(value = {
			@ApiResponse(code = 200, message = "A single number for the total number of outbound relationships for the specified Node id."),
			@ApiResponse(code = 400, message = "Invalid request, if the node id doesn't exist"),
			@ApiResponse(code = 500, message = "An Error has Occurred") })
	public ResponseEntity<String> getRelationshipCountOut(
			@ApiParam(value = "Path parameter indicating the Node id.", required = true) @PathVariable(value = "id") Long id,
			@ApiParam(value = "Optional query parameter indicating the relationship types to filter by. Multiple values can be specified by repeating the parameter in the URL, "
					+ "for example http://localhost:8080/attune/api/data/node/{id}/relationshipCount/out?relType=has&relType=likes&relType=uses"
					+ ", would count all 'has', 'likes', and 'uses' outbound relationships.", required = false) @RequestParam(value = "relType", required = false) List<String> relTypes) {
		String result = null;
		try {
			StringBuilder sb = new StringBuilder(neo4jUrlPrefix).append("db/data/node/").append(String.valueOf(id))
					.append("/degree/out");
			String url = this.getUrlForRelTypes(sb, relTypes);
			result = this.getNeo4JData(url);
			final HttpHeaders httpHeaders = new HttpHeaders();
			httpHeaders.setContentType(MediaType.APPLICATION_JSON);
			if (result != null) {
				return new ResponseEntity<>(JSONValue.toJSONString(parser.parse(result)), httpHeaders, HttpStatus.OK);
			} else {
				return new ResponseEntity<>(null, httpHeaders, HttpStatus.OK);
			}
		} catch (AttuneException ae) {
			LOGGER.error(ae.getMessage(), ae);
			if (ae.getStatusCode() != null) {
				return new ResponseEntity<>(ae.getMessage(), ae.getStatusCode());
			}
			throw ae;
		} catch (Exception e) {
			LOGGER.error(e.getMessage(), e);
			throw new AttuneException(e);
		}
	}

	@SuppressWarnings("unchecked")
	@RequestMapping(value = "/validate", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
	@PreAuthorize("hasRole('" + SecurityService.ROLE_USER + "')")
	@ApiOperation(value = "getValidationReport", notes = "Performs validation on all of the current Nodes and their relationships, then returns a JSON object that represents the results.", response = String.class, httpMethod = "GET")
	@ApiResponses(value = {
			@ApiResponse(code = 200, message = "A JSON object that contains the results of the validation."),
			@ApiResponse(code = 500, message = "An Error has Occurred") })
	public ResponseEntity<String> getValidationReport() {
		try {
			int totalNodes = 0;
			int invalidNodes = 0;
			int totalOutRelationships = 0;
			int totalInvalidOutRelationships = 0;
			int totalInRelationships = 0;
			int totalInvalidInRelationships = 0;

			JSONArray invalidNodeDetails = new JSONArray();

			String allNodeTypesJson = this.getAllNodeTypes().getBody();
			if (allNodeTypesJson != null) {
				JSONArray nodeTypes;
				synchronized (parser) {
					nodeTypes = (JSONArray) parser.parse(allNodeTypesJson);
				}
				for (int i = 0; i < nodeTypes.size(); i++) {
					String nodeType = (String) nodeTypes.get(i);
					if ("Metadata".equals(nodeType)) {
						continue;
					}
					String nodesJson = this.getNodes(nodeType, null, null).getBody();
					if (nodesJson != null) {
						JSONArray nodes;
						synchronized (parser) {
							nodes = (JSONArray) parser.parse(nodesJson);
						}
						for (int x = 0; x < nodes.size(); x++) {
							totalNodes++;
							JSONObject nodeJsonObj = (JSONObject) nodes.get(x);
							String nodeId = Neo4JUtils.getId(nodeJsonObj.toJSONString());
							// try to get a name for the node
							String nodeName = Neo4JUtils.getDataValue(nodeJsonObj.toJSONString(), "Name");
							if (nodeName == null) {
								nodeName = Neo4JUtils.getDataValue(nodeJsonObj.toJSONString(), "name");
							}

							totalOutRelationships = totalOutRelationships + Integer
									.parseInt(this.getRelationshipCountOut(Long.parseLong(nodeId), null).getBody());
							totalInRelationships = totalInRelationships + Integer
									.parseInt(this.getRelationshipCountIn(Long.parseLong(nodeId), null).getBody());

							JSONObject invalidNodeDetail = new JSONObject();
							Map<String, Object> properties = Neo4JUtils.getDataProperties(nodeJsonObj.toJSONString());
							try {
								this.validateNode(nodeType, properties);
							} catch (Exception ex) {
								LOGGER.info(ex.getMessage(), ex);
								invalidNodeDetail.put("id", nodeId);
								invalidNodeDetail.put("type", nodeType);
								invalidNodeDetail.put("name", nodeName);
								if (ex.getCause() != null) {
									invalidNodeDetail.put("error", ex.getCause().getMessage());
								} else {
									invalidNodeDetail.put("error", ex.getMessage());
								}
							}

							JSONArray invalidNodeRelationshipsDetails = new JSONArray();
							try {
								invalidNodeRelationshipsDetails = this
										.getValidateRelationshipsReport(Long.parseLong(nodeId), nodeType);
							} catch (Exception ex) {
								LOGGER.info(ex.getMessage(), ex);
								invalidNodeDetail.put("id", nodeId);
								invalidNodeDetail.put("type", nodeType);
								invalidNodeDetail.put("name", nodeName);
								invalidNodeDetail.put("error", ex.getMessage());
							}
							if (!invalidNodeDetail.isEmpty() || !invalidNodeRelationshipsDetails.isEmpty()) {
								invalidNodes++;
								if (!invalidNodeDetail.isEmpty()) {
									invalidNodeDetails.add(invalidNodeDetail);
								}
								if (!invalidNodeRelationshipsDetails.isEmpty()) {
									for (int z = 0; z < invalidNodeRelationshipsDetails.size(); z++) {
										JSONObject invalidNodeRelationshipDetail = (JSONObject) invalidNodeRelationshipsDetails
												.get(z);
										if ("out".equals(invalidNodeRelationshipDetail.get("relationshipDirection"))) {
											totalInvalidOutRelationships++;
										}
										if ("in".equals(invalidNodeRelationshipDetail.get("relationshipDirection"))) {
											totalInvalidInRelationships++;
										}
										invalidNodeRelationshipDetail.put("id", nodeId);
										invalidNodeRelationshipDetail.put("type", nodeType);
										invalidNodeRelationshipDetail.put("name", nodeName);
										invalidNodeDetails.add(invalidNodeRelationshipDetail);
									}
								}
							}
						}
					}
				}
			}

			int totalRelationships = totalOutRelationships + totalInRelationships;
			int totalInvalidRelationships = totalInvalidOutRelationships + totalInvalidInRelationships;

			JSONObject report = new JSONObject();
			report.put("totalNodes", totalNodes);
			report.put("totalValidNodes", totalNodes - invalidNodes);
			report.put("totalInvalidNodes", invalidNodes);
			report.put("totalRelationships", totalRelationships);
			report.put("totalValidRelationships", totalRelationships - totalInvalidRelationships);
			report.put("totalInvalidRelationships", totalInvalidRelationships);
			report.put("totalOutRelationships", totalOutRelationships);
			report.put("totalValidOutRelationships", totalOutRelationships - totalInvalidOutRelationships);
			report.put("totalInvalidOutRelationships", totalInvalidOutRelationships);
			report.put("totalInRelationships", totalInRelationships);
			report.put("totalValidInRelationships", totalInRelationships - totalInvalidInRelationships);
			report.put("totalInvalidInRelationships", totalInvalidInRelationships);
			report.put("totalErrors", invalidNodeDetails.size());
			report.put("errorDetails", invalidNodeDetails);

			final HttpHeaders httpHeaders = new HttpHeaders();
			httpHeaders.setContentType(MediaType.APPLICATION_JSON);
			return new ResponseEntity<>(report.toJSONString(), httpHeaders, HttpStatus.OK);
		} catch (AttuneException ae) {
			LOGGER.error(ae.getMessage(), ae);
			if (ae.getStatusCode() != null) {
				return new ResponseEntity<>(ae.getMessage(), ae.getStatusCode());
			}
			throw ae;
		} catch (Exception e) {
			LOGGER.error(e.getMessage(), e);
			throw new AttuneException(e);
		}
	}

	@SuppressWarnings("unchecked")
	private String getNeo4JData(String url) {
		try {
			ResponseEntity<String> responseEntity = this.callRestService(url, HttpMethod.GET, null, String.class);
			int statusCode = responseEntity.getStatusCode().value();
			String json = (responseEntity.hasBody() ? responseEntity.getBody() : new JSONObject().toJSONString());
			if (!((statusCode >= 200) && (statusCode < 300))) {
				String msg = json;
				throw new AttuneException(msg, HttpStatus.INTERNAL_SERVER_ERROR);
			}
			return json;
		} catch (AttuneException ae) {
			throw ae;
		} catch (Exception e) {
			throw new AttuneException(e);
		}
	}

	@SuppressWarnings("unchecked")
	private String insertNeo4JData(String jsonInput, String url) {
		try {
			ResponseEntity<String> responseEntity = this.callRestService(url, HttpMethod.POST, jsonInput, String.class);
			int statusCode = responseEntity.getStatusCode().value();
			String json = (responseEntity.hasBody() ? responseEntity.getBody() : new JSONObject().toJSONString());
			if (!((statusCode >= 200) && (statusCode < 300))) {
				String msg = json;
				throw new AttuneException(msg, HttpStatus.INTERNAL_SERVER_ERROR);
			}
			return json;
		} catch (AttuneException ae) {
			throw ae;
		} catch (Exception e) {
			throw new AttuneException(e);
		}
	}

	@SuppressWarnings("unchecked")
	private void updateNeo4JData(String jsonInput, String url) {
		try {
			ResponseEntity<String> responseEntity = this.callRestService(url, HttpMethod.PUT, jsonInput, String.class);
			int statusCode = responseEntity.getStatusCode().value();
			String json = (responseEntity.hasBody() ? responseEntity.getBody() : new JSONObject().toJSONString());
			if (!((statusCode >= 200) && (statusCode < 300))) {
				String msg = json;
				throw new AttuneException(msg, HttpStatus.INTERNAL_SERVER_ERROR);
			}
		} catch (AttuneException ae) {
			throw ae;
		} catch (Exception e) {
			throw new AttuneException(e);
		}
	}

	@SuppressWarnings("unchecked")
	private void deleteNeo4JData(String url) {
		try {
			ResponseEntity<String> responseEntity = this.callRestService(url, HttpMethod.DELETE, null, String.class);
			int statusCode = responseEntity.getStatusCode().value();
			String json = (responseEntity.hasBody() ? responseEntity.getBody() : new JSONObject().toJSONString());
			if (!((statusCode >= 200) && (statusCode < 300))) {
				String msg = json;
				throw new AttuneException(msg, HttpStatus.INTERNAL_SERVER_ERROR);
			}
		} catch (AttuneException ae) {
			throw ae;
		} catch (Exception e) {
			throw new AttuneException(e);
		}
	}

	@Override
	protected HttpHeaders createHeaders(final String url, final HttpMethod httpMethod) {
		HttpHeaders headers = new HttpHeaders();
		String neo4jCredentials = servletContext.getInitParameter("neo4j.credentials");
		byte[] encodedAuth = Base64.encodeBase64(neo4jCredentials.getBytes(Charset.forName("US-ASCII")));
		String authHeader = "Basic " + new String(encodedAuth);
		headers.set("Authorization", authHeader);
		headers.setContentType(MediaType.APPLICATION_JSON);
		return headers;
	}

	private String validateNode(String type, Map<String, Object> properties) {
		try {
			if ("Metadata".equals(type)) {
				StringBuilder msg = new StringBuilder("Can not modify Metadata through this API. Please use ");
				msg.append(this.httpServletRequest.getContextPath()).append(this.httpServletRequest.getServletPath());
				msg.append("/metadata/entity/{entityType}").append(" instead.");
				throw new AttuneException(msg.toString(), HttpStatus.BAD_REQUEST);
			} else {
				String metadata = this.getMetadata(type);
				for (String requiredField : this.getRequiredFields(type)) {
					if (!properties.containsKey(requiredField) || properties.get(requiredField) == null) {
						StringBuilder msg = new StringBuilder("Field ").append(requiredField).append(" is required!");
						throw new AttuneException(msg.toString(), HttpStatus.BAD_REQUEST);
					}
				}
				properties.remove("neo4j.id");
				properties.remove("neo4j.label");
				return metadata;
			}
		} catch (AttuneException ae) {
			throw ae;
		}
	}

	private Set<String> getRequiredFields(String type) {
		try {
			Set<String> requiredFields = new HashSet<>();
			String metadata = this.getMetadata(type);
			String requiredFieldsList = Neo4JUtils.getDataValue(metadata, "requiredFields");
			if (requiredFieldsList != null) {
				StringTokenizer st = new StringTokenizer(requiredFieldsList, ",", false);
				while (st.hasMoreTokens()) {
					String requiredField = st.nextToken();
					requiredFields.add(requiredField);
				}
			}
			return requiredFields;
		} catch (AttuneException ae) {
			throw ae;
		} catch (Exception e) {
			throw new AttuneException(e);
		}
	}

	private String getExistingNodeId(String metadataJson, String type, Map<String, Object> properties) {
		try {
			String nodeId = null;
			boolean exists;
			// Uncomment this if we want to find an existing node based on
			// requiredFields, rather than just Name
			//
			// Map<String, Object> requiredFieldsMap = new HashMap<String,
			// Object>();
			// String requiredFieldsList = Neo4JUtils.getDataValue(metadataJson,
			// "requiredFields");
			// StringTokenizer st = new StringTokenizer(requiredFieldsList, ",",
			// false);
			// while(st.hasMoreTokens()){
			// String requiredField = st.nextToken();
			// if(properties.containsKey(requiredField)){
			// Object requiredValue = properties.get(requiredField);
			// requiredFieldsMap.put(requiredField, requiredValue);
			// }
			// }

			String json = this.getNodes(type, null, null).getBody();
			if (json != null) {
				JSONArray jsonArray;
				synchronized (parser) {
					jsonArray = (JSONArray) parser.parse(json);
				}
				for (int i = 0; i < jsonArray.size(); i++) {
					JSONObject jsonObject = (JSONObject) jsonArray.get(i);
					Map<String, Object> nodeProperties = Neo4JUtils.getDataProperties(jsonObject.toJSONString());
					String id = Neo4JUtils.getId(jsonObject.toJSONString());
					// Uncomment this if we want to find an existing node based
					// on requiredFields, rather than just Name
					//
					// Map<String, Object> compareMap = new HashMap<String,
					// Object>();
					// for(String key:requiredFieldsMap.keySet()){
					// if(nodeProperties.containsKey(key) &&
					// requiredFieldsMap.get(key).equals(nodeProperties.get(key))){
					// compareMap.put(key, nodeProperties.get(key));
					// }
					// }
					// exists = requiredFieldsMap.equals(compareMap);
					exists = id != null ? id.equals(properties.remove("neo4j.id")) : false;
					if (!exists) {
						exists = nodeProperties.get("Name") != null
								? nodeProperties.get("Name").equals(properties.get("Name"))
								: false;
					}
					if (exists) {
						nodeId = id;
						break;
					}
				}
			}
			return nodeId;
		} catch (AttuneException ae) {
			throw ae;
		} catch (Exception e) {
			throw new AttuneException(e);
		}
	}

	private String getMetadata(String entityType) {
		String json = this.getNodes("Metadata", "Name", entityType).getBody();
		if (Neo4JUtils.isEmpty(json)) {
			StringBuilder msg = new StringBuilder("Metadata for entity of type ").append(entityType)
					.append(" does not exist! Please use ");
			msg.append(this.httpServletRequest.getContextPath()).append(this.httpServletRequest.getServletPath());
			msg.append("/metadata/entity/").append(entityType)
					.append(" to create metadata definition for this entity before adding instances of this type.");
			throw new AttuneException(msg.toString(), HttpStatus.BAD_REQUEST);
		}
		return json;
	}

	private void validateRelationship(Long startNodeId, String relationshipType, Long endNodeId,
			Map<String, Object> properties) {
		try {
			if (properties != null) {
				properties.remove("neo4j.id");
				properties.remove("relationshipType");
				properties.remove("startNodeId");
				properties.remove("startNodeType");
				properties.remove("startNodeName");
				properties.remove("endNodeId");
				properties.remove("endNodeType");
				properties.remove("endNodeName");
			}

			String startNodeType = Neo4JUtils.getLabels(this.getNode(startNodeId).getBody()).get(0);
			String endNodeType = Neo4JUtils.getLabels(this.getNode(endNodeId).getBody()).get(0);

			// check if Metadata exists for this start node
			String startNodeMetadataJson = this.getMetadata(startNodeType);
			String startMetadataId = Neo4JUtils.getId(startNodeMetadataJson);

			// check if Metadata exists for this end node
			String endNodeMetadataJson = this.getMetadata(endNodeType);
			String endMetadataId = Neo4JUtils.getId(endNodeMetadataJson);

			// Check metadata for outbound relationships of this type on start
			// node
			List<String> relTypes = new ArrayList<>();
			relTypes.add(relationshipType);
			if (Neo4JUtils.isEmpty(this.getRelationshipsOut(Long.parseLong(startMetadataId), relTypes).getBody())) {
				StringBuilder msg = new StringBuilder("Metadata for entity of type ").append(startNodeType)
						.append(" does not define relationship of type '").append(relationshipType);
				msg.append("' to entity of type ").append(endNodeType);
				throw new AttuneException(msg.toString(), HttpStatus.BAD_REQUEST);
			}

			// Check metadata for inbound relationships of this type on end node
			relTypes = new ArrayList<>();
			relTypes.add(relationshipType);
			if (Neo4JUtils.isEmpty(this.getRelationshipsIn(Long.parseLong(endMetadataId), relTypes).getBody())) {
				StringBuilder msg = new StringBuilder("Metadata for entity of type ").append(endNodeType)
						.append(" does not define relationship of type '").append(relationshipType);
				msg.append("' from entity of type ").append(startNodeType);
				throw new AttuneException(msg.toString(), HttpStatus.BAD_REQUEST);
			}
		} catch (AttuneException ae) {
			throw ae;
		} catch (Exception e) {
			throw new AttuneException(e);
		}
	}

	@SuppressWarnings("unchecked")
	private JSONArray getValidateRelationshipsReport(Long nodeId, String nodeType) {
		try {
			JSONArray invalidNodeRelationships = new JSONArray();

			// check if Metadata exists for this start node
			String nodeMetadataJson = this.getMetadata(nodeType);
			String metadataId = Neo4JUtils.getId(nodeMetadataJson);

			String outMetadataRelationshipsJson = this.getRelationshipsOut(Long.parseLong(metadataId), null).getBody();
			if (outMetadataRelationshipsJson != null) {
				JSONArray outMetadataRelationships;
				synchronized (parser) {
					outMetadataRelationships = (JSONArray) parser.parse(outMetadataRelationshipsJson);
				}
				for (int i = 0; i < outMetadataRelationships.size(); i++) {
					JSONObject metadataRelationshipJson = (JSONObject) outMetadataRelationships.get(i);
					String metadataRelationshipId = Neo4JUtils.getId(metadataRelationshipJson.toJSONString());
					String metadataRelationshipType = Neo4JUtils
							.getRelationshipType(metadataRelationshipJson.toJSONString());
					String metadataRelationshipEndId = Neo4JUtils
							.getRelationshipEndNode(metadataRelationshipJson.toJSONString());
					String metadataRelationshipStartNodeType = Neo4JUtils.getDataValue(nodeMetadataJson, "Name");
					String metadataRelationshipEndNodeType = Neo4JUtils
							.getDataValue(this.getNode(Long.parseLong(metadataRelationshipEndId)).getBody(), "Name");
					boolean isRequired = Boolean.parseBoolean(
							this.getRelationshipProperty(Long.parseLong(metadataRelationshipId), "required").getBody());

					List<String> relTypes = new ArrayList<>();
					relTypes.add(metadataRelationshipType);

					String relsJson = this.getRelationshipsOut(nodeId, relTypes).getBody();
					if (relsJson != null) {
						JSONArray relationships;
						synchronized (parser) {
							relationships = (JSONArray) parser.parse(relsJson);
						}
						if (relationships.isEmpty() && isRequired) {
							StringBuilder msg = new StringBuilder("Node of type ").append(nodeType)
									.append(" does not have an out relationship of type '")
									.append(metadataRelationshipType);
							msg.append("' to entity of type ").append(metadataRelationshipEndNodeType)
									.append(" which is required!");

							String startNodeJson = this.getNode(nodeId).getBody();
							String startNodeName = Neo4JUtils.getDataValue(startNodeJson, "Name");
							// try to get a name for the nodes
							if (startNodeName == null) {
								startNodeName = Neo4JUtils.getDataValue(startNodeJson, "Name");
							}

							JSONObject detail = new JSONObject();
							detail.put("relationshipId", null);
							detail.put("relationshipType", metadataRelationshipType);
							detail.put("relationshipDirection", "out");
							detail.put("startNodeId", String.valueOf(nodeId));
							detail.put("startNodeType", metadataRelationshipStartNodeType);
							detail.put("startNodeName", startNodeName);
							detail.put("endNodeId", null);
							detail.put("endNodeType", metadataRelationshipEndNodeType);
							detail.put("endNodeName", null);
							detail.put("error", msg.toString());
							invalidNodeRelationships.add(detail);
						}
						for (int y = 0; y < relationships.size(); y++) {
							JSONObject relationshipJsonObj = (JSONObject) relationships.get(y);
							String relationshipId = Neo4JUtils.getId(relationshipJsonObj.toJSONString());
							String endId = Neo4JUtils.getRelationshipEndNode(relationshipJsonObj.toJSONString());
							String endType = (String) Neo4JUtils
									.getValue(this.getNodeTypes(Long.parseLong(endId)).getBody());

							String startNodeJson = this.getNode(nodeId).getBody();
							String endNodeJson = this.getNode(Long.parseLong(endId)).getBody();
							String startNodeName = Neo4JUtils.getDataValue(startNodeJson, "Name");
							// try to get a name for the nodes
							if (startNodeName == null) {
								startNodeName = Neo4JUtils.getDataValue(startNodeJson, "name");
							}
							String endNodeName = Neo4JUtils.getDataValue(endNodeJson, "Name");
							if (endNodeName == null) {
								endNodeName = Neo4JUtils.getDataValue(endNodeJson, "name");
							}

							Map<String, Object> relationshipProperties = Neo4JUtils.getProperties(
									this.getRelationshipProperties(Long.parseLong(relationshipId)).getBody());
							try {
								this.validateRelationship(nodeId, metadataRelationshipType, Long.parseLong(endId),
										relationshipProperties);
							} catch (AttuneException ae) {
								LOGGER.info(ae.getMessage(), ae);
								JSONObject detail = new JSONObject();
								detail.put("relationshipId", relationshipId);
								detail.put("relationshipType", metadataRelationshipType);
								detail.put("relationshipDirection", "out");
								detail.put("startNodeId", String.valueOf(nodeId));
								detail.put("startNodeType", metadataRelationshipStartNodeType);
								detail.put("startNodeName", startNodeName);
								detail.put("endNodeId", endId);
								detail.put("endNodeType", endType);
								detail.put("endNodeName", endNodeName);
								detail.put("error", ae.getMessage());
								invalidNodeRelationships.add(detail);
							}
							if (isRequired && !endType.equals(metadataRelationshipEndNodeType)) {
								StringBuilder msg = new StringBuilder("Node of type ").append(nodeType)
										.append(" does not have an out relationship of type '")
										.append(metadataRelationshipType);
								msg.append("' to entity of type ").append(metadataRelationshipEndNodeType)
										.append(" which is required!");

								JSONObject detail = new JSONObject();
								detail.put("relationshipId", relationshipId);
								detail.put("relationshipType", metadataRelationshipType);
								detail.put("relationshipDirection", "out");
								detail.put("startNodeId", String.valueOf(nodeId));
								detail.put("startNodeType", metadataRelationshipStartNodeType);
								detail.put("startNodeName", startNodeName);
								detail.put("endNodeId", endId);
								detail.put("endNodeType", endType);
								detail.put("endNodeName", endNodeName);
								detail.put("error", msg.toString());
								invalidNodeRelationships.add(detail);
							}
						}
					}
				}
			}

			String inMetadataRelationshipsJson = this.getRelationshipsIn(Long.parseLong(metadataId), null).getBody();
			if (inMetadataRelationshipsJson != null) {
				JSONArray inMetadataRelationships;
				synchronized (parser) {
					inMetadataRelationships = (JSONArray) parser.parse(inMetadataRelationshipsJson);
				}
				for (int i = 0; i < inMetadataRelationships.size(); i++) {
					JSONObject metadataRelationshipJson = (JSONObject) inMetadataRelationships.get(i);
					String metadataRelationshipId = Neo4JUtils.getId(metadataRelationshipJson.toJSONString());
					String metadataRelationshipType = Neo4JUtils
							.getRelationshipType(metadataRelationshipJson.toJSONString());
					String metadataRelationshipStartId = Neo4JUtils
							.getRelationshipStartNode(metadataRelationshipJson.toJSONString());
					String metadataRelationshipStartNodeType = Neo4JUtils
							.getDataValue(this.getNode(Long.parseLong(metadataRelationshipStartId)).getBody(), "Name");
					String metadataRelationshipEndNodeType = (String) Neo4JUtils.getDataValue(nodeMetadataJson, "Name");
					boolean isRequired = Boolean.parseBoolean(
							this.getRelationshipProperty(Long.parseLong(metadataRelationshipId), "required").getBody());

					List<String> relTypes = new ArrayList<>();
					relTypes.add(metadataRelationshipType);
					String relsJson = this.getRelationshipsIn(nodeId, relTypes).getBody();
					if (relsJson != null) {
						JSONArray relationships;
						synchronized (parser) {
							relationships = (JSONArray) parser.parse(relsJson);
						}
						if (relationships.isEmpty() && isRequired) {
							StringBuilder msg = new StringBuilder("Node of type ").append(nodeType)
									.append(" does not have an in relationship of type '")
									.append(metadataRelationshipType);
							msg.append("' from entity of type ").append(metadataRelationshipStartNodeType)
									.append(" which is required!");

							String endNodeJson = this.getNode(nodeId).getBody();
							String endNodeName = Neo4JUtils.getDataValue(endNodeJson, "Name");
							if (endNodeName == null) {
								endNodeName = Neo4JUtils.getDataValue(endNodeJson, "name");
							}

							JSONObject detail = new JSONObject();
							detail.put("relationshipId", null);
							detail.put("relationshipType", metadataRelationshipType);
							detail.put("relationshipDirection", "in");
							detail.put("startNodeId", null);
							detail.put("startNodeType", metadataRelationshipStartNodeType);
							detail.put("startNodeName", null);
							detail.put("endNodeId", String.valueOf(nodeId));
							detail.put("endNodeType", metadataRelationshipEndNodeType);
							detail.put("endNodeName", endNodeName);
							detail.put("error", msg.toString());
							invalidNodeRelationships.add(detail);
						}
						for (int y = 0; y < relationships.size(); y++) {
							JSONObject relationshipJsonObj = (JSONObject) relationships.get(y);
							String relationshipId = Neo4JUtils.getId(relationshipJsonObj.toJSONString());
							String startId = Neo4JUtils.getRelationshipStartNode(relationshipJsonObj.toJSONString());
							String startType = (String) Neo4JUtils
									.getValue(this.getNodeTypes(Long.parseLong(startId)).getBody());

							String startNodeJson = this.getNode(Long.parseLong(startId)).getBody();
							String endNodeJson = this.getNode(nodeId).getBody();
							String startNodeName = Neo4JUtils.getDataValue(startNodeJson, "Name");
							// try to get a name for the nodes
							if (startNodeName == null) {
								startNodeName = Neo4JUtils.getDataValue(startNodeJson, "name");
							}
							String endNodeName = Neo4JUtils.getDataValue(endNodeJson, "Name");
							if (endNodeName == null) {
								endNodeName = Neo4JUtils.getDataValue(endNodeJson, "name");
							}

							Map<String, Object> relationshipProperties = Neo4JUtils.getProperties(
									this.getRelationshipProperties(Long.parseLong(relationshipId)).getBody());
							try {
								this.validateRelationship(Long.parseLong(startId), metadataRelationshipType, nodeId,
										relationshipProperties);
							} catch (AttuneException ae) {
								LOGGER.info(ae.getMessage(), ae);
								JSONObject detail = new JSONObject();
								detail.put("relationshipId", relationshipId);
								detail.put("relationshipType", metadataRelationshipType);
								detail.put("relationshipDirection", "in");
								detail.put("startNodeId", startId);
								detail.put("startNodeType", startType);
								detail.put("startNodeName", startNodeName);
								detail.put("endNodeId", String.valueOf(nodeId));
								detail.put("endNodeType", metadataRelationshipEndNodeType);
								detail.put("endNodeName", endNodeName);
								detail.put("error", ae.getMessage());
								invalidNodeRelationships.add(detail);
							}

							if (isRequired && !startType.equals(metadataRelationshipStartNodeType)) {
								StringBuilder msg = new StringBuilder("Node of type ").append(nodeType)
										.append(" does not have an in relationship of type '")
										.append(metadataRelationshipType);
								msg.append("' from entity of type ").append(metadataRelationshipStartNodeType)
										.append(" which is required!");

								JSONObject detail = new JSONObject();
								detail.put("relationshipId", relationshipId);
								detail.put("relationshipType", metadataRelationshipType);
								detail.put("relationshipDirection", "in");
								detail.put("startNodeId", startId);
								detail.put("startNodeType", startType);
								detail.put("startNodeName", startNodeName);
								detail.put("endNodeId", String.valueOf(nodeId));
								detail.put("endNodeType", metadataRelationshipEndNodeType);
								detail.put("endNodeName", endNodeName);
								detail.put("error", msg.toString());
								invalidNodeRelationships.add(detail);
							}
						}
					}
				}
			}
			return invalidNodeRelationships;
		} catch (AttuneException ae) {
			throw ae;
		} catch (Exception e) {
			throw new AttuneException(e);
		}
	}

	private synchronized String getExistingRelationshipId(Long startNodeId, String relationshipType, Long endNodeId) {
		try {
			String relationshipId = null;
			List<String> relTypes = new ArrayList<>();
			relTypes.add(relationshipType);
			String outboundRelsJson = this.getRelationshipsOut(startNodeId, relTypes).getBody();
			if (outboundRelsJson != null) {
				JSONArray jsonArray = (JSONArray) parser.parse(outboundRelsJson);
				for (int index = 0; index < jsonArray.size(); index++) {
					JSONObject relationship = (JSONObject) jsonArray.get(index);
					if (String.valueOf(startNodeId)
							.equals(Neo4JUtils.getRelationshipStartNode(relationship.toJSONString()))
							&& String.valueOf(endNodeId)
									.equals(Neo4JUtils.getRelationshipEndNode(relationship.toJSONString()))) {
						relationshipId = Neo4JUtils.getId(relationship.toJSONString());
						break;
					}
				}
			}

			if (relationshipId == null) {
				String inboundRelsJson = this.getRelationshipsIn(endNodeId, relTypes).getBody();
				if (inboundRelsJson != null) {
					JSONArray jsonArray = (JSONArray) parser.parse(inboundRelsJson);
					for (int index = 0; index < jsonArray.size(); index++) {
						JSONObject relationship = (JSONObject) jsonArray.get(index);
						if (String.valueOf(startNodeId)
								.equals(Neo4JUtils.getRelationshipStartNode(relationship.toJSONString()))
								&& String.valueOf(endNodeId)
										.equals(Neo4JUtils.getRelationshipEndNode(relationship.toJSONString()))) {
							relationshipId = Neo4JUtils.getId(relationship.toJSONString());
							break;
						}
					}
				}
			}
			return relationshipId;
		} catch (AttuneException ae) {
			throw ae;
		} catch (Exception e) {
			throw new AttuneException(e);
		}
	}

	private Map<String, Object> defaultNullPropertyValues(Map<String, Object> properties) {
		// Neo4J Properties can't be null, so default them to ""
		for (Map.Entry<String, Object> entry : properties.entrySet()) {
			String key = entry.getKey();
			Object value = entry.getValue();
			if (value == null) {
				properties.put(key, "");
			}
		}
		return properties;
	}

	private String getUrlForRelTypes(StringBuilder baseUrl, List<String> relTypes) throws UnsupportedEncodingException {
		if (relTypes != null && !relTypes.isEmpty()) {
			baseUrl.append("/");
			Iterator<String> typesItr = relTypes.iterator();
			while (typesItr.hasNext()) {
				String type = typesItr.next();
				type = URLDecoder.decode(type, StandardCharsets.UTF_8.toString());
				baseUrl.append(type);
				if (typesItr.hasNext()) {
					baseUrl.append("&");
				}
			}
		}
		return baseUrl.toString();
	}
}
