package com.bah.attune.web;

import com.bah.attune.data.AttuneException;
import com.bah.attune.data.MetadataModelJson;
import com.bah.attune.service.BaseballCardService;
import org.apache.commons.lang.StringEscapeUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import java.util.List;
import java.util.Map;

@SuppressWarnings("unchecked")
@Controller
public class BaseballCardController
{
   @Autowired
   private BaseballCardService service;

   @RequestMapping("baseballCard.exec")
   public String baseballCard(Model model, @RequestParam String entity, @RequestParam String name,
                              @RequestParam(required = false) String breadcrumbText) throws AttuneException
   {
      Map<String, String> map = (Map<String, String>) service.invokeDao("getAttributesMap", entity, name);
      if ( map == null )
         return "badRequest";

      
      String escapedEntity = StringEscapeUtils.escapeHtml(entity);
      String escapedName = StringEscapeUtils.escapeHtml(name);

      /*model.addAttribute("entity", escapedEntity);
      model.addAttribute("name", escapedName);
	  */
      
      // Test without escape
      model.addAttribute("entity", entity);
      model.addAttribute("name", name);
      if (breadcrumbText == null)
      {
    	  	breadcrumbText = entity + ": " + name;
      }
      else
      {
         //check if this bbc is already in breadcrumb
         int index = breadcrumbText.lastIndexOf(entity + ": " + name);
         if (index != -1)
         {
            breadcrumbText = breadcrumbText.substring(0, index + entity.length() + 2 + name.length());
         }
         else
         {
            breadcrumbText += ">>" + entity + ": " + name;
         }
      }

      model.addAttribute("breadcrumbText", breadcrumbText);

      return "baseballCard";
   }

   @RequestMapping("getAttributesMap.exec")
   @ResponseBody
   public Map<String, String> getAttributesMap(@RequestParam String entity, @RequestParam String name)
           throws AttuneException
   {
      return (Map<String, String>) service.invokeDao("getAttributesMap", entity, name);
   }


   @RequestMapping("getParentMap.exec")
   @ResponseBody
   public Map<String, List<String>> getParentMap(@RequestParam String entity, @RequestParam String name)
           throws AttuneException
   {
      return (Map<String, List<String>>) service.invokeDao("getParentMap", entity, name);
   }


   @RequestMapping("getChildrenMap.exec")
   @ResponseBody
   public Map<String, List<String>> getChildrenMap(@RequestParam String entity, @RequestParam String name)
           throws AttuneException
   {
      return (Map<String, List<String>>) service.invokeDao("getChildrenMap", entity, name);
   }


   @RequestMapping("getNetworkView.exec")
   @ResponseBody
   public MetadataModelJson getNetworkView(@RequestParam String entity, @RequestParam String name)
           throws AttuneException
   {
      return service.getNetworkView(entity, name);
   }
}
