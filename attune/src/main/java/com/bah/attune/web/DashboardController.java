package com.bah.attune.web;

import java.io.BufferedWriter;
import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang.StringEscapeUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import com.bah.attune.data.DashboardEntity;
import com.bah.attune.data.NameValuePair;
import com.bah.attune.service.BaseService;
import com.bah.attune.service.MainService;

@SuppressWarnings("unchecked")
@Controller
public class DashboardController
{
    protected static final Map<String, String> chartTypeMap = new HashMap<>();
    private static final Logger LOGGER = LoggerFactory.getLogger(DashboardController.class);

    static
    {
        chartTypeMap.put("Pie Chart", "pie");
        chartTypeMap.put("Donut Chart", "donut");
        chartTypeMap.put("Bar Chart", "bar");
        chartTypeMap.put("Horizontal Bar Chart", "bar");
        chartTypeMap.put("Line Chart", "line");
    }

    @Autowired
    private MainService service;

    @RequestMapping("dashboard.exec")
    public String dashboard(Model model)
    {
        prepareDashboard(model);

        return "dashboard";
    }
    
    
    @RequestMapping("configDashboard.exec")
    public String configDashboard(Model model)
    {
        prepareDashboard(model);
        model.addAttribute("allEntityList", service.invokeDao("getEntityList"));

        String[] allChartTypeList = new String[]
        { "Pie Chart", "Donut Chart", "Bar Chart", "Horizontal Bar Chart", "Line Chart" };
        model.addAttribute("allChartTypeList", allChartTypeList);

        return "configDashboard";
    }
    
    
    @RequestMapping(value = "/saveDashboard.exec", method = RequestMethod.POST)
    @ResponseBody 
    public Integer saveDashboard(@RequestBody String[] list)
    {
    	String[] escapedList= new String[25];
    	
    	//Escaping XML in order to prevent Persistent XSS attacks
    	for (int i = 0; i != list.length; i++) {
			String escapedListItem = StringEscapeUtils.escapeXml(list[i]);
			escapedList[i]=escapedListItem;
		}

        service.saveDashboard(escapedList);
        
        return 0;
    }

    // --------------------------------------- ajax -------------------------------------------------------------
    @RequestMapping("getFieldList.exec")
    @ResponseBody 
    public String[] getFieldList(@RequestParam String entity) 
    {
        return (String[]) service.invokeDao("getFieldList", entity);
    }
    

    @RequestMapping("getValueList.exec")
    @ResponseBody 
    public List<String> getValueList(@RequestParam String entity, @RequestParam String field) 
    {
        return (List<String>) service.invokeDao("getValueList", entity, field);
    }

    private void prepareDashboard(Model model) 
    {
        List<DashboardEntity> dashboardEntityList = new ArrayList<>();

        List<Map<String, Object>> dashboardList = (List<Map<String, Object>>) service.invokeDao("loadAll", "Dashboard");
        for (Map<String, Object> map : dashboardList)
        {
            DashboardEntity dashboardEntity = new DashboardEntity();

            String entity = (String) map.get("entity");
            String groupBy = (String) map.get("groupBy");

            dashboardEntity.setEntity(entity);
            dashboardEntity.setGroupBy(groupBy);
            dashboardEntity.setAlertCheck((String) map.get("alertCheck"));
            dashboardEntity.setAlertValue((String) map.get("alertValue"));
            dashboardEntity.setChartType((String) map.get("chartType"));
            dashboardEntity.setChartTypeValue(chartTypeMap.get((String) map.get("chartType")));

            dashboardEntity.setElementList((List<Map<String, Object>>) service.invokeDao("loadEntity", entity));
            dashboardEntity.setGroupByList((List<NameValuePair>) service.invokeDao("getGroupByList", entity, groupBy));

            String[] fieldList = (String[]) service.invokeDao("getFieldList", entity);
            String[] displayList = (String[]) service.invokeDao("getDisplayList", entity);
            String[] cleanedUpDisplayList= cleanUpDisplayList(fieldList, displayList); 

            Collection<String> nonDisplayList = CollectionUtils.disjunction(Arrays.asList(fieldList), Arrays.asList(cleanedUpDisplayList));	
            dashboardEntity.setFieldList(fieldList);	//This list doesn't get displayed on Dashboard tab's UI
            dashboardEntity.setDisplayList(cleanedUpDisplayList);	//This is what gets displayed on Dashboard tab's UI
            dashboardEntity.setNonDisplayList(nonDisplayList.toArray(new String[nonDisplayList.size()]));	//This is what gets displayed on Dashboard's UI
            dashboardEntity.setFieldValueList((List<String>) service.invokeDao("getValueList", entity, groupBy)); 

            dashboardEntityList.add(dashboardEntity);
        }

        model.addAttribute("dashboardEntityList", dashboardEntityList);
    }

    /**
     * Compares displayList and fieldList. Deletes the items from displayList that are not in fieldList. Returns cleaned-up version of displayList.
     * If there is an item in displayList that was not in fieldList, then it is a sign of a possible Persistent XSS attack. That means an item
     * was forcefully added to displayList. 
     * 
     * 6/20/2017- Solution to Persistent XSS Attack 
     * @param fieldList= The list taken from the Excel spreadsheet. 
     * @param displayList= List the user chooses to display. AttuneUser sets the list in Dashboard-> Configuration.
     * @return displayList with items removed that were not present in fieldList. 
     */
	private String[] cleanUpDisplayList(String[] fieldList, String[] displayList) {
		ArrayList<String> cleanedUpDisplayList= new ArrayList<>();
		boolean itemIsInBothDisplayAndFieldList= false; //Controls whether to add item to nonDisplayList
		
		//Loops over displayList
		for (int i=0; i!= displayList.length; i++) {
			//Takes fieldList[ii] and loops over displayList to compare fieldList[ii] with each item in displayList[i] items
			for (int ii=0; ii!=fieldList.length; ii++) {
				if(displayList[i].equals(fieldList[ii])) {
					itemIsInBothDisplayAndFieldList= true;
					cleanedUpDisplayList.add(displayList[i]);
				}
			}
			if (itemIsInBothDisplayAndFieldList==false) {
				LOGGER.error("displayList["+i+"] item= "+displayList[i]+". This displayList item did not match any of the FieldList items. This is strange and could be a sign of a Persistent XSS attack. The strange item has been erased from displayList array.");
			}
			itemIsInBothDisplayAndFieldList=false;	//Resets value.
		}
		return cleanedUpDisplayList.toArray(new String[cleanedUpDisplayList.size()]);
	}

}