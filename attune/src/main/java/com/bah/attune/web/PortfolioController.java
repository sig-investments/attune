package com.bah.attune.web;

import com.bah.attune.data.PortfolioBean;
import com.bah.attune.service.MainService;
import com.bah.attune.service.PortfolioService;
import org.apache.commons.lang.StringEscapeUtils;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;

import org.neo4j.graphdb.Node;
import org.neo4j.graphdb.PropertyContainer;
import org.neo4j.graphdb.DynamicRelationshipType;
import org.neo4j.graphdb.Relationship;
import org.neo4j.graphdb.Direction;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.ArrayList;


@SuppressWarnings("unchecked") @Controller public class PortfolioController
{
    @Autowired private MainService service;
    @Autowired private PortfolioService portfolioService;
    
    private static final String GET_PORTFOLIO_BEAN_LIST = "getPortfolioBeanList";
    private static final String GET_ENTITY_COUNT= "getEntityCount";
    private static final String OVERLAP= "overlap";
    private static final String GET_ATTRIBUTES_MAP= "getAttributesMap";
    
    private static final Logger LOGGER = LoggerFactory.getLogger(PortfolioController.class);

    @RequestMapping("portfolio.exec")
    public String portfolio(Model model, @RequestParam(required = false) String chiclet,
                                         @RequestParam(required = false) String grouping, 
                                         @RequestParam(required = false) String content)
    {
        preparePortfolio(model, chiclet, grouping, content);

        return "portfolio";
    }

    private void preparePortfolio(Model model, String selectedEntity, String groupingEntity, String contentEntity)
    {
        String relationship;

        // List of available entities that the user can select from a dropdown
        List<String> entityList = (List<String>) portfolioService.invokeDao("getPortfolioEntityList");

        // Set the defaults if there is no selected entity
        if ( StringUtils.isEmpty(selectedEntity) )
        {
        	String[] portfolioConfig = portfolioService.getPortfolioConfig();
        	if ( portfolioConfig[0] != null )
        	{
        		selectedEntity = portfolioConfig[0];
        		groupingEntity = portfolioConfig[1];
        		contentEntity = portfolioConfig[2];
        	} 
        	else
        	{
	            // Use the last entity (could be any of them), and set the default
	            // content
	            selectedEntity = entityList.get(3);
	            List<String> children = (List<String>) portfolioService.invokeDao("getChildEntitiesList", selectedEntity);
	            if ( !children.isEmpty() )
	                contentEntity = children.get(0);
	            else
	                contentEntity = "";
        	}
        }

        // Set a grouping if there was none specified
        if ( StringUtils.isEmpty(groupingEntity) )
            groupingEntity = (String) portfolioService.invokeDao("getDefaultGroupingByEntity", selectedEntity);

        relationship = (String) portfolioService.invokeDao("getRelationship", groupingEntity, selectedEntity);

        // List of portfolio beans to hold the main panel/chiclet information
        List<PortfolioBean> portfolioBeanList = (List<PortfolioBean>) portfolioService.invokeDao(GET_PORTFOLIO_BEAN_LIST,
                selectedEntity, groupingEntity, relationship, contentEntity);

        HashMap<String, HashMap<String, Integer>> countMap = new HashMap<>();


        for (PortfolioBean bean : portfolioBeanList)
        {
            if ( bean.getChildren() == null || bean.getChildren().isEmpty() )
            {
                if (bean.getIsGap())
                    increaseCount(countMap, "gap", bean);
                if (bean.getIsOverlap())
                    increaseCount(countMap, OVERLAP, bean);
            }
            else
            {
                for (PortfolioBean child : bean.getChildren())
                {
                    if ( child.getChildren() == null || child.getChildren().isEmpty() )
                    {
                        if (child.getIsGap())
                            increaseCount(countMap, "gap", bean);
                        if (child.getIsOverlap())
                            increaseCount(countMap, OVERLAP, bean);
                    }
                    else
                    {
                        for (PortfolioBean grandChild : child.getChildren())
                        {
                            if (grandChild.getIsGap())
                                increaseCount(countMap, "gap", bean);
                            if (grandChild.getIsOverlap())
                                increaseCount(countMap, OVERLAP, bean);
                        }
                    }
                }
            }
        }

        if ( countMap.size() > 0 )
        {
            //set gap count
            int gapCount = 0;
            HashMap<String, Integer> gapCountMap = countMap.get("gap");

            if (gapCountMap != null) {
            	for ( Entry<String, Integer> gapCountEntry: gapCountMap.entrySet()) {
            		String name = gapCountEntry.getKey();
            		gapCount += gapCountMap.get(name);
            	}
            }

            model.addAttribute("gapCount", gapCount);

            //set overlap count
            int overlapCount = 0;
            HashMap<String, Integer> overlapCountMap = countMap.get(OVERLAP);

            if (overlapCountMap != null) {
            	for ( Entry<String, Integer> overlapCountEntity : overlapCountMap.entrySet()) {
                    String name= overlapCountEntity.getKey();
            		overlapCount += overlapCountMap.get(name);
            	}
            
                model.addAttribute("overlapCount", overlapCount);
            }
        }

        model.addAttribute("selectedEntity", StringEscapeUtils.escapeXml(selectedEntity));
        Integer ct = (Integer) service.invokeDao(GET_ENTITY_COUNT, StringEscapeUtils.escapeXml(selectedEntity));
        System.out.println("\nThe selectedEntityCount is " + ct);
        model.addAttribute("selectedEntityCount", (Integer) service.invokeDao(GET_ENTITY_COUNT, StringEscapeUtils.escapeXml(selectedEntity)));

        // Only add the grouping entity if it's different than the selected
        // entity (entity could have no parents)
        if ( !selectedEntity.equals(groupingEntity) )
        {
            model.addAttribute("groupingEntity", StringEscapeUtils.escapeXml(groupingEntity));
            model.addAttribute("groupingEntityCount", (Integer) service.invokeDao(GET_ENTITY_COUNT, StringEscapeUtils.escapeXml(groupingEntity)));
        }

        // Only add the content entity if it exists (entity could have no
        // children)
        if ( contentEntity != null )
        {
            model.addAttribute("content", StringEscapeUtils.escapeXml(contentEntity));
            model.addAttribute("contentCount", (Integer) service.invokeDao(GET_ENTITY_COUNT, StringEscapeUtils.escapeXml(contentEntity)));
        }

        model.addAttribute("entityColor", (String) service.invokeDao("getEntityColor",StringEscapeUtils.escapeXml(selectedEntity)));
        model.addAttribute("entityList", entityList);
        model.addAttribute("portfolioBeanList", portfolioBeanList);
        model.addAttribute("chicletRelation", relationship);

    }


    private void increaseCount(HashMap<String, HashMap<String,Integer>> countMap, String category, PortfolioBean bean)
    {
        String name = bean.getName();
        HashMap<String, Integer> categoryMap = countMap.get(category);

        if (categoryMap == null) {
            categoryMap = new HashMap<>();
        }

        Integer count = categoryMap.get(name);
        if ( count == null )
            count = 0;

        categoryMap.put(name,++count);
        countMap.put(category, categoryMap);

    }


    @RequestMapping("portfolioCompare.exec")
    public String portfolioCompare(@RequestParam String entityOne,
                                                @RequestParam String nameOne, 
                                                @RequestParam String entityTwo, 
                                                @RequestParam String nameTwo, Model model)
    {
        Map<String, Map<String, String>> data = new HashMap<>();

    	String escapedEntityOne = StringEscapeUtils.escapeXml(entityOne);
    	String escapedNameOne = StringEscapeUtils.escapeXml(nameOne);
    	String escapedEntityTwo = StringEscapeUtils.escapeXml(entityTwo);
    	String escapedNameTwo = StringEscapeUtils.escapeXml(nameTwo);

    	Map<String, String> attributesMapOne = (Map<String, String>) portfolioService.invokeDao(GET_ATTRIBUTES_MAP,
    			escapedEntityOne, escapedNameOne);
        Map<String, String> attributesMapTwo = (Map<String, String>) portfolioService.invokeDao(GET_ATTRIBUTES_MAP,
                escapedEntityTwo, escapedNameTwo);

        attributesMapOne.put("name", escapedNameOne);
        attributesMapTwo.put("name", escapedNameTwo);
        
        data.put("one", attributesMapOne);
        data.put("two", attributesMapTwo);
        
        model.addAttribute("data", data);

        return "comparison";
    }


    @RequestMapping("getPortfolioAttributesMap.exec")
    @ResponseBody
    public Map<String, String> getAttributesMap(@RequestParam String entity, @RequestParam String name)
    {
        return (Map<String, String>) service.invokeDao(GET_ATTRIBUTES_MAP, entity, name);
    }


    @RequestMapping("getSubGroupingList.exec")
    @ResponseBody
    public List<String> getSubGroupingList(@RequestParam String entity)
    {
        return (List<String>) portfolioService.invokeDao("getSubGroupingList", entity);
    }


    @RequestMapping("getChildList.exec")
    @ResponseBody
    public List<String> getChildList(@RequestParam String entity) 
    {
        System.out.println(entity);
        return (List<String>) portfolioService.invokeDao("getChildList", entity);
    }

   @RequestMapping("getChildrenNode.exec")
   @ResponseBody
   public List<String> getChildrenNode(@RequestParam String entity )
   {

    String[] valArr = entity.split(",");

    System.out.println("in getchildrennodes");
     String label = valArr[1];
     String instanceName = valArr[0];
     System.out.println(label);
     System.out.println(instanceName);
     List<String> ret = new ArrayList<String>();
     List<Node> entList = (List<Node>) service.invokeDao("getEntitiesByLabel", "CapabilityArea");
                  int counter = 0; 
                  List<String> numret = new ArrayList<String>();

     //Node nodeInstance;
     for (int i = 0; i < entList.size(); i++) {
        System.out.println(entList.get(i).getProperty("Name"));
       if (entList.get(i).getProperty("Name").equals(instanceName)) {
        System.out.println("in here if");
             Iterable<Relationship> relList = entList.get(i).getRelationships(Direction.OUTGOING);
             for (Relationship myRel : relList) {
                counter++;
                System.out.println("in here for");
               String pname = (String) myRel.getEndNode().getProperty("Name");
               System.out.println(pname);
               String it = pname + ",";
               System.out.println(counter);
               ret.add(pname + ",");
             }
       }
     }

     String num = String.valueOf(counter);
     numret.add(num);

     //System.out.println("ret is " + ret);
     return numret;
   }
    
    @RequestMapping(value = "/savePortfolio.exec", method = RequestMethod.POST)
    @ResponseBody 
    public Integer savePortfolio(@RequestBody String[] list)
    {
    	portfolioService.savePortfolioConfig(list);

        return 0;
    }
}