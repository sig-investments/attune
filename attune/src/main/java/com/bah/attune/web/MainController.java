package com.bah.attune.web;

import com.bah.attune.data.AttuneException;
import com.bah.attune.data.AuditTrail;
import com.bah.attune.data.MetadataModelJson;
import com.bah.attune.service.AuditService;
import com.bah.attune.service.MainService;
import com.bah.attune.service.SecurityService;
import com.bah.attune.service.UserService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.web.authentication.logout.SecurityContextLogoutHandler;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.File;
import java.io.FileNotFoundException;
import java.util.List;

@Controller

public class MainController extends AbstractRestController   //Gene's Code. Pg 107 of 3-30-2017 report
{
   @Autowired
   private MainService service;

   @Autowired
   private UserService userService;

   @Autowired
   private AuditService auditService;

   private static final Logger LOGGER = LoggerFactory.getLogger(MainController.class);

   private String cac = "";

   private static final String[] HEADERS_TO_TRY = {
                                                     "X-Forwarded-For",
                                                     "Proxy-Client-IP",
                                                     "WL-Proxy-Client-IP",
                                                     "HTTP_X_FORWARDED_FOR",
                                                     "HTTP_X_FORWARDED",
                                                     "HTTP_X_CLUSTER_CLIENT_IP",
                                                     "HTTP_CLIENT_IP",
                                                     "HTTP_FORWARDED_FOR",
                                                     "HTTP_FORWARDED",
                                                     "HTTP_VIA",
                                                     "REMOTE_ADDR"};

   @RequestMapping("login.exec")
   public String login()
   {
      return "login";
   }


   @RequestMapping(value = "logout.exec", method = RequestMethod.GET)
   public String logout(HttpServletRequest request, HttpServletResponse response)
   {
      Authentication auth = SecurityContextHolder.getContext().getAuthentication();
      if (auth != null)
         new SecurityContextLogoutHandler().logout(request, response, auth);

      return "login" + cac;
   }


   @RequestMapping("/")
   public String redirect()
   {
      return "redirect:/maincac.exec";
   }


   @RequestMapping(value = "main.exec", method = RequestMethod.GET)
   public String main(Model model, HttpServletRequest request) throws AttuneException
   {
      model.addAttribute("username", SecurityService.getUserName());
      model.addAttribute("timelineCreated", service.invokeDao("timelineCreated"));
      model.addAttribute("lastLogin", auditService.invokeDao("getUserLastLogin"));

      try
      {
         File logo = new File(org.springframework.web.util.WebUtils.getRealPath(
                                 servletContext, "/") + "/icons/logo.png");
         if (logo.exists())
            model.addAttribute("logo", true);
      }
      catch (FileNotFoundException e)
      {
         LOGGER.error(e.getMessage(), e);
      }
      return "main";
   }


   @RequestMapping("metadata.exec")
   public String metadata()
   {
      return "metadata";
   }


   @RequestMapping("getMetadata.exec")
   @ResponseBody
   public MetadataModelJson getMetadata()
   {
      return service.getMetadata();
   }


   @RequestMapping("auditTrail.exec")
   public String auditTrail(Model model) throws AttuneException
   {
      List<AuditTrail> auditTrailList = (List<AuditTrail>)auditService.invokeDao("getAuditTrailList");
      model.addAttribute("auditTrailList", auditTrailList);
      return "auditTrail";
   }


   @RequestMapping("getTimelineCreated.exec")
   @ResponseBody
   public boolean getTimelineCreated() throws AttuneException
   {
      return (boolean) service.invokeDao("timelineCreated");
   }


   @RequestMapping("logincac.exec")
   public String logincac()
   {
      cac = "cac";
      return "logincac";
   }


   @RequestMapping(value = "maincac.exec", method = RequestMethod.GET)
   public String maincac(Model model, HttpServletRequest request) throws AttuneException
   {
      cac = "cac";
      return main(model, request);
   }
}
