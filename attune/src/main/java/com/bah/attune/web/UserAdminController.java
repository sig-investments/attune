package com.bah.attune.web;

import com.bah.attune.data.AttuneUser;
import com.bah.attune.service.UserService;
import com.bah.attune.util.Encryptor;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@Controller

public class UserAdminController extends AbstractRestController
{
   @Autowired
   private UserService service;


   private static final Logger LOGGER = LoggerFactory.getLogger(UserAdminController.class);

   @Autowired
   private Encryptor encryptor;

   @RequestMapping("userAdmin.exec")
   public String userAdmin(Model model)
   {
      List<AttuneUser> userList = (List<AttuneUser>)service.invokeDao("getUserList");
      model.addAttribute("userList", userList);

      return "userAdmin";
   }


   @RequestMapping(value = "saveUser.exec", method = RequestMethod.POST)
   @ResponseBody
   public Integer saveUser(@RequestBody AttuneUser user) throws Exception
   {
      try
      {
         service.saveUser(user);

         return 1;
      }
      catch(Exception e)
      {
         LOGGER.error("Error occur while creating user", e);
         throw e;
      }
   }

   @RequestMapping(value = "deleteUser.exec")
   @ResponseBody
   public Integer deleteUser(@RequestParam String uuid) throws Exception
   {
      try
      {
         service.deleteUser(uuid);

         return 1;
      }
      catch(Exception e)
      {
         LOGGER.error("Error occur while deleting user", e);
         throw e;
      }
   }


   @RequestMapping("getUser.exec")
   @ResponseBody
   public AttuneUser getUser(@RequestParam String uuid)
   {
      AttuneUser user = (AttuneUser)service.invokeDao("getUser", uuid);

      return user;
   }


   @RequestMapping("usernameAvailable.exec")
   @ResponseBody
   public boolean usernameAvailable(@RequestParam String uuid, @RequestParam String username)
   {
      List<AttuneUser> userList = (List<AttuneUser>)service.invokeDao("getUserList");

      for (AttuneUser user: userList)
         if ( ( uuid.equals("") || !user.getUuid().equals(uuid)) && user.getUsername().equals(username))
            return false;

      return true;
   }


   @RequestMapping("passwordAvailable.exec")
   @ResponseBody
   public boolean passwordAvailable(@RequestParam String uuid, @RequestParam String password)
   {
      if ( uuid != null && uuid.length() > 0 )
      {
         AttuneUser user = (AttuneUser) service.invokeDao("getUser", uuid);

         // user are using the current pwd
         if ( encryptor.decrypt(user.getPassword()).equals(password) )
            return true;

         String[] passwordHistoryList = StringUtils.split(user.getPasswordHistory(), ';');

         for (String encryptedPassword : passwordHistoryList)
         {
            if ( encryptor.decrypt(encryptedPassword).equals(password) )
               return false;
         }
      }

      return true;
   }

}
