package com.bah.attune.web;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.List;

import org.apache.poi.ss.usermodel.Workbook;
import org.apache.poi.ss.usermodel.WorkbookFactory;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.multipart.MultipartFile;

import com.bah.attune.data.AttuneException;
import com.bah.attune.service.ImportService;

@Controller public class ImportController
{
    private static final Logger LOGGER = LoggerFactory.getLogger(ImportController.class);

    @Autowired private ImportService importService;


    @RequestMapping("dataImport.exec")
    public String dataImport()
    {
        return "dataImport";
    }


    @RequestMapping(value = "upload.exec", method = RequestMethod.POST)
    public @ResponseBody List<String> upload(@RequestParam("file[]") MultipartFile file) throws AttuneException
    {
        try
        {
            if ( file.getOriginalFilename().endsWith(".xls") )
            {
                //Workbook wb = WorkbookFactory.create(file.getInputStream());	//Original Code
//----------------START OF GENE'S TEST CODE---------------------------            	
                Workbook wb = WorkbookFactory.create(file.getInputStream());
            	file.getInputStream().close();
//----------------END OF GENE's TEST CODE ----------------------------------------------------------
            	
                return importService.importDataSheet(wb);
            }
            else
            {
                List<String> errorMessages = new ArrayList<>();
                errorMessages.add(
                        "Due to security risks, only the '.xls' file extension is supported. Please save the workbook as an '.xls' file and re-import.");
                return errorMessages;
            }
        }
        catch (Exception e)
        {
            LOGGER.error("Error occured in upload", e);
            throw new AttuneException("Error occured in upload, Error:" + e.toString());
        }
    }
}
