package com.bah.attune.web.filters;

import javax.servlet.*;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

public class HeadersFilter implements Filter
{
   FilterConfig fc;

   @Override
   public void doFilter(final ServletRequest request,
                        final ServletResponse response, final FilterChain chain)
           throws IOException, ServletException
   {
      final HttpServletResponse httpResponse = (HttpServletResponse) response;

      httpResponse.addHeader("X-XSS-Protection", "1; mode=block");

      chain.doFilter(request, httpResponse);
   }

   @Override
   public void init(FilterConfig filterConfig)
   {
      this.fc = filterConfig;
   }

   @Override
   public void destroy()
   {
      this.fc = null;
   }
}