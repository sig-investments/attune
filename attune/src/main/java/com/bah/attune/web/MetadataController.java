package com.bah.attune.web;

import com.bah.attune.data.AttuneException;
import com.bah.attune.data.Metadata;
import com.bah.attune.data.MetadataJson;
import com.bah.attune.data.MetadataModelJson;
import com.bah.attune.rest.Neo4JUtils;
import com.bah.attune.service.SecurityService;
import io.swagger.annotations.*;
import org.apache.commons.codec.binary.Base64;
import org.json.simple.JSONArray;
import org.json.simple.JSONAware;
import org.json.simple.JSONObject;
import org.json.simple.JSONValue;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.*;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import java.io.StringWriter;
import java.io.UnsupportedEncodingException;
import java.net.URLDecoder;
import java.net.URLEncoder;
import java.nio.charset.Charset;
import java.nio.charset.StandardCharsets;
import java.util.*;

@Controller
@RequestMapping("/api/metadata/*")
@Api(value = "/api/metadata/")
public class MetadataController extends AbstractRestController
{

   private static String neo4jUrlPrefix = null;
   private static final Logger LOGGER = LoggerFactory.getLogger(MetadataController.class);
   private static boolean dashBoardMetadataExists = false;

   static
   {
      if (neo4jUrlPrefix == null)
      {
//			neo4jUrlPrefix = servletContext.getInitParameter("neo4j.url");
         neo4jUrlPrefix = "http://localhost:7474/";
      }
   }

   public MetadataController()
   {
      super();
      checkDashboardMetadata();
   }

   @RequestMapping("modify.exec")
   @ApiOperation(value = "modify", hidden = true)
   public String modify()
   {
      return "modify";
   }

   @RequestMapping("metadata.exec")
   @ApiOperation(value = "metadata", hidden = true)
   public String metadata()
   {
      return "metadata";
   }

   @RequestMapping("getMetadata.exec")
   @ResponseBody
   @ApiOperation(value = "getMetadata", hidden = true)
   public MetadataModelJson getMetadata()
   {
      try
      {
         MetadataModelJson json = new MetadataModelJson();

         List<MetadataJson> nodeList = new ArrayList<>();
         List<MetadataJson> relationshipList = new ArrayList<>();

         Map<String, String> map = new HashMap<>();
         JSONArray list = (JSONArray) parser.parse(this.getEntities(null, null).getBody());
         for (int i = 0; i < list.size(); i++)
         {
            JSONObject node = (JSONObject) list.get(i);
            String entityType = (String) node.get("Name");
            JSONArray relationships = (JSONArray) parser
                    .parse(this.getRelationshipsOut(entityType, null).getBody());
            for (int x = 0; x < relationships.size(); x++)
            {
               JSONObject relationship = (JSONObject) relationships.get(x);
               Metadata rel = new Metadata((String) relationship.get("relationshipType"),
                       (String) relationship.get("startNodeName"), (String) relationship.get("endNodeName"));

               relationshipList.add(new MetadataJson(rel));
               map.put((String) relationship.get("startNodeName"), "");
               map.put((String) relationship.get("endNodeName"), "");
            }
         }

         for (String name : map.keySet())
         {
            nodeList.add(new MetadataJson(new Metadata(name)));
         }

         json.setNodeList(nodeList);
         json.setRelationshipList(relationshipList);

         return json;
      }
      catch (Exception e)
      {
         throw new AttuneException(e);
      }
   }

   @RequestMapping(value = "/entityTypes", method = RequestMethod.GET)
   @PreAuthorize("hasRole('" + SecurityService.ROLE_USER + "')")
   @ApiOperation(value = "getEntityTypes", notes = "Returns all of the metadata entity type names", response = String.class, httpMethod = "GET")
   @ApiResponses(value = {
           @ApiResponse(code = 200, message = "A JSON array of Strings is returned that contains the Metadata entity type names, or an empty JSON array if none exist."),
           @ApiResponse(code = 400, message = "Invalid request"),
           @ApiResponse(code = 500, message = "An Error has Occurred")})
   public ResponseEntity<String> getEntityTypes()
   {
      String result = null;
      try
      {
         List<String> entityNames = new ArrayList<>();
         JSONArray jsonArray;
         synchronized (parser)
         {
            jsonArray = (JSONArray) parser.parse(this.getEntities(null, null).getBody());
         }
         for (int i = 0; i < jsonArray.size(); i++)
         {
            JSONObject jsonObject = (JSONObject) jsonArray.get(i);
            entityNames.add((String) jsonObject.get("Name"));
         }
         result = JSONValue.toJSONString(entityNames);
         final HttpHeaders httpHeaders = new HttpHeaders();
         httpHeaders.setContentType(MediaType.APPLICATION_JSON);
         return new ResponseEntity<>(((JSONAware) parser.parse(result)).toJSONString(), httpHeaders, HttpStatus.OK);
      }
      catch (AttuneException ae)
      {
         LOGGER.error(ae.getMessage(), ae);
         if (ae.getStatusCode() != null)
         {
            return new ResponseEntity<>(ae.getMessage(), ae.getStatusCode());
         }
         throw ae;
      }
      catch (Exception e)
      {
         LOGGER.error(e.getMessage(), e);
         throw new AttuneException(e);
      }
   }

   @SuppressWarnings("unchecked")
   @RequestMapping(value = "/entities", method = RequestMethod.GET)
   @PreAuthorize("hasRole('" + SecurityService.ROLE_USER + "')")
   @ApiOperation(value = "getEntities", notes = "Returns all of the metadata entities.", response = String.class, httpMethod = "GET")
   @ApiResponses(value = {
           @ApiResponse(code = 200, message = "JSON array of Metadata entities are returned, or an empty JSON array if none exist. Each object in the array will have a transient field called neo4j.id that contains it's Neo4J node id."),
           @ApiResponse(code = 400, message = "Invalid request"),
           @ApiResponse(code = 500, message = "An Error has Occurred")})
   public ResponseEntity<String> getEntities(
           @ApiParam(value = "Optional Query parameter to return Metadata entities with this property name. If specified, the propValue must be specified as well.", required = false) @RequestParam(value = "propName", required = false) String propName,
           @ApiParam(value = "Optional Query parameter to return Metadata entities with this property value. If specified, the propName must be specified as well.", required = false) @RequestParam(value = "propValue", required = false) String propValue)
   {
      String result = null;
      try
      {
         StringBuilder sb = new StringBuilder(neo4jUrlPrefix);
         sb.append("db/data/label/Metadata/nodes");
         if (propName != null)
         {
            String name = URLDecoder.decode(propName, StandardCharsets.UTF_8.toString());
            String value = JSONValue.toJSONString(URLDecoder.decode(propValue, StandardCharsets.UTF_8.toString()));
            sb.append("?").append(name).append("=").append(value);
         }
         String url = sb.toString();
         result = this.getNeo4JData(url);
         if (result != null)
         {
            JSONArray resultArray = new JSONArray();
            JSONArray array;
            synchronized (parser)
            {
               array = (JSONArray) parser.parse(result);
            }
            for (int i = 0; i < array.size(); i++)
            {
               JSONObject jsonObject = (JSONObject) array.get(i);
               String id = Neo4JUtils.getId(jsonObject.toJSONString());
               Map<String, Object> data = Neo4JUtils.getDataProperties(jsonObject.toJSONString());
               data.put("neo4j.id", id);
               resultArray.add(new JSONObject(data));
            }
            result = resultArray.toJSONString();
         }
         final HttpHeaders httpHeaders = new HttpHeaders();
         httpHeaders.setContentType(MediaType.APPLICATION_JSON);
         if (result != null)
         {
            return new ResponseEntity<>(((JSONAware) parser.parse(result)).toJSONString(), httpHeaders,
                    HttpStatus.OK);
         }
         else
         {
            return new ResponseEntity<>(null, httpHeaders, HttpStatus.OK);
         }
      }
      catch (AttuneException ae)
      {
         LOGGER.error(ae.getMessage(), ae);
         if (ae.getStatusCode() != null)
         {
            return new ResponseEntity<>(ae.getMessage(), ae.getStatusCode());
         }
         else
         {
            throw ae;
         }
      }
      catch (Exception e)
      {
         LOGGER.error(e.getMessage(), e);
         throw new AttuneException(e);
      }
   }

   @RequestMapping(value = "/entity/{entityType}", method = RequestMethod.POST, consumes = MediaType.APPLICATION_JSON_VALUE)
   @PreAuthorize("hasRole('" + SecurityService.ROLE_USER + "')")
   @ApiOperation(value = "insertEntity", notes = "Inserts the Metadata entity using the passed in properties", response = String.class, httpMethod = "POST")
   @ApiResponses(value = {
           @ApiResponse(code = 200, message = "Metadata entity was successfully inserted. A String is returned with the corresponding Neo4J node id"),
           @ApiResponse(code = 400, message = "Invalid request"),
           @ApiResponse(code = 500, message = "An Error has Occurred")})
   public ResponseEntity<String> insertEntity(
           @ApiParam(value = "Path parameter indicating the Metadata entity type name.", required = true) @PathVariable(value = "entityType") String entityType,
           @ApiParam(value = "A JSON object with the following properties: { fieldList: <String - comma separated list of field names> requiredFields: <String - comma separated list of required fields> isRoot: <boolean - true if this is the root entity, false otherwise> Name: <String - The metadata entitiy name> }", required = true) @RequestBody(required = true) Map<String, Object> entityProperties)
   {
      String result = null;
      try
      {
         Map<String, Object> properties = entityProperties;
         if (properties == null)
         {
            properties = new HashMap<>();
         }
         if (!entityType.equals(properties.get("Name")))
         {
            LOGGER.warn("Overwriting Name property to match Entity Type");
            properties.put("Name", entityType);
         }
         this.validateEntity(entityType, properties);
         String existingJson = this.getEntity(entityType).getBody();
         if (!Neo4JUtils.isEmpty(existingJson))
         {
            Map<String, Object> props = Neo4JUtils.getProperties(this.getEntityProperties(entityType).getBody());
            if (props != null && !props.isEmpty())
            {
               props.putAll(properties);
            }
            else
            {
               props = properties;
            }
            this.updateEntityProperties(entityType, props);
            return this.getEntity(entityType);
         }
         String url = new StringBuilder(neo4jUrlPrefix).append("db/data/node").toString();
         String json = null;
         if (!properties.isEmpty())
         {
            properties = defaultNullPropertyValues(properties);
            JSONObject jsonObj = new JSONObject(properties);
            json = jsonObj.toJSONString();
         }
         result = this.insertNeo4JData(json, url);
         // Add Label
         String nodeId = Neo4JUtils.getId(result);
         url = new StringBuilder(neo4jUrlPrefix).append("db/data/node/").append(nodeId).append("/labels").toString();
         this.insertNeo4JData(JSONValue.toJSONString("Metadata"), url);
         // Re-retrieve the updated data
         result = this.getEntity(entityType).getBody();
         final HttpHeaders httpHeaders = new HttpHeaders();
         httpHeaders.setContentType(MediaType.APPLICATION_JSON);
         if (result != null)
         {
            return new ResponseEntity<>(((JSONAware) parser.parse(result)).toJSONString(), httpHeaders,
                    HttpStatus.OK);
         }
         else
         {
            return new ResponseEntity<>(null, httpHeaders, HttpStatus.OK);
         }
      }
      catch (AttuneException ae)
      {
         if (ae.getStatusCode() != null)
         {
            return new ResponseEntity<>(ae.getMessage(), ae.getStatusCode());
         }
         throw ae;
      }
      catch (Exception e)
      {
         LOGGER.error(e.getMessage(), e);
         throw new AttuneException(e);
      }
   }

   @RequestMapping(value = "/entity/{entityType}", method = RequestMethod.GET)
   @PreAuthorize("hasRole('" + SecurityService.ROLE_USER + "')")
   @ApiOperation(value = "getEntity", notes = "Retrieves the JSON metadata for the passed in entity type.", response = String.class, httpMethod = "GET")
   @ApiResponses(value = {
           @ApiResponse(code = 200, message = "JSON containing the Metadata entity was successfully retrieved, or null if the entity type does not exist. The object in the array will have a transient field called neo4j.id that contains it's Neo4J node id."),
           @ApiResponse(code = 400, message = "Invalid request"),
           @ApiResponse(code = 500, message = "An Error has Occurred")})
   public ResponseEntity<String> getEntity(
           @ApiParam(value = "Path parameter indicating the Metadata entity type name.", required = true) @PathVariable(value = "entityType") String entityType)
   { String result = null;
      try
      {
         Long id = getIdForEntityType(entityType);
         if (id != null)
         {
            return getEntity(id);
         }
         StringBuilder sb = new StringBuilder(neo4jUrlPrefix);
         String url;
         if (entityType != null)
         {
            sb.append("db/data/label/Metadata/nodes");
         }
         else
         {
            StringBuilder msg = new StringBuilder("type parameter is mandatory");
            throw new AttuneException(msg.toString(), HttpStatus.BAD_REQUEST);
         }
         String name = "Name";
         String value = entityType;
         value = JSONValue.toJSONString(value);
         sb.append("?").append(name).append("=").append(value);
         url = sb.toString();
         result = this.getNeo4JData(url);
         if (result != null)
         {
            String json = result;
            if (Neo4JUtils.getSize(json) > 1)
            {
               StringBuilder msg = new StringBuilder("More than one Metadata Node found with name ")
                       .append(entityType).append(". Only one entry with this name may exist!");
               throw new AttuneException(msg.toString(), HttpStatus.BAD_REQUEST);
            }
            if (Neo4JUtils.isEmpty(json))
            {
               result = new JSONObject().toJSONString();
            }
            else
            {
               Map<String, Object> data = Neo4JUtils.getDataProperties(json);
               data.put("neo4j.id", Neo4JUtils.getId(json));
               result = new JSONObject(data).toJSONString();
            }
         }
         final HttpHeaders httpHeaders = new HttpHeaders();
         httpHeaders.setContentType(MediaType.APPLICATION_JSON);
         if (result != null)
         {
            return new ResponseEntity<>(((JSONAware) parser.parse(result)).toJSONString(), httpHeaders,
                    HttpStatus.OK);
         }
         else
         {
            return new ResponseEntity<>(null, httpHeaders, HttpStatus.OK);
         }
      }
      catch (AttuneException ae)
      {
         if (ae.getStatusCode() != null && ae.getStatusCode().equals(HttpStatus.NOT_FOUND))
         {
            return new ResponseEntity<>(HttpStatus.OK);
         }
         else if (ae.getStatusCode() != null)
         {
            return new ResponseEntity<>(ae.getMessage(), ae.getStatusCode());
         }
         throw ae;
      }
      catch (Exception e)
      {
         LOGGER.error(e.getMessage(), e);
         throw new AttuneException(e);
      }
   }

   // This conflicts with GET @Path("/entity/{entityType}"), so not exposing it
   // in REST API
   protected ResponseEntity<String> getEntity(Long id)
   {
      String result = null;
      try
      {
         String url = new StringBuilder(neo4jUrlPrefix).append("db/data/node/").append(String.valueOf(id))
                 .toString();
         result = this.getNeo4JData(url);
         if (result != null)
         {
            Map<String, Object> data = Neo4JUtils.getDataProperties(result);
            data.put("neo4j.id", String.valueOf(id));
            data.put("neo4j.label", "Metadata");
            result = new JSONObject(data).toJSONString();
         }
         final HttpHeaders httpHeaders = new HttpHeaders();
         httpHeaders.setContentType(MediaType.APPLICATION_JSON);
         if (result != null)
         {
            return new ResponseEntity<>(((JSONAware) parser.parse(result)).toJSONString(), httpHeaders,
                    HttpStatus.OK);
         }
         else
         {
            return new ResponseEntity<>(null, httpHeaders, HttpStatus.OK);
         }
      }
      catch (AttuneException ae)
      {
         if (ae.getStatusCode() != null && ae.getStatusCode().equals(HttpStatus.NOT_FOUND))
         {
            return new ResponseEntity<>(HttpStatus.OK);
         }
         else if (ae.getStatusCode() != null)
         {
            return new ResponseEntity<>(ae.getMessage(), ae.getStatusCode());
         }
         throw ae;
      }
      catch (Exception e)
      {
         LOGGER.error(e.getMessage(), e);
         throw new AttuneException(e);
      }
   }

   // This conflicts with DELETE @Path("/entity/{entityType}"), so not exposing
   // it in REST API
   protected ResponseEntity<String> deleteEntity(Long id)
   {
      try
      {
         this.validateEntityDeletion(null, id);
         String url = new StringBuilder(neo4jUrlPrefix).append("db/data/node/").append(String.valueOf(id))
                 .toString();
         this.deleteNeo4JData(url);
         return new ResponseEntity<>(HttpStatus.OK);
      }
      catch (AttuneException ae)
      {
         if (ae.getStatusCode() != null)
         {
            return new ResponseEntity<>(ae.getMessage(), ae.getStatusCode());
         }
         throw ae;
      }
      catch (Exception e)
      {
         LOGGER.error(e.getMessage(), e);
         throw new AttuneException(e);
      }
   }

   @RequestMapping(value = "/entity/{entityType}", method = RequestMethod.DELETE)
   @PreAuthorize("hasRole('" + SecurityService.ROLE_USER + "')")
   @ApiOperation(value = "deleteEntity", notes = "Deletes the Metadata entity for the passed in entity type.", response = String.class, httpMethod = "DELETE")
   @ApiResponses(value = {
           @ApiResponse(code = 204, message = "If the Metadata entity was successfully deleted, null or empty response."),
           @ApiResponse(code = 400, message = "Invalid request, if relationships still exist for the Metadata entity type"),
           @ApiResponse(code = 500, message = "An Error has Occurred")})
   public ResponseEntity<String> deleteEntity(
           @ApiParam(value = "Path parameter indicating the Metadata entity type name.", required = true) @PathVariable(value = "entityType", required = true) String entityType)
   {
      try
      {
         Long identity = getIdForEntityType(entityType);
         if (identity != null)
         {
            deleteEntity(identity);
            return new ResponseEntity<>(HttpStatus.OK);
         }
         this.validateEntityDeletion(entityType, null);
         // First delete all relationships for the entity type
         this.deleteRelationshipsForType(entityType, null);
         // Next delete the entity
         String id = Neo4JUtils.getId(this.getEntity(entityType).getBody());
         if (id != null && !id.isEmpty())
         {
            String url = new StringBuilder(neo4jUrlPrefix).append("db/data/node/").append(id).toString();
            this.deleteNeo4JData(url);
         }
         return new ResponseEntity<>(HttpStatus.OK);
      }
      catch (AttuneException ae)
      {
         if (ae.getStatusCode() != null)
         {
            return new ResponseEntity<>(ae.getMessage(), ae.getStatusCode());
         }
         throw ae;
      }
      catch (Exception e)
      {
         LOGGER.error(e.getMessage(), e);
         throw new AttuneException(e);
      }
   }

   @RequestMapping(value = "/entities", method = RequestMethod.DELETE)
   @PreAuthorize("hasRole('" + SecurityService.ROLE_USER + "')")
   @ApiOperation(value = "deleteAllEntities", notes = "Deletes all Metadata entities.", response = String.class, httpMethod = "DELETE")
   @ApiResponses(value = {
           @ApiResponse(code = 204, message = "If the Metadata entities were successfully deleted, null or empty response."),
           @ApiResponse(code = 400, message = "Invalid request, if relationships still exist for any Metadata entity types"),
           @ApiResponse(code = 500, message = "An Error has Occurred")})
   public ResponseEntity<String> deleteAllEntities()
   {
      try
      {
         // First Get all Entity Types
         String allEntityTypesJson = this.getEntityTypes().getBody();
         if (allEntityTypesJson != null)
         {
            JSONArray entityTypes;
            synchronized (parser)
            {
               entityTypes = (JSONArray) parser.parse(allEntityTypesJson);
            }
            // For each type, get all nodes
            for (int i = 0; i < entityTypes.size(); i++)
            {
               String entityType = (String) entityTypes.get(i);
               this.deleteEntity(entityType);
            }
         }
         return new ResponseEntity<>(HttpStatus.OK);
      }
      catch (AttuneException ae)
      {
         if (ae.getStatusCode() != null)
         {
            return new ResponseEntity<>(ae.getMessage(), ae.getStatusCode());
         }
         throw ae;
      }
      catch (Exception e)
      {
         LOGGER.error(e.getMessage(), e);
         throw new AttuneException(e);
      }
   }

   @RequestMapping(value = "/entity/{entityType}/properties", method = RequestMethod.GET)
   @PreAuthorize("hasRole('" + SecurityService.ROLE_USER + "')")
   @ApiOperation(value = "getEntityProperties", notes = "Returns all properties for the Metadata entity type as a single-level JSON Object", response = String.class, httpMethod = "GET")
   @ApiResponses(value = {
           @ApiResponse(code = 200, message = "A JSON object containing the properties for the Metadata entity"),
           @ApiResponse(code = 400, message = "Invalid request, if the Metadata entity type doesn't exist"),
           @ApiResponse(code = 500, message = "An Error has Occurred")})
   public ResponseEntity<String> getEntityProperties(
           @ApiParam(value = "Path parameter indicating the Metadata entity type name.", required = true) @PathVariable(value = "entityType") String entityType)
   {
      String result = null;
      try
      {
         String id = Neo4JUtils.getId(this.getEntity(entityType).getBody());
         String url = new StringBuilder(neo4jUrlPrefix).append("db/data/node/").append(id).append("/properties")
                 .toString();
         result = this.getNeo4JData(url);
         final HttpHeaders httpHeaders = new HttpHeaders();
         httpHeaders.setContentType(MediaType.APPLICATION_JSON);
         if (result != null)
         {
            return new ResponseEntity<>(((JSONAware) parser.parse(result)).toJSONString(), httpHeaders,
                    HttpStatus.OK);
         }
         else
         {
            return new ResponseEntity<>(null, httpHeaders, HttpStatus.OK);
         }
      }
      catch (AttuneException ae)
      {
         if (ae.getStatusCode() != null)
         {
            return new ResponseEntity<>(ae.getMessage(), ae.getStatusCode());
         }
         throw ae;
      }
      catch (Exception e)
      {
         LOGGER.error(e.getMessage(), e);
         throw new AttuneException(e);
      }
   }

   @RequestMapping(value = "/entity/{entityType}/properties", method = RequestMethod.PUT, consumes = MediaType.APPLICATION_JSON_VALUE)
   @PreAuthorize("hasRole('" + SecurityService.ROLE_USER + "')")
   @ApiOperation(value = "updateEntityProperties", notes = "Overwrites all properties on a Metadata Entity, passing in a single-level JSON Object that contains all of the properties.", response = String.class, httpMethod = "PUT")
   @ApiResponses(value = {
           @ApiResponse(code = 204, message = "Metadata entity properties were successfully updated, empty response"),
           @ApiResponse(code = 400, message = "Invalid request, if the Name, isRoot, fieldList, or requiredFields properties are missing; or if any field names in requiredFields are not also included in the fieldList property."),
           @ApiResponse(code = 500, message = "An Error has Occurred")})
   public ResponseEntity<String> updateEntityProperties(
           @ApiParam(value = "Path parameter indicating the Metadata entity type name.", required = true) @PathVariable(value = "entityType") String entityType,
           @ApiParam(value = "A single-level JSON object that contains the properties for the Metadata entity, values can be String, boolean, or a number.", required = true) @RequestBody(required = true) Map<String, Object> entityProperties)
   {
      try
      {
         Map<String, Object> properties = entityProperties;
         if (properties == null)
         {
            properties = new HashMap<>();
         }
         if (!entityType.equals(properties.get("Name")))
         {
            LOGGER.warn("Overwriting Name property to match Entity Type");
            properties.put("Name", entityType);
         }
         this.validateEntity(entityType, properties);
         String id = Neo4JUtils.getId(this.getEntity(entityType).getBody());
         String url = new StringBuilder(neo4jUrlPrefix).append("db/data/node/").append(id).append("/properties")
                 .toString();
         String json;
         if (!properties.isEmpty())
         {
            properties = defaultNullPropertyValues(properties);
            JSONObject jsonObj = new JSONObject(properties);
            json = jsonObj.toJSONString();
            this.updateNeo4JData(json, url);
         }
         return new ResponseEntity<>(HttpStatus.OK);
      }
      catch (AttuneException ae)
      {
         if (ae.getStatusCode() != null)
         {
            return new ResponseEntity<>(ae.getMessage(), ae.getStatusCode());
         }
         throw ae;
      }
      catch (Exception e)
      {
         LOGGER.error(e.getMessage(), e);
         throw new AttuneException(e);
      }
   }

   @RequestMapping(value = "/entity/{entityType}/properties", method = RequestMethod.DELETE)
   @PreAuthorize("hasRole('" + SecurityService.ROLE_USER + "')")
   @ApiOperation(value = "deleteEntityProperties", notes = "Deletes all properties on a Metadata Entity, except Name, isRoot, fieldList, and requiredFields", response = String.class, httpMethod = "DELETE")
   @ApiResponses(value = {
           @ApiResponse(code = 204, message = "Metadata entity properties were successfully deleted, null or empty response."),
           @ApiResponse(code = 400, message = "Invalid request if the entity type doesn't exist"),
           @ApiResponse(code = 500, message = "An Error has Occurred")})
   public ResponseEntity<String> deleteEntityProperties(
           @ApiParam(value = "Path parameter indicating the Metadata entity type name.", required = true) @PathVariable(value = "entityType") String entityType)
   {
      try
      {
         Map<String, Object> props = Neo4JUtils.getProperties(this.getEntityProperties(entityType).getBody());
         for (String propName : props.keySet())
         {
            if (!"Name".equals(propName) && !"isRoot".equals(propName) && !"fieldList".equals(propName)
                    && !"requiredFields".equals(propName))
            {
               this.deleteEntityProperty(entityType, propName);
            }
         }
         return new ResponseEntity<>(HttpStatus.OK);
      }
      catch (AttuneException ae)
      {
         if (ae.getStatusCode() != null)
         {
            return new ResponseEntity<>(ae.getMessage(), ae.getStatusCode());
         }
         throw ae;
      }
      catch (Exception e)
      {
         LOGGER.error(e.getMessage(), e);
         throw new AttuneException(e);
      }
   }

   @RequestMapping(value = "/entity/{entityType}/property/{propName:.+}", method = RequestMethod.GET)
   @PreAuthorize("hasRole('" + SecurityService.ROLE_USER + "')")
   @ApiOperation(value = "getEntityProperty", notes = "Returns the value for the specified property name and Metadata entity type. The body of the response is a single String, boolean, or number value which is still valid JSON.", response = String.class, httpMethod = "GET")
   @ApiResponses(value = {
           @ApiResponse(code = 200, message = "A a single String, boolean, or number for the specified property on the Metadata entity type"),
           @ApiResponse(code = 400, message = "Invalid request, if the Metadata entity type or property name doesn't exist"),
           @ApiResponse(code = 500, message = "An Error has Occurred")})
   public ResponseEntity<String> getEntityProperty(
           @ApiParam(value = "Path parameter indicating the Metadata entity type name.", required = true) @PathVariable(value = "entityType") String entityType,
           @ApiParam(value = "Path parameter for which property's value should be returned.", required = true) @PathVariable(value = "propName") String propName)
   {
      String result = null;
      try
      {
         String id = Neo4JUtils.getId(this.getEntity(entityType).getBody());
         String url = new StringBuilder(neo4jUrlPrefix).append("db/data/node/").append(id).append("/properties/")
                 .append(propName).toString();
         result = this.getNeo4JData(url);
         final HttpHeaders httpHeaders = new HttpHeaders();
         httpHeaders.setContentType(MediaType.APPLICATION_JSON);
         if (result != null)
         {
            return new ResponseEntity<>(JSONValue.toJSONString(parser.parse(result)), httpHeaders, HttpStatus.OK);
         }
         else
         {
            return new ResponseEntity<>(null, httpHeaders, HttpStatus.OK);
         }
      }
      catch (AttuneException ae)
      {
         if (ae.getStatusCode() != null && ae.getStatusCode().equals(HttpStatus.NOT_FOUND))
         {
            return new ResponseEntity<>(HttpStatus.OK);
         }
         else if (ae.getStatusCode() != null)
         {
            return new ResponseEntity<>(ae.getMessage(), ae.getStatusCode());
         }
         throw ae;
      }
      catch (Exception e)
      {
         LOGGER.error(e.getMessage(), e);
         throw new AttuneException(e);
      }
   }

   @RequestMapping(value = "/entity/{entityType}/property/{propName:.+}", method = RequestMethod.PUT, consumes = MediaType.APPLICATION_JSON_VALUE)
   @PreAuthorize("hasRole('" + SecurityService.ROLE_USER + "')")
   @ApiOperation(value = "updateEntityProperty", notes = "Adds or updates the specified property on the Metadata entity. The body of the request should be a single String, boolean, or number value which is still valid JSON.", response = String.class, httpMethod = "PUT")
   @ApiResponses(value = {
           @ApiResponse(code = 204, message = "Metadata entity property was successfully added or updated, empty response"),
           @ApiResponse(code = 400, message = "Invalid request  if the entity type does not exist or property value is null or not a String, boolean, or number."),
           @ApiResponse(code = 500, message = "An Error has Occurred")})
   public ResponseEntity<String> updateEntityProperty(
           @ApiParam(value = "Path parameter indicating the Metadata entity type name.", required = true) @PathVariable(value = "entityType") String entityType,
           @ApiParam(value = "Path parameter for which property's value should be updated.", required = true) @PathVariable(value = "propName") String propName,
           @ApiParam(value = "The property value property's value which should be a single String, boolean, or number value", required = true) @RequestBody(required = true) Object propValue)
   {
      try
      {
         if (propValue == null)
         {
            StringBuilder msg = new StringBuilder("Property values can not be null!");
            throw new AttuneException(msg.toString(), HttpStatus.BAD_REQUEST);
         }
         if ("Name".equals(propName))
         {
            StringBuilder msg = new StringBuilder("Can not change Name property for Metadata Entity type ")
                    .append(entityType);
            throw new AttuneException(msg.toString(), HttpStatus.BAD_REQUEST);
         }
         String id = Neo4JUtils.getId(this.getEntity(entityType).getBody());
         String url = new StringBuilder(neo4jUrlPrefix).append("db/data/node/").append(id).append("/properties/")
                 .append(propName).toString();
         this.updateNeo4JData(JSONValue.toJSONString(propValue), url);
         return new ResponseEntity<>(HttpStatus.OK);
      }
      catch (AttuneException ae)
      {
         if (ae.getStatusCode() != null)
         {
            return new ResponseEntity<>(ae.getMessage(), ae.getStatusCode());
         }
         throw ae;
      }
      catch (Exception e)
      {
         LOGGER.error(e.getMessage(), e);
         throw new AttuneException(e);
      }
   }

   @RequestMapping(value = "/entity/{entityType}/property/{propName:.+}", method = RequestMethod.DELETE)
   @PreAuthorize("hasRole('" + SecurityService.ROLE_USER + "')")
   @ApiOperation(value = "deleteEntityProperty", notes = "Delete the specified property from the Metadata Entity.", response = String.class, httpMethod = "DELETE")
   @ApiResponses(value = {
           @ApiResponse(code = 204, message = "Metadata entity property was successfully deleted, empty response"),
           @ApiResponse(code = 400, message = "Invalid request if the property name is Name, isRoot, fieldList, or requiredFields"),
           @ApiResponse(code = 500, message = "An Error has Occurred")})
   public ResponseEntity<String> deleteEntityProperty(
           @ApiParam(value = "Path parameter indicating the Metadata entity type name.", required = true) @PathVariable(value = "entityType") String entityType,
           @ApiParam(value = "Path parameter for which property's value should be updated.", required = true) @PathVariable(value = "propName") String propName)
   {
      try
      {
         if ("Name".equals(propName))
         {
            StringBuilder msg = new StringBuilder("Metadata entity type ").append(entityType)
                    .append(" must contain a name field!");
            throw new AttuneException(msg.toString(), HttpStatus.BAD_REQUEST);
         }
         if ("isRoot".equals(propName))
         {
            StringBuilder msg = new StringBuilder("Metadata entity type ").append(entityType)
                    .append(" must contain an isRoot field!");
            throw new AttuneException(msg.toString(), HttpStatus.BAD_REQUEST);
         }
         if ("fieldList".equals(propName))
         {
            StringBuilder msg = new StringBuilder("Metadata entity type ").append(entityType)
                    .append(" must contain a fieldList field!");
            throw new AttuneException(msg.toString(), HttpStatus.BAD_REQUEST);
         }
         if ("requiredFields".equals(propName))
         {
            StringBuilder msg = new StringBuilder("Metadata entity type ").append(entityType)
                    .append(" must contain a requiredFields field!");
            throw new AttuneException(msg.toString(), HttpStatus.BAD_REQUEST);
         }
         String id = Neo4JUtils.getId(this.getEntity(entityType).getBody());
         String url = new StringBuilder(neo4jUrlPrefix).append("db/data/node/").append(id).append("/properties/")
                 .append(propName).toString();
         this.deleteNeo4JData(url);
         return new ResponseEntity<>(HttpStatus.OK);
      }
      catch (AttuneException ae)
      {
         if (ae.getStatusCode() != null)
         {
            return new ResponseEntity<>(ae.getMessage(), ae.getStatusCode());
         }
         throw ae;
      }
      catch (Exception e)
      {
         LOGGER.error(e.getMessage(), e);
         throw new AttuneException(e);
      }
   }

   @SuppressWarnings("unchecked")
   @RequestMapping(value = "/entity/{entityType}/relationships/all", method = RequestMethod.GET)
   @PreAuthorize("hasRole('" + SecurityService.ROLE_USER + "')")
   @ApiOperation(value = "getRelationshipsAll", notes = "Gets all relationships for the specified Metadata entity type.", response = String.class, httpMethod = "GET")
   @ApiResponses(value = {
           @ApiResponse(code = 200, message = "JSON array of relationships for the Metadata entity type, or an empty JSON array if none exist"),
           @ApiResponse(code = 400, message = "Invalid request"),
           @ApiResponse(code = 500, message = "An Error has Occurred")})
   public ResponseEntity<String> getRelationshipsAll(
           @ApiParam(value = "Path parameter indicating the Metadata entity type name.", required = true) @PathVariable(value = "entityType") String entityType,
           @ApiParam(value = "Optional query parameter indicating the relationship types to filter by. Multiple values can be specified by repeating the parameter in the URL, "
                   + "for example http://localhost:8080/attune/api/metadata/entity/{entityType}/relationships/all?relType=has&relType=likes&relType=uses"
                   + ", would return all 'has', 'likes', and 'uses' relationships.", required = false) @RequestParam(value = "relType", required = false) List<String> relType)
   {
      String result = null;
      try
      {
         String id = Neo4JUtils.getId(this.getEntity(entityType).getBody());
         StringBuilder sb = new StringBuilder(neo4jUrlPrefix).append("db/data/node/").append(id)
                 .append("/relationships/all");
         String url = getUrlForRelTypes(sb, relType);
         result = this.getNeo4JData(url);
         if (result != null)
         {
            JSONArray resultArray = new JSONArray();
            JSONArray array;
            synchronized (parser)
            {
               array = (JSONArray) parser.parse(result);
            }
            for (int i = 0; i < array.size(); i++)
            {
               JSONObject resultJson = (JSONObject) array.get(i);
               Map<String, Object> data = Neo4JUtils.getDataProperties(resultJson.toJSONString());
               JSONObject relationshipJson = new JSONObject(data);
               relationshipJson.put("neo4j.id", Neo4JUtils.getId(resultJson.toJSONString()));

               String metadataRelationshipStartId = Neo4JUtils.getRelationshipStartNode(resultJson.toJSONString());
               String metadataRelationshipEndId = Neo4JUtils.getRelationshipEndNode(resultJson.toJSONString());
               relationshipJson.put("relationshipType", Neo4JUtils.getRelationshipType(resultJson.toJSONString()));
               relationshipJson.put("startNodeId", metadataRelationshipStartId);
               relationshipJson.put("startNodeType", "Metadata");
               relationshipJson.put("startNodeName", Neo4JUtils.getDataValue(
                       this.getEntity(Long.parseLong(metadataRelationshipStartId)).getBody(), "Name"));
               relationshipJson.put("endNodeId", metadataRelationshipEndId);
               relationshipJson.put("endNodeType", "Metadata");
               relationshipJson.put("endNodeName", Neo4JUtils
                       .getDataValue(this.getEntity(Long.parseLong(metadataRelationshipEndId)).getBody(), "Name"));
               resultArray.add(relationshipJson);
            }
            result = resultArray.toJSONString();
         }
         final HttpHeaders httpHeaders = new HttpHeaders();
         httpHeaders.setContentType(MediaType.APPLICATION_JSON);
         if (result != null)
         {
            return new ResponseEntity<>(((JSONAware) parser.parse(result)).toJSONString(), httpHeaders,
                    HttpStatus.OK);
         }
         else
         {
            return new ResponseEntity<>(null, httpHeaders, HttpStatus.OK);
         }
      }
      catch (AttuneException ae)
      {
         if (ae.getStatusCode() != null)
         {
            return new ResponseEntity<>(ae.getMessage(), ae.getStatusCode());
         }
         throw ae;
      }
      catch (Exception e)
      {
         LOGGER.error(e.getMessage(), e);
         throw new AttuneException(e);
      }
   }

   @RequestMapping(value = "/entity/{entityType}/relationships/all", method = RequestMethod.DELETE)
   @PreAuthorize("hasRole('" + SecurityService.ROLE_USER + "')")
   @ApiOperation(value = "deleteRelationshipsAll", notes = "Deletes all relationships for the specified Metadata entity type.", response = String.class, httpMethod = "DELETE")
   @ApiResponses(value = {
           @ApiResponse(code = 204, message = "Relationships were sucessfully deleted, empty response"),
           @ApiResponse(code = 400, message = "Invalid request"),
           @ApiResponse(code = 500, message = "An Error has Occurred")})
   public ResponseEntity<String> deleteRelationshipsAll(
           @ApiParam(value = "Path parameter indicating the Metadata entity type name.", required = true) @PathVariable(value = "entityType") String entityType,
           @ApiParam(value = "Optional query parameter indicating the relationship types to delete. Multiple values can be specified by repeating the parameter in the URL, "
                   + "for example http://localhost:8080/attune/api/metadata/entity/{entityType}/relationships/all?relType=has&relType=likes&relType=uses"
                   + ", would delete all 'has', 'likes', and 'uses' relationships.", required = false) @RequestParam(value = "relType", required = false) List<String> relType)
   {
      try
      {
         String relsJson = this.getRelationshipsAll(entityType, relType).getBody();
         if (relsJson != null)
         {
            JSONArray relationships;
            synchronized (parser)
            {
               relationships = (JSONArray) parser.parse(relsJson);
            }
            // Delete each of the relationships
            for (int y = 0; y < relationships.size(); y++)
            {
               JSONObject relationshipJsonObj = (JSONObject) relationships.get(y);
               String relationshipNodeId = Neo4JUtils.getId(relationshipJsonObj.toJSONString());
               this.deleteRelationship(Long.parseLong(relationshipNodeId));
            }
         }
         return new ResponseEntity<>(HttpStatus.OK);
      }
      catch (AttuneException ae)
      {
         if (ae.getStatusCode() != null)
         {
            return new ResponseEntity<>(ae.getMessage(), ae.getStatusCode());
         }
         throw ae;
      }
      catch (Exception e)
      {
         LOGGER.error(e.getMessage(), e);
         throw new AttuneException(e);
      }
   }

   @RequestMapping(value = "/entity/{startEntityType}/relationships/{endEntityType}", method = RequestMethod.DELETE)
   @PreAuthorize("hasRole('" + SecurityService.ROLE_USER + "')")
   @ApiOperation(value = "deleteRelationshipsAll", notes = "Deletes all relationships for the specified starting and ending Metadata entity types.", response = String.class, httpMethod = "DELETE")
   @ApiResponses(value = {
           @ApiResponse(code = 204, message = "Relationships were sucessfully deleted, empty response"),
           @ApiResponse(code = 400, message = "Invalid request if the entity types are invalid"),
           @ApiResponse(code = 500, message = "An Error has Occurred")})
   public ResponseEntity<String> deleteRelationshipsAll(
           @ApiParam(value = "Path parameter indicating the starting Metadata entity type name.", required = true) @PathVariable(value = "startEntityType") String startEntityType,
           @ApiParam(value = "Path parameter indicating the ending Metadata entity type name.", required = true) @PathVariable(value = "endEntityType") String endEntityType,
           @ApiParam(value = "Optional query parameter indicating the relationship types to delete. Multiple values can be specified by repeating the parameter in the URL, "
                   + "for example http://localhost:8080/attune/api/metadata/entity/{entityType}/relationships/all?relType=has&relType=likes&relType=uses"
                   + ", would delete all 'has', 'likes', and 'uses' relationships.", required = false) @RequestParam(value = "relType", required = false) List<String> relType)
   {
      try
      {
         String relsJson = this.getRelationships(startEntityType, endEntityType, relType, null).getBody();
         if (relsJson != null)
         {
            JSONArray relationships;
            synchronized (parser)
            {
               relationships = (JSONArray) parser.parse(relsJson);
            }
            // Delete each of the relationships
            for (int y = 0; y < relationships.size(); y++)
            {
               JSONObject relationshipJsonObj = (JSONObject) relationships.get(y);
               String relationshipNodeId = Neo4JUtils.getId(relationshipJsonObj.toJSONString());
               this.deleteRelationship(Long.parseLong(relationshipNodeId));
            }
         }
         return new ResponseEntity<>(HttpStatus.OK);
      }
      catch (AttuneException ae)
      {
         if (ae.getStatusCode() != null)
         {
            return new ResponseEntity<>(ae.getMessage(), ae.getStatusCode());
         }
         throw ae;
      }
      catch (Exception e)
      {
         LOGGER.error(e.getMessage(), e);
         throw new AttuneException(e);
      }
   }

   @SuppressWarnings("unchecked")
   @RequestMapping(value = "/entity/{entityType}/relationships/in", method = RequestMethod.GET)
   @PreAuthorize("hasRole('" + SecurityService.ROLE_USER + "')")
   @ApiOperation(value = "getRelationshipsIn", notes = "Gets all inbound relationships for the specified Metadata entity type.", response = String.class, httpMethod = "GET")
   @ApiResponses(value = {
           @ApiResponse(code = 200, message = "JSON array of relationships for the Metadata entity type, or an empty JSON array if none exist"),
           @ApiResponse(code = 400, message = "Invalid request"),
           @ApiResponse(code = 500, message = "An Error has Occurred")})
   public ResponseEntity<String> getRelationshipsIn(
           @ApiParam(value = "Path parameter indicating the Metadata entity type name.", required = true) @PathVariable(value = "entityType") String entityType,
           @ApiParam(value = "Optional query parameter indicating the relationship types to filter by. Multiple values can be specified by repeating the parameter in the URL, "
                   + "for example http://localhost:8080/attune/api/metadata/entity/{entityType}/relationships/all?relType=has&relType=likes&relType=uses"
                   + ", would return all 'has', 'likes', and 'uses' inbound relationships.", required = false) @RequestParam(value = "relType", required = false) List<String> relType)
   {
      String result = null;
      try
      {
         String id = Neo4JUtils.getId(this.getEntity(entityType).getBody());
         StringBuilder sb = new StringBuilder(neo4jUrlPrefix).append("db/data/node/").append(id)
                 .append("/relationships/in");
         String url = getUrlForRelTypes(sb, relType);
         result = this.getNeo4JData(url);
         if (result != null)
         {
            JSONArray resultArray = new JSONArray();
            JSONArray array;
            synchronized (parser)
            {
               array = (JSONArray) parser.parse(result);
            }
            for (int i = 0; i < array.size(); i++)
            {
               JSONObject resultJson = (JSONObject) array.get(i);
               Map<String, Object> data = Neo4JUtils.getDataProperties(resultJson.toJSONString());
               JSONObject relationshipJson = new JSONObject(data);
               relationshipJson.put("neo4j.id", Neo4JUtils.getId(resultJson.toJSONString()));

               String metadataRelationshipStartId = Neo4JUtils.getRelationshipStartNode(resultJson.toJSONString());
               String metadataRelationshipEndId = Neo4JUtils.getRelationshipEndNode(resultJson.toJSONString());
               relationshipJson.put("relationshipType", Neo4JUtils.getRelationshipType(resultJson.toJSONString()));
               relationshipJson.put("startNodeId", metadataRelationshipStartId);
               relationshipJson.put("startNodeType", "Metadata");
               relationshipJson.put("startNodeName", Neo4JUtils.getDataValue(
                       this.getEntity(Long.parseLong(metadataRelationshipStartId)).getBody(), "Name"));
               relationshipJson.put("endNodeId", metadataRelationshipEndId);
               relationshipJson.put("endNodeType", "Metadata");
               relationshipJson.put("endNodeName", Neo4JUtils
                       .getDataValue(this.getEntity(Long.parseLong(metadataRelationshipEndId)).getBody(), "Name"));
               resultArray.add(relationshipJson);
            }
            result = resultArray.toJSONString();
         }
         final HttpHeaders httpHeaders = new HttpHeaders();
         httpHeaders.setContentType(MediaType.APPLICATION_JSON);
         if (result != null)
         {
            return new ResponseEntity<>(((JSONAware) parser.parse(result)).toJSONString(), httpHeaders,
                    HttpStatus.OK);
         }
         else
         {
            return new ResponseEntity<>(null, httpHeaders, HttpStatus.OK);
         }
      }
      catch (AttuneException ae)
      {
         if (ae.getStatusCode() != null)
         {
            return new ResponseEntity<>(ae.getMessage(), ae.getStatusCode());
         }
         throw ae;
      }
      catch (Exception e)
      {
         LOGGER.error(e.getMessage(), e);
         throw new AttuneException(e);
      }
   }

   @RequestMapping(value = "/entity/{entityType}/relationships/in", method = RequestMethod.DELETE)
   @PreAuthorize("hasRole('" + SecurityService.ROLE_USER + "')")
   @ApiOperation(value = "deleteRelationshipsIn", notes = "Deletes all inbound relationships for the specified Metadata entity type.", response = String.class, httpMethod = "DELETE")
   @ApiResponses(value = {
           @ApiResponse(code = 204, message = "Relationships were sucessfully deleted, empty response"),
           @ApiResponse(code = 400, message = "Invalid request"),
           @ApiResponse(code = 500, message = "An Error has Occurred")})
   public ResponseEntity<String> deleteRelationshipsIn(
           @ApiParam(value = "Path parameter indicating the Metadata entity type name.", required = true) @PathVariable(value = "entityType") String entityType,
           @ApiParam(value = "Optional query parameter indicating the relationship types to filter by. Multiple values can be specified by repeating the parameter in the URL, "
                   + "for example http://localhost:8080/attune/api/metadata/entity/{entityType}/relationships/all?relType=has&relType=likes&relType=uses"
                   + ", would delete all 'has', 'likes', and 'uses' IN relationships.", required = false) @RequestParam(value = "relType", required = false) List<String> relType)
   {
      try
      {
         String relsJson = this.getRelationshipsIn(entityType, relType).getBody();
         if (relsJson != null)
         {
            JSONArray relationships;
            synchronized (parser)
            {
               relationships = (JSONArray) parser.parse(relsJson);
            }
            // Delete each of the relationships
            for (int y = 0; y < relationships.size(); y++)
            {
               JSONObject relationshipJsonObj = (JSONObject) relationships.get(y);
               String relationshipNodeId = Neo4JUtils.getId(relationshipJsonObj.toJSONString());
               this.deleteRelationship(Long.parseLong(relationshipNodeId));
            }
         }
         return new ResponseEntity<>(HttpStatus.OK);
      }
      catch (AttuneException ae)
      {
         if (ae.getStatusCode() != null)
         {
            return new ResponseEntity<>(ae.getMessage(), ae.getStatusCode());
         }
         throw ae;
      }
      catch (Exception e)
      {
         LOGGER.error(e.getMessage(), e);
         throw new AttuneException(e);
      }
   }

   @SuppressWarnings("unchecked")
   @RequestMapping(value = "/entity/{entityType}/relationships/out", method = RequestMethod.GET)
   @PreAuthorize("hasRole('" + SecurityService.ROLE_USER + "')")
   @ApiOperation(value = "getRelationshipsOut", notes = "Gets all outbound relationships for the specified Metadata entity type.", response = String.class, httpMethod = "GET")
   @ApiResponses(value = {
           @ApiResponse(code = 200, message = "JSON array of relationships for the Metadata entity type, or an empty JSON array if none exist"),
           @ApiResponse(code = 400, message = "Invalid request"),
           @ApiResponse(code = 500, message = "An Error has Occurred")})
   public ResponseEntity<String> getRelationshipsOut(
           @ApiParam(value = "Path parameter indicating the Metadata entity type name.", required = true) @PathVariable(value = "entityType") String entityType,
           @ApiParam(value = "Optional query parameter indicating the relationship types to filter by. Multiple values can be specified by repeating the parameter in the URL, "
                   + "for example http://localhost:8080/attune/api/metadata/entity/{entityType}/relationships/all?relType=has&relType=likes&relType=uses"
                   + ", would return all 'has', 'likes', and 'uses' outbound relationships.", required = false) @RequestParam(value = "relType", required = false) List<String> relType)
   {
      String result = null;
      try
      {
         String id = Neo4JUtils.getId(this.getEntity(entityType).getBody());
         StringBuilder sb = new StringBuilder(neo4jUrlPrefix).append("db/data/node/").append(id)
                 .append("/relationships/out");
         String url = getUrlForRelTypes(sb, relType);
         result = this.getNeo4JData(url);
         if (result != null)
         {
            JSONArray resultArray = new JSONArray();
            JSONArray array;
            synchronized (parser)
            {
               array = (JSONArray) parser.parse(result);
            }
            for (int i = 0; i < array.size(); i++)
            {
               JSONObject resultJson = (JSONObject) array.get(i);
               Map<String, Object> data = Neo4JUtils.getDataProperties(resultJson.toJSONString());
               JSONObject relationshipJson = new JSONObject(data);
               relationshipJson.put("neo4j.id", Neo4JUtils.getId(resultJson.toJSONString()));

               String metadataRelationshipStartId = Neo4JUtils.getRelationshipStartNode(resultJson.toJSONString());
               String metadataRelationshipEndId = Neo4JUtils.getRelationshipEndNode(resultJson.toJSONString());
               relationshipJson.put("relationshipType", Neo4JUtils.getRelationshipType(resultJson.toJSONString()));
               relationshipJson.put("startNodeId", metadataRelationshipStartId);
               relationshipJson.put("startNodeType", "Metadata");
               relationshipJson.put("startNodeName", Neo4JUtils.getDataValue(
                       this.getEntity(Long.parseLong(metadataRelationshipStartId)).getBody(), "Name"));
               relationshipJson.put("endNodeId", metadataRelationshipEndId);
               relationshipJson.put("endNodeType", "Metadata");
               relationshipJson.put("endNodeName", Neo4JUtils
                       .getDataValue(this.getEntity(Long.parseLong(metadataRelationshipEndId)).getBody(), "Name"));
               resultArray.add(relationshipJson);
            }
            result = resultArray.toJSONString();
         }
         final HttpHeaders httpHeaders = new HttpHeaders();
         httpHeaders.setContentType(MediaType.APPLICATION_JSON);
         if (result != null)
         {
            return new ResponseEntity<>(((JSONAware) parser.parse(result)).toJSONString(), httpHeaders,
                    HttpStatus.OK);
         }
         else
         {
            return new ResponseEntity<>(null, httpHeaders, HttpStatus.OK);
         }
      }
      catch (AttuneException ae)
      {
         if (ae.getStatusCode() != null)
         {
            return new ResponseEntity<>(ae.getMessage(), ae.getStatusCode());
         }
         throw ae;
      }
      catch (Exception e)
      {
         LOGGER.error(e.getMessage(), e);
         throw new AttuneException(e);
      }
   }

   @RequestMapping(value = "/entity/{entityType}/relationships/out", method = RequestMethod.DELETE)
   @PreAuthorize("hasRole('" + SecurityService.ROLE_USER + "')")
   @ApiOperation(value = "deleteRelationshipsOut", notes = "Deletes all outbound relationships for the specified Metadata entity type.", response = String.class, httpMethod = "DELETE")
   @ApiResponses(value = {
           @ApiResponse(code = 204, message = "Relationships were sucessfully deleted, empty response"),
           @ApiResponse(code = 400, message = "Invalid request"),
           @ApiResponse(code = 500, message = "An Error has Occurred")})
   public ResponseEntity<String> deleteRelationshipsOut(
           @ApiParam(value = "Path parameter indicating the Metadata entity type name.", required = true) @PathVariable(value = "entityType") String entityType,
           @ApiParam(value = "Optional query parameter indicating the relationship types to filter by. Multiple values can be specified by repeating the parameter in the URL, "
                   + "for example http://localhost:8080/attune/api/metadata/entity/{entityType}/relationships/all?relType=has&relType=likes&relType=uses"
                   + ", would delete all 'has', 'likes', and 'uses' OUT relationships.", required = false) @RequestParam(value = "relType", required = false) List<String> relType)
   {
      try
      {
         String relsJson = this.getRelationshipsOut(entityType, relType).getBody();
         if (relsJson != null)
         {
            JSONArray relationships;
            synchronized (parser)
            {
               relationships = (JSONArray) parser.parse(relsJson);
            }
            // Delete each of the relationships
            for (int y = 0; y < relationships.size(); y++)
            {
               JSONObject relationshipJsonObj = (JSONObject) relationships.get(y);
               String relationshipNodeId = Neo4JUtils.getId(relationshipJsonObj.toJSONString());
               this.deleteRelationship(Long.parseLong(relationshipNodeId));
            }
         }
         return new ResponseEntity<>(HttpStatus.OK);
      }
      catch (AttuneException ae)
      {
         if (ae.getStatusCode() != null)
         {
            return new ResponseEntity<>(ae.getMessage(), ae.getStatusCode());
         }
         throw ae;
      }
      catch (Exception e)
      {
         LOGGER.error(e.getMessage(), e);
         throw new AttuneException(e);
      }
   }

   @SuppressWarnings("unchecked")
   @RequestMapping(value = "/relationship/types", method = RequestMethod.GET)
   @PreAuthorize("hasRole('" + SecurityService.ROLE_USER + "')")
   @ApiOperation(value = "getRelationshipTypes", notes = "Returns all relationship types that have been defined for all Metadata entities.", response = String.class, httpMethod = "GET")
   @ApiResponses(value = {
           @ApiResponse(code = 200, message = "A JSON array of Strings that contains all relationship types, or an empty array if none exist"),
           @ApiResponse(code = 500, message = "An Error has Occurred")})
   public ResponseEntity<String> getRelationshipTypes()
   {
      String result = null;
      try
      {
         List<String> relTypes = new ArrayList<>();
         List<String> entityTypes = (List<String>) Neo4JUtils.getValue(this.getEntityTypes().getBody());
         for (String entityType : entityTypes)
         {
            String relsJson = this.getRelationshipsAll(entityType, null).getBody();
            if (relsJson != null)
            {
               JSONArray relationships;
               synchronized (parser)
               {
                  relationships = (JSONArray) parser.parse(relsJson);
               }
               for (int i = 0; i < relationships.size(); i++)
               {
                  JSONObject relationship = (JSONObject) relationships.get(i);
                  String relType = Neo4JUtils.getRelationshipType(relationship.toJSONString());
                  if (!relTypes.contains(relType))
                  {
                     relTypes.add(relType);
                  }
               }
            }
         }
         result = JSONValue.toJSONString(relTypes);
         final HttpHeaders httpHeaders = new HttpHeaders();
         httpHeaders.setContentType(MediaType.APPLICATION_JSON);
         if (result != null)
         {
            return new ResponseEntity<>(((JSONAware) parser.parse(result)).toJSONString(), httpHeaders,
                    HttpStatus.OK);
         }
         else
         {
            return new ResponseEntity<>(null, httpHeaders, HttpStatus.OK);
         }
      }
      catch (AttuneException ae)
      {
         if (ae.getStatusCode() != null)
         {
            return new ResponseEntity<>(ae.getMessage(), ae.getStatusCode());
         }
         throw ae;
      }
      catch (Exception e)
      {
         LOGGER.error(e.getMessage(), e);
         throw new AttuneException(e);
      }
   }

   @SuppressWarnings("unchecked")
   @RequestMapping(value = "/relationship/{id}", method = RequestMethod.GET)
   @PreAuthorize("hasRole('" + SecurityService.ROLE_USER + "')")
   @ApiOperation(value = "getRelationship", notes = "Returns the JSON Metadata entity relationship for the specificed Neo4J id.", response = String.class, httpMethod = "GET")
   @ApiResponses(value = {
           @ApiResponse(code = 200, message = "Returns a JSON object representing the relationship, which contains the neo4j.id, relationshipType, startNodeId, startNodeType, startNodeName, endNodeId, endNodeType, and endNodeName"),
           @ApiResponse(code = 400, message = "Invalid request if the id is invalid"),
           @ApiResponse(code = 500, message = "An Error has Occurred")})
   public ResponseEntity<String> getRelationship(
           @ApiParam(value = "The relationship ID.", required = true) @PathVariable(value = "id") Long id)
   {
      String result = null;
      try
      {
         String url = new StringBuilder(neo4jUrlPrefix).append("db/data/relationship/").append(String.valueOf(id))
                 .toString();
         result = this.getNeo4JData(url);
         if (result != null)
         {
            JSONObject resultJson;
            synchronized (parser)
            {
               resultJson = (JSONObject) parser.parse(result);
            }
            Map<String, Object> data = Neo4JUtils.getDataProperties(resultJson.toJSONString());
            JSONObject relationshipJson = new JSONObject(data);
            relationshipJson.put("neo4j.id", Neo4JUtils.getId(resultJson.toJSONString()));

            String metadataRelationshipStartId = Neo4JUtils.getRelationshipStartNode(resultJson.toJSONString());
            String metadataRelationshipEndId = Neo4JUtils.getRelationshipEndNode(resultJson.toJSONString());
            relationshipJson.put("relationshipType", Neo4JUtils.getRelationshipType(resultJson.toJSONString()));
            relationshipJson.put("startNodeId", metadataRelationshipStartId);
            relationshipJson.put("startNodeType", "Metadata");
            relationshipJson.put("startNodeName", Neo4JUtils
                    .getDataValue(this.getEntity(Long.parseLong(metadataRelationshipStartId)).getBody(), "Name"));
            relationshipJson.put("endNodeId", metadataRelationshipEndId);
            relationshipJson.put("endNodeType", "Metadata");
            relationshipJson.put("endNodeName", Neo4JUtils
                    .getDataValue(this.getEntity(Long.parseLong(metadataRelationshipEndId)).getBody(), "Name"));
            result = relationshipJson.toJSONString();
         }
         final HttpHeaders httpHeaders = new HttpHeaders();
         httpHeaders.setContentType(MediaType.APPLICATION_JSON);
         if (result != null)
         {
            return new ResponseEntity<>(((JSONAware) parser.parse(result)).toJSONString(), httpHeaders,
                    HttpStatus.OK);
         }
         else
         {
            return new ResponseEntity<>(null, httpHeaders, HttpStatus.OK);
         }
      }
      catch (AttuneException ae)
      {
         if (ae.getStatusCode() != null && ae.getStatusCode().equals(HttpStatus.NOT_FOUND))
         {
            return new ResponseEntity<>(HttpStatus.OK);
         }
         else if (ae.getStatusCode() != null)
         {
            return new ResponseEntity<>(ae.getMessage(), ae.getStatusCode());
         }
         throw ae;
      }
      catch (Exception e)
      {
         LOGGER.error(e.getMessage(), e);
         throw new AttuneException(e);
      }
   }

   @SuppressWarnings("unchecked")
   @RequestMapping(value = "/entity/{startEntityType}/relationships/{endEntityType}", method = RequestMethod.GET)
   @PreAuthorize("hasRole('" + SecurityService.ROLE_USER + "')")
   @ApiOperation(value = "getRelationships", notes = "Returns the JSON Metadata entity relationship for the specified starting Metadata entity type and ending Metadata entity type.", response = String.class, httpMethod = "GET")
   @ApiResponses(value = {
           @ApiResponse(code = 200, message = "A JSON array containing the relationships, or an empty array if none exist"),
           @ApiResponse(code = 400, message = "Invalid request if the entity type names are invalid"),
           @ApiResponse(code = 500, message = "An Error has Occurred")})
   public ResponseEntity<String> getRelationships(
           @ApiParam(value = "Path parameter indicating the starting Metadata entity type name.", required = true) @PathVariable(value = "startEntityType") String startEntityType,
           @ApiParam(value = "Path parameter indicating the ending Metadata entity type name.", required = true) @PathVariable(value = "endEntityType") String endEntityType,
           @ApiParam(value = "Optional query parameter indicating the relationship types to get. Multiple values can be specified by repeating the parameter in the URL, "
                   + "for example http://localhost:8080/attune/api/metadata/entity/{entityType}/relationships/all?relType=has&relType=likes&relType=uses"
                   + ", would return all 'has', 'likes', and 'uses' relationships.", required = false) @RequestParam(value = "relType", required = false) List<String> relType,
           @ApiParam(value = "Optional Query parameter indicating the relationship direction. Valid values are \"in\", \"out\", and \"all\".", required = false) @RequestParam(value = "relDir", required = false) String relDir)
   {
      String result = null;
      try
      {
         JSONArray results = new JSONArray();
         String allRelsJson;
         if (relDir != null)
         {
            if ("all".equals(relDir))
            {
               allRelsJson = this.getRelationshipsAll(startEntityType, relType).getBody();
            }
            else if ("out".equals(relDir))
            {
               allRelsJson = this.getRelationshipsOut(startEntityType, relType).getBody();
            }
            else if ("in".equals(relDir))
            {
               allRelsJson = this.getRelationshipsIn(startEntityType, relType).getBody();
            }
            else
            {
               StringBuilder msg = new StringBuilder("Query parameter relDir must equal all, out, or in.");
               throw new AttuneException(msg.toString(), HttpStatus.BAD_REQUEST);
            }
         }
         else
         {
            allRelsJson = this.getRelationshipsAll(startEntityType, relType).getBody();
         }

         if (allRelsJson != null)
         {
            JSONArray jsonArray;
            synchronized (parser)
            {
               jsonArray = (JSONArray) parser.parse(allRelsJson);
            }
            for (int i = 0; i < jsonArray.size(); i++)
            {
               JSONObject jsonObject = (JSONObject) jsonArray.get(i);
               String relEndEntityId = Neo4JUtils.getRelationshipEndNode(jsonObject.toJSONString());
               String relEndEntityType = Neo4JUtils
                       .getDataValue(this.getEntity(Long.parseLong(relEndEntityId)).getBody(), "Name");
               if (endEntityType.equals(relEndEntityType))
               {
                  results.add(jsonObject);
               }
            }
            result = results.toJSONString();
         }
         final HttpHeaders httpHeaders = new HttpHeaders();
         httpHeaders.setContentType(MediaType.APPLICATION_JSON);
         if (result != null)
         {
            return new ResponseEntity<>(((JSONAware) parser.parse(result)).toJSONString(), httpHeaders,
                    HttpStatus.OK);
         }
         else
         {
            return new ResponseEntity<>(null, httpHeaders, HttpStatus.OK);
         }
      }
      catch (AttuneException ae)
      {
         if (ae.getStatusCode() != null)
         {
            return new ResponseEntity<>(ae.getMessage(), ae.getStatusCode());
         }
         throw ae;
      }
      catch (Exception e)
      {
         LOGGER.error(e.getMessage(), e);
         throw new AttuneException(e);
      }
   }

   @SuppressWarnings({"unchecked"})
   @RequestMapping(value = "/entity/{startEntityType}/relationships/{relationshipType}/{endEntityType}", method = RequestMethod.POST, consumes = MediaType.APPLICATION_JSON_VALUE)
   @PreAuthorize("hasRole('" + SecurityService.ROLE_USER + "')")
   @ApiOperation(value = "insertRelationship", notes = "Creates a relationship between the specified starting and ending Metadata entity types. "
           + "The body of the request may optionally contain a single-level JSON object that contains properties for the relationships, "
           + "such as {\"required\": true} to indicate that this is a required relationships. Other user-defined properties may be specified as well.", response = String.class, httpMethod = "POST")
   @ApiResponses(value = {
           @ApiResponse(code = 200, message = "Returns a JSON object representing the relationship that was just created, which contains the neo4j.id, relationshipType, startNodeId, startNodeType, startNodeName, endNodeId, endNodeType, and endNodeName"),
           @ApiResponse(code = 400, message = "Invalid request if the entity type names are invalid"),
           @ApiResponse(code = 500, message = "An Error has Occurred")})
   public ResponseEntity<String> insertRelationship(
           @ApiParam(value = "Path parameter indicating the starting Metadata entity type name.", required = true) @PathVariable(value = "startEntityType") String startEntityType,
           @ApiParam(value = "Path parameter indicating the relationship type.", required = true) @PathVariable(value = "relationshipType") String relationshipType,
           @ApiParam(value = "Path parameter indicating the ending Metadata entity type name.", required = true) @PathVariable(value = "endEntityType") String endEntityType,
           @RequestBody(required = false) Map<String, Object> relationshipProperties)
   {
      String result = null;
      try
      {
         Map<String, Object> properties = relationshipProperties;
         String id = Neo4JUtils.getId(this.getEntity(startEntityType).getBody());
         String endId = Neo4JUtils.getId(this.getEntity(endEntityType).getBody());
         if (properties == null)
         {
            properties = new HashMap<>();
         }
         if (!properties.containsKey("required"))
         {
            properties.put("required", false);
         }
         this.validateRelationship(startEntityType, relationshipType, endEntityType, properties);

         String existingRelationshipId = this.getExistingRelationshipId(startEntityType, relationshipType,
                 endEntityType, properties);
         if (existingRelationshipId != null)
         {
            Map<String, Object> props = Neo4JUtils
                    .getDataProperties(this.getRelationship(Long.parseLong(existingRelationshipId)).getBody());
            if (props != null && !props.isEmpty())
            {
               props.putAll(properties);
            }
            else
            {
               props = properties;
            }
            this.updateRelationshipProperties(Long.parseLong(existingRelationshipId), props);
            return this.getRelationship(Long.parseLong(existingRelationshipId));
         }

         JSONObject jsonObj = new JSONObject();
         jsonObj.put("to", new StringBuilder(neo4jUrlPrefix).append("db/data/node/").append(endId).toString());
         jsonObj.put("type", relationshipType);
         if (!properties.isEmpty())
         {
            properties = defaultNullPropertyValues(properties);
            jsonObj.put("data", properties);
         }
         String json = jsonObj.toJSONString();
         String url = new StringBuilder(neo4jUrlPrefix).append("db/data/node/").append(id).append("/relationships")
                 .toString();
         result = this.insertNeo4JData(json, url);
         if (result != null)
         {
            JSONObject resultJson;
            synchronized (parser)
            {
               resultJson = (JSONObject) parser.parse(result);
            }
            Map<String, Object> data = Neo4JUtils.getDataProperties(resultJson.toJSONString());
            JSONObject relationshipJson = new JSONObject(data);
            relationshipJson.put("neo4j.id", Neo4JUtils.getId(resultJson.toJSONString()));

            String metadataRelationshipStartId = Neo4JUtils.getRelationshipStartNode(resultJson.toJSONString());
            String metadataRelationshipEndId = Neo4JUtils.getRelationshipEndNode(resultJson.toJSONString());
            relationshipJson.put("relationshipType", Neo4JUtils.getRelationshipType(resultJson.toJSONString()));
            relationshipJson.put("startNodeId", metadataRelationshipStartId);
            relationshipJson.put("startNodeType", "Metadata");
            relationshipJson.put("startNodeName", Neo4JUtils
                    .getDataValue(this.getEntity(Long.parseLong(metadataRelationshipStartId)).getBody(), "Name"));
            relationshipJson.put("endNodeId", metadataRelationshipEndId);
            relationshipJson.put("endNodeType", "Metadata");
            relationshipJson.put("endNodeName", Neo4JUtils
                    .getDataValue(this.getEntity(Long.parseLong(metadataRelationshipEndId)).getBody(), "Name"));
            result = relationshipJson.toJSONString();
         }
         final HttpHeaders httpHeaders = new HttpHeaders();
         httpHeaders.setContentType(MediaType.APPLICATION_JSON);
         if (result != null)
         {
            return new ResponseEntity<>(((JSONAware) parser.parse(result)).toJSONString(), httpHeaders,
                    HttpStatus.OK);
         }
         else
         {
            return new ResponseEntity<>(null, httpHeaders, HttpStatus.OK);
         }
      }
      catch (AttuneException ae)
      {
         if (ae.getStatusCode() != null)
         {
            return new ResponseEntity<>(ae.getMessage(), ae.getStatusCode());
         }
         throw ae;
      }
      catch (Exception e)
      {
         LOGGER.error(e.getMessage(), e);
         throw new AttuneException(e);
      }
   }

   @RequestMapping(value = "/relationship/{id}", method = RequestMethod.DELETE)
   @PreAuthorize("hasRole('" + SecurityService.ROLE_USER + "')")
   @ApiOperation(value = "deleteRelationship", notes = "Deletes the Metadata entity relationship for the specificed Neo4J id.", response = String.class, httpMethod = "DELETE")
   @ApiResponses(value = {
           @ApiResponse(code = 204, message = "If the relationship was successfully deleted, null or empty response."),
           @ApiResponse(code = 400, message = "Invalid request if the id is invalid"),
           @ApiResponse(code = 500, message = "An Error has Occurred")})
   public ResponseEntity<String> deleteRelationship(
           @ApiParam(value = "The relationship ID.", required = true) @PathVariable(value = "id") Long id)
   {
      try
      {
         this.validateRelationshipDeletion(id);
         String url = new StringBuilder(neo4jUrlPrefix).append("db/data/relationship/").append(id).toString();
         this.deleteNeo4JData(url);
         return new ResponseEntity<>(HttpStatus.OK);
      }
      catch (AttuneException ae)
      {
         if (ae.getStatusCode() != null)
         {
            return new ResponseEntity<>(ae.getMessage(), ae.getStatusCode());
         }
         throw ae;
      }
      catch (Exception e)
      {
         LOGGER.error(e.getMessage(), e);
         throw new AttuneException(e);
      }
   }

   @RequestMapping(value = "/relationships/{entityType}", method = RequestMethod.DELETE)
   @PreAuthorize("hasRole('" + SecurityService.ROLE_USER + "')")
   @ApiOperation(value = "deleteRelationshipsForType", notes = "Deletes all the Metadata entity relationships for the specificed Metadata entity type.", response = String.class, httpMethod = "DELETE")
   @ApiResponses(value = {
           @ApiResponse(code = 204, message = "If the relationships were successfully deleted, null or empty response."),
           @ApiResponse(code = 400, message = "Invalid request if the entity type name is invalid"),
           @ApiResponse(code = 500, message = "An Error has Occurred")})
   public ResponseEntity<String> deleteRelationshipsForType(
           @ApiParam(value = "Path parameter indicating the Metadata entity type name.", required = true) @PathVariable(value = "entityType") String entityType,
           @ApiParam(value = "Optional query parameter indicating the relationship types to filter by. Multiple values can be specified by repeating the parameter in the URL, "
                   + "for example http://localhost:8080/attune/api/metadata/entity/{entityType}/relationships/all?relType=has&relType=likes&relType=uses"
                   + ", would delete all 'has', 'likes', and 'uses' relationships.", required = false) @RequestParam(value = "relType", required = false) List<String> relType)
   {
      try
      {
         String nodeRelationshipsJson = this.getRelationshipsAll(entityType, relType).getBody();
         if (nodeRelationshipsJson != null)
         {
            JSONArray relationships;
            synchronized (parser)
            {
               relationships = (JSONArray) parser.parse(nodeRelationshipsJson);
            }
            // Delete each of the relationships
            for (int y = 0; y < relationships.size(); y++)
            {
               JSONObject relationshipJsonObj = (JSONObject) relationships.get(y);
               String relationshipNodeId = Neo4JUtils.getId(relationshipJsonObj.toJSONString());
               this.deleteRelationship(Long.parseLong(relationshipNodeId));
            }
         }
         return new ResponseEntity<>(HttpStatus.OK);
      }
      catch (AttuneException ae)
      {
         if (ae.getStatusCode() != null)
         {
            return new ResponseEntity<>(ae.getMessage(), ae.getStatusCode());
         }
         throw ae;
      }
      catch (Exception e)
      {
         LOGGER.error(e.getMessage(), e);
         throw new AttuneException(e);
      }
   }

   @RequestMapping(value = "/relationships", method = RequestMethod.DELETE)
   @PreAuthorize("hasRole('" + SecurityService.ROLE_USER + "')")
   @ApiOperation(value = "deleteAllRelationships", notes = "Deletes all Metadata entity relationships.", response = String.class, httpMethod = "DELETE")
   @ApiResponses(value = {
           @ApiResponse(code = 204, message = "If the relationships were successfully deleted, null or empty response."),
           @ApiResponse(code = 400, message = "Invalid request if there are any required relationships"),
           @ApiResponse(code = 500, message = "An Error has Occurred")})
   public ResponseEntity<String> deleteAllRelationships()
   {
      try
      {
         // First Get all Node Types
         String allEntityTypesJson = this.getEntityTypes().getBody();
         if (allEntityTypesJson != null)
         {
            JSONArray entityTypes;
            synchronized (parser)
            {
               entityTypes = (JSONArray) parser.parse(allEntityTypesJson);
            }
            // For each type, get all nodes
            for (int i = 0; i < entityTypes.size(); i++)
            {
               String entityType = (String) entityTypes.get(i);
               this.deleteRelationshipsForType(entityType, null);
            }
         }
         return new ResponseEntity<>(HttpStatus.OK);
      }
      catch (AttuneException ae)
      {
         if (ae.getStatusCode() != null)
         {
            return new ResponseEntity<>(ae.getMessage(), ae.getStatusCode());
         }
         throw ae;
      }
      catch (Exception e)
      {
         LOGGER.error(e.getMessage(), e);
         throw new AttuneException(e);
      }
   }

   @RequestMapping(value = "/relationship/{id}/properties", method = RequestMethod.GET)
   @PreAuthorize("hasRole('" + SecurityService.ROLE_USER + "')")
   @ApiOperation(value = "getRelationshipProperties", notes = "Returns all properties for the specified Metadata entity relationship as a single-level JSON Object", response = String.class, httpMethod = "GET")
   @ApiResponses(value = {
           @ApiResponse(code = 200, message = "A JSON object containing the properties for the Metadata entity relationship"),
           @ApiResponse(code = 400, message = "Invalid request, if the id is invalid"),
           @ApiResponse(code = 500, message = "An Error has Occurred")})
   public ResponseEntity<String> getRelationshipProperties(
           @ApiParam(value = "The relationship ID.", required = true) @PathVariable(value = "id") Long id)
   {
      String result = null;
      try
      {
         String url = new StringBuilder(neo4jUrlPrefix).append("db/data/relationship/").append(String.valueOf(id))
                 .append("/properties").toString();
         result = this.getNeo4JData(url);
         final HttpHeaders httpHeaders = new HttpHeaders();
         httpHeaders.setContentType(MediaType.APPLICATION_JSON);
         if (result != null)
         {
            return new ResponseEntity<>(((JSONAware) parser.parse(result)).toJSONString(), httpHeaders,
                    HttpStatus.OK);
         }
         else
         {
            return new ResponseEntity<>(null, httpHeaders, HttpStatus.OK);
         }
      }
      catch (AttuneException ae)
      {
         if (ae.getStatusCode() != null)
         {
            return new ResponseEntity<>(ae.getMessage(), ae.getStatusCode());
         }
         throw ae;
      }
      catch (Exception e)
      {
         LOGGER.error(e.getMessage(), e);
         throw new AttuneException(e);
      }
   }

   @RequestMapping(value = "/relationship/{id}/property/{propName:.+}", method = RequestMethod.GET)
   @PreAuthorize("hasRole('" + SecurityService.ROLE_USER + "')")
   @ApiOperation(value = "getRelationshipProperty", notes = "Returns the value for the specified property name and Metadata entity relationship id. The body of the response is a single String, boolean, or number value which is still valid JSON.", response = String.class, httpMethod = "GET")
   @ApiResponses(value = {
           @ApiResponse(code = 200, message = "A a single String, boolean, or number for the specified property on the Metadata entity relationship"),
           @ApiResponse(code = 400, message = "Invalid request, if the relationship id or property name doesn't exist"),
           @ApiResponse(code = 500, message = "An Error has Occurred")})
   public ResponseEntity<String> getRelationshipProperty(
           @ApiParam(value = "The relationship ID.", required = true) @PathVariable(value = "id") Long id,
           @ApiParam(value = "Path parameter with the property name who's value should be returned.", required = true) @PathVariable(value = "propName") String propName)
   {
      String result = null;
      try
      {
         String url = new StringBuilder(neo4jUrlPrefix).append("db/data/relationship/").append(String.valueOf(id))
                 .append("/properties/").append(propName).toString();
         result = this.getNeo4JData(url);
         final HttpHeaders httpHeaders = new HttpHeaders();
         httpHeaders.setContentType(MediaType.APPLICATION_JSON);
         if (result != null)
         {
            return new ResponseEntity<>(JSONValue.toJSONString(parser.parse(result)), httpHeaders, HttpStatus.OK);
         }
         else
         {
            return new ResponseEntity<>(null, httpHeaders, HttpStatus.OK);
         }
      }
      catch (AttuneException ae)
      {
         if (ae.getStatusCode() != null && ae.getStatusCode().equals(HttpStatus.NOT_FOUND))
         {
            return new ResponseEntity<>(HttpStatus.OK);
         }
         else if (ae.getStatusCode() != null)
         {
            return new ResponseEntity<>(ae.getMessage(), ae.getStatusCode());
         }
         throw ae;
      }
      catch (Exception e)
      {
         LOGGER.error(e.getMessage(), e);
         throw new AttuneException(e);
      }
   }

   @RequestMapping(value = "/relationship/{id}/properties", method = RequestMethod.PUT, consumes = MediaType.APPLICATION_JSON_VALUE)
   @PreAuthorize("hasRole('" + SecurityService.ROLE_USER + "')")
   @ApiOperation(value = "updateRelationshipProperties", notes = "Overwrites all properties on a Metadata entity relationship, passing in a single-level JSON Object that contains all of the properties..", response = String.class, httpMethod = "PUT")
   @ApiResponses(value = {
           @ApiResponse(code = 204, message = "Relationship properties were successfully updated, empty response"),
           @ApiResponse(code = 400, message = "Invalid request, if the id doesn't exist."),
           @ApiResponse(code = 500, message = "An Error has Occurred")})
   public ResponseEntity<String> updateRelationshipProperties(
           @ApiParam(value = "The relationship ID.", required = true) @PathVariable(value = "id") Long id,
           @ApiParam(value = "A single-level JSON object that contains the properties for the relationship, values can be String, boolean, or a number.", required = true) @RequestBody(required = true) Map<String, Object> relationshipProperties)
   {
      try
      {
         Map<String, Object> properties = relationshipProperties;
         if (properties == null)
         {
            properties = new HashMap<>();
         }
         if (!properties.containsKey("required"))
         {
            properties.put("required", false);
         }
         String startEntityType = Neo4JUtils.getDataValue(this
                 .getEntity(Long.parseLong(Neo4JUtils.getRelationshipStartNode(this.getRelationship(id).getBody())))
                 .getBody(), "Name");
         String relationshipType = Neo4JUtils.getRelationshipType(this.getRelationship(id).getBody());
         String endEntityType = Neo4JUtils.getDataValue(this
                 .getEntity(Long.parseLong(Neo4JUtils.getRelationshipEndNode(this.getRelationship(id).getBody())))
                 .getBody(), "Name");
         this.validateRelationship(startEntityType, relationshipType, endEntityType, properties);
         String url = new StringBuilder(neo4jUrlPrefix).append("db/data/relationship/").append(String.valueOf(id))
                 .append("/properties").toString();
         String json;
         if (!properties.isEmpty())
         {
            properties = defaultNullPropertyValues(properties);
            JSONObject jsonObj = new JSONObject(properties);
            json = jsonObj.toJSONString();
            this.updateNeo4JData(json, url);
         }
         return new ResponseEntity<>(HttpStatus.OK);
      }
      catch (AttuneException ae)
      {
         if (ae.getStatusCode() != null)
         {
            return new ResponseEntity<>(ae.getMessage(), ae.getStatusCode());
         }
         throw ae;
      }
      catch (Exception e)
      {
         LOGGER.error(e.getMessage(), e);
         throw new AttuneException(e);
      }
   }

   @RequestMapping(value = "/relationship/{id}/properties", method = RequestMethod.DELETE)
   @PreAuthorize("hasRole('" + SecurityService.ROLE_USER + "')")
   @ApiOperation(value = "deleteRelationshipProperties", notes = "Deletes all properties on a Metadata entity relationship.", response = String.class, httpMethod = "DELETE")
   @ApiResponses(value = {
           @ApiResponse(code = 204, message = "Relationship properties were successfully deleted, null or empty response"),
           @ApiResponse(code = 400, message = "Invalid request if the id doesn't exist."),
           @ApiResponse(code = 500, message = "An Error has Occurred")})
   public ResponseEntity<String> deleteRelationshipProperties(
           @ApiParam(value = "The relationship ID.", required = true) @PathVariable(value = "id") Long id)
   {
      try
      {
         String url = new StringBuilder(neo4jUrlPrefix).append("db/data/relationship/").append(String.valueOf(id))
                 .append("/properties").toString();
         this.deleteNeo4JData(url);
         return new ResponseEntity<>(HttpStatus.OK);
      }
      catch (AttuneException ae)
      {
         if (ae.getStatusCode() != null)
         {
            return new ResponseEntity<>(ae.getMessage(), ae.getStatusCode());
         }
         throw ae;
      }
      catch (Exception e)
      {
         LOGGER.error(e.getMessage(), e);
         throw new AttuneException(e);
      }
   }

   @RequestMapping(value = "/relationship/{id}/property/{propName:.+}", method = RequestMethod.PUT, consumes = MediaType.APPLICATION_JSON_VALUE)
   @PreAuthorize("hasRole('" + SecurityService.ROLE_USER + "')")
   @ApiOperation(value = "updateRelationshipProperty", notes = "Adds or updates the specified property on the Metadata entity relationship. The body of the request should be a single String, boolean, or number value which is still valid JSON.", response = String.class, httpMethod = "PUT")
   @ApiResponses(value = {
           @ApiResponse(code = 204, message = "Relationship property was successfully added or updated, empty response"),
           @ApiResponse(code = 400, message = "Invalid request if the id doesn't exist; or the property value is null or not a String, boolean, or number."),
           @ApiResponse(code = 500, message = "An Error has Occurred")})
   public ResponseEntity<String> updateRelationshipProperty(
           @ApiParam(value = "The relationship ID.", required = true) @PathVariable(value = "id") Long id,
           @ApiParam(value = "Path parameter for which property's value should be updated.", required = true) @PathVariable(value = "propName") String propName,
           @ApiParam(value = "The property value property's value which should be a single String, boolean, or number value", required = true) @RequestBody(required = true) Object propValue)
   {
      try
      {
         if (propValue == null)
         {
            StringBuilder msg = new StringBuilder("Property values can not be null!");
            throw new AttuneException(msg.toString(), HttpStatus.BAD_REQUEST);
         }
         String url = new StringBuilder(neo4jUrlPrefix).append("db/data/relationship/").append(String.valueOf(id))
                 .append("/properties/").append(propName).toString();
         this.updateNeo4JData(JSONValue.toJSONString(propValue), url);
         return new ResponseEntity<>(HttpStatus.OK);
      }
      catch (AttuneException ae)
      {
         if (ae.getStatusCode() != null)
         {
            return new ResponseEntity<>(ae.getMessage(), ae.getStatusCode());
         }
         throw ae;
      }
      catch (Exception e)
      {
         LOGGER.error(e.getMessage(), e);
         throw new AttuneException(e);
      }
   }

   @RequestMapping(value = "/relationship/{id}/property/{propName:.+}", method = RequestMethod.DELETE)
   @PreAuthorize("hasRole('" + SecurityService.ROLE_USER + "')")
   @ApiOperation(value = "deleteRelationshipProperty", notes = "Delete the specified property from the Metadata Entity relationship.", response = String.class, httpMethod = "DELETE")
   @ApiResponses(value = {
           @ApiResponse(code = 204, message = "Relationship property was successfully deleted, empty response"),
           @ApiResponse(code = 400, message = "Invalid request if the id or property name doesn't exist"),
           @ApiResponse(code = 500, message = "An Error has Occurred")})
   public ResponseEntity<String> deleteRelationshipProperty(
           @ApiParam(value = "The relationship ID.", required = true) @PathVariable(value = "id") Long id,
           @ApiParam(value = "Path parameter for which property should be deleted.", required = true) @PathVariable(value = "propName") String propName)
   {
      try
      {
         if ("required".equals(propName))
         {
            StringBuilder msg = new StringBuilder("Relationships must contain a required field!");
            throw new AttuneException(msg.toString(), HttpStatus.BAD_REQUEST);
         }
         String url = new StringBuilder(neo4jUrlPrefix).append("db/data/relationship/").append(String.valueOf(id))
                 .append("/properties/").append(propName).toString();
         this.deleteNeo4JData(url);
         return new ResponseEntity<>(HttpStatus.OK);
      }
      catch (AttuneException ae)
      {
         if (ae.getStatusCode() != null)
         {
            return new ResponseEntity<>(ae.getMessage(), ae.getStatusCode());
         }
         throw ae;
      }
      catch (Exception e)
      {
         LOGGER.error(e.getMessage(), e);
         throw new AttuneException(e);
      }
   }

   @RequestMapping(value = "/entity/{entityType}/relationshipCount/all", method = RequestMethod.GET)
   @PreAuthorize("hasRole('" + SecurityService.ROLE_USER + "')")
   @ApiOperation(value = "getRelationshipCountAll", notes = "Returns the total number of all relationships for the specified Metadata entity type. The body of the response is a single number value which is still valid JSON.", response = String.class, httpMethod = "GET")
   @ApiResponses(value = {
           @ApiResponse(code = 200, message = "A single number for the total number of relationships for the specified Metadata entity type"),
           @ApiResponse(code = 400, message = "Invalid request, if the Metadata entity type doesn't exist"),
           @ApiResponse(code = 500, message = "An Error has Occurred")})
   public ResponseEntity<String> getRelationshipCountAll(
           @ApiParam(value = "Path parameter indicating the Metadata entity type name.", required = true) @PathVariable(value = "entityType") String entityType,
           @ApiParam(value = "Optional query parameter indicating the relationship types to filter by. Multiple values can be specified by repeating the parameter in the URL, "
                   + "for example http://localhost:8080/attune/api/metadata/entity/{entityType}/relationshipCount/all?relType=has&relType=likes&relType=uses"
                   + ", would count all 'has', 'likes', and 'uses' relationships.", required = false) @RequestParam(value = "relType", required = false) List<String> relType)
   {
      String result = null;
      try
      {
         String id = Neo4JUtils.getId(this.getEntity(entityType).getBody());
         StringBuilder sb = new StringBuilder(neo4jUrlPrefix).append("db/data/node/").append(id)
                 .append("/degree/all");
         String url = getUrlForRelTypes(sb, relType);
         result = this.getNeo4JData(url);
         final HttpHeaders httpHeaders = new HttpHeaders();
         httpHeaders.setContentType(MediaType.APPLICATION_JSON);
         if (result != null)
         {
            return new ResponseEntity<>(JSONValue.toJSONString(parser.parse(result)), httpHeaders, HttpStatus.OK);
         }
         else
         {
            return new ResponseEntity<>(null, httpHeaders, HttpStatus.OK);
         }
      }
      catch (AttuneException ae)
      {
         if (ae.getStatusCode() != null)
         {
            return new ResponseEntity<>(ae.getMessage(), ae.getStatusCode());
         }
         throw ae;
      }
      catch (Exception e)
      {
         LOGGER.error(e.getMessage(), e);
         throw new AttuneException(e);
      }
   }

   @RequestMapping(value = "/entity/{entityType}/relationshipCount/in", method = RequestMethod.GET)
   @PreAuthorize("hasRole('" + SecurityService.ROLE_USER + "')")
   @ApiOperation(value = "getRelationshipCountIn", notes = "Returns the number of inbound relationships for the specified Metadata entity type. The body of the response is a single number value which is still valid JSON.", response = String.class, httpMethod = "GET")
   @ApiResponses(value = {
           @ApiResponse(code = 200, message = "The number of inbound relationships for the specified Metadata entity type"),
           @ApiResponse(code = 400, message = "Invalid request, if the Metadata entity type doesn't exist"),
           @ApiResponse(code = 500, message = "An Error has Occurred")})
   public ResponseEntity<String> getRelationshipCountIn(
           @ApiParam(value = "Path parameter indicating the Metadata entity type name.", required = true) @PathVariable(value = "entityType") String entityType,
           @ApiParam(value = "Optional query parameter indicating the relationship types to filter by. Multiple values can be specified by repeating the parameter in the URL, "
                   + "for example http://localhost:8080/attune/api/metadata/entity/{entityType}/relationshipCount/in?relType=has&relType=likes&relType=uses"
                   + ", would count all 'has', 'likes', and 'uses' inbound relationships.", required = false) @RequestParam(value = "relType", required = false) List<String> relType)
   {
      String result = null;
      try
      {
         String id = Neo4JUtils.getId(this.getEntity(entityType).getBody());
         StringBuilder sb = new StringBuilder(neo4jUrlPrefix).append("db/data/node/").append(id)
                 .append("/degree/in");
         String url = getUrlForRelTypes(sb, relType);
         result = this.getNeo4JData(url);
         final HttpHeaders httpHeaders = new HttpHeaders();
         httpHeaders.setContentType(MediaType.APPLICATION_JSON);
         if (result != null)
         {
            return new ResponseEntity<>(JSONValue.toJSONString(parser.parse(result)), httpHeaders, HttpStatus.OK);
         }
         else
         {
            return new ResponseEntity<>(null, httpHeaders, HttpStatus.OK);
         }
      }
      catch (AttuneException ae)
      {
         if (ae.getStatusCode() != null)
         {
            return new ResponseEntity<>(ae.getMessage(), ae.getStatusCode());
         }
         throw ae;
      }
      catch (Exception e)
      {
         LOGGER.error(e.getMessage(), e);
         throw new AttuneException(e);
      }
   }

   @RequestMapping(value = "/entity/{entityType}/relationshipCount/out", method = RequestMethod.GET)
   @PreAuthorize("hasRole('" + SecurityService.ROLE_USER + "')")
   @ApiOperation(value = "getRelationshipCountOut", notes = "Returns the number of outbound relationships for the specified Metadata entity type. The body of the response is a single number value which is still valid JSON.", response = String.class, httpMethod = "GET")
   @ApiResponses(value = {
           @ApiResponse(code = 200, message = "The number of outbound relationships for the specified Metadata entity type"),
           @ApiResponse(code = 400, message = "Invalid request, if the Metadata entity type doesn't exist"),
           @ApiResponse(code = 500, message = "An Error has Occurred")})
   public ResponseEntity<String> getRelationshipCountOut(
           @ApiParam(value = "Path parameter indicating the Metadata entity type name.", required = true) @PathVariable(value = "entityType") String entityType,
           @ApiParam(value = "Optional query parameter indicating the relationship types to filter by. Multiple values can be specified by repeating the parameter in the URL, "
                   + "for example http://localhost:8080/attune/api/metadata/entity/{entityType}/relationshipCount/out?relType=has&relType=likes&relType=uses"
                   + ", would count all 'has', 'likes', and 'uses' outbound relationships.", required = false) @RequestParam(value = "relType", required = false) List<String> relType)
   {
      String result = null;
      try
      {
         String id = Neo4JUtils.getId(this.getEntity(entityType).getBody());
         StringBuilder sb = new StringBuilder(neo4jUrlPrefix).append("db/data/node/").append(id)
                 .append("/degree/out");
         String url = getUrlForRelTypes(sb, relType);
         result = this.getNeo4JData(url);
         final HttpHeaders httpHeaders = new HttpHeaders();
         httpHeaders.setContentType(MediaType.APPLICATION_JSON);
         if (result != null)
         {
            return new ResponseEntity<>(JSONValue.toJSONString(parser.parse(result)), httpHeaders, HttpStatus.OK);
         }
         else
         {
            return new ResponseEntity<>(null, httpHeaders, HttpStatus.OK);
         }
      }
      catch (AttuneException ae)
      {
         if (ae.getStatusCode() != null)
         {
            return new ResponseEntity<>(ae.getMessage(), ae.getStatusCode());
         }
         throw ae;
      }
      catch (Exception e)
      {
         LOGGER.error(e.getMessage(), e);
         throw new AttuneException(e);
      }
   }

   @RequestMapping(value = "/getRoot", method = RequestMethod.GET)
   @PreAuthorize("hasRole('" + SecurityService.ROLE_USER + "')")
   @ApiOperation(value = "getRootEntity", notes = "Returns the Metadata entity type name of the root entity, or null if none exists.", response = String.class, httpMethod = "GET")
   @ApiResponses(value = {
           @ApiResponse(code = 200, message = "A a single String value for the root Metadata entity type, or null if one doesn't exist."),
           @ApiResponse(code = 500, message = "An Error has Occurred")})
   public ResponseEntity<String> getRootEntity()
   {
      String result = null;
      try
      {
         String json = this.getEntities(null, null).getBody();
         if (json != null)
         {
            Object jsonObj;
            synchronized (parser)
            {
               jsonObj = parser.parse(json);
            }
            JSONObject jsonObject;
            JSONArray jsonArray = (JSONArray) jsonObj;
            if (!jsonArray.isEmpty())
            {
               for (int index = 0; index < jsonArray.size(); index++)
               {
                  jsonObject = (JSONObject) jsonArray.get(index);
                  boolean isRoot = Boolean
                          .parseBoolean(Neo4JUtils.getDataValue(jsonObject.toJSONString(), "isRoot"));
                  if (isRoot)
                  {
                     result = jsonObject.toJSONString();
                     break;
                  }
               }
            }
         }
         final HttpHeaders httpHeaders = new HttpHeaders();
         httpHeaders.setContentType(MediaType.APPLICATION_JSON);
         if (result != null)
         {
            return new ResponseEntity<>(((JSONAware) parser.parse(result)).toJSONString(), httpHeaders,
                    HttpStatus.OK);
         }
         else
         {
            return new ResponseEntity<>(null, httpHeaders, HttpStatus.OK);
         }
      }
      catch (AttuneException ae)
      {
         if (ae.getStatusCode() != null)
         {
            return new ResponseEntity<>(ae.getMessage(), ae.getStatusCode());
         }
         throw ae;
      }
      catch (Exception e)
      {
         LOGGER.error(e.getMessage(), e);
         throw new AttuneException(e);
      }
   }

   @SuppressWarnings("unchecked")
   @RequestMapping(value = "/validate", method = RequestMethod.GET)
   @PreAuthorize("hasRole('" + SecurityService.ROLE_USER + "')")
   @ApiOperation(value = "getValidationReport", notes = "Performs validation on all of the current Metadata entities and their relationships, then returns a JSON object that represents the results.", response = String.class, httpMethod = "GET")
   @ApiResponses(value = {
           @ApiResponse(code = 200, message = "A JSON object that contains the results of the validation."),
           @ApiResponse(code = 500, message = "An Error has Occurred")})
   public ResponseEntity<String> getValidationReport()
   {
      try
      {
         int totalNodes = 0;
         int invalidNodes = 0;
         int totalOutRelationships = 0;
         int totalInvalidOutRelationships = 0;
         int totalInRelationships = 0;
         int totalInvalidInRelationships = 0;
         JSONArray invalidNodeDetails = new JSONArray();

         String allEntityTypesJson = this.getEntityTypes().getBody();
         if (allEntityTypesJson != null)
         {
            JSONArray entityTypes;
            synchronized (parser)
            {
               entityTypes = (JSONArray) parser.parse(allEntityTypesJson);
            }
            for (int i = 0; i < entityTypes.size(); i++)
            {
               String entityType = (String) entityTypes.get(i);
               if ("Metadata".equals(entityType))
               {
                  continue;
               }
               String entityJson = this.getEntity(entityType).getBody();
               if (entityJson != null)
               {
                  totalNodes++;
                  JSONObject entityJsonObj;
                  synchronized (parser)
                  {
                     entityJsonObj = (JSONObject) parser.parse(entityJson);
                  }
                  String entityId = Neo4JUtils.getId(entityJsonObj.toJSONString());
                  // try to get a name for the entity
                  String entityName = Neo4JUtils.getDataValue(entityJsonObj.toJSONString(), "Name");
                  if (entityName == null)
                  {
                     entityName = Neo4JUtils.getDataValue(entityJsonObj.toJSONString(), "Name");
                  }

                  totalOutRelationships = totalOutRelationships
                          + Integer.parseInt(this.getRelationshipCountOut(entityType, null).getBody());
                  totalInRelationships = totalInRelationships
                          + Integer.parseInt(this.getRelationshipCountIn(entityType, null).getBody());

                  JSONObject invalidNodeDetail = new JSONObject();

                  Map<String, Object> properties = Neo4JUtils.getDataProperties(entityJsonObj.toJSONString());
                  try
                  {
                     this.validateEntity(entityType, properties);
                  }
                  catch (Exception ex)
                  {
                     LOGGER.info(ex.getMessage(), ex);
                     invalidNodeDetail.put("id", entityId);
                     invalidNodeDetail.put("type", "Metadata");
                     invalidNodeDetail.put("name", entityType);
                     if (ex.getCause() != null)
                     {
                        invalidNodeDetail.put("error", ex.getCause().getMessage());
                     }
                     else
                     {
                        invalidNodeDetail.put("error", ex.getMessage());
                     }
                  }

                  JSONArray invalidNodeRelationshipsDetails = new JSONArray();
                  try
                  {
                     invalidNodeRelationshipsDetails = this
                             .getValidateRelationshipsReport(Long.parseLong(entityId));
                  }
                  catch (Exception ex)
                  {
                     LOGGER.info(ex.getMessage(), ex);
                     invalidNodeDetail.put("id", entityId);
                     invalidNodeDetail.put("type", "Metadata");
                     invalidNodeDetail.put("name", entityType);
                     invalidNodeDetail.put("error", ex.getMessage());
                  }

                  if (!invalidNodeDetail.isEmpty() || !invalidNodeRelationshipsDetails.isEmpty())
                  {
                     invalidNodes++;
                     if (!invalidNodeDetail.isEmpty())
                     {
                        invalidNodeDetails.add(invalidNodeDetail);
                     }
                     if (!invalidNodeRelationshipsDetails.isEmpty())
                     {
                        for (int z = 0; z < invalidNodeRelationshipsDetails.size(); z++)
                        {
                           JSONObject invalidNodeRelationshipDetail = (JSONObject) invalidNodeRelationshipsDetails
                                   .get(z);
                           if ("out".equals(invalidNodeRelationshipDetail.get("relationshipDirection")))
                           {
                              totalInvalidOutRelationships++;
                           }
                           if ("in".equals(invalidNodeRelationshipDetail.get("relationshipDirection")))
                           {
                              totalInvalidInRelationships++;
                           }
                           invalidNodeRelationshipDetail.put("id", entityId);
                           invalidNodeRelationshipDetail.put("type", entityType);
                           invalidNodeRelationshipDetail.put("name", entityName);
                           invalidNodeDetails.add(invalidNodeRelationshipDetail);
                        }
                     }
                  }
               }
            }
         }

         int totalRelationships = totalOutRelationships + totalInRelationships;
         int totalInvalidRelationships = totalInvalidOutRelationships + totalInvalidInRelationships;

         JSONObject report = new JSONObject();
         report.put("totalNodes", totalNodes);
         report.put("totalValidNodes", totalNodes - invalidNodes);
         report.put("totalInvalidNodes", invalidNodes);
         report.put("totalRelationships", totalRelationships);
         report.put("totalValidRelationships", totalRelationships - totalInvalidRelationships);
         report.put("totalInvalidRelationships", totalInvalidRelationships);
         report.put("totalOutRelationships", totalOutRelationships);
         report.put("totalValidOutRelationships", totalOutRelationships - totalInvalidOutRelationships);
         report.put("totalInvalidOutRelationships", totalInvalidOutRelationships);
         report.put("totalInRelationships", totalInRelationships);
         report.put("totalValidInRelationships", totalInRelationships - totalInvalidInRelationships);
         report.put("totalInvalidInRelationships", totalInvalidInRelationships);
         report.put("totalErrors", invalidNodeDetails.size());
         report.put("errorDetails", invalidNodeDetails);

         final HttpHeaders httpHeaders = new HttpHeaders();
         httpHeaders.setContentType(MediaType.APPLICATION_JSON);
         return new ResponseEntity<>(report.toJSONString(), httpHeaders, HttpStatus.OK);
      }
      catch (AttuneException ae)
      {
         if (ae.getStatusCode() != null)
         {
            return new ResponseEntity<>(ae.getMessage(), ae.getStatusCode());
         }
         throw ae;
      }
      catch (Exception e)
      {
         LOGGER.error(e.getMessage(), e);
         throw new AttuneException(e);
      }
   }

   private String getNeo4JData(String url)
   {
      return this.getNeo4JData(url, null);
   }

   @SuppressWarnings("unchecked")
   private String getNeo4JData(String url, Map<String, ?> uriVariables)
   {
      try
      {
         Map<String, ?> vars = uriVariables;
         if (vars == null)
         {
            vars = new HashMap<>();
         }
         ResponseEntity<String> responseEntity = this.callRestService(url, HttpMethod.GET, null, String.class, vars);
         int statusCode = responseEntity.getStatusCode().value();
         String json = responseEntity.hasBody() ? responseEntity.getBody() : new JSONObject().toJSONString();
         if (!((statusCode >= 200) && (statusCode < 300)))
         {
            String msg = json;
            throw new AttuneException(msg, HttpStatus.INTERNAL_SERVER_ERROR);
         }
         return json;
      }
      catch (AttuneException ae)
      {
         throw ae;
      }
      catch (Exception e)
      {
         throw new AttuneException(e);
      }
   }

   @SuppressWarnings("unchecked")
   private String insertNeo4JData(String jsonInput, String url)
   {
      try
      {
         ResponseEntity<String> responseEntity = this.callRestService(url, HttpMethod.POST, jsonInput, String.class);
         int statusCode = responseEntity.getStatusCode().value();
         String json = responseEntity.hasBody() ? responseEntity.getBody() : new JSONObject().toJSONString();
         if (!((statusCode >= 200) && (statusCode < 300)))
         {
            String msg = json;
            throw new AttuneException(msg, HttpStatus.INTERNAL_SERVER_ERROR);
         }
         return json;
      }
      catch (AttuneException ae)
      {
         throw ae;
      }
      catch (Exception e)
      {
         throw new AttuneException(e);
      }
   }

   @SuppressWarnings("unchecked")
   private void updateNeo4JData(String jsonInput, String url)
   {
      try
      {
         ResponseEntity<String> responseEntity = this.callRestService(url, HttpMethod.PUT, jsonInput, String.class);
         int statusCode = responseEntity.getStatusCode().value();
         String json = responseEntity.hasBody() ? responseEntity.getBody() : new JSONObject().toJSONString();
         if (!((statusCode >= 200) && (statusCode < 300)))
         {
            String msg = json;
            throw new AttuneException(msg, HttpStatus.INTERNAL_SERVER_ERROR);
         }
      }
      catch (AttuneException ae)
      {
         throw ae;
      }
      catch (Exception e)
      {
         throw new AttuneException(e);
      }
   }

   @SuppressWarnings("unchecked")
   private void deleteNeo4JData(String url)
   {
      try
      {
         ResponseEntity<String> responseEntity = this.callRestService(url, HttpMethod.DELETE, null, String.class);
         int statusCode = responseEntity.getStatusCode().value();
         String json = responseEntity.hasBody() ? responseEntity.getBody() : new JSONObject().toJSONString();
         if (!((statusCode >= 200) && (statusCode < 300)))
         {
            String msg = json;
            throw new AttuneException(msg, HttpStatus.INTERNAL_SERVER_ERROR);
         }
      }
      catch (AttuneException ae)
      {
         throw ae;
      }
      catch (Exception e)
      {
         throw new AttuneException(e);
      }
   }

   @Override
   protected HttpHeaders createHeaders(final String url, final HttpMethod httpMethod)
   {
      HttpHeaders headers = new HttpHeaders();
      String neo4jCredentials = this.servletContext.getInitParameter("neo4j.credentials");
      byte[] encodedAuth = Base64.encodeBase64(neo4jCredentials.getBytes(Charset.forName("US-ASCII")));
      String authHeader = "Basic " + new String(encodedAuth);
      headers.set("Authorization", authHeader);
      headers.setContentType(MediaType.APPLICATION_JSON);
      return headers;
   }

   private void validateEntity(String entityType, Map<String, Object> properties)
   {
      if (!properties.containsKey("Name") || properties.get("Name") == null)
      {
         properties.put("Name", entityType);
      }
      if (!properties.containsKey("isRoot") || properties.get("isRoot") == null)
      {
         StringBuilder msg = new StringBuilder("Metadata entity type ").append(entityType)
                 .append(" must contain a isRoot field!");
         throw new AttuneException(msg.toString(), HttpStatus.BAD_REQUEST);
      }
      if (!properties.containsKey("fieldList") || properties.get("fieldList") == null)
      {
         StringBuilder msg = new StringBuilder("Metadata entity type ").append(entityType)
                 .append(" must contain a fieldList field!");
         throw new AttuneException(msg.toString(), HttpStatus.BAD_REQUEST);
      }
      if (!properties.containsKey("requiredFields") || properties.get("requiredFields") == null)
      {
         StringBuilder msg = new StringBuilder("Metadata entity type ").append(entityType)
                 .append(" must contain a requiredFields field!");
         throw new AttuneException(msg.toString(), HttpStatus.BAD_REQUEST);
      }

      if (properties.containsKey("neo4j.id"))
      {
         properties.remove("neo4j.id");
      }
      if (properties.containsKey("neo4j.label"))
      {
         properties.remove("neo4j.label");
      }

      StringTokenizer fieldListST = new StringTokenizer((String) properties.get("fieldList"), ",", false);
      List<String> fieldList = new ArrayList<>();
      while (fieldListST.hasMoreTokens())
      {
         fieldList.add(fieldListST.nextToken());
      }
      StringTokenizer requiredFieldsST = new StringTokenizer((String) properties.get("requiredFields"), ",", false);
      List<String> requiredFields = new ArrayList<>();
      while (requiredFieldsST.hasMoreTokens())
      {
         requiredFields.add(requiredFieldsST.nextToken());
      }

      if (!fieldList.containsAll(requiredFields))
      {
         StringBuilder msg = new StringBuilder("fieldList property for Entity type ").append(entityType)
                 .append(" must contain all values in requiredFields!");
         throw new AttuneException(msg.toString(), HttpStatus.BAD_REQUEST);
      }

      String root = this.getRootEntity().getBody();
      if (root == null && !(Boolean) properties.get("isRoot"))
      {
         StringBuilder msg = new StringBuilder(
                 "No Entity types with isRoot=true have been defined yet. Please add one before other nodes are added.");
         throw new AttuneException(msg.toString(), HttpStatus.BAD_REQUEST);
      }
   }

   private void validateEntityDeletion(String type, Long id)
   {
      String entityType = type;
      if (entityType == null)
      {
         entityType = Neo4JUtils.getDataValue(this.getEntity(id).getBody(), "Name");
      }
      String result;
      StringBuilder sb = new StringBuilder(neo4jUrlPrefix);
      sb.append("db/data/label/").append(entityType).append("/nodes");
      String url = sb.toString();
      result = this.getNeo4JData(url);
      if (!Neo4JUtils.isEmpty(result))
      {
         StringBuilder msg = new StringBuilder("Instances of type ").append(entityType).append(" still exist!");
         msg.append(" Please delete all instances of this type before deleting it's associated Metadata.");
         throw new AttuneException(msg.toString(), HttpStatus.BAD_REQUEST);
      }
   }

   private void validateRelationship(String startEntityType, String relationshipType, String endEntityType,
                                     Map<String, Object> properties)
   {
      if (properties == null || (!properties.containsKey("required") || properties.get("required") == null))
      {
         StringBuilder msg = new StringBuilder("Relationship must contain a required field!");
         throw new AttuneException(msg.toString(), HttpStatus.BAD_REQUEST);
      }
      if (startEntityType != null && Neo4JUtils.isEmpty(this.getEntity(startEntityType).getBody()))
      {
         StringBuilder msg = new StringBuilder("Metadata Entity of type ").append(startEntityType)
                 .append(" does not exist!");
         throw new AttuneException(msg.toString(), HttpStatus.BAD_REQUEST);
      }
      if (endEntityType != null && Neo4JUtils.isEmpty(this.getEntity(endEntityType).getBody()))
      {
         StringBuilder msg = new StringBuilder("Metadata Entity of type ").append(endEntityType)
                 .append(" does not exist!");
         throw new AttuneException(msg.toString(), HttpStatus.BAD_REQUEST);
      }

      if (!properties.isEmpty())
      {
         properties.remove("neo4j.id");
         properties.remove("relationshipType");
         properties.remove("startNodeId");
         properties.remove("startNodeType");
         properties.remove("startNodeName");
         properties.remove("endNodeId");
         properties.remove("endNodeType");
         properties.remove("endNodeName");
      }

      // check for circular dependency (if endEntityType has an out
      // relationship of this type to startEntityType)
      List<String> relType = new ArrayList<>();
      relType.add(relationshipType);
      String json = this.getRelationships(endEntityType, startEntityType, relType, "out").getBody();
      if (!Neo4JUtils.isEmpty(json))
      {
         StringBuilder msg = new StringBuilder("Circular dependency found! Can not create ").append(relationshipType)
                 .append(" relationship from ");
         msg.append(startEntityType).append(" to ").append(endEntityType).append(" because ").append(endEntityType)
                 .append(" already has a ");
         msg.append(relationshipType).append(" relationship to ").append(startEntityType);
         throw new AttuneException(msg.toString(), HttpStatus.BAD_REQUEST);
      }
   }

   private void validateRelationshipDeletion(Long relationshipId)
   {
      try
      {
         String metadataRelationshipJson = this.getRelationship(relationshipId).getBody();
         String metadataStartEntityId = Neo4JUtils.getRelationshipStartNode(metadataRelationshipJson);
         String metadataEndEntityId = Neo4JUtils.getRelationshipEndNode(metadataRelationshipJson);
         String relationshipType = Neo4JUtils.getRelationshipType(metadataRelationshipJson);
         String startEntityType = Neo4JUtils
                 .getDataValue(this.getEntity(Long.parseLong(metadataStartEntityId)).getBody(), "Name");
         String endEntityType = Neo4JUtils
                 .getDataValue(this.getEntity(Long.parseLong(metadataEndEntityId)).getBody(), "Name");

         List<String> relTypes = new ArrayList<>();
         relTypes.add(relationshipType);

         if (startEntityType != null)
         {
            StringBuilder getNodesSB = new StringBuilder(neo4jUrlPrefix);
            String getNodesUrl;
            getNodesSB.append("db/data/label/")
                    .append(URLEncoder.encode(startEntityType, StandardCharsets.UTF_8.toString())).append("/nodes");
            getNodesUrl = getNodesSB.toString();
            String getNodesJsonResult = this.getNeo4JData(getNodesUrl);
            if (getNodesJsonResult != null)
            {
               JSONArray jsonArray;
               synchronized (parser)
               {
                  jsonArray = (JSONArray) parser.parse(getNodesJsonResult);
               }
               for (int i = 0; i < jsonArray.size(); i++)
               {
                  JSONObject jsonObject = (JSONObject) jsonArray.get(i);
                  String nodeId = Neo4JUtils.getId(jsonObject.toJSONString());
                  StringBuilder getRelsSB = new StringBuilder(neo4jUrlPrefix).append("db/data/node/")
                          .append(nodeId).append("/relationships/all");
                  String getRelsUrl = getUrlForRelTypes(getRelsSB, relTypes);
                  String relationshipsJson = this.getNeo4JData(getRelsUrl);
                  if (!Neo4JUtils.isEmpty(relationshipsJson))
                  {
                     StringWriter sw = new StringWriter();
                     sw.append(this.getNeo4JData(getRelsUrl));
                     StringBuilder msg = new StringBuilder("Instances of type ").append(startEntityType)
                             .append(" still have '").append(relationshipType)
                             .append("' relationships to other entities!");
                     msg.append(
                             " Please delete all instances of these relationships before deleting it's associated Metadata.");
                     throw new AttuneException(msg.toString(), HttpStatus.BAD_REQUEST);
                  }
               }
            }
         }

         if (endEntityType != null)
         {
            StringBuilder getNodesSB = new StringBuilder(neo4jUrlPrefix);
            String getNodesUrl;
            getNodesSB.append("db/data/label/")
                    .append(URLEncoder.encode(endEntityType, StandardCharsets.UTF_8.toString())).append("/nodes");
            getNodesUrl = getNodesSB.toString();
            String getNodesJsonResult = this.getNeo4JData(getNodesUrl);
            if (getNodesJsonResult != null)
            {
               JSONArray jsonArray;
               synchronized (parser)
               {
                  jsonArray = (JSONArray) parser.parse(getNodesJsonResult);
               }
               for (int i = 0; i < jsonArray.size(); i++)
               {
                  JSONObject jsonObject = (JSONObject) jsonArray.get(i);
                  String nodeId = Neo4JUtils.getId(jsonObject.toJSONString());
                  StringBuilder getRelsSB = new StringBuilder(neo4jUrlPrefix).append("db/data/node/")
                          .append(nodeId).append("/relationships/all");
                  String getRelsUrl = getUrlForRelTypes(getRelsSB, relTypes);
                  String relationshipsJson = this.getNeo4JData(getRelsUrl);
                  if (!Neo4JUtils.isEmpty(relationshipsJson))
                  {
                     StringWriter sw = new StringWriter();
                     sw.append(this.getNeo4JData(getRelsUrl));
                     StringBuilder msg = new StringBuilder("Instances of type ").append(endEntityType)
                             .append(" still have '").append(relationshipType)
                             .append("' relationships to other entities!");
                     msg.append(
                             " Please delete all instances of these relationships before deleting it's associated Metadata.");
                     throw new AttuneException(msg.toString(), HttpStatus.BAD_REQUEST);
                  }
               }
            }
         }
      }
      catch (AttuneException ae)
      {
         throw ae;
      }
      catch (Exception e)
      {
         throw new AttuneException(e);
      }
   }

   private String getExistingRelationshipId(String startEntityType, String relationshipType, String endEntityType,
                                            Map<String, Object> properties)
   {
      try
      {
         String relationshipId = null;
         String startEntityId = Neo4JUtils.getId(this.getEntity(startEntityType).getBody());
         String endEntityId = Neo4JUtils.getId(this.getEntity(endEntityType).getBody());

         List<String> relTypes = new ArrayList<>();
         relTypes.add(relationshipType);

         String outboundRelsJson = this.getRelationshipsOut(startEntityType, relTypes).getBody();
         if (outboundRelsJson != null)
         {
            JSONArray jsonArray;
            synchronized (parser)
            {
               jsonArray = (JSONArray) parser.parse(outboundRelsJson);
            }
            for (int index = 0; index < jsonArray.size(); index++)
            {
               JSONObject relationship = (JSONObject) jsonArray.get(index);
               if (startEntityId.equals(Neo4JUtils.getRelationshipStartNode(relationship.toJSONString()))
                       && endEntityId.equals(Neo4JUtils.getRelationshipEndNode(relationship.toJSONString())))
               {
                  relationshipId = Neo4JUtils.getId(relationship.toJSONString());
                  break;
               }
            }
         }

         if (relationshipId == null)
         {
            String inboundRelsJson = this.getRelationshipsIn(endEntityType, relTypes).getBody();
            if (inboundRelsJson != null)
            {
               JSONArray jsonArray;
               synchronized (parser)
               {
                  jsonArray = (JSONArray) parser.parse(inboundRelsJson);
               }
               for (int index = 0; index < jsonArray.size(); index++)
               {
                  JSONObject relationship = (JSONObject) jsonArray.get(index);
                  if (startEntityId.equals(Neo4JUtils.getRelationshipStartNode(relationship.toJSONString()))
                          && endEntityId.equals(Neo4JUtils.getRelationshipEndNode(relationship.toJSONString())))
                  {
                     relationshipId = Neo4JUtils.getId(relationship.toJSONString());
                     break;
                  }
               }
            }
         }
         return relationshipId;
      }
      catch (AttuneException ae)
      {
         throw ae;
      }
      catch (Exception e)
      {
         throw new AttuneException(e);
      }
   }

   @SuppressWarnings("unchecked")
   private JSONArray getValidateRelationshipsReport(Long entityId)
   {
      try
      {
         JSONArray invalidNodeRelationships = new JSONArray();

         String entityType = Neo4JUtils.getDataValue(this.getEntity(entityId).getBody(), "Name");

         String outMetadataRelationshipsJson = this.getRelationshipsOut(entityType, null).getBody();
         if (outMetadataRelationshipsJson != null)
         {
            JSONArray outMetadataRelationships;
            synchronized (parser)
            {
               outMetadataRelationships = (JSONArray) parser.parse(outMetadataRelationshipsJson);
            }
            for (int i = 0; i < outMetadataRelationships.size(); i++)
            {
               JSONObject metadataRelationshipJson = (JSONObject) outMetadataRelationships.get(i);
               String metadataRelationshipId = Neo4JUtils.getId(metadataRelationshipJson.toJSONString());
               String metadataRelationshipType = Neo4JUtils
                       .getRelationshipType(metadataRelationshipJson.toJSONString());
               String metadataRelationshipStartId = Neo4JUtils
                       .getRelationshipStartNode(metadataRelationshipJson.toJSONString());
               String metadataRelationshipEndId = Neo4JUtils
                       .getRelationshipEndNode(metadataRelationshipJson.toJSONString());
               String metadataRelationshipStartEntityType = Neo4JUtils.getDataValue(
                       this.getEntity(Long.parseLong(metadataRelationshipStartId)).getBody(), "Name");
               String metadataRelationshipEndEntityType = Neo4JUtils
                       .getDataValue(this.getEntity(Long.parseLong(metadataRelationshipEndId)).getBody(), "Name");

               List<String> relTypes = new ArrayList<String>();
               relTypes.add(metadataRelationshipType);

               Map<String, Object> relationshipProperties = Neo4JUtils.getProperties(
                       this.getRelationshipProperties(Long.parseLong(metadataRelationshipId)).getBody());
               try
               {
                  this.validateRelationship(metadataRelationshipStartEntityType, metadataRelationshipType,
                          metadataRelationshipEndEntityType, relationshipProperties);
               }
               catch (AttuneException ae)
               {
                  LOGGER.info(ae.getMessage(), ae);
                  JSONObject detail = new JSONObject();
                  detail.put("id", entityId);
                  detail.put("relationshipId", metadataRelationshipId);
                  detail.put("relationshipType", metadataRelationshipType);
                  detail.put("relationshipDirection", "out");
                  detail.put("startNodeId", metadataRelationshipStartId);
                  detail.put("startNodeType", "Metadata");
                  detail.put("startNodeName", metadataRelationshipStartEntityType);
                  detail.put("endNodeId", metadataRelationshipEndId);
                  detail.put("endNodeType", "Metadata");
                  detail.put("endNodeName", metadataRelationshipEndEntityType);
                  detail.put("error", ae.getMessage());
                  invalidNodeRelationships.add(detail);
               }
            }
         }

         String inMetadataRelationshipsJson = this.getRelationshipsIn(entityType, null).getBody();
         if (inMetadataRelationshipsJson != null)
         {
            JSONArray inMetadataRelationships;
            synchronized (parser)
            {
               inMetadataRelationships = (JSONArray) parser.parse(inMetadataRelationshipsJson);
            }
            for (int i = 0; i < inMetadataRelationships.size(); i++)
            {
               JSONObject metadataRelationshipJson = (JSONObject) inMetadataRelationships.get(i);
               String metadataRelationshipId = Neo4JUtils.getId(metadataRelationshipJson.toJSONString());
               String metadataRelationshipType = Neo4JUtils
                       .getRelationshipType(metadataRelationshipJson.toJSONString());
               String metadataRelationshipStartId = Neo4JUtils
                       .getRelationshipStartNode(metadataRelationshipJson.toJSONString());
               String metadataRelationshipEndId = Neo4JUtils
                       .getRelationshipEndNode(metadataRelationshipJson.toJSONString());
               String metadataRelationshipStartEntityType = Neo4JUtils.getDataValue(
                       this.getEntity(Long.parseLong(metadataRelationshipStartId)).getBody(), "Name");
               String metadataRelationshipEndEntityType = Neo4JUtils
                       .getDataValue(this.getEntity(Long.parseLong(metadataRelationshipEndId)).getBody(), "Name");
               List<String> relTypes = new ArrayList<>();
               relTypes.add(metadataRelationshipType);

               Map<String, Object> relationshipProperties = Neo4JUtils.getProperties(
                       this.getRelationshipProperties(Long.parseLong(metadataRelationshipId)).getBody());
               try
               {
                  this.validateRelationship(metadataRelationshipStartEntityType, metadataRelationshipType,
                          metadataRelationshipEndEntityType, relationshipProperties);
               }
               catch (AttuneException ae)
               {
                  JSONObject detail = new JSONObject();
                  detail.put("id", entityId);
                  detail.put("relationshipId", metadataRelationshipId);
                  detail.put("relationshipType", metadataRelationshipType);
                  detail.put("relationshipDirection", "in");
                  detail.put("startNodeId", metadataRelationshipStartId);
                  detail.put("startNodeType", "Metadata");
                  detail.put("startNodeName", metadataRelationshipStartEntityType);
                  detail.put("endNodeId", metadataRelationshipEndId);
                  detail.put("endNodeType", "Metadata");
                  detail.put("endNodeName", metadataRelationshipEndEntityType);
                  detail.put("error", ae.getMessage());
                  invalidNodeRelationships.add(detail);
               }
            }
         }
         return invalidNodeRelationships;
      }
      catch (AttuneException ae)
      {
         throw ae;
      }
      catch (Exception e)
      {
         throw new AttuneException(e);
      }
   }

   private void checkDashboardMetadata()
   {
      try
      {
         if (!MetadataController.dashBoardMetadataExists)
         {
//            if (Neo4JUtils.isEmpty(this.getEntity("Dashboard").getBody()))
//            {
//               Map<String, Object> properties = new HashMap<String, Object>();
//               properties.put("Name", "Dashboard");
//               properties.put("isRoot", false);
//               properties.put("fieldList", "Name,groupBy,chartType,alertCheck,displayList,entity,alertValue");
//               properties.put("requiredFields", "Name,groupBy,chartType,displayList,entity");
//               this.insertEntity("Dashboard", properties);
//            }
            MetadataController.setDashBoardMetadataExists(true);
         }
      }
      catch (Exception e)
      {
         throw new AttuneException(e.getMessage(), e);
      }
   }

   private static synchronized void setDashBoardMetadataExists(boolean exists)
   {
      MetadataController.dashBoardMetadataExists = exists;
   }

   private Map<String, Object> defaultNullPropertyValues(Map<String, Object> properties)
   {
      // Neo4J Properties can't be null, so default them to ""
      for (Map.Entry<String, Object> entry : properties.entrySet())
      {
         String key = entry.getKey();
         Object value = entry.getValue();
         if (value == null)
         {
            properties.put(key, "");
         }
      }
      return properties;
   }

   private Long getIdForEntityType(String entityType)
   {
      try
      {
         return Long.parseLong(entityType);
      }
      catch (NumberFormatException nfe)
      {
         return null;
      }
   }

   private String getUrlForRelTypes(StringBuilder baseUrl, List<String> relTypes) throws UnsupportedEncodingException
   {
      if (relTypes != null && !relTypes.isEmpty())
      {
         baseUrl.append("/");
         Iterator<String> typesItr = relTypes.iterator();
         while (typesItr.hasNext())
         {
            String type = typesItr.next();
            type = URLDecoder.decode(type, StandardCharsets.UTF_8.toString());
            baseUrl.append(type);
            if (typesItr.hasNext())
            {
               baseUrl.append("&");
            }
         }
      }
      return baseUrl.toString();
   }
}
