package com.bah.attune.web;

import com.bah.attune.data.*;
import com.bah.attune.service.DataAnalaysisService;
import com.bah.attune.service.MainService;
import com.bah.attune.service.SecurityService;
import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpMethod;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.neo4j.graphdb.Node;
import org.neo4j.graphdb.PropertyContainer;
import org.neo4j.graphdb.DynamicRelationshipType;
import org.neo4j.graphdb.Relationship;
import org.neo4j.graphdb.Direction;


import java.util.Arrays;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Iterator;

@Controller
public class DataAnalysisController extends AbstractRestController
{
   @Autowired
   private DataAnalaysisService service;

   @Autowired
   private MainService mainService;

   private static final String ATTUNE_BASE_URL = "attune.base.url";
   private static final String ENTITY_DIRECTORY = "/api/metadata/entity/";
   private static final String NODE_DIRECTORY = "/api/data/node/";

   @RequestMapping("dataAnalysis.exec")
   public String dataAnalysis(Model model)
   {
      model.addAttribute("entities", (List<NameValuePair>) mainService.invokeDao("getEntityList"));

      return "dataAnalysis";
   }

   @RequestMapping(value = "runAnalysis.exec", method = RequestMethod.POST)
   public String runAnalysis(@RequestBody AnalysisParameters analysisParameters, Model model)
   {
      model.addAttribute("analysisResult", (AnalysisResult) service.invokeDao("getAnalysisResults", analysisParameters));
      return "dataAnalysisResults";
   }

   @ResponseBody
   @RequestMapping(value = "getEntNames.exec", method = RequestMethod.POST)
   public String getEntNames(@RequestBody NameValuePair pair, Model model)
   {
     String label = pair.getValue();
      List<Node> entList = (List<Node>) service.invokeDao("getEntitiesByLabel", label);

      String ret = "";
      for (int j = 0; j < entList.size(); j++) {
        ret = ret.concat(entList.get(j).getProperty("Name") + ",");
      }
      ret = ret.substring(0, ret.length() - 1);
      model.addAttribute("entNamesByLabel", ret);

      return ret;
   }

   @ResponseBody
   @RequestMapping(value = "addEntRelationships.exec", method = RequestMethod.POST)
   public String addEntRelationships(@RequestBody NameValuePair pair, Model model)
   {
     String pname = pair.getName();
     String lname = pair.getName();
     String[] valArr = pair.getValue().split(",");
     String[] nameArr = pair.getName().split(",");
     String nameId = nameArr[0];
     String label = nameArr[1];
     String parentLabel = nameArr[2];
     List<Node> entList = (List<Node>) service.invokeDao("getEntitiesByLabel", label);
     List<Node> parentEntList = (List<Node>) service.invokeDao("getEntitiesByLabel", parentLabel);
     String ret = "";
     List<Node> parents;

      for (int j = 0; j < entList.size(); j++) {
        if (nameId.equals(entList.get(j).getProperty("Name"))) {
            Iterable<Relationship> relList = entList.get(j).getRelationships(Direction.INCOMING);

            //delete existing relationships
            for (Relationship myRel : relList) {
              myRel.delete();
            }
            for (int k = 0; k < parentEntList.size(); k++) {
                  for (int z = 0; z < valArr.length; z++) {
                    if (valArr[z].equals(parentEntList.get(k).getProperty("Name"))) {
                  //entList.get(j).createRelationshipTo(parentEntList.get(k), DynamicRelationshipType.withName("HAS_PARENT"));
                  parentEntList.get(k).createRelationshipTo(entList.get(j), DynamicRelationshipType.withName("HAS"));
                }
                }
            }
        }
      }

      return ret;
   }

   @ResponseBody
   @RequestMapping(value = "addChildRelationships.exec", method = RequestMethod.POST)
   public String addChildRelationships(@RequestBody NameValuePair pair, Model model)
   {
     String pname = pair.getName();
     String lname = pair.getName();
     String[] valArr = pair.getValue().split(",");
     String[] nameArr = pair.getName().split(",");
     String nameId = nameArr[0];
     String label = nameArr[1];
     String childLabel = nameArr[2];
     List<Node> entList = (List<Node>) service.invokeDao("getEntitiesByLabel", label);
     List<Node> childEntList = (List<Node>) service.invokeDao("getEntitiesByLabel", childLabel);

     String ret = "";

      for (int j = 0; j < entList.size(); j++) {
        if (nameId.equals(entList.get(j).getProperty("Name"))) {
            //delete existing relationships
            Iterable<Relationship> relList = entList.get(j).getRelationships(Direction.OUTGOING);
            for (Relationship myRel : relList) {
              myRel.delete();
            }
            for (int k = 0; k < childEntList.size(); k++) {
                  for (int z = 0; z < valArr.length; z++) {
                    if (valArr[z].equals(childEntList.get(k).getProperty("Name"))) {
                  entList.get(j).createRelationshipTo(childEntList.get(k), DynamicRelationshipType.withName("HAS"));
                }
                }
            }
        }
      }


      return ret;
   }

   @ResponseBody
   @RequestMapping(value = "getParentRelationshipsOfNode.exec", method = RequestMethod.POST)
   public String getParentRelationshipsOfNode(@RequestBody NameValuePair pair)
   {

     String label = pair.getName();
     String instanceName = pair.getValue();
     String ret = "";
     List<Node> entList = (List<Node>) service.invokeDao("getEntitiesByLabel", label);
     for (int i = 0; i < entList.size(); i++) {
       if (entList.get(i).getProperty("Name").equals(instanceName)) {
             Iterable<Relationship> relList = entList.get(i).getRelationships(Direction.INCOMING);

             for (Relationship myRel : relList) {
               String pname = (String) myRel.getStartNode().getProperty("Name");
               ret = ret.concat(pname + ",");
             }
       }
     }



     return ret;
   }

   @ResponseBody
   @RequestMapping(value = "getChildRelationshipsOfNode.exec", method = RequestMethod.POST)
   public String getChildRelationshipsOfNode(@RequestBody NameValuePair pair)
   {
     String label = pair.getName();
     String instanceName = pair.getValue();
     String ret = "";
     List<Node> entList = (List<Node>) service.invokeDao("getEntitiesByLabel", label);
     //Node nodeInstance;
     for (int i = 0; i < entList.size(); i++) {
       if (entList.get(i).getProperty("Name").equals(instanceName)) {
             Iterable<Relationship> relList = entList.get(i).getRelationships(Direction.OUTGOING);

             for (Relationship myRel : relList) {
               String pname = (String) myRel.getEndNode().getProperty("Name");
               ret = ret.concat(pname + ",");
             }
       }
     }


     return ret;
   }


   @ResponseBody
   @RequestMapping(value = "updateEntity.exec", method = RequestMethod.POST)
   public String updateEntity(@RequestBody Parameter parameter)
   {
      service.invokeDao("updateEntity", parameter);
      return null;
   }

   @ResponseBody
   @RequestMapping(value = "addEntity.exec", method = RequestMethod.POST)
   public String addEntity(@RequestBody Entity entity)
   {
      service.invokeDao("addEntity", entity);
      return null;
   }

   @ResponseBody
   @RequestMapping(value = "saveEntity.exec", method = RequestMethod.POST)
   public String saveEntity(@RequestBody Entity entity)
   {
      service.invokeDao("saveEntity", entity);
      return null;
   }

   @RequestMapping(value = "deleteEntity.exec", method = RequestMethod.DELETE)
   @ResponseBody
   public void deleteEntity(@RequestBody Parameter parameter)
   {
      String url = null;

      try
      {
         // delete data node & relationships
         url = new StringBuilder(AbstractRestController.servletContext.getInitParameter(ATTUNE_BASE_URL)).append("/api/data/nodes/").append(parameter.getValue()).toString();
         callRestService(url, HttpMethod.DELETE, null, String.class).getBody();
         // delete entity & relationships
         url = new StringBuilder(AbstractRestController.servletContext.getInitParameter(ATTUNE_BASE_URL)).append(ENTITY_DIRECTORY).append(parameter.getValue()).toString();
         callRestService(url, HttpMethod.DELETE, null, String.class).getBody();
      }
      catch (AttuneException ae)
      {
         throw ae;
      }
      catch (Exception e)
      {
         throw new AttuneException(e);
      }

   }

   @RequestMapping(value = "deleteEntityRelationship.exec", method = RequestMethod.DELETE)
   @ResponseBody
   public void deleteEntityRelationship(@RequestBody String id)
   {
      try
      {
         //delete relationships
         String url = new StringBuilder(AbstractRestController.servletContext.getInitParameter(ATTUNE_BASE_URL)).append("/api/metadata").append("/relationship/").append(id).toString();
         callRestService(url, HttpMethod.DELETE, null, String.class).getBody();

      }
      catch (AttuneException ae)
      {
         throw ae;
      }
      catch (Exception e)
      {
         throw new AttuneException(e);
      }
   }

   @RequestMapping(value = "insertRelationship.exec", method = RequestMethod.POST)
   @ResponseBody
   public void insertRelationship(@RequestBody EntityRelationship relationship)
   {
      try
      {
         String url = new StringBuilder(AbstractRestController.servletContext.getInitParameter(ATTUNE_BASE_URL)).append(ENTITY_DIRECTORY).append(relationship.getStartNodeType()).append("/relationships/").append(relationship.getRelationshipType()).append("/").append(relationship.getEndNodeType()).toString();

         Map<String, Object> relProperties = new HashMap<>();
         relProperties.put("required", relationship.getRequired());
         String data = new JSONObject(relProperties).toJSONString();
         callRestService(url, HttpMethod.POST, data, String.class).getBody();

      }
      catch (AttuneException ae)
      {
         throw ae;
      }
      catch (Exception e)
      {
         throw new AttuneException(e);
      }
   }

   @ResponseBody
   @PreAuthorize("hasRole('" + SecurityService.ROLE_USER + "')")
   @RequestMapping(value = "getRelationships.exec", method = RequestMethod.POST)
   public List<EntityRelationship> getRelationships(@RequestBody String entityType)
   {

      List<EntityRelationship> rels = new ArrayList<>();

//      try
//      {
//         String url = new StringBuilder(AbstractRestController.servletContext.getInitParameter(ATTUNE_BASE_URL)).append(ENTITY_DIRECTORY).append(entityType).append("/relationships/").append("all").toString();
//         List<String> relTypes = new ArrayList<>();
//         relTypes.add("has");
//         url = url + this.getQueryParamString("relType", relTypes);
//
//         String json = (String) callRestService(url, HttpMethod.GET, null, String.class).getBody();
//         if (json != null)
//         {
//            Object jsonObj = parser.parse(json);
//            JSONArray jsonArray = (JSONArray) jsonObj;
//            for (int i = 0; i < jsonArray.size(); i++)
//            {
//               JSONObject o = (JSONObject) jsonArray.get(i);
//               EntityRelationship entityRelationship = new EntityRelationship();
//               entityRelationship.setNeo4jId(o.get("neo4j.id").toString());
//               entityRelationship.setStartNodeId(o.get("startNodeId").toString());
//               entityRelationship.setStartNodeType(o.get("startNodeType").toString());
//               entityRelationship.setStartNodeName(o.get("startNodeName").toString());
//               entityRelationship.setEndNodeId(o.get("endNodeId").toString());
//               entityRelationship.setEndNodeType(o.get("endNodeType").toString());
//               entityRelationship.setEndNodeName(o.get("endNodeName").toString());
//               entityRelationship.setRequired(o.get("required").toString());
//               entityRelationship.setRelationshipType(o.get("relationshipType").toString());
//               rels.add(entityRelationship);
//            }
//         }
//      }
//      catch (AttuneException ae)
//      {
//         throw ae;
//      }
//      catch (Exception e)
//      {
//         throw new AttuneException(e);
//      }

      return rels;
   }

   @RequestMapping(value = "getEntityProperties.exec", method = RequestMethod.POST)
   @ResponseBody
   public Map<String, Object> getEntityProperties(@RequestBody String entityType)
   {
      Map<String, Object> properties = new HashMap<>();

      try
      {
         String url = new StringBuilder(AbstractRestController.servletContext.getInitParameter(ATTUNE_BASE_URL)).append(ENTITY_DIRECTORY).append(entityType).toString();
         String json = (String) callRestService(url, HttpMethod.GET, null, String.class).getBody();
         if (json != null)
         {
            Object jsonObj = parser.parse(json);
            JSONObject jsonObject = (JSONObject) jsonObj;
            for (Object key : jsonObject.keySet())
            {
               String propName = (String) key;
               Object propValue = jsonObject.get(propName);
               properties.put(propName, propValue);
            }
         }

      }
      catch (AttuneException ae)
      {
         throw ae;
      }
      catch (Exception e)
      {
         throw new AttuneException(e);
      }

      return properties;
   }

   @SuppressWarnings("unchecked")
   @RequestMapping(value = "getEntities.exec", method = RequestMethod.POST)
   @ResponseBody
   public List<String> getEntities()
   {

      List<String> entityTypes = new ArrayList<>();

      try
      {
         String url = new StringBuilder(AbstractRestController.servletContext.getInitParameter(ATTUNE_BASE_URL)).append("/api/metadata/entityTypes").toString();
         ResponseEntity<String> responseEntity = this.callRestService(url, HttpMethod.GET, null, String.class);
         int statusCode = responseEntity.getStatusCode().value();
         String json = responseEntity.hasBody() ? responseEntity.getBody() : new JSONObject().toJSONString();
         if (!((statusCode >= 200) && (statusCode < 300)))
         {
            String msg = json;
            throw new AttuneException(msg, responseEntity.getStatusCode());
         }
         if (json != null)
         {
            Object jsonObj = parser.parse(json);
            JSONArray jsonArray = (JSONArray) jsonObj;
            for (int i = 0; i < jsonArray.size(); i++)
            {
               String entityType = (String) jsonArray.get(i);
               entityTypes.add(entityType);
            }
         }

      }
      catch (AttuneException ae)
      {
         throw ae;
      }
      catch (Exception e)
      {
         throw new AttuneException(e);
      }

      return entityTypes;
   }

   @RequestMapping(value = "deleteNode.exec", method = RequestMethod.DELETE)
   @ResponseBody
   public void deleteDataNode(@RequestBody Parameter parameter)
   {
      try
      {
         String url = new StringBuilder(AbstractRestController.servletContext.getInitParameter(ATTUNE_BASE_URL)).append(NODE_DIRECTORY).append(parameter.getValue()).toString();
         callRestService(url, HttpMethod.DELETE, null, String.class).getBody();
      }
      catch (AttuneException ae)
      {
         throw ae;
      }
      catch (Exception e)
      {
         throw new AttuneException(e);
      }
   }

   @SuppressWarnings("unchecked")
   @RequestMapping(value = "updateNode.exec", method = RequestMethod.PUT)
   @ResponseBody
   public void updateNode(@RequestBody Entity entity)
   {
      try
      {
         String url = new StringBuilder(AbstractRestController.servletContext.getInitParameter(ATTUNE_BASE_URL)).append(NODE_DIRECTORY).append(entity.getNeo4jId()).append("/properties").toString();

         String data = null;
         if (entity.getProperties() != null && !entity.getProperties().isEmpty())
         {
            Map<String, Object> properties = new HashMap<>();
            for (NameValuePair pair : entity.getProperties())
            {
               properties.put(pair.getName(), pair.getValue());
            }
            data = new JSONObject(properties).toJSONString();
         }

         this.callRestService(url, HttpMethod.PUT, data, String.class).getBody();

      }
      catch (AttuneException ae)
      {
         throw ae;
      }
      catch (Exception e)
      {
         throw new AttuneException(e);
      }
   }

   @RequestMapping(value = "insertNode.exec", method = RequestMethod.POST)
   @ResponseBody
   public void insertNode(@RequestBody Entity entity)
   {
     String n4j = entity.getNeo4jId();
      try
      {
         String url = new StringBuilder(AbstractRestController.servletContext.getInitParameter(ATTUNE_BASE_URL)).append(NODE_DIRECTORY).append(entity.getLabel()).toString();
         String data;
         if (entity.getProperties() != null && !entity.getProperties().isEmpty())
         {
            Map<String, Object> properties = new HashMap<>();
            for (NameValuePair pair : entity.getProperties())
            {
               properties.put(pair.getName(), pair.getValue());
            }
            data = new JSONObject(properties).toJSONString();
            callRestService(url, HttpMethod.POST, data, String.class).getBody();
         }
      }
      catch (AttuneException ae)
      {
         throw ae;
      }
      catch (Exception e)
      {
         throw new AttuneException(e);
      }
   }
}
