package com.bah.attune.servlet;

import com.bah.attune.service.UserService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.context.support.SpringBeanAutowiringSupport;

import javax.servlet.*;
import java.io.IOException;

//import mil.af.dcgs.formation.util.AuditLogger;

/**
 * This class creates a background thread that periodically scans the Formation
 * users for inactive accounts.  Making it a Servlet allows it to leverage the
 * Spring frameworks Autowired support (however it must still make calls to the
 * UserService for transaction support.
 * <p>
 * It accepts two init parameters, frequency and maxInactiveDays.  Frequency
 * is the time, in seconds, to sleep between scans by the background thread.
 * The background thread can be disabled entirely by passing it zero or a
 * negative number.  The default value is 3600 (one hour).
 * <p>
 * maxInactiveDays is the number of days between the last login date and the
 * time when the background thread runs.  Any account that hasn't logged in
 * longer than maxInactiveDays will be disabled.  The default value is 90.
 *
 * @author Tom Dunn
 */
public class AttuneUserServlet extends GenericServlet
{

   /**
    * ID used for serialization version
    */
   private static final long serialVersionUID = 1L;

   Logger LOGGER = LoggerFactory.getLogger(this.getClass());

   /**
    * by default, check for inactive user accounts once per hour (if no frequency provided in web.xml)
    */
   public final static Integer DEFAULT_INACTIVE_USER_ACCOUNT_CHECK_TIME = 21600;

   /**
    * application module name used for audit log entries
    */
   private static final String APPLICATION_MODULE = "Attune";

   private Thread backgroundThread = null;

   private DeactivateInactivedUserThread runnable;

   private Integer maxInactiveDays;

   @Autowired
   private UserService userService;

   /**
    * Process the init parameters from the web.xml file to set the frequency
    * and maxInactiveDays.  If frequency is greater than zero, this method
    * spawns a background thread to perform inactive account checks periodically.
    * When run, this method creates an audit trail entry that the Service has
    * started.
    *
    * @param config - The servlet configuration from the servlet container
    */
   public void init(ServletConfig config) throws ServletException
   {
      super.init(config);

      SpringBeanAutowiringSupport.processInjectionBasedOnCurrentContext(this);
      String frequencyValue = getServletConfig().getInitParameter("frequency");
      Integer frequency = DEFAULT_INACTIVE_USER_ACCOUNT_CHECK_TIME;

      if (frequencyValue != null)
      {
         try
         {
            frequency = Integer.parseInt(frequencyValue);
         }
         catch (NumberFormatException e)
         {
            LOGGER.error("Could not parse frequency", e);
         }
      }

      String maxInactiveDaysValue = getServletConfig().getInitParameter("maxInactiveDays");

      if (maxInactiveDaysValue != null)
      {
         try
         {
            maxInactiveDays = Integer.parseInt(maxInactiveDaysValue);
         }
         catch (NumberFormatException e)
         {
            LOGGER.error("Could not parse maxInactiveDays", e);
         }
      }

      if (frequency > 0)
      {
         runnable = new DeactivateInactivedUserThread(this, frequency);
         backgroundThread = new Thread(runnable);
         backgroundThread.start();
      }
//      AuditLogger.logAuditRecord(LOGGER, AuditLogger.MODULE_START_EVENT, "AttuneUserServlet module initialized",
//              APPLICATION_MODULE, this.getClass(), "init");
   }

   /**
    * Terminate the background thread and create an audit trail entry that the Servlet
    * has stopped.
    */
   public void destroy()
   {
      try
      {
         if (runnable != null)
         {
            runnable.terminate();
         }
         if (backgroundThread != null)
         {
            backgroundThread.interrupt();
            backgroundThread.join();
         }
      }
      catch (InterruptedException e)
      {
         LOGGER.info("Thread join interrupted", e);
      }
//      AuditLogger.logAuditRecord(LOGGER, AuditLogger.MODULE_STOP_EVENT, "AttuneUserServlet module stopped",
//              APPLICATION_MODULE, this.getClass(), "destroy");
   }

   /**
    * NOOP, this method is required by the base class, but performs no function.
    *
    * @param req - the Servlet request
    * @param res - the Servlet response
    */
   @Override
   public void service(ServletRequest req, ServletResponse res) throws IOException, ServletException
   {
      // this method intentionally left blank
   }

   public String getServletName()
   {
      return this.getClass().getSimpleName();
   }

   /**
    * Pass through method from the background thread to the autowired UserService.
    */
   void checkForInactiveFormationUserAccounts() throws Exception
   {
      userService.checkForInactiveUserAccounts(maxInactiveDays);
   }
}
