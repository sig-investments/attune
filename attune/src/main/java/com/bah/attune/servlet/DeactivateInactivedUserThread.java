package com.bah.attune.servlet;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class DeactivateInactivedUserThread implements Runnable
{

   Logger LOGGER = LoggerFactory.getLogger(this.getClass());

   private AttuneUserServlet servlet = null;

   private Integer frequency = null;

   private volatile boolean running = true;

   public DeactivateInactivedUserThread(AttuneUserServlet servlet, Integer frequency)
   {
      this.servlet = servlet;
      this.frequency = frequency;
   }

   public Integer getFrequency()
   {
      return frequency;
   }

   @Override
   public void run()
   {
      while (running)
      {
         if (running)
         {
            try
            {
               servlet.checkForInactiveFormationUserAccounts();
            }
            catch (Exception e)
            {
               LOGGER.info("Interrupted thread sleep", e);
            }
         }
         try
         {
            Thread.sleep(frequency.intValue() * 1000);
         }
         catch (InterruptedException e)
         {
            if (running)
            {
               LOGGER.info("Interrupted thread sleep", e);
            }
            running = false;
         }
      }
   }

   public void terminate()
   {
      running = false;
   }

}
