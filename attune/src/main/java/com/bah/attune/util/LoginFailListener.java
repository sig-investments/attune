package com.bah.attune.util;

import com.bah.attune.service.AuditService;
import com.bah.attune.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.web.authentication.SimpleUrlAuthenticationFailureHandler;
import org.springframework.stereotype.Component;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

@Component
public class LoginFailListener extends SimpleUrlAuthenticationFailureHandler
{
   @Autowired
   private AuditService auditService;

   @Autowired
   private UserService userService;

   @Override
   public void onAuthenticationFailure(HttpServletRequest request, HttpServletResponse response,
                                       AuthenticationException exception) throws IOException, ServletException
   {
      String username = request.getParameter("username");

      userService.handleLoginFail(username);
      if (!exception.getMessage().isEmpty()) {
         request.setAttribute("loginError", exception.getMessage());
      }  

      request.getRequestDispatcher("/login.exec").forward(request, response);
      //System.out.println(exception.getMessage());


   }
}
