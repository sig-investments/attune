package com.bah.attune.util;

import org.apache.logging.log4j.Logger;
import org.apache.logging.log4j.Marker;
import org.apache.logging.log4j.MarkerManager;
import org.apache.logging.log4j.core.LogEvent;
import org.apache.logging.log4j.core.config.plugins.Plugin;
import org.apache.logging.log4j.core.lookup.StrLookup;
import org.apache.logging.log4j.core.lookup.SystemPropertiesLookup;
import org.apache.logging.log4j.status.StatusLogger;
import org.apache.logging.log4j.web.WebLookup;

@Plugin(name = "attune", category = StrLookup.CATEGORY)
public class Log4J2PropertyLookup extends SystemPropertiesLookup {
	
	private static final Logger LOGGER = StatusLogger.getLogger();
    private static final Marker LOOKUP = MarkerManager.getMarker("LOOKUP");

	@Override
	public String lookup(LogEvent event, String key) {
		try {
			if("attune.log.file.path".equals(key)){
				String logFilePath = null;
				if(super.lookup(event, key) != null){
					//formation.log.file.path System Property
					logFilePath = super.lookup(event, key);
				} else if(System.getProperty("jboss.server.log.dir") != null){
					//JBoss
					logFilePath = System.getProperty("jboss.server.log.dir");
				} else if(System.getProperty("jetty.logs") != null){
					//Jetty
					logFilePath = System.getProperty("jetty.logs");
				} else if(System.getProperty("catalina.home") != null){
					//Tomcat
					logFilePath = System.getProperty("catalina.home") + "/logs";
				}
				if(logFilePath == null){
					WebLookup webLookup = new WebLookup();
					logFilePath = webLookup.lookup(event, "rootDir");
				}
				if(logFilePath == null){
					logFilePath = "./";
					LOGGER.warn(LOOKUP, "Could not determine value for system property [{}], defaulting to " + logFilePath, key);					
				}
				return logFilePath;
			} else {
				LOGGER.info(LOOKUP, "Log4J2PropertyLookup resolving property " + key);
				return super.lookup(event, key);
			}
		}  catch (final Exception ex) {
            LOGGER.warn(LOOKUP, "Error while getting system property [{}].", key, ex);
            return null;
        }
	}

}
