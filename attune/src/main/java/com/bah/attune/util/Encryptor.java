package com.bah.attune.util;

import org.jasypt.encryption.pbe.StandardPBEStringEncryptor;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.util.StringUtils;

@Component
public class Encryptor
{
   private static Logger LOGGER = LoggerFactory.getLogger(Encryptor.class);

   @Autowired
   private StandardPBEStringEncryptor encryptor;

   public String encrypt(String cleartext)
   {
      if (cleartext == null || cleartext.length() == 0 ||
              (StringUtils.startsWithIgnoreCase(cleartext, "ENC(") && StringUtils.endsWithIgnoreCase(cleartext, ")")))
         return cleartext;

      return "ENC(" + encryptor.encrypt(cleartext) + ")";
   }


   public String decrypt(String encryptedText)
   {
      if (encryptedText == null || encryptedText.length() == 0 ||
              !StringUtils.startsWithIgnoreCase(encryptedText, "ENC(") ||
              !StringUtils.endsWithIgnoreCase(encryptedText, ")"))
      {
         return encryptedText;
      }

      try
      {
         return encryptor.decrypt(encryptedText.substring(4, encryptedText.length() - 1));
      }
      catch (Exception e)
      {
         return encryptedText;
      }
   }
}
