package com.bah.attune.util;

import com.bah.attune.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.Authentication;
import org.springframework.security.web.authentication.SimpleUrlAuthenticationSuccessHandler;
import org.springframework.stereotype.Component;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

@Component
public class LoginSuccessListener extends SimpleUrlAuthenticationSuccessHandler
{
   @Autowired
   private UserService userService;


   @Override
   public void onAuthenticationSuccess(HttpServletRequest request, HttpServletResponse response, Authentication authentication) throws IOException, ServletException
   {
      userService.handleLoginSuccess(request.getParameter("username"));

      this.handle(request, response, authentication);
      this.clearAuthenticationAttributes(request);
   }
}
