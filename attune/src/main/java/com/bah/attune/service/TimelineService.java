package com.bah.attune.service;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.io.StringBufferInputStream;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Paths;

import javax.servlet.ServletContext;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;

import com.bah.attune.dao.BaseDao;
import com.bah.attune.dao.TimelineDao;
import com.bah.attune.data.AttuneException;
import com.bah.attune.data.TimelineData;
import com.bah.attune.data.TimelineEvent;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.w3c.dom.Document;
import org.xhtmlrenderer.pdf.ITextRenderer;


@Service public class TimelineService extends BaseService
{
    private static final Logger LOGGER = LoggerFactory.getLogger(TimelineService.class);

    @Autowired private TimelineDao dao;
    private ServletContext servletContext;

    @Override
    public BaseDao getDao()
    {
        return dao;
    }
    
    public void setServletContext(ServletContext context) {
    	this.servletContext = context;							
    }															

    public byte[] createPDFinMemory(String htmlContent) throws AttuneException
    {
        try
        {
            System.out.println("in createPDFinMemory functino");
           // System.out.println(htmlContent);
            // Build the html

            System.out.println("before stringbuilder");
            StringBuilder htmlBuilder = buildHtml(htmlContent);

            System.out.println(" before factory");

            DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
            factory.setFeature("http://xml.org/sax/features/external-general-entities", false);
            factory.setFeature("http://xml.org/sax/features/external-parameter-entities", false);
            System.out.println("before builder");

            // Create the pdf document, and save it to the images directory

            DocumentBuilder builder = factory.newDocumentBuilder();
            //System.out.println(htmlBuilder.toString());
            Document document = builder.parse(new StringBufferInputStream(htmlBuilder.toString()));
                        System.out.println(" after before builder");

            ITextRenderer renderer = new ITextRenderer();
            renderer.setDocument(document, "");

                                    System.out.println(" after renderer");

            OutputStream output = new ByteArrayOutputStream();
            renderer.layout();
            renderer.createPDF(output);
            output.close();

            return ((ByteArrayOutputStream) output).toByteArray();
        }
        catch (Exception e)
        {
            LOGGER.error("Error occured in createPDFinMemory ", e);
            throw new AttuneException(
                    "Error occured in createPDFinMemory, Parameters:" + htmlContent + " , Error:" + e.toString());
        }
    }


    private StringBuilder buildHtml(String htmlContent) throws IOException
    {
        StringBuilder htmlBuilder = new StringBuilder();
        String root =  org.springframework.web.util.WebUtils.getRealPath(servletContext, "/");  

        htmlBuilder.append("<html>\n");
        htmlBuilder.append("	<head>\n");
        htmlBuilder.append("		<title>Timeline</title>\n");
        htmlBuilder.append("		<style>\n");

        System.out.println("\nIn buildHTML function before loops\n");

        for (String line : Files.readAllLines(Paths.get(root + "/css/vis.css"), StandardCharsets.UTF_8))
            htmlBuilder.append(line + "\n");


        for (String line : Files.readAllLines(Paths.get(root + "/css/timeline.css"), StandardCharsets.UTF_8))
            htmlBuilder.append(line + "\n");

        System.out.println("\nIn buildHTML function after loops\n");


        htmlBuilder.append("		</style>\n");
        htmlBuilder.append("	</head>\n");
        htmlBuilder.append("	<body>\n");
        htmlBuilder.append(htmlContent.replaceAll("&nbsp", " "));
        htmlBuilder.append("	</body>\n");
        htmlBuilder.append("</html>");

        return htmlBuilder;
    }
    
    
    public boolean saveTimelineData(TimelineData timelineData)
    {
    	try
		{
    		for (TimelineEvent event: timelineData.getData())
        	{
        		dao.saveTimelineEvent(event);
        	}
		}
    	catch (Exception e)
    	{
    		System.out.println("Error");
    		return false;
    	}
    	
    	return true;
    }
    
}
