package com.bah.attune.service;

import com.bah.attune.dao.AuditDao;
import com.bah.attune.dao.BaseDao;
import com.bah.attune.dao.UserDao;
import com.bah.attune.data.AttuneUser;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@Service
public class AuditService extends BaseService
{
   @Autowired
   private AuditDao dao;

   @Autowired
   private UserDao userDao;


   public BaseDao getDao()
   {
      return dao;
   }


   @Transactional
   public void log(String action)
   {
      dao.log(SecurityService.getUserName(), action, "N/A", "N/A");
   }


   @Transactional
   public void log(String username, String action)
   {
      dao.log(username, action, "N/A", "N/A");
   }


   @Transactional
   public void log(String action, String originalValue, String updatedValue)
   {
      dao.log(SecurityService.getUserName(), action, originalValue, updatedValue);
   }


   @Transactional
   public void log(String username, String action, String originalValue, String updatedValue)
   {
      dao.log(username, action, originalValue, updatedValue);
   }


   @Transactional
   public void logUserEdit(AttuneUser user)
   {
      if (user.getUuid().equals(""))
      {
         dao.log(SecurityService.getUserName(), "Create User", "N/A", user.toString());
      }
      else
      {
         AttuneUser originalUser = userDao.getUser(user.getUuid());

         dao.log(SecurityService.getUserName(), "Edit User", originalUser.toString(), user.toString());
      }
   }
}

