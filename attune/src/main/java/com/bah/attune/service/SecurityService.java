package com.bah.attune.service;

import com.bah.attune.dao.UserDao;
import com.bah.attune.data.AttuneUser;
import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.User;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

@Service
public class SecurityService implements UserDetailsService
{

   public static final String USER_ATTUNE = "Attune";
   public static final String PASSWORD_ATTUNE = "Welcome!";

   public static final String USER_REVIEW = "Review";
   public static final String PASSWORD_REVIEW = "Welcome!";

   public static final String ROLE_USER = "ROLE_USER";
   public static final String ROLE_ADMIN = "ROLE_ADMIN";

   public static final String USERNAME_FIELD = "username";
   public static final String PSW_FIELD = "password";

   public static final String AUTHORITIES_FIELD = "authorities";
   public static final String ACCT_EXPIRED_FIELD = "accountExpired";
   public static final String ACCT_LOCKED_FIELD = "accountLocked";
   public static final String CREDENTIALS_EXPIRED_FIELD = "credentialsExpired";
   public static final String ENABLED_FIELD = "enabled";

   @Autowired
   private AuditService auditService;

   @Autowired
   private UserDao userDao;

   @Override
   public UserDetails loadUserByUsername(String username)
   {
      AttuneUser user = new AttuneUser();

      boolean enabled = true;
      boolean accountNonExpired = true;
      boolean credentialsNonExpired = true;
      boolean accountNonLocked = true;

      if (username.contains("CN="))
      {
//         user = getUserByDodId(getDodId(username));
          String name = getCN(username);
          user.setUsername(name);
          user.setPassword("");
      }
      else
      {
         if (username.equals(USER_ATTUNE))
         {
            user.setUsername(USER_ATTUNE);
            user.setPassword(PASSWORD_ATTUNE);
         }
         else if (username.equals(USER_REVIEW))
         {
            user.setUsername(USER_REVIEW);
            user.setPassword(PASSWORD_REVIEW);
         }
         else
         {
            user = userDao.getUserByname(username);
         }
      }

      if ( user != null )
      {
         if ( ! username.equals(USER_ATTUNE) && ! username.equals(USER_REVIEW) && ! username.contains("CN="))
            enabled = user.getIsActive();

         return new User(user.getUsername(), user.getPassword(), enabled, accountNonExpired,
                           credentialsNonExpired, accountNonLocked, getAuthorities(user));
      }
      else
      {
         return new User("nonexist", "nonexist", enabled, accountNonExpired,
                 credentialsNonExpired, accountNonLocked, getAuthorities(user));

      }
   }


   public Collection<GrantedAuthority> getAuthorities(AttuneUser user)
   {
      return getGrantedAuthorities(getRoles(user));
   }


   public List<String> getRoles(AttuneUser user)
   {
      List<String> roles = new ArrayList<>();

      roles.add(ROLE_USER);

      if ( user != null )
      {
         if ( user.getUsername().equals("Attune") ||
              user.getUsername().equals("Administrator.System") ||
              ( user.getRole() != null && user.getRole().equals("admin")) )
         {
            roles.add(ROLE_ADMIN);
         }
      }
      return roles;
   }


   public static List<GrantedAuthority> getGrantedAuthorities(List<String> roles)
   {
      List<GrantedAuthority> authorities = new ArrayList<>();

      for (String role : roles)
      {
         authorities.add(new SimpleGrantedAuthority(role));
      }

      return authorities;
   }


   public static String getUserName()
   {
      String userName;

      if (SecurityContextHolder.getContext().getAuthentication() == null)
      {
         return null;
      }

      Object principal = SecurityContextHolder.getContext().getAuthentication().getPrincipal();
      if (principal instanceof UserDetails)
      {
         userName = ((UserDetails) principal).getUsername();
      }
      else
      {
         userName = USER_ATTUNE;// for testing only
      }
      return userName;
   }


   public static JSONObject getCurrentUser()
   {
      String userName = getUserName();
      if (userName == null)
      {
         userName = USER_ATTUNE;
      }
      UserDetails user = new SecurityService().loadUserByUsername(userName);

      JSONObject userJson = null;
      if (user != null)
      {
         userJson = new JSONObject();
         JSONArray authorities = new JSONArray();
         for (GrantedAuthority auth : user.getAuthorities())
         {
            authorities.add(auth.getAuthority());
         }
         if (authorities.isEmpty() && USER_ATTUNE.equals(user.getUsername()) )
         {
            authorities.add(ROLE_USER);
         }
         userJson.put(AUTHORITIES_FIELD, authorities);
         userJson.put(USERNAME_FIELD, user.getUsername());
         userJson.put(PSW_FIELD, user.getPassword());
         userJson.put(ACCT_EXPIRED_FIELD, !user.isAccountNonExpired());
         userJson.put(ACCT_LOCKED_FIELD, !user.isAccountNonLocked());
         userJson.put(CREDENTIALS_EXPIRED_FIELD, !user.isCredentialsNonExpired());
         userJson.put(ENABLED_FIELD, user.isEnabled());
      }
      return userJson;
   }


   /* /C=US/O=U.S. Government/OU=DoD/OU=PKI/OU=CONTRACTOR/CN=KIRK.JAMES.T.1099001909 */
   private String getDodId(String header)
   {
      String edipi = null;

      String[] headerList = header.split("/");
      if (headerList.length == 1)
         headerList = header.split(",");

      if (headerList != null && headerList.length > 1)
      {
         String cn = extractCN(headerList);

         String[] cnTokens = cn.split("\\.");
         if (cnTokens == null || cnTokens.length < 2)
            throw new RuntimeException(String.format("Invalid Certificate. CN=", cn));

         edipi = cnTokens[cnTokens.length - 1].trim();
      }

      return edipi;

   }

   private String getCN(String header)
   {
      String cn = null;

      String[] headerList = header.split("/");
      if (headerList.length == 1)
         headerList = header.split(",");

      if (headerList != null && headerList.length > 1)
      {
         String h = extractCN(headerList);

         String[] cnTokens = h.split("\\.");

         cn = cnTokens[cnTokens.length - 1].trim();

         int index = h.lastIndexOf(".");
         cn = h.substring(0, index);
      }

      return cn;

   }


   private String extractCN(String[] tokenList)
   {
      for (String token : tokenList)
      {
         String[] tokenSplit = token.split("=");
         if (tokenSplit != null && tokenSplit.length > 1)
         {
            if (tokenSplit[0].trim().equals("CN"))
            {
               return tokenSplit[1].trim();
            }
         }
      }

      return null;
   }
}
