package com.bah.attune.service;

import com.bah.attune.dao.BaseDao;
import com.bah.attune.dao.UserDao;
import com.bah.attune.data.AttuneUser;
import com.bah.attune.util.Encryptor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.List;
import java.util.concurrent.TimeUnit;

@Service
public class UserService extends BaseService
{
   /**
    * Timestamp to use for account creation if database has NULL,
    * use midnight Jan 1, 2017 by default
    */
   private static final Calendar DEFAULT_ACCOUNT_CREATION_TIMESTAMP;
   SimpleDateFormat fmt = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");

   static
   {
      DEFAULT_ACCOUNT_CREATION_TIMESTAMP = GregorianCalendar.getInstance();
      DEFAULT_ACCOUNT_CREATION_TIMESTAMP.set(2017, Calendar.AUGUST, 01, 0, 0, 0);
   }

   private static final int MAX_INACTIVE_DAYS = 90;

   private static final int MAX_LOGIN_FAIL_COUNT = 5;

   private static final int MAX_LOGIN_FAIL_MINUTES = 15;

   @Autowired
   private AuditService auditService;

   @Autowired
   private Encryptor encryptor;

   @Autowired
   private UserDao dao;


   @Override
   public BaseDao getDao()
   {
      return dao;
   }


   @Transactional
   public void saveUser(AttuneUser user) throws Exception
   {
      auditService.logUserEdit(user);

      AttuneUser originalUser = dao.getUser(user.getUuid());

      if ( originalUser != null )
      {
         if (originalUser.getIsActive().booleanValue() != user.getIsActive().booleanValue())
         {
            if (user.getIsActive())
               auditService.log("Activate User", user.getUsername(), "N/A");
            else
               auditService.log("Deactivate User", user.getUsername(), "N/A");

         }

         if (!encryptor.decrypt(originalUser.getPassword()).equals(user.getPassword()))
            auditService.log("Change Password", user.getUsername(), "N/A");
      }

      dao.saveUser(user);


   }


   @Transactional
   public void deleteUser(String uuid)
   {
      AttuneUser user = dao.getUser(uuid);

      dao.deleteUser(uuid);

      auditService.log("Delete User", user.getUsername(), "N/A");

   }


   @Transactional
   public void checkForInactiveUserAccounts(Integer maxIdleDays) throws Exception
   {
      if (maxIdleDays == null)
         maxIdleDays = MAX_INACTIVE_DAYS;

      List<AttuneUser> userList = dao.getUserList();
      for (AttuneUser user : userList)
      {
         // skip inactive accounts
         if ( user.getIsActive() != null && ! user.getIsActive() )
            continue;

         Date lastLogin = user.getLastLoginDate();
         Date accountCreate = user.getAccountCreateDate();

         if ( accountCreate == null )
            accountCreate = DEFAULT_ACCOUNT_CREATION_TIMESTAMP.getTime();

         if ( lastLogin == null )
            lastLogin = accountCreate;

         long daysSinceLogin = timeDifference(lastLogin, new Date(), TimeUnit.DAYS);

         if (daysSinceLogin > maxIdleDays)
            deactivateUser(user);
      }
   }


   @Transactional
   public void deactivateUser(AttuneUser user)
   {
      user.setIsActive(false);
      user.setLastLoginFailCount(0);
      user.setLastLoginFailDate(null);
      dao.updateUser(user);

      auditService.log("SYSTEM", "Deactivate User", user.getUsername(), "N/A");

   }


   private static long timeDifference(Date date1, Date date2, TimeUnit timeunit)
   {
      return timeunit.convert(date2.getTime() - date1.getTime(), TimeUnit.MILLISECONDS);
   }



   @Transactional
   public void handleLoginSuccess(String username)
   {
      auditService.log(username, "Login Success", "N/A", "N/A");
      dao.recordUserLoginSuccess(username);
   }


   @Transactional
   public void handleLoginFail(String username)
   {

      AttuneUser user = dao.getUserByname(username);
      if ( user != null )
      {
         auditService.log(username, "Login Failed", "N/A", "N/A");

         Date lastLoginFailDate = user.getLastLoginFailDate();
         if ( lastLoginFailDate == null)
         {
            dao.recordUserLoginFail(username);
         }
         else
         {
            long minutes = timeDifference(lastLoginFailDate, new Date(), TimeUnit.MINUTES);

            // have pass max login fail time, reset count and date
            if  ( minutes > MAX_LOGIN_FAIL_MINUTES)
            {
               dao.recordUserLoginFail(username);
            }
            else
            {
               int count = user.getLastLoginFailCount();
               dao.updateUserLoginFail(username, ++count);

               if ( count == MAX_LOGIN_FAIL_COUNT )
                  deactivateUser(user);
            }
         }

      }
   }
}

