package com.bah.attune.service;

import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.bah.attune.dao.BaseDao;
import com.bah.attune.dao.ImportDao;
import com.bah.attune.dao.PortfolioDao;

@Service public class PortfolioService extends BaseService
{
    @Autowired private PortfolioDao dao;
    @Autowired private ImportDao importDao;   

    @Override
    public BaseDao getDao()
    {
        return dao;
    }
    
    @Transactional
	public void savePortfolioConfig(String[] list)
	{
    	if ( !dao.findPortfolioConfig() )
			createPortfolioConfig();
    	
		dao.savePortfolioConfig(list);
	}
	
	@Transactional
	public void createPortfolioConfig() {
		Collection<String> labels = new ArrayList<>();
		labels.add("PortfolioConfig");

		Map<String, Object> properties = new HashMap<>();
		properties.put("chiclet", "chiclet");
		properties.put("grouping", "grouping");
		properties.put("content", "content");

		importDao.createNode(labels, properties);
	}
	
	public String[] getPortfolioConfig()
	{
		return dao.getPortfolioConfig();
	}
    
}
