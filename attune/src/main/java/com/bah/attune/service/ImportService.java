package com.bah.attune.service;

import java.math.BigDecimal;
import java.text.NumberFormat;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;
import org.joda.time.DateTime;
import org.parboiled.common.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.bah.attune.dao.ImportDao;
import com.bah.attune.data.AttuneException;
import com.bah.attune.data.NameValuePair;

@Service
public class ImportService extends BaseService {
	private static final Logger LOGGER = LoggerFactory.getLogger(ImportService.class);

	@Autowired
	private ImportDao importDao;

	@Autowired
	private ValidationService validationService;

	private List<List<NameValuePair>> entities;

	@Transactional
	public List<String> importDataSheet(Workbook wb) {
		LOGGER.info("start importing...");

		long s = System.currentTimeMillis();
		long s0 = s;
		validationService.validateWorkbook(wb);
		LOGGER.info("validation time ="
				+ (System.currentTimeMillis() - s));

		List<String> errorMessages = validationService
				.getWorkbookValidationErrorMessages();

		if (errorMessages.isEmpty()) {
			s = System.currentTimeMillis();

			clearDB();
			LOGGER.info("clearDB time ="
					+ (System.currentTimeMillis() - s));
			s = System.currentTimeMillis();

			setEntities(wb);
			LOGGER.info("setEntities time ="
					+ (System.currentTimeMillis() - s));
			s = System.currentTimeMillis();

			createMetadataModel(wb);
			LOGGER.info("createMetadataModel time ="
					+ (System.currentTimeMillis() - s));
			s = System.currentTimeMillis();

			LOGGER.info("importing data nodes...");
			createDataNodes(wb);
			LOGGER.info("createDataNodes time ="
					+ (System.currentTimeMillis() - s));
			s = System.currentTimeMillis();

			createDataRelationships(wb);
			LOGGER.info("createDataRelationships time ="
					+ (System.currentTimeMillis() - s));
			s = System.currentTimeMillis();

			createDashboard(wb);
			LOGGER.info("createDashboard time ="
					+ (System.currentTimeMillis() - s));
			s = System.currentTimeMillis();

			createTimeline(wb);
			LOGGER.info("createTimeline time ="
					+ (System.currentTimeMillis() - s));

			LOGGER.info("Total import time ="
					+ (System.currentTimeMillis() - s0));
		}

		return errorMessages;
	}

	
	private void setEntities(Workbook wb) {
		try {
			Row row;
			Sheet sheet = wb.getSheet("Entity");
			List<List<NameValuePair>> entityValuesList = new ArrayList<>();

			int i = 1;
			Row titleRow = sheet.getRow(0);

			while (sheet.getRow(i) != null) {

				List<NameValuePair> entityProperties = new ArrayList<>();
				row = sheet.getRow(i);

				for (int j = 0; j<titleRow.getLastCellNum(); j++) {
					NameValuePair pair = new NameValuePair();
					Cell name = titleRow.getCell(j);
					Cell value = row.getCell(j);

					pair.setName(removeSpecialCharacters(name.toString()));

					if ( value != null && "Yes".equalsIgnoreCase(value.getStringCellValue()) && "isRoot".equals(pair.getName())) {
						pair.setValue("true");
					}
					else if ((value == null || value.toString().equals("")) && "isRoot".equals(pair.getName())){
						pair.setValue("false");
					}
					else if (value == null){
						pair.setValue("");
					}
					else {
						pair.setValue(value.toString());
					}

					entityProperties.add(pair);
				}

				entityValuesList.add(entityProperties);
				i++;
			}

			this.entities = entityValuesList;
		} catch (Exception ex) {
			LOGGER.error("Error occured in ImportService" + ex);
			throw new AttuneException(
					"Error occured in ImportService: setEntities "
							+ ex.toString());
		}
	}

	
	private void createMetadataModel(Workbook wb) {
		createEntityNodes();
		createMetadataRelationships(wb);
	}

	
	private void createEntityNodes() {
		try {
			for (List<NameValuePair> entity : entities) {
				Collection<String> labels = new ArrayList<>();
				labels.add("Metadata");

				Map<String, Object> properties = new HashMap<>();
				for(NameValuePair property:entity) {
					properties.put(property.getName(),property.getValue());
				}

				importDao.createNode(labels, properties);
			}

		} catch (Exception ex) {
			LOGGER.error("Error occured in ImportService" + ex);

			throw new AttuneException(
					"Error occured in ImportService: createEntityNodes"
							+ ex.toString());
		}
	}

	
	private void createMetadataRelationships(Workbook wb) {
		try {
			Sheet sheet = wb.getSheet("Metadata");
			Row row;

			int i = 1;
			while (sheet.getRow(i) != null) {
				row = sheet.getRow(i);

				String startEntity = row.getCell(0).getStringCellValue();
				String relationship = row.getCell(1).getStringCellValue();
				String endEntity = row.getCell(2).getStringCellValue();


				boolean required = false;
				if (row.getCell(3) != null
						&& "Yes".equalsIgnoreCase(row.getCell(3)
								.getStringCellValue()))
					required = true;

				int overlap = 0;
				if (row.getCell(4) != null){
					overlap = (int) row.getCell(4).getNumericCellValue();
				}

				importDao.createMetadataRelationship(startEntity, endEntity,
						relationship);
				importDao.setRequired(startEntity, endEntity, required);

				if (overlap > 0)
					importDao.setOverlap(startEntity,endEntity,overlap);

				i++;
			}
		} catch (Exception ex) {
			LOGGER.error("Error occured in ImportService" + ex);
			throw new AttuneException(
					"Error occured in ImportService: createMetadataRelationships"
							+ ex.toString());
		}
	}

	private void createDataNodes(Workbook wb) {
		try {
			for (List<NameValuePair> entity : entities) {
				long s = System.currentTimeMillis();
				analyzeEntity(wb, entity.get(0));
				LOGGER.info("  import time for " + entity.get(0).getValue()
						+ "=" + (System.currentTimeMillis() - s));
			}
		} catch (Exception ex) {
			LOGGER.error("Error occured in ImportService" + ex);
			throw new AttuneException(
					"Error occured in ImportService: createDataNodes"
							+ ex.toString());
		}

	}

	private void analyzeEntity(Workbook wb, NameValuePair entity) {
		Sheet sheet;
		Row row;
		Cell cell;

		sheet = wb.getSheet(entity.getValue());

		Collection<String> labels = new ArrayList<>();
		labels.add(entity.getValue());

		Short headerRowColumnCount = sheet.getRow(0).getLastCellNum();

		String fieldlist = "";

		row = sheet.getRow(0);
		for (int k = 0; k <= headerRowColumnCount; k++) {
			if (row.getCell(k) != null) {
				fieldlist += removeSpecialCharacters(row.getCell(k)
						.getStringCellValue()) + ",";
			}
		}

		fieldlist = fieldlist.substring(0, fieldlist.length() - 1);

		importDao.setFieldlist(entity.getValue(), fieldlist);

		int i = 1;
		long s = System.currentTimeMillis();

		LOGGER.info("importing " + entity.getValue());

		while (sheet.getRow(i) != null) {
			row = sheet.getRow(i);

			if (row.getCell(0) != null
					&& StringUtils.isNotEmpty(row.getCell(0)
							.getStringCellValue())) {
				Map<String, Object> properties = new HashMap<>();

				for (int j = 0; j <= headerRowColumnCount; j++) {
					if (row.getCell(j) != null) {
						cell = row.getCell(j);

						String value = getStringCellValue(cell);

						if (value != null) {
							String property = sheet.getRow(0).getCell(j)
									.getStringCellValue();

							// Call this method prior to removing
							// special characters from the property
							// name.
							value = formatValueForPropertyType(property, value);

							property = removeSpecialCharacters(property);

							properties.put(property, value);
						}
					}
				}

				importDao.createNode(labels, properties);

			}

			i++;

			if (i % 100 == 0) {
				LOGGER.info("  i=" + i + " create entity node time = "
						+ (System.currentTimeMillis() - s));
				s = System.currentTimeMillis();
			}
		}
	}

	private String removeSpecialCharacters(String property) {
		String newString = property;
		newString = newString.replace("!", "");
		newString = newString.replace("*", "");
		newString = newString.replace("#", "");
		newString = newString.replace("$", "");
		newString = newString.replace("/", "");

		// Protect against XSS.
		newString = newString.replace("<", "");
		newString = newString.replace(">", "");

		return newString;
	}

	private void createDataRelationships(Workbook wb) {
		LOGGER.info("importing relationships...");
		try {
			Sheet sheet = wb.getSheet("Relationship");
			Row row;

			int i = 1;
			long s0 = System.currentTimeMillis();
			long s = s0;
			while (sheet.getRow(i) != null
					&& sheet.getRow(i).getCell(0) != null
					&& StringUtils.isNotEmpty(sheet.getRow(i).getCell(0)
							.getStringCellValue())) {
				row = sheet.getRow(i);

				processRelationshipRow(row);

				i++;

				if (i % 500 == 0) {
					LOGGER.info("  i=" + i + " processing time ="
							+ (System.currentTimeMillis() - s));
					s = System.currentTimeMillis();
				}
			}

		} catch (Exception ex) {
			LOGGER.error("Error occured in ImportService" + ex);

			throw new AttuneException(
					"Error occured in ImportService: createDataRelationships "
							+ ex.toString());
		}
	}

	
	private void processRelationshipRow(Row row) {
		importDao.processRelationshipRow(row);
	}

	@Transactional
	public void clearDB() {
		importDao.deleteRelationships();
		importDao.deleteNodes();
	}

	
	private void createDashboard(Workbook wb) {
		Collection<String> labels = new ArrayList<>();
		labels.add("Dashboard");

		Sheet sheet = wb.getSheet("Dashboard");
		for (int i = 1; i < 4; i++) {
			String alertCheck = "";
			String alertValue = "";

			Row row = sheet.getRow(i);
			String name = row.getCell(0).getStringCellValue();
			String entity = row.getCell(1).getStringCellValue();
			String groupBy = row.getCell(2).getStringCellValue();
			String displayList = row.getCell(3).getStringCellValue();
			if (row.getCell(4) != null) {
				alertCheck = row.getCell(4).getStringCellValue();

				Cell cell = row.getCell(5);
				if (cell.getCellType() == Cell.CELL_TYPE_NUMERIC) {
					alertValue = cell.getNumericCellValue() + "";
					if (alertValue.endsWith(".0"))
						alertValue = alertValue.substring(0,
								alertValue.length() - 2);
				} else
					alertValue = cell.getStringCellValue();

			}
			String chartType = row.getCell(6).getStringCellValue();

			Map<String, Object> properties = new HashMap<>();
			properties.put("Name", name);
			properties.put("entity", entity);
			properties.put("groupBy", groupBy);
			properties.put("displayList", displayList);
			properties.put("alertCheck", alertCheck);
			properties.put("alertValue", alertValue);
			properties.put("chartType", chartType);

			importDao.createNode(labels, properties);
		}
	}

	
	private void createTimeline(Workbook wb) {
		Sheet sheet = wb.getSheet("Timeline");
		if (sheet == null)
			return;

		Collection<String> labels = new ArrayList<>();
		labels.add("Timeline");

		String entity = sheet.getRow(0).getCell(1).getStringCellValue();
		Map<String, Object> properties = new HashMap<>();
		properties.put("entity", entity);

		if (sheet.getRow(1) != null)
			properties.put("order by", sheet.getRow(1).getCell(1)
					.getStringCellValue());

		importDao.createNode(labels, properties);
	}

	private String formatValueForPropertyType(String property, String value) {
		String newValue = value;
		if (property.contains("/")) {
			// Handle Excel's formatting of dates
			DateTime beginningOfTime = new DateTime(1900, 1, 1, 0, 0);
			DateTime correctedTime = beginningOfTime.plusDays((int) Double
					.parseDouble(newValue) - 2);
			newValue = correctedTime.toString("MM/dd/yyyy");
		} else if (property.contains("$")) {
			NumberFormat nf = NumberFormat.getCurrencyInstance();
			newValue = nf.format(new BigDecimal(newValue));
		} else if (property.contains("#") && newValue.endsWith(".0")) {
			newValue = newValue.substring(0, newValue.length() - 2);
		}

		return newValue;
	}

	private String getStringCellValue(Cell cell) {
		String value;

		if (cell.getCellType() == Cell.CELL_TYPE_STRING) {
			value = cell.getStringCellValue();
		} else if (cell.getCellType() == Cell.CELL_TYPE_BLANK) {
			return null;
		} else if (cell.getCellType() == Cell.CELL_TYPE_BOOLEAN) {
			Boolean b = cell.getBooleanCellValue();
			value = b.toString();
		} else if (cell.getCellType() == Cell.CELL_TYPE_ERROR) {
			return null;
		} else if (cell.getCellType() == Cell.CELL_TYPE_FORMULA) {
			return null;
		} else if (cell.getCellType() == Cell.CELL_TYPE_NUMERIC) {
			Double d = cell.getNumericCellValue();
			value = d.toString();
		} else {
			return null;
		}

		// Protect against XSS.
		value = value.replace("<", "");
		value = value.replace(">", "");
		
		//Remove carriage returns
		value = value.replaceAll("\\n", "");
		value = value.replaceAll("\\r", "");

		return value;
	}
}
