package com.bah.attune.service;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.neo4j.graphdb.Direction;
import org.neo4j.graphdb.Node;
import org.neo4j.graphdb.Relationship;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.bah.attune.dao.BaseDao;
import com.bah.attune.dao.BaseballCardDao;
import com.bah.attune.data.Metadata;
import com.bah.attune.data.MetadataJson;
import com.bah.attune.data.MetadataModelJson;

@Service public class BaseballCardService extends BaseService
{
    @Autowired private BaseballCardDao dao;

    
    @Override
    public BaseDao getDao()
    {
        return dao;
    }


    public MetadataModelJson getNetworkView(String entity, String name)
    {
        MetadataModelJson json = new MetadataModelJson();

        List<MetadataJson> nodeList = new ArrayList<MetadataJson>();
        List<MetadataJson> relationshipList = new ArrayList<MetadataJson>();

        Map<String, String> map = new HashMap<String, String>();
        List<Node> list = dao.buildNetworkViewModel(entity, name);
        for (Node node : list)
        {
            if ( node.getProperty("Name").equals(name) )
            {
                for (Relationship relationship : node.getRelationships(Direction.OUTGOING))
                {
                    Metadata rel = new Metadata(relationship.getType().toString(),
                            (String) relationship.getStartNode().getProperty("Name"),
                            (String) relationship.getEndNode().getProperty("Name"));

                    relationshipList.add(new MetadataJson(rel));
                    map.put((String) relationship.getStartNode().getProperty("Name"),
                            relationship.getStartNode().getLabels().iterator().next().toString());
                    map.put((String) relationship.getEndNode().getProperty("Name"),
                            relationship.getEndNode().getLabels().iterator().next().toString());
                }

                for (Relationship relationship : node.getRelationships(Direction.INCOMING))
                {
                    Metadata rel = new Metadata(relationship.getType().toString(),
                            (String) relationship.getStartNode().getProperty("Name"),
                            (String) relationship.getEndNode().getProperty("Name"));

                    relationshipList.add(new MetadataJson(rel));
                    map.put((String) relationship.getStartNode().getProperty("Name"),
                            relationship.getStartNode().getLabels().iterator().next().toString());
                    map.put((String) relationship.getEndNode().getProperty("Name"),
                            relationship.getEndNode().getLabels().iterator().next().toString());
                }
            }
        }

        for (String nodeName : map.keySet())
        {
            nodeList.add(new MetadataJson(new Metadata(nodeName, map.get(nodeName))));
        }

        json.setNodeList(nodeList);
        json.setRelationshipList(relationshipList);

        return json;
    }
}
