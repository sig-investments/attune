package com.bah.attune.dao;

import com.bah.attune.data.NameValuePair;
import com.bah.attune.data.PortfolioBean;

import org.apache.commons.lang.StringEscapeUtils;
import org.atteo.evo.inflector.English;
import org.neo4j.graphdb.Label;
import org.neo4j.graphdb.Node;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.neo4j.conversion.Result;
import org.springframework.stereotype.Repository;

import java.util.*;

@Repository
public class PortfolioDao extends BaseDao
{
	private static final String MATCH_N1= "match (n1:`";
	private static final String N_NAME= "n.Name";
	private static final String NAME= "` {Name:'";
	private static final String RETURN_DISTINCT_N2= "`) return distinct n2";
	private static final String COLOR= "Color";
    private static final Logger LOGGER = LoggerFactory.getLogger(PortfolioDao.class);

	public List<NameValuePair> getChildLabelsAndCounts(List<String> labels, String startEntity, String entityName)
	{
		List<NameValuePair> portfolioNameValuePairList = new ArrayList<>();

		for (String label : labels)
		{
			String query = MATCH_N1 + StringEscapeUtils.escapeJavaScript(startEntity) + "` { Name: '"
					+ StringEscapeUtils.escapeJavaScript(entityName) + "'})-[*]->(n2:`"
					+ StringEscapeUtils.escapeJavaScript(label) + "`) return count(n2)";

			for (Map<String, Object> row : runQuery(query))
			{
				String count = row.get("count(n2)").toString();

				NameValuePair portfolioNameValuePair = new NameValuePair(label, count);

				portfolioNameValuePairList.add(portfolioNameValuePair);
			}
		}

		return portfolioNameValuePairList;
	}

	public List<String> getPortfolioEntityList()
	{
		List<String> list = new ArrayList<>();

		String query = "match (n:`Metadata`) return distinct "+N_NAME+" order by "+N_NAME;

		for (Map<String, Object> row : runQuery(query))
		{
			if (!row.get(N_NAME).toString().contains("Capability Analysis"))
				list.add((String) row.get(N_NAME));
		}

		return list;
	}

	public List<String> getChildEntitiesList(String entity)
	{
		List<String> list = new ArrayList<>();

		String query = "match (n1:Metadata { Name: '" + StringEscapeUtils.escapeJavaScript(entity)
				+ "' })-[*]->(n2:Metadata) return distinct n2.Name";

		for (Map<String, Object> row : runQuery(query))
		{
			String label = row.get("n2.Name").toString();

			list.add(label);
		}

		return list;
	}

	public Map<String, String> getAttributesMap(String entity, String name)
	{
		Map<String, String> attributesMap = new HashMap<>();
		String query = "MATCH (n:" + entity + " {Name:'" + StringEscapeUtils.escapeJavaScript(name) + "'}) return n";
		Map<String, Object> map = runQuery(query).single();

		Node node = (Node) map.get("n");
		for (String property : node.getPropertyKeys())
		{
			if ("Name".equals(property) || "Icon".equals(property))
			{
				continue;
			}
			String value = PortfolioDao.getValueAsString(node.getProperty(property));
			attributesMap.put(property, value);
		}

		return attributesMap;
	}

	public String getIcon(String entity, String name)
	{
		String url = "/attune/images/generic.png";

		String query = "MATCH (n:" + StringEscapeUtils.escapeJavaScript(entity) + " {Name:'"
				+ StringEscapeUtils.escapeJavaScript(name) + "'}) return n";

		Map<String, Object> map = runQuery(query).single();

		Node node = (Node) map.get("n");

		if (node.hasProperty("Icon"))
		{
			url = "/attune/icons/" + (String) node.getProperty("Icon");
		}

		return url;

	}

	public List<String> getSubGroupingList(String entity)
	{
		List<String> list = new ArrayList<>();

		String query = "match (n)-[:has*]->(m:Metadata {Name:'" + StringEscapeUtils.escapeJavaScript(entity)
				+ "'}) return " +N_NAME;
		Result<Map<String, Object>> result = runQuery(query);

		for (Map<String, Object> row : result)
		{
			String name = row.get(N_NAME).toString();
			list.add(name);
		}
		return list;
	}

	public List<String> getChildList(String entity)
	{
		List<String> list = new ArrayList<>();
		addChild(entity, list);
		return list;
	}

	private void addChild(String entity, List<String> list)
	{
		String query = "match (m:Metadata {Name:'" + StringEscapeUtils.escapeJavaScript(entity)
				+ "'})-->(n) return "+ N_NAME;
		Result<Map<String, Object>> result = runQuery(query);

		for (Map<String, Object> row : result)
		{
			String child = (String) row.get(N_NAME);
			if (!list.contains(child))
				list.add(child);

			addChild(child, list);
		}
	}

	/***
	 * This function is used to determine what grouping the entity should fall
	 * under. This picks the first level ancestor of the node if it has it, and
	 * returns itself if it has none (therefore it will be grouped by itself).
	 * 
	 * @param entity
	 * @return
	 */
	public String getDefaultGroupingByEntity(String entity)
	{
		// The grouping will be itself if it has no parents
		String grouping = entity;

		String query = "match (n1:Metadata {Name: '" + StringEscapeUtils.escapeJavaScript(entity)
				+ "'})<--(n2:Metadata) return distinct n2.Name";

		Result<Map<String, Object>> results = runQuery(query);

		// Select one parent and use that
		for (Map<String, Object> row : results)
		{
			grouping = row.get("n2.Name").toString();
			break;
		}

		return grouping;
	}

	/***
	 * Returns the relationship between the first and second entities. Three
	 * possible outcomes: 1. Orphan (the two entities are the same 2. Parent
	 * (entity1 is the parent of the entity2) 3. Grandparent (entity1 is some
	 * distant ancestor of entity2)
	 * 
	 * 
	 * @param entity1
	 * @param entity2
	 * @return
	 */
	public String getRelationship(String entity1, String entity2)
	{
		if (entity1.equals(entity2))
			return "isOrphan";
		else
		{
			String parentQuery = "match (n1:Metadata {Name:'" + StringEscapeUtils.escapeJavaScript(entity1)
					+ "'})-->(n2:Metadata {Name: '" + StringEscapeUtils.escapeJavaScript(entity2) + "'}) return n2";

			if (runQuery(parentQuery).iterator().hasNext())
				return "isParent";
			else
				return "isGrandparent";
		}
	}

	/***
	 * This returns the list of name value pairs of each type of children (of
	 * any distance separated), along with the number of children of that type
	 * it has. <br>
	 * For example, a 'Ship' could have a child 'Event' and 'Event' could have a
	 * child 'Location'. The list of name value pairs would contain 'Event' and
	 * 'Location', and how many of those types are below the 'Ship'.
	 * 
	 * @param entity
	 * @param label
	 * @return
	 */
	public List<NameValuePair> getChildLabelsAndCounts(String entity, String label, String content)
	{
		List<NameValuePair> counts = new ArrayList<>();
		NameValuePair count = new NameValuePair();
		String query = MATCH_N1 + label + "` {Name: '" + StringEscapeUtils.escapeJavaScript(entity)
				+ "'})-[*]->(n2) return distinct n2";

		for (Map<String, Object> row : runQuery(query))
		{
			Node n = (Node) row.get("n2");
			Iterator<Label> iter = n.getLabels().iterator();

			while (iter.hasNext())
			{
				Label l = iter.next();

				// Update the count and replace it
				if (l.toString().equals(content))
				{
					if (count.getName() == null)
					{
						count.setName(l.toString());
						count.setValue("1");
					} else if (count.getName().equals(l.toString()))
					{
						int value = Integer.parseInt(count.getValue()) + 1;
						count.setValue(String.valueOf(value));
					}
				}
			}
		}
		if (count.getName() != null)
		{
			counts.add(count);
		}
		else
		{
			counts.add(new NameValuePair(content,"0"));
		}
		return counts;
	}

	public List<PortfolioBean> getPortfolioBeanList(String chiclet, String grouping, String relationship,
			String content)
	{
		List<PortfolioBean> beans = new ArrayList<>();

		switch (relationship)
		{
		case "isOrphan":
			handleOrphan(beans, chiclet, content);
			break;

		case "isParent":
			handleParent(beans, grouping, chiclet, content);
			break;

		case "isGrandparent":
			handleGrandparent(beans, grouping, chiclet, content);
			break;

		default:
			break;
		}

		return beans;
	}

	private void handleGrandparent(List<PortfolioBean> beans, String grouping, String chiclet, String content)
	{
		HashMap<String, List<String>> requiredMap = getRequiredMap();
		HashMap<String, Map<String, Integer>> overlapMap = getOverlapMap();

		for (Node group : getEntitiesByLabel(grouping))
		{
			PortfolioBean groupBean = new PortfolioBean();

			groupBean.setLabel(grouping);
			groupBean.setName(group.getProperty("Name").toString());

			List<PortfolioBean> subgroupBeans = new ArrayList<>();
			String subgroupQuery = MATCH_N1 + StringEscapeUtils.escapeJavaScript(grouping) + NAME
					+ StringEscapeUtils.escapeJavaScript(groupBean.getName()) + "'})" + "-->(n2)-[*]->(n3:`"
					+ StringEscapeUtils.escapeJavaScript(chiclet) + RETURN_DISTINCT_N2;

			for (Map<String, Object> row1 : runQuery(subgroupQuery))
			{
				Node n = (Node) row1.get("n2");

				PortfolioBean subgroupBean = new PortfolioBean();

				String subgroupName = n.getProperty("Name").toString();
				String subgroupLabel = n.getLabels().iterator().next().toString();

				subgroupBean.setName(subgroupName);
				subgroupBean.setLabel(subgroupLabel);

				List<PortfolioBean> childBeans = new ArrayList<>();
				String childQuery = MATCH_N1 + StringEscapeUtils.escapeJavaScript(subgroupLabel) + NAME
						+ StringEscapeUtils.escapeJavaScript(subgroupName) + "'})" + "-[*]->" + "(n2:`"
						+ StringEscapeUtils.escapeJavaScript(chiclet) + RETURN_DISTINCT_N2;

				for (Map<String, Object> row2 : runQuery(childQuery))
				{
					Node child = (Node) row2.get("n2");

					PortfolioBean childBean = new PortfolioBean();

					String name = child.getProperty("Name").toString();
					String label = child.getLabels().iterator().next().toString();
					String color = null;
					if (child.hasProperty(COLOR))
					{
						color = child.getProperty(COLOR).toString();
					}

					childBean.setName(name);
					childBean.setLabel(label);
					childBean.setColor(color);
					childBean.setCounts(getChildLabelsAndCounts(name, label, content));
					childBean.setIsGap(checkGap(requiredMap, name, label));
					childBean.setIsOverlap(checkOverlap(overlapMap, name, label, content));

					childBeans.add(childBean);
				}
				
				subgroupBean.setChildren(childBeans);
				subgroupBeans.add(subgroupBean);
			}

			groupBean.setChildren(subgroupBeans);
			beans.add(groupBean);
		}

	}

	private void handleParent(List<PortfolioBean> beans, String grouping, String chiclet, String content)
	{
		HashMap<String, List<String>> requiredMap = getRequiredMap();
		HashMap<String, Map<String, Integer>> overlapMap = getOverlapMap();

		for (Node group : getEntitiesByLabel(grouping))
		{
			PortfolioBean groupBean = new PortfolioBean();

			groupBean.setLabel(grouping);
			groupBean.setName(group.getProperty("Name").toString());

			List<PortfolioBean> children = new ArrayList<>();
			String childQuery = MATCH_N1 + StringEscapeUtils.escapeJavaScript(grouping) + NAME
					+ StringEscapeUtils.escapeJavaScript(groupBean.getName()) + "'})" + "-->(n2:`"
					+ StringEscapeUtils.escapeJavaScript(chiclet) + RETURN_DISTINCT_N2;

			for (Map<String, Object> row : runQuery(childQuery))
			{
				Node n = (Node) row.get("n2");
				PortfolioBean child = new PortfolioBean();

				String label = n.getLabels().iterator().next().toString();
				String name = n.getProperty("Name").toString();
				String color = null;
				if (n.hasProperty(COLOR))
				{
					color = n.getProperty(COLOR).toString();
				}

				child.setName(name);
				child.setLabel(label);
				child.setColor(color);
				child.setCounts(getChildLabelsAndCounts(name, label, content));
				child.setIsGap(checkGap(requiredMap, name, label));
				child.setIsOverlap(checkOverlap(overlapMap, name, label, content));

				children.add(child);
			}

			groupBean.setChildren(children);
			beans.add(groupBean);
		}
	}

	private void handleOrphan(List<PortfolioBean> beans, String chiclet, String content)
	{
		PortfolioBean bean = new PortfolioBean();
		List<PortfolioBean> children = new ArrayList<>();
		HashMap<String, List<String>> requiredMap = getRequiredMap();
		HashMap<String, Map<String, Integer>> overlapMap = getOverlapMap();

		bean.setLabel(chiclet);
		bean.setName("All " + English.plural(chiclet));

		for (Node n : getEntitiesByLabel(chiclet))
		{
			PortfolioBean child = new PortfolioBean();

			String label = n.getLabels().iterator().next().toString();
			String name = n.getProperty("Name").toString();
			String color = null;
			if (n.hasProperty(COLOR))
			{
				color = n.getProperty(COLOR).toString();
			}

			child.setName(name);
			child.setLabel(label);
			child.setColor(color);
			child.setCounts(getChildLabelsAndCounts(name, label, content));
			child.setIsGap(checkGap(requiredMap, name, label));
			child.setIsOverlap(checkOverlap(overlapMap, name, label, content));

			children.add(child);
		}

		bean.setChildren(children);
		beans.add(bean);
	}

	private HashMap<String, List<String>> getRequiredMap()
	{
		HashMap<String, List<String>> metaMap = new HashMap<>();
		String metaQuery = "match (n)-[r]->(m) where r.required = true return "+N_NAME+", m.Name";

		for (Map row : runQuery(metaQuery))
		{
			String startEntity = row.get(N_NAME).toString();
			String endEntity = row.get("m.Name").toString();

			List<String> requiredList = metaMap.get(startEntity);

			if (requiredList == null)
			{
				requiredList = new ArrayList<>();
			}
			requiredList.add(endEntity);
			metaMap.put(startEntity, requiredList);

		}
		return metaMap;
	}

	private HashMap<String, Map<String, Integer>> getOverlapMap()
	{
		HashMap<String, Map<String, Integer>> metaMap = new HashMap();
		String overlapQuery = "match (n)-[r]->(m) where r.overlap > 0 return "+N_NAME+", m.Name, r.overlap";

		for (Map row : runQuery(overlapQuery))
		{
			String startEntity = row.get(N_NAME).toString();
			String endEntity = row.get("m.Name").toString();
			Integer overlapCount = Integer.parseInt(row.get("r.overlap").toString());

			Map<String, Integer> overlapMap = metaMap.get(startEntity);

			if (overlapMap == null)
			{
				overlapMap = new HashMap<>();
			}
			overlapMap.put(endEntity, overlapCount);
			metaMap.put(startEntity, overlapMap);
		}
		return metaMap;
	}

	public boolean checkGap(HashMap<String, List<String>> requiredMap, String name, String label)
	{
		if (!requiredMap.isEmpty())
		{
			boolean isGap = true;

			String nodeQuery = "match (n:`" + label + "`{ Name: '" + StringEscapeUtils.escapeJavaScript(name)
					+ "'})-[r]-(m) return distinct labels(m)";
			for (Map row : runQuery(nodeQuery))
			{
				List<String> list = (List<String>) row.get("labels(m)");

				if (!list.isEmpty())
				{
					List<String> requiredList = requiredMap.get(label);
					if (requiredList != null && requiredList.contains(list.get(0)))
					{
						isGap = false;
						break;
					}
					// There are no required relationships for this node
					else if (requiredList == null)
					{
						isGap = false;
						break;
					}
				}
			}
			return isGap;
		} else
			return false;
	}

	private boolean checkOverlap(HashMap<String, Map<String, Integer>> overlapMap, String name, String label,
			String content)
	{

		if (!overlapMap.isEmpty() && overlapMap.get(label) != null)
		{

			Map<String, Integer> overlapCountMap = overlapMap.get(label);

			// make sure this is content we are concerned with
			if (overlapCountMap.get(content) != null)
			{

				Integer overlapCount = overlapCountMap.get(content);

				String nodeQuery = "match (n:`" + label + "`{ Name: '" + StringEscapeUtils.escapeJavaScript(name)
						+ "'})-[r]->(m:`" + content + "`) return count(m)";
				Map<String, Object> result = runQuery(nodeQuery).singleOrNull();

				if (result != null)
				{
					Integer count = Integer.parseInt(result.get("count(m)").toString());
					if (count.intValue() >= overlapCount.intValue())
						return true;
				}
			}
		}

		return false;
	}
	
	public boolean findPortfolioConfig(){
		String query = "match (n:PortfolioConfig) return n";
		Result<Map<String, Object>> result = runQuery(query);
		
		return result.iterator().hasNext();
	}
	
	public void savePortfolioConfig(String[] list){
		String query = "match (n:PortfolioConfig) " + "set n.chiclet='" + list[0].trim() + "'" + ",   n.grouping='"
				+ list[1].trim() + "'" + ",   n.content='" + list[2].trim() + "'";
		runQuery(query);
	}
	
	public String[] getPortfolioConfig(){
		String[] list = new String[3];
		String query = "match (n:PortfolioConfig) return n";
		
		for (Map<String, Object> row : runQuery(query))
		{
			Node n = (Node) row.get("n");
			
			list[0] = n.getProperty("chiclet").toString();
			list[1] = n.getProperty("grouping").toString();
			list[2] = n.getProperty("content").toString();
		}
		
		return list;
	}

}
