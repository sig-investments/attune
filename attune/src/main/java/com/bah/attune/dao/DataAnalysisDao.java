package com.bah.attune.dao;

import java.util.*;

import org.apache.commons.lang.StringEscapeUtils;
import org.neo4j.graphdb.Node;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.springframework.util.StringUtils;

import com.bah.attune.data.AnalysisParameters;
import com.bah.attune.data.AnalysisResult;
import com.bah.attune.data.Entity;
import com.bah.attune.data.NameValuePair;
import com.bah.attune.data.Parameter;

@Repository public class DataAnalysisDao extends BaseDao
{
    @Autowired
    ImportDao importDao;

    public AnalysisResult getAnalysisResults(AnalysisParameters parameters)
    {
        AnalysisResult result = new AnalysisResult();

        result.setCriteria(parameters.getAdditionalParameters());

        String query = "match (n:`" + StringEscapeUtils.escapeJavaScript(parameters.getEntityType()) + "` {";

        int parameterCount = 0;
        Parameter relationshipParam = null;
        for (Parameter p : parameters.getAdditionalParameters())
        {
            // If this parameter is not a property of the entity, then it must
            // be a relationship
            // Additionally, only look at parameters whose values aren't 'Any',
            // meaning the user is
            // trying to filter by that parameter
            if ( !isField(parameters.getEntityType(), p.getProperty()) && !"Any".equals(p.getValue()) )
            {
                relationshipParam = p;
            }
            else if ( !"Any".equals(p.getValue()) )
            {
                String property = p.getProperty();
                String value = p.getValue();

                query += "`" + StringEscapeUtils.escapeJavaScript(property) + "`: '" + StringEscapeUtils.escapeJavaScript(value) + "', ";

                parameterCount++;
            }
        }

        // Remove the last comma if there were any parameters, otherwise remove
        // the open bracket
        if ( parameterCount > 0 )
            query = query.substring(0, query.lastIndexOf(',')) + "}";
        else
            query = query.substring(0, query.lastIndexOf('{'));

        query += ")";

        // Insert the relationsip query if there is a relationship parameter
        if ( relationshipParam != null )
            query += "<--(m:`" + StringEscapeUtils.escapeJavaScript(relationshipParam.getProperty()) + "` {Name:'" + StringEscapeUtils.escapeJavaScript(relationshipParam.getValue()) + "'})";

        // Add the return statement
        query += " return n";

        List<Entity> entities = new ArrayList<Entity>();
        for (Map<String, Object> row : runQuery(query))
        {
            Node n = (Node) row.get("n");
            Entity entity = new Entity();
            entity.setNeo4jId(Long.toString(n.getId()));
            entity.setLabel(n.getLabels().iterator().next().toString());

            List<NameValuePair> pairs = new ArrayList<NameValuePair>();
            for (Parameter p : parameters.getAdditionalParameters())
            {
                String property = p.getProperty();
                String value = "";

                if ( n.hasProperty(property) )
                    value = n.getProperty(property).toString();
                else if ( !isField(n.getLabels().iterator().next().toString(), property) )
                    value = getRelationshipNodeName(n.getLabels().iterator().next().toString(),
                            n.getProperty("Name").toString(), property);

                NameValuePair pair = new NameValuePair(property, value);

                //Current Assumption in Attune is that Name has to be the first property returned in the list
                if("Name".equals(property)){
                    pairs.add(0, pair);
                } else {
                    pairs.add(pair);
                }
            }

            entity.setProperties(pairs);
            entities.add(entity);
        }

        result.setEntities(entities);

        return result;
    }

    public void addEntity(Entity entity) {
      String label = entity.getLabel();
      List<NameValuePair> props = entity.getProperties();
      String query = "CREATE (n: `" + label + "` { ";

      for (int i = 0; i < props.size(); i++) {
        if (i + 1 != props.size()){
          query = query.concat("`" + props.get(i).getName() + "`: '" + props.get(i).getValue() + "', ");
        } else {
          query = query.concat("`" + props.get(i).getName() + "`: '" + props.get(i).getValue() + "'})");
        }
      }

      //run parameter update query
      try {runQuery(query);}
      catch (Exception e){
        System.out.println("error: \n" + e.getMessage() + "\nERROR END");
      }
    }

    public boolean updateEntity(Parameter parameter){
        boolean result = true;
        List<String> args = Arrays.asList(parameter.getProperty().split(","));
        String entity = args.get(0);
        String name = args.get(1);
        //String nid = args.get(2);
        String myfield = args.get(3);
        String entid = args.get(4);
        String val = parameter.getValue();

        if (!parameter.getValue().equals(name)) {
            //String parameterQuery = "MATCH (n:" + entity + ") WHERE n.`" + val + "` = '" + nid + "' SET n.`" + myfield + "` = '" + name + "'";
            String parameterQuery = "MATCH (n:" + entity + ") WHERE id(n) = " + entid + " SET n.`" + myfield + "` = '" + name + "'";
            //String parameterQuery = "MATCH (n:" + entity + ") WHERE n.id = '" + entid + "' RETURN n.`" + val + "`";

           //String parameterQuery = "MATCH (n:" + entity + ") SET n." + StringEscapeUtils.escapeJavaScript(parameter.getValue()) + " = n." + StringEscapeUtils.escapeJavaScript(name) + " REMOVE n." + StringEscapeUtils.escapeJavaScript(name);
            String metadataQuery = "MATCH (n:`Metadata` {Name: '"+entity+"'}) return n.fieldList";


            //run parameter update query
            try {runQuery(parameterQuery);}
            catch (Exception e){
              result = false;
            }
            //if successful then run metadata update query
            if (result != false){
                try {
                    Map<String, Object> map = runQuery(metadataQuery).single();
                    String fieldListString = (String) map.get("n.fieldList");
                    String resultString ="";
                    String[] fieldList = fieldListString.split(",");

                    for (String field: fieldList){
                        if (resultString !="") {
                            resultString = resultString.concat(",");
                        }
                        if (field.equals(name)) {
                            resultString = resultString.concat(parameter.getValue());
                        }
                        else {
                            resultString = resultString.concat(field);
                        }
                    }

                    //finally update the metadata in the database
                    String updateQuery = "MATCH (n:`Metadata` {Name: '"+entity+"'}) SET n.fieldList ='"+resultString+"'";
                    runQuery(updateQuery);
                }
                catch (Exception e){result = false;}

            }

        }


        return result;
    }

    public boolean saveEntity(Entity entity){
        boolean result = true;
        Map<String, Object> properties = new HashMap<>();
        Collection<String> labels = new ArrayList<>();
        labels.add(entity.getLabel());

        for (NameValuePair property : entity.getProperties()){
            properties.put(property.getName(),property.getValue());
        }
        try {
            importDao.createNode(labels, properties);
        }
        catch (Exception e){
            System.out.println(e.getStackTrace());
            result = false;
        }

        return result;
    }
    public void createParentRelationship(Node childNode, Node parentNode, String childLabel, String parentLabel) {
      String query = "CREATE (child:Person)-[:HAS_PARENT]->(parent:Person)";
    }
    private String getRelationshipNodeName(String entity1, String name, String entity2)
    {
        String query = "match (n:`" + StringEscapeUtils.escapeJavaScript(entity1) + "` {Name:'" + StringEscapeUtils.escapeJavaScript(name) + "'})<--(m:`" + StringEscapeUtils.escapeJavaScript(entity2) + "`) return m";
        String result = "";

        Iterator<Map<String, Object>> iter = runQuery(query).iterator();
        Node theParent = null;

        if ( iter.hasNext() )
            theParent = (Node) iter.next().get("m");

        if ( theParent != null && theParent.hasProperty("Name") )
            result = theParent.getProperty("Name").toString();

        return result;
    }


    private boolean isField(String entity, String fieldName)
    {
        String query = "match (n: `Metadata` {Name:'" + StringEscapeUtils.escapeJavaScript(entity) + "'}) return n";
        Map<String, Object> map = runQuery(query).single();

        Node node = (Node) map.get("n");
        String fieldList = (String) node.getProperty("fieldList");

        for (String field : StringUtils.tokenizeToStringArray(fieldList, ","))
        {
            if ( field.equals(fieldName) )
                return true;
        }

        return false;
    }
}
