package com.bah.attune.dao;

import org.apache.commons.lang.StringEscapeUtils;
import org.neo4j.graphdb.Node;
import org.springframework.stereotype.Repository;

import java.util.*;

@Repository public class BaseballCardDao extends BaseDao
{
    public Map<String, String> getAttributesMap(String entity, String name)
    {
        String query = "MATCH (n:`" + entity + "` {Name:'" + StringEscapeUtils.escapeJavaScript(name) + "'}) return n";

        Map<String, Object> map = runQuery(query).singleOrNull();
        if ( map == null )
        {
            return null;
        }
        else
        {
            Map<String, String> attributesHashMap = new HashMap<String, String>();

            TreeMap<String, String> attributesMap = new TreeMap<String, String>();

            Node node = (Node) map.get("n");
            for (String property : node.getPropertyKeys())
            {
                if ("Name".equals(property) || "Icon".equals(property) || "Color".equals(property))
                {
                    continue;
                }

                String value = BaseballCardDao.getValueAsString(node.getProperty(property));
                attributesHashMap.put(property, value);
            }
            attributesMap.putAll(attributesHashMap);

            return attributesMap;

        }
    }


    public Map<String, List<String>> getParentMap(String entity, String name)
    {
        Map<String, List<String>> parentMap = new HashMap<String, List<String>>();
        String query = "MATCH (n:`" + entity + "` {Name:'" + StringEscapeUtils.escapeJavaScript(name) + "'}) <-[r]-(n2) return labels(n2), (n2.Name)";

        for (Map row : runQuery(query))
        {
            String labels = row.get("labels(n2)").toString();
            labels = labels.replaceAll("\\[", "").replaceAll("\\]", "");

            List<String> parentList = parentMap.get(labels);
            if (parentList == null){
                parentList = new ArrayList<String>();
            }

            if ( labels.contains("Timeline Milestone") )
            {
                labels = labels.replaceAll("(, Timeline Milestone)|(Timeline Milestone, )", "");
            }

            parentList.add(row.get("(n2.Name)").toString());
            parentMap.put(labels, parentList);
        }

        return parentMap;
    }


    public Map<String, List<String>> getChildrenMap(String entity, String name)
    {
        Map<String, List<String>> childrenMap = new HashMap<String, List<String>>();
        String query = "MATCH (n:`" + StringEscapeUtils.escapeJavaScript(entity) + "` {Name:'" + StringEscapeUtils.escapeJavaScript(name) + "'}) -[r]->(n2) return labels(n2), (n2.Name)";
        List<String> valuesList = new ArrayList<String>();
        for (Map row : runQuery(query))
        {
            String labels = row.get("labels(n2)").toString();
            if ( labels.contains("Timeline Milestone") )
            {
                labels = labels.replaceAll("(, Timeline Milestone)|(Timeline Milestone, )", "");
            }
            String key = labels.replaceAll("\\[", "").replaceAll("\\]", "");
            if ( !childrenMap.containsKey(key) )
            {
                valuesList = new ArrayList<String>();
                valuesList.add(row.get("(n2.Name)").toString());
            }
            else
            {
                valuesList.add(row.get("(n2.Name)").toString());
            }
            childrenMap.put(labels.replaceAll("\\[", "").replaceAll("\\]", ""), valuesList);
        }

        return childrenMap;
    }

    public List<Node> buildNetworkViewModel(String entity, String name)
    {
        List<Node> nodeList = new ArrayList<Node>();

        String query = "MATCH (n:`" + StringEscapeUtils.escapeJavaScript(entity) + "` {Name:'" + StringEscapeUtils.escapeJavaScript(name) + "'}) -[r]-(n2) return n, n2";

        for (Map row : runQuery(query))
        {
            if ( !nodeList.contains((Node) row.get("n")) )
            {
                nodeList.add((Node) row.get("n"));
            }
            nodeList.add((Node) row.get("n2"));
        }

        return nodeList;
    }
}
