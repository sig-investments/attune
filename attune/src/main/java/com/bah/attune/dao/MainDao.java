package com.bah.attune.dao;

import com.bah.attune.data.NameValuePair;
import org.apache.commons.lang.StringEscapeUtils;
import org.neo4j.graphdb.Node;
import org.springframework.stereotype.Repository;
import org.springframework.util.StringUtils;

import java.util.ArrayList;
import java.util.List;
import java.util.Locale;
import java.util.Map;

@SuppressWarnings({ "rawtypes" })
@Repository
public class MainDao extends BaseDao
{
    public static final String ICON = "Icon";
    public static final String ATTACHMENTS = "Attachments";

    public List<Node> buildMetadataModel()
    {
        List<Node> nodeList = new ArrayList<Node>();

        String query = "match (n:`Metadata`) return n";

        for (Map row : runQuery(query))
        {
            nodeList.add((Node) row.get("n"));
        }

        return nodeList;
    }

    
    public List<NameValuePair> getEntityList()
    {
        List<NameValuePair> list = new ArrayList<NameValuePair>();

        String query = "match (n:`Metadata`) return n.Name order by n.Name";

        for (Map row : runQuery(query))
        {
            list.add(new NameValuePair((String) row.get("n.Name"), ""));
        }

        return list;
    }

    
    public Integer getEntityCount(String entity)
    {
        String query = "match (n:`" + StringEscapeUtils.escapeJavaScript(entity) + "`) return count(n) as count";

        Integer count = 0;

        for (Map row : runQuery(query))
        {
            count = (Integer) row.get("count");
            break;
        }

        return count;
    }

    public String getEntityColor(String entity){

        List<String> restrictedList = new ArrayList<String>();
        restrictedList.add("#235e81"); //grouping color
        restrictedList.add("#1199b6"); //content color
        restrictedList.add("#ef8d58"); //gap color
        restrictedList.add("Red");
        restrictedList.add("red");

        String query = "match (n: `Metadata` {Name:'"+ StringEscapeUtils.escapeJavaScript(entity) +"'}) return n.Color";
        Map<String, Object> result = runQuery(query).singleOrNull();
        String color = "";

        if (result != null && result.get("n.Color") != null) {
            color = result.get("n.Color").toString();
            //Compare against list of restricted colors
            if (!restrictedList.contains(color)) {
                return color;
            }
        }
        return "";
    }
    
    
    public String[] getFieldList(String entity)
    {
        List<String> returnList = new ArrayList<String>();

        String query = "match (n: `Metadata` {Name:'" + StringEscapeUtils.escapeJavaScript(entity) + "'}) return n";
        Map<String, Object> map = runQuery(query).single();

        Node node = (Node) map.get("n");
        String fieldList = (String) node.getProperty("fieldList");

        for (String field : StringUtils.tokenizeToStringArray(fieldList, ","))
        {
            if (!field.endsWith(ICON) && !field.equals(ATTACHMENTS))
                returnList.add(field);
        }

        // add the required relationship
        String relationshipQuery = "match (n:`Metadata`{ Name: '" + StringEscapeUtils.escapeJavaScript(entity) + "'})<-[r]-(m) return r.required, m.Name";
        for (Map row : runQuery(relationshipQuery))
        {
            if ( (boolean) row.get("r.required") )
                returnList.add((String) row.get("m.Name") );
        }
                
        return returnList.toArray(new String[0]);
    }

    
    public String[] getDisplayList(String entity)
    {
        String query = "match (n:Dashboard{entity:'" + StringEscapeUtils.escapeJavaScript(entity) + "'}) return n";
        Map<String, Object> map = runQuery(query).single();

        Node node = (Node) map.get("n");
        String displayList = (String) node.getProperty("displayList");
        if ("*".equals(displayList))
            return getFieldList(entity);
        else
            return StringUtils.tokenizeToStringArray(displayList, ",");
    }

    
    public List<NameValuePair> getGroupByList(String entity, String groupBy)
    {
        if ( isField(entity, groupBy))
            return  getGroupByFieldList(entity, groupBy);
        else
            return getGroupByEntityList(entity, groupBy);
    }
    
    
    public List<String> getValueList(String entity, String fieldName)
    {
    	if ( isField(entity, fieldName))
    		return getFieldValueList(entity, fieldName);
    	else
    		return getEntityValueList(fieldName);
    }
    
    
    public List<String> getSearchResults(String name) {
    	name = name.toLowerCase();
    	List<String> results = new ArrayList<String>(); 
    	
    	String query = "match (n) where has(n.Name) return n.Name";
    	
    	for (Map<String, Object> row: runQuery(query)) {
    		String result = ((String) row.get("n.Name"));
    		
    		// Give the results that start with the name precedence over 
    		// those that just contain it
    		if (result.toLowerCase(Locale.ENGLISH).startsWith(name.toLowerCase(Locale.ENGLISH)) 
    				&& !results.contains(result)) 
    			results.add(0, result);	
    		else if (result.toLowerCase().contains(name) && !results.contains(result))
    			results.add(results.size(), result);
    	}
    	return results;
    }

    private List<NameValuePair> getGroupByFieldList(String entity, String groupBy)
    {
        List<NameValuePair> list = new ArrayList<NameValuePair>();

        String query = "match (n:`" + StringEscapeUtils.escapeJavaScript(entity) + "`) return n.`" + StringEscapeUtils.escapeJavaScript(groupBy) + "`, count(n.`" + StringEscapeUtils.escapeJavaScript(groupBy) + "`) order by n.`"
                + groupBy + "`";

        for (Map row : runQuery(query))
        {
            list.add(new NameValuePair((String) row.get("n.`" + groupBy + "`"),
                    (Integer) row.get("count(n.`" + groupBy + "`)") + ""));
        }

        return list;
    }
    
    
    private List<String> getFieldValueList(String entity, String field)
    {
        List<String> valueList = new ArrayList<String>();
        String query = "match (n:`" + StringEscapeUtils.escapeJavaScript(entity) + "`) return distinct n.`" + StringEscapeUtils.escapeJavaScript(field) + "` as value order by value";

        for (Map row : runQuery(query))
        {        	
        	String value = MainDao.getValueAsString(row.get("value"));
        	if (value != null && !value.isEmpty())
        		valueList.add(value);
        }

        return valueList;
    }
    
    
    private List<NameValuePair> getGroupByEntityList(String entity, String groupBy)
    {
        List<NameValuePair> list = new ArrayList<NameValuePair>();

        String query = "match (n:`" + StringEscapeUtils.escapeJavaScript(entity) + "`)-[]-(m:`" + StringEscapeUtils.escapeJavaScript(groupBy) + "`) return m.Name, count(n) order by m.Name";

        for (Map row : runQuery(query))
        {
            list.add(new NameValuePair((String) row.get("m.Name"), (Integer) row.get("count(n)") + ""));
        }

        return list;
    }
    
    
    private List<String> getEntityValueList(String entity)
    {
        List<String> valueList = new ArrayList<String>();
        String query = "match (n:`" + StringEscapeUtils.escapeJavaScript(entity) + "`) return distinct n.Name order by n.Name";

        for (Map row : runQuery(query))
        {
            valueList.add((String) row.get("n.Name"));
        }

        return valueList;
    }
    
    
    private boolean isField(String entity, String fieldName)
    {
        String query = "match (n:Metadata {Name:'" + StringEscapeUtils.escapeJavaScript(entity) + "'}) return n";
        Map<String, Object> map = runQuery(query).single();

        Node node = (Node) map.get("n");
        String fieldList = (String) node.getProperty("fieldList");

        for (String field : StringUtils.tokenizeToStringArray(fieldList, ","))
        {
            if ( field.equals(fieldName))
                return true;
        }
        
        return false;
    }


    public void saveDashboard(String[] list)
    {
        for (int i = 1; i <= 3; i++)
        {
            String query = "match (n:Dashboard{Name: 'entity" + i + "'}) " + "set n.entity='" + list[i * 6].trim() + "'"
                    + ",   n.chartType='" + list[i * 6 + 1].trim() + "'" + ",   n.groupBy='" + list[i * 6 + 2].trim() + "'"
                    + ",   n.alertCheck='" + list[i * 6 + 3].trim() + "'" + ",   n.alertValue='" + list[i * 6 + 4].trim() + "'"
                    + ",   n.displayList='" + list[i * 6 + 5].trim() + "'";

            runQuery(query);
        }
        
    }
    
    public boolean timelineCreated()
    {
        String query = "match (n:Timeline) return n";
        
        if ( runQuery(query).singleOrNull() != null )
            return true;
        else
            return false;   
    }

}