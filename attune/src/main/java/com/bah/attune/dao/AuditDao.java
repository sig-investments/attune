package com.bah.attune.dao;

import com.bah.attune.data.AuditTrail;
import com.bah.attune.service.SecurityService;
import org.neo4j.graphdb.Node;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import java.net.*;
import java.text.SimpleDateFormat;
import java.util.*;

@Repository
public class AuditDao extends BaseDao
{
   @Autowired
   private ImportDao importDao;

   public List<AuditTrail> getAuditTrailList()
   {
      String query = "match (n:AuditTrail) return n";

      List<AuditTrail> auditList = new ArrayList<AuditTrail>();

      for (Map<String, Object> row : runQuery(query))
      {
         Node n = (Node) row.get("n");

         AuditTrail audit = new AuditTrail();  
         audit.setUsername(n.getProperty("username").toString());
         audit.setTime(n.getProperty("time").toString());
         audit.setIP(n.getProperty("IP").toString());
         audit.setAction(n.getProperty("action").toString());
         audit.setOriginalValue(n.getProperty("originalValue").toString());
         audit.setUpdatedValue(n.getProperty("updatedValue").toString());

         auditList.add(audit);
      }

      return auditList;
   }


   public void log(String username, String action, String originalValue, String updatedValue)
   {
      SimpleDateFormat fmt = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
      String time = fmt.format(Calendar.getInstance().getTime());

      Collection<String> labels = new ArrayList<String>();
      labels.add("AuditTrail");

      Map<String, Object> properties = new HashMap<String, Object>();
      properties.put("username", username);
      properties.put("time", time);
      properties.put("IP", getIp());
      properties.put("action", action);
      properties.put("originalValue", originalValue);
      properties.put("updatedValue", updatedValue);

      importDao.createNode(labels, properties);
   }


   private String getIp()
   {
      List<String> ipv4Addresses = new ArrayList<>();
      List<String> ipv6Addresses = new ArrayList<>();
      try
      {
         Enumeration<NetworkInterface> interfaces = NetworkInterface.getNetworkInterfaces();
         while (interfaces.hasMoreElements())
         {
            NetworkInterface nic = interfaces.nextElement();
            Enumeration<InetAddress> addresses = nic.getInetAddresses();
            while (addresses.hasMoreElements())
            {
               InetAddress address = addresses.nextElement();

               if (address instanceof Inet4Address && !address.isLoopbackAddress())
                  ipv4Addresses.add(address.toString());

               if (address instanceof Inet6Address && !address.isLoopbackAddress())
                  ipv6Addresses.add(address.toString());
            }
         }
      }
      catch (SocketException e)
      {
      }

      String ip = "";
      if ( ipv4Addresses.size() > 0 )
         ip = ipv4Addresses.get(0).substring(1);
      else if ( ipv6Addresses.size() > 0)
         ip = ipv6Addresses.get(0).substring(1);

      return ip;

   }


   public String getUserLastLogin()
   {
      //match (n:AuditTrail{username: 'Attune', action: 'Login Success'})
      // return n ORDER BY n.time DESC LIMIT 1

      String query = "match (n:AuditTrail{username: '" +
              SecurityService.getUserName() + "', action: 'Login Success'})" +
              " return n ORDER BY n.time DESC LIMIT 1";

      Map map = runQuery(query).singleOrNull();

      if ( map == null )
         return "N/A";
      else
      {
         Node audit = (Node)map.get("n");
         return (String)audit.getProperty("time");
      }
   }
}
