package com.bah.attune.dao;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.commons.lang.StringEscapeUtils;
import org.neo4j.graphdb.Node;
import org.neo4j.graphdb.PropertyContainer;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.neo4j.conversion.Result;
import org.springframework.data.neo4j.support.Neo4jTemplate;

public class BaseDao
{
    @Autowired
    private Neo4jTemplate template;

    public Neo4jTemplate neo4j()
    {
        return template;
    }

    public List<Map<String, Object>> loadAll(String label)
    {
        List<Map<String, Object>> list = new ArrayList<Map<String, Object>>();
        String query = "match (n:`" + StringEscapeUtils.escapeJavaScript(label) + "`) return n order by n.Name";

        for (Map<String, Object> row : runQuery(query))
        {
            Map<String, Object> map = new HashMap<String, Object>();

            collectProperties(map, (Node) row.get("n"));

            list.add(map);
        }

        return list;

    }
    
    public List<Node> getEntitiesByLabel(String entityLabel)
    {
    	List<Node> nodes = new ArrayList<Node>();
    	String query = "match (n:`" + StringEscapeUtils.escapeJavaScript(entityLabel) + "`) return n";
    	
    	for (Map<String, Object> row: runQuery(query))
    		nodes.add((Node) row.get("n"));
    	
    	return nodes;
    }



    public Result<Map<String, Object>> runQuery(String query)
    {
        return neo4j().query(query, null);
    }
    
    
    public List<Map<String, Object>> loadEntity(String label)
    {
        // find required relationship
        List<String> relationshipList = new ArrayList<String>();
        
        String relationshipQuery = "match (n:`Metadata`{ Name: '" + StringEscapeUtils.escapeJavaScript(label) + "'})<-[r]-(m) return r.required, m.Name";
        for (Map row : runQuery(relationshipQuery))
        {
            if ( (boolean) row.get("r.required") )
                relationshipList.add((String) row.get("m.Name") );
        }

        String query = "match (n:`" + label + "`) return n";
        if ( ! relationshipList.isEmpty() )
            query = "match (n:`" + label + "`)<-[]-(m:`" + StringEscapeUtils.escapeJavaScript(relationshipList.get(0)) + "`) return n, m.Name";
        
        List<Map<String, Object>> list = new ArrayList<Map<String, Object>>();
        for (Map<String, Object> row : runQuery(query))
        {
            Map<String, Object> map = new HashMap<String, Object>();

            collectProperties(map, (Node) row.get("n"));
            if ( ! relationshipList.isEmpty() )
                map.put(relationshipList.get(0),(String) row.get("m.Name"));
            
            list.add(map);
        }
        
        
        return list;

    }
    
    public static String getValueAsString(Object value){
    	String StringValue = null;        
    	if(value != null){
        	if(Boolean.class.isAssignableFrom(value.getClass())){
        		StringValue = ((Boolean) value).toString();
        	} else if(Number.class.isAssignableFrom(value.getClass())){
        		StringValue = ((Number) value).toString();
        	} else {
        		StringValue = (String) value;
        	}
    	}
    	return StringValue;
    }
    
    protected void collectProperties(Map<String, Object> map, PropertyContainer container)
    {
        for (String key : container.getPropertyKeys())
            map.put(key, container.getProperty(key));
    }
}
