package com.bah.attune.dao;

import com.bah.attune.data.AttuneException;
import org.apache.commons.lang.StringEscapeUtils;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.Row;
import org.neo4j.graphdb.Node;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import java.util.Collection;
import java.util.Map;

@Repository
public class ImportDao extends BaseDao
{
   public Node createNode(Collection<String> labels,
                          Map<String, Object> properties)
   {
      Node node = neo4j().createNode(properties, labels);

      return node;
   }

   @Transactional
   public void createMetadataRelationship(String startEntity,
                                          String endEntity, String relationship)
   {
      String createRelationship = "MATCH (n1:`Metadata` { Name: '"
              + StringEscapeUtils.escapeJavaScript(startEntity) + "'}), " + "(n2:`Metadata` { Name: '"
              + StringEscapeUtils.escapeJavaScript(endEntity) + "'})  " + "CREATE UNIQUE (n1)-[:`" + StringEscapeUtils.escapeJavaScript(relationship)
              + "`]->(n2)";

      runQuery(createRelationship);
   }

   @Transactional
   public void setRequired(String startEntity, String endEntity,
                           Boolean required)
   {
      String setRequired = "MATCH (n1:`Metadata` { Name: '" + StringEscapeUtils.escapeJavaScript(startEntity)
              + "'})" + "-[r]-" + "(n2:`Metadata` { Name: '" + StringEscapeUtils.escapeJavaScript(endEntity)
              + "'}) " + "SET r.required = " + required;

      runQuery(setRequired);
   }

   @Transactional
   public void setOverlap(String startEntity, String endEntity,
                          int overlap)
   {
      String setOverlap = "MATCH (n1:`Metadata` { Name: '" + StringEscapeUtils.escapeJavaScript(startEntity)
              + "'})" + "-[r]-" + "(n2:`Metadata` { Name: '" + StringEscapeUtils.escapeJavaScript(endEntity)
              + "'}) " + "SET r.overlap = " + overlap;

      runQuery(setOverlap);
   }

   @Transactional
   public void processRelationshipRow(Row row) throws AttuneException
   {
      String createStmt = "MATCH (n1:`" + StringEscapeUtils.escapeJavaScript(getStringCellValue(row.getCell(0)))
              + "` { Name: '" + StringEscapeUtils.escapeJavaScript(getStringCellValue(row.getCell(1)))
              + "'}), (n2:`" + StringEscapeUtils.escapeJavaScript(getStringCellValue(row.getCell(3)))
              + "` { Name: '" + StringEscapeUtils.escapeJavaScript(getStringCellValue(row.getCell(4)))
              + "'}) CREATE UNIQUE (n1)-[r:`"
              + StringEscapeUtils.escapeJavaScript(getStringCellValue(row.getCell(2))) + "`]->(n2)";

      runQuery(createStmt);
   }

   @Transactional
   public void setFieldlist(String entity, String fieldlist)
   {
      String setFieldlist = "match (n:Metadata { Name:'" + StringEscapeUtils.escapeJavaScript(entity) + "'}) "
              + " set n.fieldList = '" + fieldlist + "'";

      runQuery(setFieldlist);
   }

   @Transactional
   public void deleteNode(String label)
   {
      String deleteStmt = "match (n:" + StringEscapeUtils.escapeJavaScript(label) + ") delete n";

      runQuery(deleteStmt);
   }

   public void deleteNodes()
   {
//      String deleteRelationshipsStmt = "match (n) delete n";

      String deleteNodeStmt = "MATCH (n) WHERE NOT n: AttuneUser AND NOT n: AuditTrail delete n";

      runQuery(deleteNodeStmt);
   }

   public void deleteRelationships()
   {
      String deleteRelationshipsStmt = "match ()-[r]-() delete r";
      runQuery(deleteRelationshipsStmt);
   }

   private String getStringCellValue(Cell cell) throws AttuneException
   {
      String value;

      if (cell.getCellType() == Cell.CELL_TYPE_STRING)
      {
         value = StringEscapeUtils.escapeJava(cell.getStringCellValue());
      }
      else if (cell.getCellType() == Cell.CELL_TYPE_BLANK)
      {
         value = null;
      }
      else if (cell.getCellType() == Cell.CELL_TYPE_BOOLEAN)
      {
         Boolean b = cell.getBooleanCellValue();
         value = b.toString();
      }
      else if (cell.getCellType() == Cell.CELL_TYPE_ERROR)
      {
         return null;
      }
      else if (cell.getCellType() == Cell.CELL_TYPE_FORMULA)
      {
         return null;
      }
      else if (cell.getCellType() == Cell.CELL_TYPE_NUMERIC)
      {
         Double d = cell.getNumericCellValue();
         value = d.toString();
      }
      else
      {
         return null;
      }

      // Protect against XSS.
      if (value != null)
      {
         value = value.replace("<", "");
         value = value.replace(">", "");
      }

      return value;
   }
}
