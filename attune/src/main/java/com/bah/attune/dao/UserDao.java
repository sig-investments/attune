package com.bah.attune.dao;

import com.bah.attune.data.AttuneUser;
import com.bah.attune.util.Encryptor;
import org.apache.commons.lang3.StringUtils;
import org.neo4j.graphdb.Node;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import java.lang.reflect.Field;
import java.nio.ByteBuffer;
import java.text.SimpleDateFormat;
import java.util.*;

@SuppressWarnings({"rawtypes"})
@Repository
public class UserDao extends BaseDao
{
   private final static int MAX_PASSWORD_HISTORY_COUNT = 24;

   @Autowired
   private Encryptor encryptor;

   private SimpleDateFormat fmt = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");

   public List<AttuneUser> getUserList()
   {
      List<AttuneUser> userList = new ArrayList<AttuneUser>();

      String query = "match (n:`AttuneUser`) return n";

      for (Map row : runQuery(query))
      {
         Node userNode = (Node) row.get("n");
         userList.add(buildUserFromNode(userNode));
      }

      return userList;
   }


   public AttuneUser getUser(String uuid)
   {
      String query = "match (n:AttuneUser{uuid:'" + uuid + "'}) return n;";
      Map map = runQuery(query).singleOrNull();
      if ( map == null )
         return null;
      else
      {
         Node userNode = (Node)map.get("n");
         return buildUserFromNode(userNode);
      }
   }

   public AttuneUser getUserByname(String username)
   {
      String query = "match (n:AttuneUser{username:'" + username + "'}) return n;";
      Map map = runQuery(query).singleOrNull();
      if ( map == null )
         return null;
      else
      {
         Node userNode = (Node)map.get("n");
         return buildUserFromNode(userNode);
      }
   }

   private AttuneUser buildUserFromNode(Node userNode)
   {
      AttuneUser user = new AttuneUser();

      user.setUuid((String)userNode.getProperty("uuid"));
      user.setUsername((String)userNode.getProperty("username"));
      user.setPassword(encryptor.decrypt((String)userNode.getProperty("password")));
      user.setFirstName((String)userNode.getProperty("firstName"));
      user.setLastName((String)userNode.getProperty("lastName"));
      user.setPhone((String)userNode.getProperty("phone"));
      user.setEmail((String)userNode.getProperty("email"));
      user.setRole((String)userNode.getProperty("role"));
      user.setIsActive((Boolean)userNode.getProperty("isActive"));

      user.setLastLoginDate(getDate(userNode.getProperty("lastLoginDate").toString()));
      user.setAccountCreateDate(getDate(userNode.getProperty("accountCreateDate").toString()));

      user.setPasswordHistory((String)userNode.getProperty("passwordHistory"));

      user.setLastLoginFailDate(getDate(userNode.getProperty("lastLoginFailDate").toString()));
      user.setLastLoginFailCount((Integer)userNode.getProperty("lastLoginFailCount"));

      return user;
   }


   public void saveUser(AttuneUser user) throws Exception
   {
      String query = "match (n:AttuneUser{uuid:'" + user.getUuid() + "'}) return n;";
      Map map = runQuery(query).singleOrNull();
      if ( map == null )
         createUser(user);
      else
         updateUser(user);
   }


   public void createUser(AttuneUser user) throws Exception
   {
      Collection<String> labels = new ArrayList<>();
      labels.add("AttuneUser");

      Map<String, Object> properties = new HashMap<>();

      Class userClass = user.getClass();
      Field[] fieldList = userClass.getDeclaredFields();

      for(Field field :fieldList)
         properties.put(field.getName(), field.get(user));

      properties.put("password", encryptor.encrypt(user.getPassword()));
      properties.put("uuid", shortUUID());

      String date = fmt.format(new Date());

      properties.put("accountCreateDate", date);
      properties.put("lastLoginDate", "");
      properties.put("passwordHistory", encryptor.encrypt(user.getPassword()));

      properties.put("lastLoginFailDate", "");
      properties.put("lastLoginFailCount", 0);

      neo4j().createNode(properties, labels);
   }


   public void updateUser(AttuneUser user)
   {
      String query = "match (n:AttuneUser{uuid:'" + user.getUuid() + "'}) set" +
                    " n.username='" + user.getUsername() + "', " +
                    " n.firstName='" + user.getFirstName() + "', " +
                    " n.lastName='" + user.getLastName() + "', " +
                    " n.phone='" + user.getPhone() + "', " +
                    " n.email='" + user.getEmail() + "', " +
                    " n.role='" + user.getRole() + "'";

      runQuery(query);

      // update password history if necessary
      AttuneUser existingUser = this.getUser(user.uuid);
      if ( !encryptor.decrypt(existingUser.getPassword()).equals(user.getPassword()) )
      {
         String[] passwordHistoryList = StringUtils.split(existingUser.getPasswordHistory(), ';');

         String passwordHistory = "";
         if ( passwordHistoryList.length == MAX_PASSWORD_HISTORY_COUNT )
         {
            // remove the first password in history
            for (int i = 1; i < passwordHistoryList.length; i++)
            {
               passwordHistory += passwordHistoryList[i] + ";";
            }
         }
         else
         {
            passwordHistory = existingUser.getPasswordHistory() + ";";
         }

         passwordHistory += encryptor.encrypt(user.getPassword());

         query = "match (n:AttuneUser{uuid:'" + user.getUuid() + "'}) set" +
                 " n.password='" + encryptor.encrypt(user.getPassword()) + "', " +
                 " n.passwordHistory='" + passwordHistory + "'";

         runQuery(query);
      }


      if ( existingUser.getIsActive().booleanValue() != user.getIsActive().booleanValue())
      {
         query = "match (n:AttuneUser{uuid:'" + user.getUuid() + "'}) set" +
                 " n.isActive=" + user.getIsActive();

         // if activating user, clear fail logins
         if ( ! existingUser.getIsActive() )
            query += ", n.lastLoginFailDate='', n.lastLoginFailCount=0";

         runQuery(query);

      }
   }


   public void deleteUser(String uuid)
   {
      String query = "match (n:AttuneUser{uuid:'" + uuid + "'}) delete n;";
      runQuery(query);
   }


   public void recordUserLoginSuccess(String username)
   {
      String date = fmt.format(new Date());

      String query = "match (n:AttuneUser{username:'" + username + "'}) set" +
              " n.lastLoginDate='" + date + "'";

      runQuery(query);
   }


   public static String shortUUID()
   {
      UUID uuid = UUID.randomUUID();
      long l = ByteBuffer.wrap(uuid.toString().getBytes()).getLong();
      return Long.toString(l, Character.MAX_RADIX);
   }


   private Date getDate(String dateStr)
   {
      Date date = null;

      try
      {
         if ( dateStr.length() > 0 )
            date =fmt.parse(dateStr);
      }
      catch (Exception e)
      {
      }

      return date;
   }

   public void recordUserLoginFail(String username)
   {
      String date = fmt.format(new Date());

      String query = "match (n:AttuneUser{username:'" + username + "'}) set" +
              " n.lastLoginFailDate='" + date + "', n.lastLoginFailCount=1";

      runQuery(query);
   }

   public void updateUserLoginFail(String username, int count)
   {
      String query = "match (n:AttuneUser{username:'" + username + "'}) set" +
              "  n.lastLoginFailCount=" + count;

      runQuery(query);
   }
}