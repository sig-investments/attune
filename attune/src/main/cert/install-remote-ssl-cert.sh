HOST=
PORT=443
KEYSTOREFILE=./keystore.jks
KEYSTOREPASS=changeit
TRUSTSTOREFILE=./truststore.jks
TRUSTSTOREPASS=changeit

if [ $# -eq 0 ]; then
  echo
  echo "Usage: install-remote-ssl-cert.sh -host <host> [-port <port>] [-keystore <keystore file>] [-keystorepass <keystore password>] [-truststore <truststore file>] [-truststorepass <truststore password>]"
  echo
  echo "No arguments provided, minimum argument -host required."
  exit 1
fi

if [ $1 == "-help" ]; then
  echo
  echo "Usage: install-remote-ssl-cert.sh -host <host> [-port <port>] [-keystore <keystore file>] [-keystorepass <keystore password>] [-truststore <truststore file>] [-truststorepass <truststore password>]"
  echo
  exit 0
fi

while [[ ${1} ]]; do
        case "${1}" in
            -host)
                HOST=${2}
                shift
                ;;
            -port)
                PORT=${2}
                shift
                ;;
            -keystore)
                KEYSTOREFILE=${2}
                shift
                ;;
            -keystorepass)
                KEYSTOREPASS=${2}
                shift
                ;;
            -truststore)
                TRUSTSTOREFILE=${2}
                shift
                ;;
            -truststorepass)
                TRUSTSTOREPASS=${2}
                shift
                ;;	
            *)
                echo "Unknown parameter: ${1}" >&2
                exit 1
        esac

        if ! shift; then
            echo 'Missing parameter argument -host.' >&2
            return 1
        fi
done

openssl version >/dev/null 2>&1 || { echo >&2 "OpenSSL is required for this script. Please install openssl."; exit 1; }
sed --version >/dev/null 2>&1 || { echo >&2 "Sed is required for this script. Please install sed."; exit 1; }

echo
echo "Installing remote SSL certificate from ${HOST}:${PORT} into Keystore ${KEYSTOREFILE} and Truststore ${TRUSTSTOREFILE}"
echo
sudo rm -f ./${HOST}.txt && sudo echo -n | openssl s_client -connect ${HOST}:${PORT} > ./${HOST}.txt
sed -n -e '/^subject=/p' ${HOST}.txt > ${HOST}.alias
sed -n -e '/-BEGIN CERTIFICATE-/,/-END CERTIFICATE-/p' ./${HOST}.txt > ./${HOST}.pem
ALIAS=$(cat ${HOST}.alias)
ALIAS=$(echo "$ALIAS" | sed 's/.*CN=//')

echo Importing certificate using alias ${ALIAS}
echo

# update keystore and import certificate
keytool -delete -noprompt -alias ${ALIAS} -file ${HOST}.pem -keystore ${KEYSTOREFILE} -storepass ${KEYSTOREPASS}
keytool -import -noprompt -trustcacerts -alias ${ALIAS} -file ${HOST}.pem -keystore ${KEYSTOREFILE} -storepass ${KEYSTOREPASS}
echo Updated Keystore ${KEYSTOREFILE}
echo

keytool -delete -noprompt -alias ${ALIAS} -file ${HOST}.pem -keystore ${TRUSTSTOREFILE} -storepass ${TRUSTSTOREPASS}
keytool -import -noprompt -trustcacerts -alias ${ALIAS} -file ${HOST}.pem -keystore ${TRUSTSTOREFILE} -storepass ${TRUSTSTOREPASS}
echo Updated Truststore ${TRUSTSTOREFILE}
echo

# verify we've got it.
echo Verifying Alias ${ALIAS} in Keystore ${KEYSTOREFILE}
echo
keytool -list -v -keystore ${KEYSTOREFILE} -storepass ${KEYSTOREPASS} -alias ${ALIAS}

echo Verifying Alias ${ALIAS} in Truststore ${TRUSTSTOREFILE}
echo
keytool -list -v -keystore ${TRUSTSTOREFILE} -storepass ${TRUSTSTOREPASS} -alias ${ALIAS}

rm ${HOST}.txt -rf
rm ${HOST}.alias -rf
rm ${HOST}.pem -rf
echo Certificate installed.