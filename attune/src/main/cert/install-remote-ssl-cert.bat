@echo off
SETLOCAL
:parse
IF "%~1"=="" GOTO endparse
IF "%~1"=="-host" set HOST=%~2
IF "%~1"=="-port" set PORT=%~2
IF "%~1"=="-keystore" set KEYSTOREFILE=%~2
IF "%~1"=="-keystorepass" set KEYSTOREPASS=%~2
IF "%~1"=="-truststore" set TRUSTSTOREFILE=%~2
IF "%~1"=="-truststorepass" set TRUSTSTOREPASS=%~2
IF "%~1"=="-help" GOTO help
SHIFT

set maxbytesize=1

set opensslCheckFile="openssl.check"
openssl version > %opensslCheckFile%
FOR /F "usebackq" %%A IN ('%opensslCheckFile%') DO set size=%%~zA
if %size% LSS %maxbytesize% (
    echo.OpenSSL is required for this script. Please install openssl for Windows http://gnuwin32.sourceforge.net/packages/openssl.htm
	GOTO end
) ELSE (
    set /p opensslVersion=<%opensslCheckFile%
    echo.Detected %opensslVersion%
)

set sedCheckFile="sed.check"
sed --version > %sedCheckFile%
FOR /F "usebackq" %%A IN ('%sedCheckFile%') DO set size=%%~zA
if %size% LSS %maxbytesize% (
    echo.Sed is required for this script. Please install sed for Windows http://gnuwin32.sourceforge.net/packages/sed.htm
	GOTO end
) ELSE (
    set /p sedVersion=<%sedCheckFile%
    echo.Detected sed
)
  
GOTO parse

:endparse
IF "%HOST%"=="" GOTO error
IF "%PORT%"=="" (SET PORT=443)
IF "%KEYSTOREFILE%"=="" (SET KEYSTOREFILE=.\keystore.jks)
IF "%KEYSTOREPASS%"=="" (SET KEYSTOREPASS=changeit)
IF "%TRUSTSTOREFILE%"=="" (SET TRUSTSTOREFILE=.\truststore.jks)
IF "%TRUSTSTOREPASS%"=="" (SET TRUSTSTOREPASS=changeit)

echo.
echo Installing remote SSL certificate from %HOST%:%PORT% into Keystore %KEYSTOREFILE% and Truststore %TRUSTSTOREFILE%
echo.

rem # get the SSL certificate
openssl s_client -showcerts -connect %HOST%:%PORT% > %HOST%.pem
sed -n -e '/^subject=/p' %HOST%.pem > %HOST%.alias
set /p ALIAS=<%HOST%.alias

set "value=%ALIAS:*CN=%"
if "%value%"=="%ALIAS%" echo subject not found in certificate &goto :end
for /f "delims=\" %%a in ("%value%") do set "value=%%~a"
set ALIAS=%value:~1%
echo Importing certificate using alias %ALIAS%
echo.
sed -n -i -e '/-BEGIN CERTIFICATE-/,/-END CERTIFICATE-/p' %HOST%.pem

rem # update keystore and import certificate
keytool -delete -noprompt -alias %ALIAS% -file %HOST%.pem -keystore %KEYSTOREFILE% -storepass %KEYSTOREPASS%
keytool -import -noprompt -trustcacerts -alias %ALIAS% -file %HOST%.pem -keystore %KEYSTOREFILE% -storepass %KEYSTOREPASS%
echo Updated Keystore %KEYSTOREFILE%
echo.

keytool -delete -noprompt -alias %ALIAS% -file %HOST%.pem -keystore %TRUSTSTOREFILE% -storepass %TRUSTSTOREPASS%
keytool -import -noprompt -trustcacerts -alias %ALIAS% -file %HOST%.pem -keystore %TRUSTSTOREFILE% -storepass %TRUSTSTOREPASS%
echo Updated Truststore %TRUSTSTOREFILE%
echo.

rem # verify we've got it.
echo Verifying Alias %ALIAS% in Keystore %KEYSTOREFILE%
echo.
keytool -list -v -keystore %KEYSTOREFILE% -storepass %KEYSTOREPASS% -alias %ALIAS%

echo Verifying Alias %ALIAS% in Truststore %TRUSTSTOREFILE%
echo.
keytool -list -v -keystore %TRUSTSTOREFILE% -storepass %TRUSTSTOREPASS% -alias %ALIAS%

echo Certificate installed.
GOTO end

:help
echo "install-remote-ssl-cert.bat -host <host> [-port <port>] [-keystore <keystore file>] [-keystorepass <keystore password>] [-truststore <truststore file>] [-truststorepass <truststore password>]"
GOTO end
:error
echo "Missing arguments, -host is required!"
GOTO end
:end
rm -f openssl.check
rm -f sed.check
rm -f %HOST%.alias
rm -f %HOST%.pem
ENDLOCAL