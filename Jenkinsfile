#!groovy

stage 'Prepare Build'
node{
  def WORKSPACE = pwd()
  echo "Building Attune ${env.BRANCH_NAME} Branch..."
  echo "WORKSPACE folder is $WORKSPACE"
  properties([[$class: 'BuildDiscarderProperty', strategy: [$class: 'LogRotator', artifactDaysToKeepStr: '1', artifactNumToKeepStr: '3', daysToKeepStr: '1', numToKeepStr: '3']],disableConcurrentBuilds()])
  checkout scm
  sh "sudo rm ./docker.properties -rf"
  sh "sudo rm ./*-searchImageResults.txt -rf"
  sh "sudo rm ./*.cer -rf"
  sh "sudo rm ./*.pkcs8 -rf"
  sh "sudo find ./attune/src/main/docker -type f -exec chmod 777 {} \\;"
  sh "sudo find ./attune/src/main/docker/ssl -type f -exec chmod 777 {} \\;"

  sh '''#!/bin/bash
  sudo echo host_ip=$(/sbin/ifconfig eth0 | sed -En \'s/127.0.0.1//;s/.*inet (addr:)?(([0-9]*\\.){3}[0-9]*).*/\\2/p\') >> docker.properties'''

  sh '''#!/bin/bash
  sudo echo jenkins_ip=$(echo $JENKINS_URL | awk -F/ \'{print $3}\' | awk -F: \'{print $1}\') >> ./docker.properties'''
}

/*
 These scripts build or pull a Neo4J Docker Container image from the AWS Elastic Container Registry,
 Set some properties for the build, and then start the Neo4J Docker container
*/
/*
stage 'Prepare Neo4j'
node{
  try{
    def WORKSPACE = pwd()
    def buildprops = readProperties file: './attune/src/main/env/build.properties'
    attune_neo4j_docker_image=buildprops['attune_neo4j_docker_image']
    attune_docker_registry=buildprops['attune_docker_registry']
	println "Searching for $attune_neo4j_docker_image in registry $attune_docker_registry"
    sh "sudo ./attune/src/main/docker/execute-docker-cmd.sh --cmd search --registry $attune_docker_registry --image $attune_neo4j_docker_image"
    String searchImageResultsFile="./"+((String)attune_neo4j_docker_image).replaceAll(":", "-").replaceAll("/", "-")+"-searchImageResults.txt"
    String searchImageResults=readFile(searchImageResultsFile).trim()
    println "Image search results were: $searchImageResults"
    if(searchImageResults.indexOf("Not Found") > -1){
      println "Building image $attune_neo4j_docker_image for registry $attune_docker_registry"
      sh "sudo ./attune/src/main/docker/execute-docker-cmd.sh --cmd build --registry $attune_docker_registry --image $attune_neo4j_docker_image"
      //sh "sudo ./attune/src/main/docker/execute-docker-cmd.sh --cmd commit --msg 'Adding $attune_neo4j_docker_image to Container Registry' --auth 'Jenkins' --registry $attune_docker_registry --image $attune_neo4j_docker_image"
      println "Pushing image $attune_neo4j_docker_image to registry $attune_docker_registry"
      sh "sudo ./attune/src/main/docker/execute-docker-cmd.sh --cmd push --registry $attune_docker_registry --image $attune_neo4j_docker_image"
    } else {
      println "Pulling image $attune_neo4j_docker_image from registry $attune_docker_registry"
      sh "sudo ./attune/src/main/docker/execute-docker-cmd.sh --cmd pull --registry $attune_docker_registry --image $attune_neo4j_docker_image"
    }

    sh "sudo ./attune/src/main/docker/attune/neo4j/set-neo4j-properties.sh --registry $attune_docker_registry --image $attune_neo4j_docker_image"
    println "Starting Docker Container for $attune_neo4j_docker_image"
    sh "sudo ./attune/src/main/docker/execute-docker-cmd.sh --cmd start --registry $attune_docker_registry --image $attune_neo4j_docker_image"

    println "Processing Container ID and IP Address for Docker Container for $attune_neo4j_docker_image"
    def dockerprops = readProperties file: './docker.properties'
    neo4j_container_name=dockerprops['CONTAINER_NAME_NEO4J']
    sh "sudo ./attune/src/main/docker/write-container-ids.sh $neo4j_container_name"
    sh "sudo ./attune/src/main/docker/write-container-ip-addresses.sh $neo4j_container_name"
  } catch(error) {
    println "ERROR in stage 'Prepare Neo4j': ${error}"
    tearDownContainers(true)
    throw error
  }
}
*/
/*
 These scripts build or pull a Tomcat Docker Container image from the AWS Elastic Container Registry,
 Set some properties for the build, and then start the Tomcat Docker container

 !!!!  NOTE:  The Tomcat container must be processed and started LAST, since it depends on host and IP information from other containers such as Neo4j
*/
/*
stage 'Prepare Tomcat'
node{
  try{
    def WORKSPACE = pwd()
    def buildprops = readProperties file: './attune/src/main/env/build.properties'
    attune_tomcat_docker_image=buildprops['attune_tomcat_docker_image']
    attune_docker_registry=buildprops['attune_docker_registry']
    println "Searching for $attune_tomcat_docker_image in registry $attune_docker_registry"
    sh "sudo ./attune/src/main/docker/execute-docker-cmd.sh --cmd search --registry $attune_docker_registry --image $attune_tomcat_docker_image"
    String searchImageResultsFile="./"+((String)attune_tomcat_docker_image).replaceAll(":", "-").replaceAll("/", "-")+"-searchImageResults.txt"
    String searchImageResults=readFile(searchImageResultsFile).trim()
    println "Image search results were: $searchImageResults"
    if(searchImageResults.indexOf("Not Found") > -1){
      println "Building image $attune_tomcat_docker_image for registry $attune_docker_registry"
      sh "sudo ./attune/src/main/docker/execute-docker-cmd.sh --cmd build --registry $attune_docker_registry --image $attune_tomcat_docker_image"
      //sh "sudo ./attune/src/main/docker/execute-docker-cmd.sh --cmd commit --msg 'Adding $attune_tomcat_docker_image to Container Registry' --auth 'Jenkins' --registry $attune_docker_registry --image $attune_tomcat_docker_image"
      println "Pushing image $attune_tomcat_docker_image to registry $attune_docker_registry"
      sh "sudo ./attune/src/main/docker/execute-docker-cmd.sh --cmd push --registry $attune_docker_registry --image $attune_tomcat_docker_image"
    } else {
      println "Pulling image $attune_tomcat_docker_image from registry $attune_docker_registry"
      sh "sudo ./attune/src/main/docker/execute-docker-cmd.sh --cmd pull --registry $attune_docker_registry --image $attune_tomcat_docker_image"
    }

    sh "sudo ./attune/src/main/docker/attune/tomcat/set-tomcat-properties.sh --registry $attune_docker_registry --image $attune_tomcat_docker_image"
    println "Starting Docker Container for $attune_tomcat_docker_image"
    sh "sudo ./attune/src/main/docker/execute-docker-cmd.sh --cmd start --registry $attune_docker_registry --image $attune_tomcat_docker_image"

    println "Processing Container ID and IP Address for Docker Container for $attune_tomcat_docker_image"
    def dockerprops = readProperties file: './docker.properties'
    tomcat_container_name=dockerprops['CONTAINER_NAME_TOMCAT']
    sh "sudo ./attune/src/main/docker/write-container-ids.sh $tomcat_container_name"
    sh "sudo ./attune/src/main/docker/write-container-ip-addresses.sh $tomcat_container_name"
  } catch(error) {
    println "ERROR in stage 'Prepare Tomcat': ${error}"
    tearDownContainers(true)
    throw error
  }
}

stage 'Update Docker and Maven Properties'
node{
  try{
    def WORKSPACE = pwd()
    sh "#!/bin/bash \n" + "sudo chown jenkins:jenkins ./docker.properties"
    def dockerprops = readProperties file: './docker.properties'
    echo "Loaded $WORKSPACE/docker.properties"
    HOST_NAME_TOMCAT=dockerprops['HOST_NAME_TOMCAT']
    attune_port=dockerprops['tomcat_container_http_port']
    attune_base_url="http://${HOST_NAME_TOMCAT}:${attune_port}/attune"
    attune_credentials="Attune:Attune"
    HOST_NAME_NEO4J=dockerprops['HOST_NAME_NEO4J']
    neo4j_port=dockerprops['neo4j_container_http_port']
    neo4j_url="http://${HOST_NAME_NEO4J}:${neo4j_port}/"
    neo4j_credentials="neo4j:password"
    attune_data_service_url_prefix="${attune_base_url}/api/data"
    attune_metadata_service_url_prefix="${attune_base_url}/api/metadata"
    TRUSTSTORE_TOMCAT=dockerprops['TRUSTSTORE_TOMCAT']
    java_ssl_trustStore="${TRUSTSTORE_TOMCAT}"
    TRUSTSTORE_PASSWORD_TOMCAT=dockerprops['TRUSTSTORE_PASSWORD_TOMCAT']
    java_ssl_trustStorePassword="${TRUSTSTORE_PASSWORD_TOMCAT}"
    KEYSTORE_TOMCAT=dockerprops['KEYSTORE_TOMCAT']
    java_ssl_keyStore="${KEYSTORE_TOMCAT}"
    KEYSTORE_PASSWORD_TOMCAT=dockerprops['KEYSTORE_PASSWORD_TOMCAT']
    java_ssl_keyStorePassword="${KEYSTORE_PASSWORD_TOMCAT}"

    echo "Updating $WORKSPACE/docker.properties"
    sh "#!/bin/bash \n" + "sudo echo attune_port=${attune_port} >> ./docker.properties"
    sh "#!/bin/bash \n" + "sudo echo attune_base_url=${attune_base_url} >> ./docker.properties"
    sh "#!/bin/bash \n" + "sudo echo attune_credentials=${attune_credentials} >> ./docker.properties"
    sh "#!/bin/bash \n" + "sudo echo neo4j_url=${neo4j_url} >> ./docker.properties"
    sh "#!/bin/bash \n" + "sudo echo neo4j_credentials=${neo4j_credentials} >> ./docker.properties"

    sh "#!/bin/bash \n" + "sudo echo attune_data_service_url_prefix=${attune_data_service_url_prefix} >> ./docker.properties"
    sh "#!/bin/bash \n" + "sudo echo attune_metadata_service_url_prefix=${attune_metadata_service_url_prefix} >> ./docker.properties"

    sh "#!/bin/bash \n" + "sudo echo java_ssl_trustStore=${java_ssl_trustStore} >> ./docker.properties"
    sh "#!/bin/bash \n" + "sudo echo java_ssl_trustStorePassword=${java_ssl_trustStorePassword} >> ./docker.properties"
    sh "#!/bin/bash \n" + "sudo echo java_ssl_keyStore=${java_ssl_keyStore} >> ./docker.properties"
    sh "#!/bin/bash \n" + "sudo echo java_ssl_keyStorePassword=${java_ssl_keyStorePassword} >> ./docker.properties"
  } catch(error) {
    println "ERROR in stage 'Update Docker and Maven Properties': ${error}"
    tearDownContainers(false)
    throw error
  }
}

stage 'Setup SSL'
node{
  def WORKSPACE = pwd()
  try{
    sh "sudo ./attune/src/main/docker/ssl/configureSSLCerts.sh TOMCAT NEO4J"
  } catch(error) {
    println "ERROR in stage 'Setup SSL': ${error}"
    tearDownContainers(false)
    throw error
  }
}
*/
stage 'Build Attune'
node {
  try{
    //def WORKSPACE = pwd()
    //def dockerprops = readProperties file: './docker.properties'
    //attune_port=dockerprops['attune_port']
    //attune_base_url=dockerprops['attune_base_url']
    //neo4j_url=dockerprops['neo4j_url']
    //neo4j_credentials=dockerprops['neo4j_credentials']
    //attune_credentials=dockerprops['attune_credentials']
    //attune_data_service_url_prefix=dockerprops['attune_data_service_url_prefix']
    //attune_metadata_service_url_prefix=dockerprops['attune_metadata_service_url_prefix']
    //java_ssl_trustStore=dockerprops['java_ssl_trustStore']
    //java_ssl_trustStorePassword=dockerprops['java_ssl_trustStorePassword']
    //java_ssl_keyStore=dockerprops['java_ssl_keyStore']
    //java_ssl_keyStorePassword=dockerprops['java_ssl_keyStorePassword']

    def mvnHome = tool 'Maven 3.3.9'
    //sh "sudo ${mvnHome}/bin/mvn -P buildcac -Dattune.port=${attune_port} -Dattune.base.url=${attune_base_url} -Dneo4j.url=${neo4j_url} -Dneo4j.credentials=${neo4j_credentials} -Dattune.credentials=${attune_credentials} -Dattune.data.service.url.prefix=${attune_data_service_url_prefix} -Dattune.metadata.service.url.prefix=${attune_metadata_service_url_prefix} -Djava.ssl.trustStore=${java_ssl_trustStore} -Djava.ssl.trustStorePassword=${java_ssl_trustStorePassword} -Djava.ssl.keyStore=${java_ssl_keyStore} -Djava.ssl.keyStorePassword=${java_ssl_keyStorePassword} clean verify"
	sh "sudo ${mvnHome}/bin/mvn -P build clean verify"
    junit allowEmptyResults: true, testResults: '**/target/surefire-reports/TEST-*.xml'
  } catch(error) {
    println "ERROR in stage 'Build Attune': ${error}"
    def mvnHome = tool 'Maven 3.3.9'
    sh "sudo ${mvnHome}/bin/mvn surefire-report:report"
    tearDownContainers(false)
    throw error
  }
}
/*
stage 'Integration Test'
node{
  def WORKSPACE = pwd()
  try{
    if (fileExists('attune/target/attune.war')) {
      def dockerprops = readProperties file: './docker.properties'
      CONTAINER_ID_TOMCAT=dockerprops['CONTAINER_ID_TOMCAT']
      skip_integration_tests=false
      attune_port=dockerprops['tomcat_http_port']
      attune_base_url="http://localhost:${attune_port}/attune"
      neo4j_url=dockerprops['neo4j_url']
      neo4j_credentials=dockerprops['neo4j_credentials']
      attune_credentials=dockerprops['attune_credentials']
      attune_data_service_url_prefix="${attune_base_url}/api/data"
      attune_metadata_service_url_prefix="${attune_base_url}/api/metadata"
      java_ssl_trustStore=dockerprops['java_ssl_trustStore']
      java_ssl_trustStorePassword=dockerprops['java_ssl_trustStorePassword']
      java_ssl_keyStore=dockerprops['java_ssl_keyStore']
      java_ssl_keyStorePassword=dockerprops['java_ssl_keyStorePassword']

      sh "sudo docker cp ./attune/target/attune.war $CONTAINER_ID_TOMCAT:/usr/local/tomcat/webapps/attune.war"

      println "waiting up to 180 SECONDS for $attune_base_url ..."
      timeout(time: 180, unit: 'SECONDS') {
        waitUntil {
          def r = sh script: "wget -q \'$attune_base_url\' -O /dev/null", returnStatus: true
          return (r == 0);
        }
      }

      def mvnHome = tool 'Maven 3.3.9'
      sh "sudo ${mvnHome}/bin/mvn -P buildcac -Dskip.integration.tests=${skip_integration_tests} -Dattune.port=${attune_port} -Dattune.base.url=${attune_base_url} -Dneo4j.url=${neo4j_url} -Dneo4j.credentials=${neo4j_credentials} -Dattune.credentials=${attune_credentials} -Dattune.data.service.url.prefix=${attune_data_service_url_prefix} -Dattune.metadata.service.url.prefix=${attune_metadata_service_url_prefix} -Djava.ssl.trustStore=${java_ssl_trustStore} -Djava.ssl.trustStorePassword=${java_ssl_trustStorePassword} -Djava.ssl.keyStore=${java_ssl_keyStore} -Djava.ssl.keyStorePassword=${java_ssl_keyStorePassword} install"
    }
  } catch(error) {
    println "ERROR in stage 'Integration Test': ${error}"
    def mvnHome = tool 'Maven 3.3.9'
    sh "sudo ${mvnHome}/bin/mvn surefire-report:report"
    tearDownContainers(false)
    throw error
  }
}

stage 'Shutdown Containers'
node{
  tearDownContainers(false)
}
*/
stage 'Update Staging'
node{
  /*
   Tests passed, so copy war to staging machine
  */
  def WORKSPACE = pwd()
  def buildprops = readProperties file: './attune/src/main/env/build.properties'
  update_staging=buildprops['update_staging']
  staging_server_ip=buildprops['staging_server_ip']
  staging_server_user=buildprops['staging_server_user']
  staging_server_key=buildprops['staging_server_key']
  staging_server_app_server_deploy_path=buildprops['staging_server_app_server_deploy_path']
  staging_server_backup_path=buildprops['staging_server_backup_path']

  try{
    if ('true' == update_staging && fileExists('attune/target/attune.war')) {
      echo "Updating Staging environment, Copying ${WORKSPACE}/attune/target/attune.war to ${staging_server_ip}:${staging_server_app_server_deploy_path}/attune.war"
      /*
       First rebuild war with default settings
      */
	  def mvnHome = tool 'Maven 3.3.9'
      sh "sudo ${mvnHome}/bin/mvn -P build clean verify"

	  /*
       First remove backups older than 60 days and then backup the current war

      	sh "sudo ssh -i ${WORKSPACE}/${staging_server_key} -o StrictHostKeyChecking=no -n ${staging_server_user}@${staging_server_ip} find . -type f -regex \"\\./attune-[0-9]+.war\" -mtime +60 -exec rm {} +"
	    sh "sudo ssh -i ${WORKSPACE}/${staging_server_key} -o StrictHostKeyChecking=no -n ${staging_server_user}@${staging_server_ip} cp ${staging_server_app_server_deploy_path}/attune.war ${staging_server_backup_path}/attune-${env.BUILD_NUMBER}.war"
      */

	  sh "sudo ssh -i ${staging_server_key} -o StrictHostKeyChecking=no -n ${staging_server_user}@${staging_server_ip} find . -type f -regex \"\\./attune-[0-9]+.war\" -mtime +60 -exec rm {} +"
	  //sh "sudo ssh -i ${staging_server_key} -o StrictHostKeyChecking=no -n ${staging_server_user}@${staging_server_ip} cp ${staging_server_app_server_deploy_path}/attune.war ${staging_server_backup_path}/attune-${env.BUILD_NUMBER}.war"

	  /*
       Now copy the war
       	  sh "sudo scp -i ${WORKSPACE}/${staging_server_key} -o StrictHostKeyChecking=no ${WORKSPACE}/attune/target/attune.war ${staging_server_user}@${staging_server_ip}:${staging_server_app_server_deploy_path}/attune.war"

      */
	  sh "sudo scp -i ${staging_server_key} -o StrictHostKeyChecking=no ${WORKSPACE}/attune/target/attune.war ${staging_server_user}@${staging_server_ip}:${staging_server_app_server_deploy_path}/attune.war"
	  echo "Update of Staging environment complete."
    } else {
	  echo "Update of Staging environment was disabled."
	}
  } catch(error) {
    sh "#!/bin/bash \n" + "sudo chown jenkins:jenkins ${WORKSPACE}/attune/target/attune.war"
    println "ERROR Copying ${WORKSPACE}/attune/target/attune.war to ${staging_server_ip}:${staging_server_app_server_deploy_path}/attune.war: ${error}"
    throw error
  }
}

stage 'Sonarqube Scan'
node{
  try{
    def WORKSPACE = pwd()
    def buildprops = readProperties file: './attune/src/main/env/build.properties'
    sonar_host_url=buildprops['sonar.host.url']
    def mvnHome = tool 'Maven 3.3.9'
    sh "sudo ${mvnHome}/bin/mvn -P build -Dsonar.host.url=${sonar_host_url} sonar:sonar"
  } catch(error) {
    println "ERROR in stage ' Sonarqube Scan ': ${error}"
    throw error
  }
}

def tearDownContainers(boolean removeImage){
  def dockerprops = readProperties file: './docker.properties'
  String[] suffixes = ["NEO4J","TOMCAT"]
  for(int x = 0; x < suffixes.size(); x++){
    String suffix = suffixes[x]
    String container = dockerprops['CONTAINER_ID_'+suffix]
    sh "sudo ./attune/src/main/docker/execute-docker-cmd.sh --cmd stop --container $container"
    sh "sudo ./attune/src/main/docker/execute-docker-cmd.sh --cmd remove --container $container"
    if(removeImage){
      String image = dockerprops['IMAGE_ID_'+suffix]
      String tag=sh(returnStdout: true, script: './get-docker-image-tags.sh $image')
      sh "sudo ./attune/src/main/docker/execute-docker-cmd.sh --cmd remove-image --image $tag"
    }
  }
}
