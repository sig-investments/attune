# ATTUNE


## Enterprise System-of-Systems Design and Portfolio Management

The Attune enterprise tool automates our Enterprise Integration (EI) approach to visualize our clients’ entire enterprise portfolio that is fully traceable from mission capability requirements to systems and services. Attune is a highly visual web application built on open source technologies.  

To cost-effectively maintain and modernize their capabilities, organizations must identify where to make intelligent cuts and consolidation, where to develop new capabilities to fill gaps, and how to integrate what is left in ways that improve mission performance.

##### TRACING MISSION CAPABILITIES REQUIREMENTS

Attune enables you to perform enterprise design activities and determine the mission, common, and core services needed to make intelligent trade-off analyses and to do open architecture design.
- __Mission-Driven__ - Missions cross organizations so it is critical to provide full traceability and visibility of systems, activities, and requirements to mission needs. This is the foundation for optimizing capabilities and mission readiness; changes must be quantifiable via clear mission impact
- __Enterprise Perspective__ — To deliver integrated system-of-systems (SoS)
that meet mission requirements, leaders need to understand capability/system gaps and redundancies across organizational boundaries to make more informed decisions on intelligent technology injection and/or convergence. From the enterprise perspective, the government should take an “ABCD” approach when it comes to technology—adopt first, buy second, create third, and decommission fourth
- __Open Source Architecture__ —To drive out inefficiencies, the US government needs to own the architecture, the system, and the data to reduce vendor lock-in. In addition, the government needs to create modular architecture with open/interoperable, well-defined interfaces to plug in new technologies when they become available, fostering innovation
and reuse across the enterprise

Attune is a highly visual, open source web application that provides a true enterprise environment for application/ service portfolio management and enterprise
design. Attune helps you to:

- __Define and Visualize Your Portfolio__ - Understand the massive amount of
data in your enterprise portfolio through interactive and intuitive data visualizations.
Then ask:
  - What are the mission, common, and core services across my enterprise so I can make consolidation decisions?
  - Where do I have gaps that need to be addressed?

- __Trace Portfolio Relationships__ - Understand connections of program support and the relevancy to missions and capabilities, enabling impact assessment of decisions
- __Define System and Service Specifications__ - View and manage system and service
specifications across the whole enterprise. Specifications provide the blueprint for implementation
- __Quantify Your Portfolio__ - Quickly view and find what assets you have and their detailed information, identify potential gaps and duplication, and see the cost and acquisition information tied to the systems. Then ask:
  - Should I build or buy?
  - Does it make sense for me to reuse certain systems?

- __Perform Trade-Off Analysis__ - Perform what-if analysis to determine trade-offs of different courses of actions to ensure mission readiness is still maintained by different decision scenarios. For example:
  - Will I still be able to meet mission demands after making certain cuts?
  - How can I best optimize my capabilities given the current funding?

  




### Tech

Attune uses a number of open source projects to work properly:

* [Bootstrap](http://getbootstrap.com) - HTML, CSS, and JS framework for developing responsive, mobile first projects on the web
* [Cytoscape.js](http://js.cytoscape.org/) - JS library for drawing graphs 
* [D3.js](http://d3js.org/) - JavaScript library for manipulating documents based on data.
* [Inflection.js](https://code.google.com/p/inflection-js/) - Library for correctly pluralizing strings 
* [jQuery](https://jquery.com/) -HTML document traversal and manipulation, event handling, animation
* [jQuery.editable-select.js](https://github.com/indrimuska/jquery-editable-select) - Library to transform normal select element into an editable one with suggestions
* [Sortable.js](https://github.com/RubaXa/Sortable) - JS library that allows the user to rearrange list items via drag n’ drop 
* [Vis.js/vis.css](http://visjs.org/) - Timeline library 
* [Simple-sidebar.css](https://github.com/IronSummitMedia/startbootstrap-simple-sidebar) -A bootstrap friendly sidebar 
* [accounting.js](http://openexchangerates.github.io/accounting.js/) -Library to format strings/numbers to be represented as accounting strings with a currency and decimals
* [Guava](https://github.com/google/guava) - Java library used to do some arithmetic on Sets (and other data structures)
* [evo-inflector](https://github.com/atteo/evo-inflector) - Java library used to pluralize strings
* [Bootstrap Hover Dropdown](https://github.com/CWSpear/bootstrap-hover-dropdown) - Bootstrap plugin that allows dropdowns to be enabled by hover rather than click.


### Getting Started

This application requires `docker` and `docker-compose` to run locally.

To run the application, execute the following on the command line:

```bash
$ docker-compose up --build
```

Then, open up a browser to `http://localhost:7474/`. This opens up the Neo4J console. Log in with `neo4j/neo4j`, and change the password to `password` (you should be prompted by Neo4J to change it on the first log in).

Then, restart attune in a separate command line:

```bash
$ docker-compose restart attune
```

After it successfully restarts, **Attune should now be accessible at** `http://localhost:8080/attune`.

The application is deployed withing a Tomcat instance, which has a management/admin console, accessible at `http://localhost:8080`. The management credentials for local development are `admin/admin`.

### Copyright
Copyright © 2017 Booz Allen Hamilton. All Rights Reserved. The contents of this file is the intellectual property of Booz Allen Hamilton Inc. (“BAH”) and is subject to copyright protection under the laws of the United States and other countries. You acknowledge that misappropriation, misuse, or redistribution of content on the file could cause irreparable harm to BAH and/or to third parties. You may not, copy, reproduce, distribute, publish, display, execute, modify, create derivative works of, transmit, sell or offer for resale, or in any way exploit any part of this file without BAH’s express written permission. You also may not “mirror” any material contained in this file on any other server without prior written permission from BAH..
